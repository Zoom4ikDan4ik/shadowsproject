package ru.krogenit.guns;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.item.ItemStack;
import org.lwjgl.input.Mouse;
import ru.krogenit.guns.item.ItemGun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClientTickHandlerGuns {
    private final Minecraft mc = Minecraft.getMinecraft();

    private int ticksToChangeMotion = 0;
    private int motionPause = 0;
    private float motionYaw = 0.0F;
    private float motionPitch = 0.0F;

    private static final List<PositionedSoundRecord> soundToPlay = Collections.synchronizedList(new ArrayList<>());

    public static void addShootSound(PositionedSoundRecord sound) {
        if(soundToPlay.size() == 0) soundToPlay.add(sound);
    }

    @SubscribeEvent
    public void event(TickEvent.RenderTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {
            float renderTickTime = event.renderTickTime;
            if (mc.thePlayer != null) {

                if (mc.currentScreen == null) {
                    ItemStack item = mc.thePlayer.getCurrentEquippedItem();
                    if (item != null && item.getItem() instanceof ItemGun) {
                        if (Mouse.isButtonDown(1) && !mc.thePlayer.moving.isCrawling) {
                            float speed = renderTickTime / 20f;
                            float power = 0.1f;
                            ticksToChangeMotion -= speed;
                            if (ticksToChangeMotion  <= 0) {
                                ticksToChangeMotion = 5 + (int) (Math.random() * 15.0D);
                                motionPitch = ((float) Math.random() - power) * power;
                                motionYaw = ((float) Math.random() - power) * power;
                                motionPause = (int) (Math.random() * 5.0D);
                            }

                            motionPause -= speed;
                            if (motionPause <= 0) {
                                mc.thePlayer.setAngles(motionYaw, motionPitch);
                            }
                        }
                    }
                }

                if(soundToPlay.size() > 0)
                    mc.getSoundHandler().sndManager.playSound(soundToPlay.remove(0));
            }
        }
    }
}
