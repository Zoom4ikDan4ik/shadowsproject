package ru.krogenit.guns.item;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector4f;
import ru.krogenit.guns.ClientTickHandlerGuns;
import ru.krogenit.guns.CoreGunsClient;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.entity.EntityBulletClient;
import ru.krogenit.guns.keys.ThreadShootKeyGuns;
import ru.krogenit.guns.network.packet.PacketReloadClient;
import ru.krogenit.guns.network.packet.PacketShootClient;
import ru.krogenit.guns.render.AbstractItemGunRenderer;
import ru.krogenit.guns.render.EnumReloadType;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.util.SoundType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.List;

public class ItemGunClient extends ItemGun {

    public ItemGunClient(String name, int rate, ItemAmmo ammo, float knockback, float[] accuracy, float damage, int hardness, Vector4f multiply, EnumGunType gunType, int bulletPerShoot, float reloadSpeed, float unloadSpeed) {
        super(name, rate, ammo, knockback, accuracy, damage, hardness, multiply, gunType, bulletPerShoot, reloadSpeed, unloadSpeed);
        setCreativeTab(CoreGunsClient.tabGuns);
    }

    @Override
    public void onItemLeftClick(ItemStack item, World w, EntityPlayer p, float playerAcc) {
        int ammo = getInteger(item, "ammo");
        AbstractItemGunRenderer render = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(item, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
        if (canShoot(item, w, ammo)) {
            if (render.onShoot(item, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON)) {
                if (!CoreGunsCommon.IS_UNLIMITED_AMMO) setInteger(item, "ammo", getInteger(item, "ammo") - 1);

                double value = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.WEAPON_RECOIL_MOD).getValue();
                playerAcc *= value;

                boolean hasGlushak = false;//inventory.hasGlushak();TODO:глушитель
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketShootClient(playerAcc));
                playShootSound(item, hasGlushak);
                render.onFlashLight(item, p, hasGlushak);
                ThreadShootKeyGuns.lastShootTime = System.currentTimeMillis();
                float damage = getWeaponDamage(item);
                float totalAcc = getAccuracy(item, playerAcc);

                for(int i=0;i<bulletsPerShot;i++) {
                    EntityBulletClient bullet = new EntityBulletClient(w, p, damage, totalAcc, damageMultiply);
                    w.spawnEntityInWorld(bullet);
                }

                p.rotationPitch -= getKonckback(item);
                p.rotationYaw += (w.rand.nextFloat() - 0.5f) * getKonckback(item) * 5f;
            }
        } else {
            render.onEmpty(item);
            ThreadShootKeyGuns.lastShootTime = System.currentTimeMillis() + 750;
        }
    }

    @Override
    public void onReload(ItemStack item, World w, EntityPlayer p, boolean removeAmmo) {
        boolean hasAmmo = getBoolean(item, "hasammo");
        if (hasAmmo) {
            if(CoreGunsCommon.IS_FULL_RELOAD) {
                if(getAmmo(item) < ammo.maxAmmo) {
                    ItemStack ammo = findAmmoNew(w, item, p, 1, this.ammo);
//                if (ammo == null) ammo = findAmmoNew(w, item, p, 0, this.ammo);
                    if (ammo != null) {
                        AbstractItemGunRenderer render = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(item, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
                        if (render.onReload(item, EnumReloadType.RELOAD, getInteger(item, "ammo"), this.ammo.getAmmo(ammo))) {
                            putAmmo(w, item, p);
                            loadAmmo(item, ammo);
                        }
                    }
                }
            } else {
                if(!removeAmmo) {
                    AbstractItemGunRenderer render = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(item, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
                    render.onReload(item, EnumReloadType.UNLOAD, getInteger(item, "ammo"), 0);
                } else {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketReloadClient());
                    putAmmo(w, item, p);
                }
            }
        } else {
            ItemStack ammo = findAmmoNew(w, item, p, 1, this.ammo);
//            if (ammo == null) ammo = findAmmoNew(w, item, p, 0, this.ammo);
            if(ammo != null) {
                AbstractItemGunRenderer render = (AbstractItemGunRenderer) MinecraftForgeClient.getItemRenderer(item, IItemRenderer.ItemRenderType.EQUIPPED_FIRST_PERSON);
                int newammo = this.ammo.getAmmo(ammo);
                if (render.onReload(item, EnumReloadType.LOAD, -1, newammo)) {
                    setInteger(item, "ammo", newammo);
                    setBoolean(item, "hasammo", true);
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketReloadClient());
                }
            }
        }
    }

    private void loadAmmo(ItemStack itemStack, ItemStack ammoStack) {
        setInteger(itemStack, "ammo", ammo.getAmmo(ammoStack));
        setBoolean(itemStack, "hasammo", true);
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketReloadClient());
    }


    protected ItemStack findAmmoNew(World w, ItemStack item, EntityPlayer p, int minAmmo, ItemAmmo ammo) {
        ItemStack foundedAmmo = null;
        int maxAmmoFound = 0;
        for (int i = 0; i < p.inventory.mainInventory.length; i++) {
            if (p.inventory.mainInventory[i] != null && p.inventory.mainInventory[i].getItem() == ammo) {
                ItemStack ammoStack = p.inventory.mainInventory[i];
                int newammo = ammo.getAmmo(ammoStack);
                if (newammo >= minAmmo && newammo >= maxAmmoFound) {
                    maxAmmoFound = newammo;
                    foundedAmmo = ammoStack;
                }
            }
        }

        ClientMainPlayer clientPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        MatrixInventory matrixInventory = clientPlayer.getMatrixInventory();
        for(ItemStack itemStack : matrixInventory.getItems().values()) {
            if(itemStack != null) {
                if(itemStack.getItem() == ammo) {
                    int newammo = ammo.getAmmo(itemStack);
                    if (newammo >= minAmmo && newammo >= maxAmmoFound) {
                        maxAmmoFound = newammo;
                        foundedAmmo = itemStack;
                    }
                }
            }
        }

        return foundedAmmo;
    }

    @Override
    protected boolean putAmmo(World w, ItemStack item, EntityPlayer p) {
        setInteger(item, "ammo", 0);
        setBoolean(item, "hasammo", false);
        return true;
    }

    protected void playShootSound(ItemStack item, boolean hasGlushak) {
        PositionedSoundRecord sound;
        if (hasGlushak) {
            sound = new PositionedSoundRecord(new ResourceLocation(CoreGunsCommon.MODID + ":guns." + getName() + "." + getName() + "_shoot_silence"), 1.0f, SoundHelperGuns.getRandomPitch());
        } else {
            sound = new PositionedSoundRecord(new ResourceLocation(SoundType.DEFAULT_DOMAIN + getShootSound()), 1.0f, SoundHelperGuns.getRandomPitch());
        }

        ClientTickHandlerGuns.addShootSound(sound);
    }

    public void addInformation(ItemStack i, EntityPlayer p, List list, boolean flag) {
        if (!Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            list.add("<Нажмите " + EnumChatFormatting.RED + "Shift" + EnumChatFormatting.GRAY + ">");
        } else {
            boolean hasAmmo = getBoolean(i, "hasammo");
            if (hasAmmo) {
                int curAmmo = this.getInteger(i, "ammo");
                list.add("Патронов в магазине: " + curAmmo);
            } else {
                list.add("Вставьте магазин");
            }
        }
    }
}
