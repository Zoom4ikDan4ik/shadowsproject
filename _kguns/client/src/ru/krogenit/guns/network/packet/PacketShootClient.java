package ru.krogenit.guns.network.packet;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketShootClient implements IPacketOut, IPacketIn {

    private float acc;

    @Override
    public void read(ByteBufInputStream data) throws IOException {
        acc = data.readFloat();
    }

    @Override
    public void write(ByteBufOutputStream data) throws IOException {
        data.writeFloat(acc);
    }
}
