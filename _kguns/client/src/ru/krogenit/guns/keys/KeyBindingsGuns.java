package ru.krogenit.guns.keys;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;
import ru.krogenit.client.key.AbstractKey;

import java.util.ArrayList;

public class KeyBindingsGuns {
    private final ArrayList<AbstractKey> keys = new ArrayList<>();

    public KeyBindingsGuns init() {
        registerKey(new KeyReload(new KeyBinding("guns.key.reload", Keyboard.KEY_R, "key.categories.guns")));
        return this;
    }

    private void registerKey(AbstractKey key) {
        ClientRegistry.registerKeyBinding(key.getKeyBinding());
        keys.add(key);
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(InputEvent.KeyInputEvent e) {
        for (AbstractKey key : keys) {
            KeyBinding bind = key.getKeyBinding();
            if (bind.isPressed()) {
                key.keyDown();
            } else if (!Keyboard.isKeyDown(key.getKeyBinding().getKeyCode())) {
                key.keyUp();
            }
        }
    }
}
