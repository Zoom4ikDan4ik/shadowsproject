package ru.krogenit.guns.render.melee;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.item.ItemMeleeWeaponClient;
import ru.krogenit.guns.render.AbstractMeleeWeaponRenderer;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.glScalef;

public class AzalletRenderer extends AbstractMeleeWeaponRenderer {

    private final TextureDDS diffuse = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Azallet_D.dds"));
    private final TextureDDS normal = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Azallet_N.dds"));
    private final TextureDDS specular = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Azallet_S.dds"));
    private final TextureDDS gloss = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Azallet_G.dds"));
    private final TextureDDS emission = new TextureDDS(new ResourceLocation(CoreGunsCommon.MODID, "textures/Azallet_E.dds"));

    public AzalletRenderer(ItemMeleeWeaponClient itemMelee) {
        super(new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/weapons/Azallet.obj")), itemMelee);
    }

    @Override
    public void renderInModifyGui(ItemStack itemStack) {
        float scale = 0.11f;
        glScalef(scale, scale, scale);
        renderWeapon();
    }

    @Override
    protected void renderWeapon() {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        TextureLoaderDDS.bindTexture(diffuse);
        TextureLoaderDDS.bindNormalMap(normal, shader);
        TextureLoaderDDS.bindSpecularMap(specular, shader);
        TextureLoaderDDS.bindGlossMap(gloss, shader);
        TextureLoaderDDS.bindEmissionMap(emission, shader, 5f);
        weaponModel.render(shader);
        shader.setNormalMapping(false);
        shader.setSpecularMapping(false);
        shader.setGlossMapping(false);
        shader.setEmissionMapping(false);
        TextureLoaderDDS.unbind();
        KrogenitShaders.finishCurrentShader();
    }
}
