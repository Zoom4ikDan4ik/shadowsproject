package ru.krogenit.guns.render.entity;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.guns.CoreGunsCommon;
import ru.krogenit.guns.entity.EntitySleeve;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

import static org.lwjgl.opengl.GL11.*;

public class RenderEntitySleeve extends Render {

    private static final Model gilza = new Model(new ResourceLocation(CoreGunsCommon.MODID, "models/gilza.obj"));
    private static final Minecraft mc = Minecraft.getMinecraft();

    @Override
    public void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {
        EntitySleeve sleeve = (EntitySleeve) entity;
//        float speed = AnimationHelper.getAnimationSpeed();

        float addY = 0f;
        int ticksOn = sleeve.ticksExisted;
        if (ticksOn > 200) {
            addY = (ticksOn - 200) / 1000f;
        }

        glPushMatrix();
        glTranslatef((float) x, (float) y - addY, (float) z);

        glRotatef(sleeve.xRotation, 1, 0, 0);
        glRotatef(sleeve.yRotation, 0, 1, 0);
        glRotatef(sleeve.zRotation, 0, 0, 1);
        float scale = 0.1f;
        glScalef(scale, scale, scale);
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        shader.setUseTexture(false);
        glRotatef(90f, 0,0,1);
        if (sleeve.getSleeveType() == 0) gilza.render(shader);
        glPopMatrix();
        shader.setUseTexture(true);
        KrogenitShaders.finishCurrentShader();
    }

    @Override
    public void doRenderPost(Entity entity, double x, double y, double z, float rotationYaw, float tickTime) {

    }

    @Override
    public ResourceLocation getEntityTexture(Entity entity) {
        return null;
    }
}
