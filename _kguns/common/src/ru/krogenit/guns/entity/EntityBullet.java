package ru.krogenit.guns.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector4f;

public class EntityBullet extends Entity {

    protected boolean inGround;
    public int throwableShake;

    public EntityPlayer thrower;
    public int ticksInAir;

    public boolean isRenderingBullet;
    public float color, damage;
    public Vector4f damageMultiply;

    public static boolean bulletPas = false;

    public EntityBullet(World par1World) {
        super(par1World);
        this.setSize(0.25F, 0.25F);
    }

    protected void entityInit() {
    }


    public EntityBullet(World par1World, EntityPlayer p, float damage, float accuracy, Vector4f multiply) {
        super(par1World);
        this.isRenderingBullet = par1World.rand.nextInt(5) == 0 ? true : true;
        this.damage = damage;
        this.thrower = p;
        this.setSize(0.25F, 0.25F);
        this.setLocationAndAngles(p.posX, p.posY + (double) p.getEyeHeight(), p.posZ, p.rotationYaw, p.rotationPitch);
        this.posX -= MathHelper.cos(this.rotationYaw / 180.0F * (float) Math.PI) * 0.0F;
        this.posY -= 0.10000000149011612D;
        this.posZ -= MathHelper.sin(this.rotationYaw / 180.0F * (float) Math.PI) * 0.0F;
        this.setPosition(this.posX, this.posY, this.posZ);
        this.yOffset = 0.0F;
        float f = 0.4F;
        this.motionX = -MathHelper.sin(this.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float) Math.PI) * f;
        this.motionZ = MathHelper.cos(this.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float) Math.PI) * f;
        this.motionY = -MathHelper.sin((this.rotationPitch + this.func_70183_g()) / 180.0F * (float) Math.PI) * f;
        this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, this.getBulletSpeed(), 1.5F, accuracy);
        this.damageMultiply = multiply;
    }

    public EntityBullet(World par1World, double par2, double par4, double par6) {
        super(par1World);
        this.setSize(0.25F, 0.25F);
        this.setPosition(par2, par4, par6);
        this.yOffset = 0.0F;
    }

    public float getBulletSpeed() {
        return 30f;
    }

    protected float func_70183_g() {
        return 0.0F;
    }

    public void setThrowableHeading(double par1, double par3, double par5, float par7, float par8, float var9) {
        float f2 = MathHelper.sqrt_double(par1 * par1 + par3 * par3 + par5 * par5);
        par1 /= f2;
        par3 /= f2;
        par5 /= f2;
        par1 += this.rand.nextGaussian() * 0.007499999832361937D * (double) par8 * (double) var9 / 5.0D;
        par3 += this.rand.nextGaussian() * 0.007499999832361937D * (double) par8 * (double) var9 / 5.0D;
        par5 += this.rand.nextGaussian() * 0.007499999832361937D * (double) par8 * (double) var9 / 5.0D;
        par1 *= par7;
        par3 *= par7;
        par5 *= par7;
        this.motionX = par1;
        this.motionY = par3;
        this.motionZ = par5;
        float f3 = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
        this.prevRotationYaw = this.rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
        this.prevRotationPitch = this.rotationPitch = (float) (Math.atan2(par3, f3) * 180.0D / Math.PI);
    }

    private boolean isEntityWithPerPartCollision(Entity e) {
        return e instanceof EntityPlayer;
    }

    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {

    }

    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {
        setDead();
    }
}
