package ru.krogenit.guns.item;

import lombok.Getter;
import net.minecraft.item.Item;
import ru.xlv.core.common.item.IItemUsable;

public abstract class ItemMedicine extends Item implements IItemUsable {

    @Getter private final String name;
    protected static final String TIMER = "useTimer";
    protected static final String SLOT_INDEX = "slotIndex";

    public ItemMedicine(String name) {
        setUnlocalizedName(name);
        setMaxStackSize(1);
        this.name = name;
    }
}
