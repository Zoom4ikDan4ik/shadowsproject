package ru.krogenit.guns.item;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector4f;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.IAttributeProvider;
import ru.xlv.core.common.item.ISlotSpecified;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter @Setter
public abstract class ItemGun extends ItemWithoutAnim implements IAttributeProvider, ISlotSpecified {

    private static final String SOUND_FORMAT = "weapon.%s.%s_%s";

    protected Map<CharacterAttributeType, CharacterAttributeMod> characterAttributeBoostMap = new HashMap<>();
    protected int rate;
    protected float damage;
    protected float accuracy, accWalk, accSprint, accJump, accShift, accPkm, accLying;
    protected float knockback;
    protected ItemAmmo ammo;
    protected Vector4f damageMultiply;
    protected float reloadSpeed, unloadSpeed;
    protected float[] baseAccuracy;
    protected final List<MatrixInventory.SlotType> slotTypes = new ArrayList<>();
    protected EnumGunType gunType;
    protected int bulletsPerShot;

    public ItemGun(String name) {
        super(1, name);
        this.baseAccuracy = new float[] {0,0,0,0,0,0,0};
        this.damageMultiply = new Vector4f(1.0f,1.0f, 1.0f, 1.0f);
    }

    public ItemGun(String name, int rate, ItemAmmo ammo, float knockback, float[] accuracy, float damage, int hardness, Vector4f multiply, EnumGunType gunType, int bulletsPerShot, float reloadSpeed, float unloadSpeed) {
        this(name);
        setMaxDamage(hardness);
        this.rate = rate;
        this.ammo = ammo;
        this.knockback = knockback;
        this.baseAccuracy = accuracy;
        setAccuracy(accuracy);
        this.damage = damage;
        this.damageMultiply = multiply;
        this.reloadSpeed = reloadSpeed;
        this.unloadSpeed = unloadSpeed;
        this.gunType = gunType;
        this.bulletsPerShot = bulletsPerShot;
    }

    public void setAccuracy(float[] accuracy) {
        this.accuracy = accuracy[0];
        this.accWalk = accuracy[1];
        this.accSprint = accuracy[2];
        this.accJump = accuracy[3];
        this.accShift = accuracy[4];
        this.accPkm = accuracy[5];
        this.accLying = accuracy[6];
        this.baseAccuracy = accuracy;
    }

    public void onFinishReload(ItemStack item, World w, EntityPlayer p) { }

    protected float getAccuracy(ItemStack item, float acc) {
        float totalAcc = accuracy + acc;
        if (totalAcc < 0) totalAcc = 0;
        return totalAcc;
    }

    protected float getWeaponDamage(ItemStack item) {
        return damage / (float) bulletsPerShot;
    }

    protected boolean canShoot(ItemStack item, World w, int ammo) {
        return ammo > 0;
    }

    protected float getKonckback(ItemStack item) {
        return knockback;
    }

    protected void onShoot(ItemStack item, World w) {}

    public abstract void onReload(ItemStack item, World w, EntityPlayer p, boolean removeAmmo);

    protected ItemStack pullOutAmmo(ItemStack item) {
        return item;
    }

    protected boolean putAmmo(World w, ItemStack item, EntityPlayer p) {
        return false;
    }

    public abstract void onItemLeftClick(ItemStack item, World w, EntityPlayer p, float playerAcc);

    public int getAmmo(ItemStack itemStack) {
        return this.getInteger(itemStack, "ammo");
    }

    public ItemGun addSlotType(MatrixInventory.SlotType slotType) {
        this.slotTypes.add(slotType);
        return this;
    }

    @Override
    public List<MatrixInventory.SlotType> getSlotTypes() {
        return slotTypes;
    }

    public String getShootSound() {
        return String.format(SOUND_FORMAT, getName(), getName(), "shoot");
    }

    public String getLoadSound() {
        return String.format(SOUND_FORMAT, getName(), getName(), "load");
    }

    public String getUnloadSound() {
        return String.format(SOUND_FORMAT, getName(), getName(), "unload");
    }

    public String getNoAmmoSound() {
        return String.format(SOUND_FORMAT, getName(), getName(), "no_ammo");
    }

}
