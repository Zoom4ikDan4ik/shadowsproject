package ru.xlv.cases.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.cases.XlvsCasesMod;
import ru.xlv.cases.handle.result.CaseRollResult;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.util.Localization;

import java.io.IOException;

@NoArgsConstructor
public class PacketCaseRoll implements IPacketCallbackOnServer {

    private CaseRollResult caseRollResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int caseIndex = bbis.readInt();
        caseRollResult = XlvsCasesMod.INSTANCE.getCaseHandler().roll(entityPlayer, caseIndex);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(caseRollResult.getType() == CaseRollResult.Type.SUCCESS);
        bbos.writeUTF(Localization.getFormattedString(caseRollResult.getType().getResponseMessage(), caseRollResult.getParams()));
        if (caseRollResult.getType() == CaseRollResult.Type.SUCCESS) {
            ByteBufUtils.writeItemStack(bbos.buffer(), ((ItemStack) caseRollResult.getParams()[0]));
        }
    }
}
