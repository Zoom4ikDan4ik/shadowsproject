package ru.xlv.cases.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class CaseLocalization extends Localization {

    private final String responseCaseRollResultSuccessMessage = "responseCaseRollResultSuccessMessage";
    private final String responseCaseRollResultNotEnoughMoneyMessage = "responseCaseRollResultNotEnoughMoneyMessage";

    private transient final File configFile = new File("config/cases/localization.json");
}
