package ru.xlv.cases.handle.result;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.cases.XlvsCasesMod;

@Getter
@Builder
public class CaseRollResult {

    @Getter
    @RequiredArgsConstructor
    public enum Type {
        SUCCESS(XlvsCasesMod.INSTANCE.getLocalization().getResponseCaseRollResultSuccessMessage()),
        NOT_ENOUGH_MONEY(XlvsCasesMod.INSTANCE.getLocalization().getResponseCaseRollResultNotEnoughMoneyMessage());

        private final String responseMessage;
    }

    private final Type type;
    private final Object[] params;
}
