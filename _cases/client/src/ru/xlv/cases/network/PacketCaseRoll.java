package ru.xlv.cases.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketCaseRoll implements IPacketCallbackEffective<PacketCaseRoll.Result> {

    @Getter
    public static class Result {
        private boolean success;
        private String responseMessage;
        private ItemStack itemStack;
    }

    private final Result result = new Result();

    private int caseIndex;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(caseIndex);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result.success = bbis.readBoolean();
        result.responseMessage = bbis.readUTF();
        if(result.success) {
            result.itemStack = ByteBufUtils.readItemStack(bbis.getBuffer());
        }
    }

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
