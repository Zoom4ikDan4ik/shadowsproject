package ru.xlv.post.database;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.post.common.PostObject;

@Getter
@RequiredArgsConstructor
public class PostObjectModel {
    private final String ownerName;
    private final PostObject postObject;
}
