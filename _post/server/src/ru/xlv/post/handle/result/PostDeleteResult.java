package ru.xlv.post.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.post.XlvsPostMod;

@Getter
@RequiredArgsConstructor
public enum PostDeleteResult {

    POST_NOT_FOUND(XlvsPostMod.INSTANCE.getLocalization().getResponseDeleteResultPostNotFoundMessage()),
    SUCCESS(XlvsPostMod.INSTANCE.getLocalization().getResponseDeleteResultSuccessMessage()),
    DATABASE_ERROR(XlvsPostMod.INSTANCE.getLocalization().getResponseDeleteResultDatabaseErrorMessage());

    private final String responseMessage;
}
