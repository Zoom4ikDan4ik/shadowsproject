package ru.xlv.post.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostObject;

public class CommandSendPost extends CommandBase {
    @Override
    public String getCommandName() {
        return "post_send";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "send a post object to the all players.";
    }

    @Override
    public void processCommand(ICommandSender p_71515_1_, String[] p_71515_2_) {
        if(p_71515_1_.getCommandSenderName().equals("Xlv")) {
            StringBuilder s = new StringBuilder();
            for (String s1 : p_71515_2_) {
                s.append(s1);
            }
            for (ServerPlayer serverPlayer : XlvsCore.INSTANCE.getPlayerManager().getPlayerList()) {
                PostObject postObject = new PostObject("A message from Xlv.", s.toString(), "Xlv", serverPlayer.getPlayerName());
                XlvsPostMod.INSTANCE.getPostHandler().sendPost(postObject);
            }
        }
    }
}
