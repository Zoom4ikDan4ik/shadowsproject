package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostAttachment;
import ru.xlv.post.common.PostAttachmentCredits;
import ru.xlv.post.common.PostIO;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.handle.result.PostSendResult;

import java.io.IOException;
import java.util.Arrays;

@NoArgsConstructor
public class PacketPostSend implements IPacketCallbackOnServer {

    private PostObject postObject;
    private PostSendResult postSendResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String recipient = bbis.readUTF();
        String article = bbis.readUTF();
        String text = bbis.readUTF();
        int attachmentAmount = bbis.readInt();
        PostAttachment<?>[] postAttachments = new PostAttachment[attachmentAmount];
        for (int i = 0; i < attachmentAmount; i++) {
            int type = bbis.readInt();
            PostAttachment<?> attachment;
            switch (type) {
                case 0:
                    int matrixX = bbis.readInt();
                    int matrixY = bbis.readInt();
                    attachment = XlvsPostMod.INSTANCE.getPostHandler().getPostAttachmentItemStack(entityPlayer, matrixX, matrixY);
                    break;
                case 1:
                    attachment = new PostAttachmentCredits(bbis.readInt());
                    break;
                default:
                    this.postSendResult = PostSendResult.UNKNOWN;
                    packetCallbackSender.send();
                    return;
            }
            if(attachment == null) return;
            postAttachments[i] = attachment;
        }
        PostObject postObject = new PostObject(article, text, entityPlayer.getCommandSenderName(), recipient);
        if(postAttachments.length > 0) {
            postObject.getAttachments().addAll(Arrays.asList(postAttachments));
        }
        this.postObject = postObject;
        XlvsPostMod.INSTANCE.getPostHandler().sendPost(postObject).thenAccept(postSendResult -> {
            this.postSendResult = postSendResult;
            packetCallbackSender.send();
        });
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(postSendResult == PostSendResult.SUCCESS);
        if(postSendResult == PostSendResult.SUCCESS) {
            PostIO.writePostObject(postObject, bbos);
        } else {
            bbos.writeUTF(postSendResult.getResponseMessage());
        }
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
