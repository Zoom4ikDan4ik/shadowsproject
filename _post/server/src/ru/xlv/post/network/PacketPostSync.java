package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.post.XlvsPostMod;
import ru.xlv.post.common.PostIO;
import ru.xlv.post.common.PostObject;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketPostSync implements IPacketCallbackOnServer {

    private static final RequestController<String> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private boolean success = true;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
//        if(!REQUEST_CONTROLLER.canRequest(entityPlayer.getCommandSenderName())) {
//            success = false;
//        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(success);
        if(success) {
            List<PostObject> allPost = XlvsPostMod.INSTANCE.getPostHandler().getAllPost(entityPlayer.getCommandSenderName());
            if(allPost == null) {
                bbos.writeInt(0);
                return;
            }
            bbos.writeInt(allPost.size());
            for (PostObject postObject : allPost) {
                PostIO.writePostObject(postObject, bbos);
            }
        } else {
            bbos.writeUTF(XlvsCore.INSTANCE.getLocalization().responseTooManyRequests);
        }
    }
}
