package ru.xlv.post.common;

import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public abstract class PostAttachmentClientData {
    public abstract void write(ByteBufOutputStream byteBufOutputStream) throws IOException;
}
