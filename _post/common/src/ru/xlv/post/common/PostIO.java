package ru.xlv.post.common;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;
import java.util.UUID;

public class PostIO {

    public static PostObject readPostObject(ByteBufInputStream byteBufInputStream) throws IOException {
        PostObject postObject = new PostObject(
                UUID.fromString(byteBufInputStream.readUTF()),
                byteBufInputStream.readUTF(),
                byteBufInputStream.readUTF(),
                byteBufInputStream.readUTF(),
                byteBufInputStream.readUTF()
        );
        postObject.setCreationTimeMills(System.currentTimeMillis() - byteBufInputStream.readLong());
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            int a = byteBufInputStream.readInt();
            if(a < 0 || a >= PostAttachmentType.values().length) throw new IOException();
            PostAttachmentType postAttachmentType = PostAttachmentType.values()[a];
            PostAttachment<?> postAttachment;
            switch (postAttachmentType) {
                case ITEM_STACK:
                    postAttachment = new PostAttachmentItemStack(ByteBufUtils.readItemStack(byteBufInputStream.getBuffer()));
                    break;
                case CREDITS:
                    postAttachment = new PostAttachmentCredits(byteBufInputStream.readInt());
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + postAttachmentType);
            }
            postObject.getAttachments().add(postAttachment);
        }
        return postObject;
    }

    public static void writePostObject(PostObject postObject, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(postObject.getUuid().toString());
        byteBufOutputStream.writeUTF(postObject.getTitle());
        byteBufOutputStream.writeUTF(postObject.getText());
        byteBufOutputStream.writeUTF(postObject.getSender());
        byteBufOutputStream.writeUTF(postObject.getRecipient());
        byteBufOutputStream.writeLong(System.currentTimeMillis() - postObject.getCreationTimeMills());
        byteBufOutputStream.writeInt(postObject.getAttachments().size());
        for (PostAttachment<?> attachment : postObject.getAttachments()) {
            switch (attachment.getType()) {
                case ITEM_STACK:
                    ByteBufUtils.writeItemStack(byteBufOutputStream.buffer(), (ItemStack) attachment.getAttachment());
                    break;
                case CREDITS:
                    byteBufOutputStream.writeInt(((Integer) attachment.getAttachment()));
            }
        }
    }
}
