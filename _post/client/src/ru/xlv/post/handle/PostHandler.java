package ru.xlv.post.handle;

import lombok.Getter;
import net.minecraft.client.Minecraft;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.post.common.PostAttachmentClientData;
import ru.xlv.post.common.PostObject;
import ru.xlv.post.network.PacketPostSend;
import ru.xlv.post.network.PacketPostSync;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PostHandler {

    @Getter
    private final List<PostObject> postObjectList = Collections.synchronizedList(new ArrayList<>());

    public SyncResultHandler<PacketPostSend.Result> sendPost(String recipient, String article, String text, List<PostAttachmentClientData> postAttachmentSimpleData) {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostSend(recipient, article, text, postAttachmentSimpleData));
    }

    public SyncResultHandler<PacketPostSync.Result> getPostFromServer() {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostSync());
    }

    public void syncPost(List<PostObject> postObjects) {
        postObjectList.clear();
        postObjectList.addAll(postObjects);
    }

    public void onNewPost(PostObject postObject) {
        postObjectList.add(postObject);
        SoundUtils.playGuiSound(SoundType.LETTER_RECEIVED);
    }

    public List<PostObject> getOutgoingPost() {
        return postObjectList.stream()
                .filter(postObject -> postObject.getSender().equals(Minecraft.getMinecraft().thePlayer.getCommandSenderName()))
                .collect(Collectors.toList());
    }

    public List<PostObject> getIncomingPost() {
        return postObjectList.stream()
                .filter(postObject -> !postObject.getSender().equals(Minecraft.getMinecraft().thePlayer.getCommandSenderName()))
                .collect(Collectors.toList());
    }
}
