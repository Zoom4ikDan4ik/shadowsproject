package ru.xlv.post.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.network.IPacketOut;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nullable;
import java.io.IOException;

@NoArgsConstructor
public class PacketPostTakeAttachment implements IPacketCallbackEffective<String> {

    private String uuid, responseMessage;

    public PacketPostTakeAttachment(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(uuid);
    }

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        responseMessage = bbis.readUTF();
    }

    @Nullable
    @Override
    public String getResult() {
        return responseMessage;
    }
}
