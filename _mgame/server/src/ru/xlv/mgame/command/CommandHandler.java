package ru.xlv.mgame.command;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.command.Command;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mgame.XlvsMGameMod;
import ru.xlv.mgame.arena.deathmatch.ArenaDeathMatchManager;
import ru.xlv.mgame.arena.deathmatch.DeathMatchJoinResult;
import ru.xlv.mgame.arena.duel.ArenaDuelManager;
import ru.xlv.mgame.arena.duel.DuelAcceptResult;
import ru.xlv.mgame.arena.duel.DuelInviteResult;
import ru.xlv.mgame.arena.teambattle.ArenaTeamBattleManager;
import ru.xlv.mgame.arena.teambattle.JoinTeamBattleResult;
import ru.xlv.mochar.util.Utils;

@SuppressWarnings("unused")
public class CommandHandler {

    @Command
    public static void duel(EntityPlayer entityPlayer, String[] args) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (args.length > 0) {
            ArenaDuelManager duelHandler = XlvsMGameMod.INSTANCE.getArenaDuelHandler();
            switch (args[0]) {
                case "invite": {
                    String username = args[1];
                    DuelInviteResult duelInviteResult = duelHandler.initiateDuel(serverPlayer.getPlayerName(), username);
                    Utils.sendMessage(serverPlayer, duelInviteResult.getResponseMessage());
                    break;
                }
                case "accept": {
                    String username = args[1];
                    DuelAcceptResult duelAcceptResult = duelHandler.acceptDuel(serverPlayer.getPlayerName(), username);
                    Utils.sendMessage(serverPlayer, duelAcceptResult.getResponseMessage());
                    break;
                }
            }
        }
    }

    @Command
    public static void deathMatch(EntityPlayer entityPlayer, String[] args) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        ArenaDeathMatchManager deathMatchManager = XlvsMGameMod.INSTANCE.getArenaDeathMatchManager();
        if (args.length > 0) {
            switch (args[0]) {
                case "join": {
                    DeathMatchJoinResult deathMatchJoinResult = deathMatchManager.joinArena(serverPlayer.getPlayerName());
                    Utils.sendMessage(serverPlayer, deathMatchJoinResult.getResponseMessage());
                    break;
                }
                case "leave": {
                    deathMatchManager.leaveArena(serverPlayer.getPlayerName());
                }
            }
        }
    }

    @Command
    public static void teamBattle(EntityPlayer entityPlayer, String[] args) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        ArenaTeamBattleManager teamBattleManager = XlvsMGameMod.INSTANCE.getArenaTeamBattleManager();
        if (args.length > 0) {
            switch (args[0]) {
                case "join": {
                    JoinTeamBattleResult joinTeamBattleResult = teamBattleManager.joinArena(serverPlayer.getPlayerName());
                    Utils.sendMessage(serverPlayer, joinTeamBattleResult.getResponseMessage());
                    break;
                }
                case "leave": {
                    teamBattleManager.leaveArena(serverPlayer.getPlayerName());
                }
            }
        }
    }
}
