package ru.xlv.mgame.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class ArenaLocalization extends Localization {

    private final String playerReceivedReward = "Player {0} received {1} credits for {2} score points.";
    private final String playerKilledPlayer = "{0} killed {1}";

    @Override
    public File getConfigFile() {
        return new File("config/mini_games/localization.json");
    }
}
