package ru.xlv.mgame.arena;

import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.KeyValueStore;

public class PlayerRatingManager {

    private KeyValueStore.Key ratingKey;

    public int getRating(ServerPlayer serverPlayer) {
        Object o = serverPlayer.getKeyValueStore().get(getKey(serverPlayer));
        return o == null ? 0 : (int) o;
    }

    public void addRating(ServerPlayer serverPlayer, int amount) {
        int prev = serverPlayer.getKeyValueStore().get(getKey(serverPlayer), Integer.class);
        serverPlayer.getKeyValueStore().set(getKey(serverPlayer), prev + amount);
    }

    public void consumeRating(ServerPlayer serverPlayer, int amount) {
        int prev = serverPlayer.getKeyValueStore().get(getKey(serverPlayer), Integer.class);
        int value = Math.max(prev - amount, 0);
        serverPlayer.getKeyValueStore().set(getKey(serverPlayer), value);
    }

    private KeyValueStore.Key getKey(ServerPlayer serverPlayer) {
        if (ratingKey == null) {
            ratingKey = serverPlayer.getKeyValueStore().genKey();
        }
        return ratingKey;
    }
}
