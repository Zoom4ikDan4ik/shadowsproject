package ru.xlv.mgame.arena.teambattle;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.mgame.XlvsMGameMod;

@Getter
@RequiredArgsConstructor
public enum JoinTeamBattleResult {

    SUCCESS(XlvsMGameMod.INSTANCE.getTeamBattleLocalization().getResponseJoinResultSuccessMessage()),
    NO_AVAILABLE_ARENA_FOUND(XlvsMGameMod.INSTANCE.getTeamBattleLocalization().getResponseJoinResultNoAvailableArenaFoundMessage()),
    UNKNOWN(XlvsMGameMod.INSTANCE.getTeamBattleLocalization().getResponseJoinResultUnknownMessage());

    private final String responseMessage;
}
