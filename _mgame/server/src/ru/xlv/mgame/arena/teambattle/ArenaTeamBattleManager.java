package ru.xlv.mgame.arena.teambattle;

import lombok.RequiredArgsConstructor;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mgame.arena.ArenaManager;
import ru.xlv.mgame.util.ArenaLocalization;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class ArenaTeamBattleManager extends ArenaManager<ArenaTeamBattle> {

    private final TeamBattleConfig config;
    private final ArenaLocalization arenaLocalization;
    private final TeamBattleLocalization localization;

    public JoinTeamBattleResult joinArena(String username) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if (serverPlayer == null) {
            return JoinTeamBattleResult.UNKNOWN;
        }
        List<ServerPlayer> list = new ArrayList<>();
        list.add(serverPlayer);
        GroupServer group = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(serverPlayer);
        if(group != null) {
            list.addAll(group.getPlayers());
        }
        synchronized (getArenaList()) {
            if(getArenaList().isEmpty()) {
                ArenaTeamBattle arenaTeamBattle = new ArenaTeamBattle(config, arenaLocalization, localization);
                arenaTeamBattle.start();
                getArenaList().add(arenaTeamBattle);
            } else {
                Optional<ArenaTeamBattle> first = getArenaList()
                        .stream()
                        .filter(arenaTeamBattle -> !arenaTeamBattle.isActive())
                        .filter(arenaTeamBattle -> arenaTeamBattle.getNumberOfPlayers() + list.size() <= config.getMaxPlayersLimit())
                        .findFirst();
                if (first.isPresent()) {
                    first.get().addPlayers(list);
                    return JoinTeamBattleResult.SUCCESS;
                }
            }
        }
        return JoinTeamBattleResult.NO_AVAILABLE_ARENA_FOUND;
    }

    public void leaveArena(String username) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if (serverPlayer == null) {
            return;
        }
        synchronized (getArenaList()) {
            getArenaList()
                    .stream()
                    .filter(arenaTeamBattle -> arenaTeamBattle.getPlayerTeam(serverPlayer) != null)
                    .findFirst()
                    .ifPresent(arenaTeamBattle -> arenaTeamBattle.removePlayer(serverPlayer));
        }
    }
}
