package ru.xlv.mgame.arena.duel;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.mgame.XlvsMGameMod;

@Getter
@RequiredArgsConstructor
public enum DuelAcceptResult {

    SUCCESS(XlvsMGameMod.INSTANCE.getDuelLocalization().getResponseAcceptResultSuccessMessage()),
    NOT_VALID_INVITE(XlvsMGameMod.INSTANCE.getDuelLocalization().getResponseAcceptResultNotValidInviteMessage());

    private final String responseMessage;
}
