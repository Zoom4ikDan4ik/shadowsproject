package ru.xlv.mgame.arena.deathmatch;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.mgame.XlvsMGameMod;

@Getter
@RequiredArgsConstructor
public enum DeathMatchJoinResult {

    SUCCESS(XlvsMGameMod.INSTANCE.getDeathMatchLocalization().getResponseDeathMatchJoinResultSuccessMessage()),
    NO_AVAILABLE_ARENA(XlvsMGameMod.INSTANCE.getDeathMatchLocalization().getResponseDeathMatchJoinResultNoAvailableArenaMessage()),
    PLAYER_ALREADY_JOINED(XlvsMGameMod.INSTANCE.getDeathMatchLocalization().getResponseDeathMatchJoinResultPlayerAlreadyJoinedMessage()),
    UNKNOWN(XlvsMGameMod.INSTANCE.getDeathMatchLocalization().getResponseDeathMatchJoinResultUnknownMessage());

    private final String responseMessage;
}
