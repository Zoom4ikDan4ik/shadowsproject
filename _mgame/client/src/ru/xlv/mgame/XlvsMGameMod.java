package ru.xlv.mgame;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;

import static ru.xlv.mgame.XlvsMGameMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsMGameMod {

    static final String MODID = "xlvsmgame";

    @Mod.Instance(MODID)
    public static XlvsMGameMod INSTANCE;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        
    }
}
