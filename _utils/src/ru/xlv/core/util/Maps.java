package ru.xlv.core.util;

import lombok.experimental.UtilityClass;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Predicate;

@UtilityClass
public class Maps {

    /**
     * Позволяет удалить элемент из списка карты.
     * */
    public <T, V> boolean removeElemFromMappedList(@Nonnull Map<T, List<V>> map, T key, V element) {
        List<V> vs = map.get(key);
        if (vs != null) {
            return vs.remove(element);
        }
        return false;
    }

    /**
     * Позволяет добавить элемент в список карты.
     * */
    public <T, V> void addElemToMappedList(@Nonnull Map<T, List<V>> map, T key, V element) {
        List<V> vs = map.get(key);
        if (vs != null) {
            vs.add(element);
        } else {
            List<V> list = new ArrayList<>();
            list.add(element);
            map.put(key, list);
        }
    }

    /**
     * Позволяет добавить элементы в список карты.
     * */
    public <T, V> void addElementsToMappedList(@Nonnull Map<T, List<V>> map, T key, V... elements) {
        List<V> vs = map.get(key);
        if (vs != null) {
            vs.addAll(Arrays.asList(elements));
        } else {
            List<V> list = new ArrayList<>(Arrays.asList(elements));
            map.put(key, list);
        }
    }

    public <T, V> V getElemFromMappedList(@Nonnull Map<T, List<V>> map, T key, Predicate<V> predicate) {
        List<V> vs = map.get(key);
        return vs == null ? null : Flex.getCollectionElement(vs, predicate);
    }

    @Nonnull
    public <K, V> Map<K, V> of(K k, V v) {
        HashMap<K, V> kvHashMap = new HashMap<>();
        kvHashMap.put(k, v);
        return kvHashMap;
    }

    @Nonnull
    public <K, V> Map<K, V> of(@Nonnull Class<K> kClass, @Nonnull Class<V> vClass, @Nonnull Object... keyValuePars) {
        if(keyValuePars.length % 2 != 0) throw new IllegalArgumentException(Arrays.toString(keyValuePars));
        HashMap<K, V> hashMap = new HashMap<>();
        for (int i = 0; i < keyValuePars.length; i += 2) {
            Object key = keyValuePars[i];
            Object value = keyValuePars[i + 1];
            if(key.getClass() != kClass || value.getClass() != vClass) throw new IllegalArgumentException();
            hashMap.put(kClass.cast(key), vClass.cast(value));
        }
        return hashMap;
    }
}
