package ru.xlv.decblocks.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.util.IIcon;

public class BlockWall extends net.minecraft.block.BlockWall {

    private Block block;

    public BlockWall(String name, Block p_i45435_1_) {
        super(p_i45435_1_);
        setBlockName(name);
        this.block = p_i45435_1_;
    }

    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int p_149691_2_)
    {
        return block.getBlockTextureFromSide(p_149691_1_);
    }
}
