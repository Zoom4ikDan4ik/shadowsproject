package ru.xlv.training.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketTrainingStop implements IPacketOut {
    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {

    }
}
