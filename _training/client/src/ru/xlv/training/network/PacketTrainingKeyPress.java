package ru.xlv.training.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketTrainingKeyPress implements IPacketOut {

    private int keyCode;

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(keyCode);
    }
}
