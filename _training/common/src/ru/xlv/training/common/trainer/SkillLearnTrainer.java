package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SkillLearnTrainer implements ITrainer {

    private final int skillId;
    private final String description;

    @Override
    public ITrainer clone() {
        return new SkillLearnTrainer(skillId, description);
    }
}
