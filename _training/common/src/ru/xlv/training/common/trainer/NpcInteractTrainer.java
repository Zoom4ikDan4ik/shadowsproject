package ru.xlv.training.common.trainer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class NpcInteractTrainer implements ITrainer {

    private final String npcName;
    private final int x, y, z;
    private final String description;

    @Override
    public ITrainer clone() {
        return new NpcInteractTrainer(npcName, x, y, z, description);
    }
}
