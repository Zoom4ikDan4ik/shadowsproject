package ru.xlv.training;

import lombok.Getter;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.module.Construct;
import ru.xlv.core.common.module.InstanceHolder;
import ru.xlv.core.module.ServerModule;
import ru.xlv.training.event.EventListener;
import ru.xlv.training.handle.TrainingHandler;
import ru.xlv.training.handle.TrainingManager;
import ru.xlv.training.network.PacketTrainingKeyPress;
import ru.xlv.training.network.PacketTrainingStop;
import ru.xlv.training.network.PacketTrainingSync;
import ru.xlv.training.util.ConfigTraining;
import ru.xlv.training.util.TrainerConstructor;

@Getter
public class XlvsTrainingMod extends ServerModule<XlvsTrainingMod> {

    @InstanceHolder
    public static XlvsTrainingMod INSTANCE;

    private TrainingManager trainingManager;
    private TrainingHandler trainingHandler;

    public XlvsTrainingMod(XlvsCore core) {
        super(core);
    }

    @Construct
    @SuppressWarnings("unused")
    public void init() {
        ConfigTraining configTraining = new ConfigTraining();
        configTraining.load();
        trainingManager = new TrainingManager(configTraining.getTrainingSpawnPoint(), configTraining.getServerSpawnPoint());
        TrainerConstructor trainerConstructor = new TrainerConstructor();
        configTraining.getTrainerList().stream()
                .map(trainerConstructor::construct)
                .forEach(trainingManager::addTrainer);
        trainingHandler = new TrainingHandler(trainingManager);
        EventListener eventListener = new EventListener(trainingManager, trainingHandler);
        XlvsCoreCommon.EVENT_BUS.register(eventListener);
        MinecraftForge.EVENT_BUS.register(eventListener);
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(getModuleId(),
                new PacketTrainingKeyPress(),
                new PacketTrainingStop(),
                new PacketTrainingSync()
        );
    }

    @Override
    public String getModuleId() {
        return "xlvstraining";
    }
}
