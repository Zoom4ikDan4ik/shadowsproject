//Fragment Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330
out vec4 bloomColor;

in vec2 texCoords;

uniform sampler2D diffuse;

uniform float bloomThreshold;
uniform float bloomIntensity;

void main() {
	vec4 textureColor = texture(diffuse, texCoords);
	
	//float bright = (textureColor.r * 0.2126) + (textureColor.g * 0.7152) + (textureColor.b * 0.0722);
	//float bright = (textureColor.r + textureColor.g + textureColor.b) / 3.0;
	if(textureColor.r > bloomThreshold || textureColor.g > bloomThreshold || textureColor.g > bloomThreshold) {
		bloomColor = textureColor * bloomIntensity;
	} else {
		bloomColor = vec4(0.0,0.0,0.0,textureColor.w);
	}
}