//Krogenit
#version 330
out vec4 outputColor;

#define POINT_LIGHTS 1

uniform sampler2D gColor;
uniform sampler2D gLightMap;
uniform sampler2D gPosition;
uniform sampler2D gNormal;

uniform sampler2D lightmap;

in vec2 texCoords;
in vec3 lightPos;

uniform struct PointLight {
   vec3 position;
   float attenuation;
   vec3 color;
   float specular;
} pointLights[POINT_LIGHTS];

vec3 calculatePointLight(in PointLight light, in vec3 E, in vec3 delta, in float dist, in vec3 normal, in float specularUnit, in float shininess) {
	vec3 L = normalize(delta);
	vec3 R = normalize(-reflect(L,normal));
	float nDotL = max(dot(normal,L), 0.0);
	
	float att = 1.0 - dist / light.attenuation;
	//float att = 1.0 / (light.attenuation + light.attenuation * dist + 
  			     //light.attenuation * (dist * dist)); 
	
	if(att > 0.0) {
		vec3 colorL = light.color;

		//if(nDotL > 0.0) {
			float rDotE = max(dot(R,E),0.0);
			float power = pow(rDotE, 1.0 + 127.0 * shininess);
			vec3 amb = colorL * att / 2.0;		
			vec3 dif = colorL * nDotL * att * 2.0; 
			vec3 spec = 6 * colorL * power * att * specularUnit * light.specular;

			vec3 final = amb + dif + spec;
			
			return final;
		//} else {
		//	float att1 = pow(att, 3);
		//	return colorL * att1;
		//}
	} else {
		return vec3(0.0,0.0,0.0);
	}
}

void main() {
	vec4 position = texture(gPosition, texCoords);
	vec3 pos = position.xyz;
	
	vec3 delta = (lightPos - pos.xyz);
	float dist = length(delta);
	if(dist <= pointLights[0].attenuation) {
		vec4 colorAlbedo = texture(gColor, texCoords);
		vec4 gLightMapCoord = texture(gLightMap, texCoords);
		//gLightMapCoord.x = 0.05;
		vec3 colorLightmap = texture(lightmap, gLightMapCoord.xy).xyz;

		float specularUnit = position.w;
		vec4 gNormalIn = texture(gNormal, texCoords);
		vec3 normal = gNormalIn.xyz;
		float shininess = gNormalIn.w;
		
		vec3 lightValue = calculatePointLight(pointLights[0], normalize(-pos), delta, dist, normal, specularUnit, shininess);

		outputColor.rgb = colorAlbedo.rgb / colorLightmap * lightValue;
		outputColor.w = colorAlbedo.w;
	} else {
		outputColor = vec4(0.0, 0.0, 0.0, 0.0);
	}
}