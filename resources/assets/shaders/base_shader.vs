//Vertex Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 330 core

#define DIRECTIONAL_LIGHTS 1

layout (location = 4) in vec3 in_position;
layout (location = 5) in vec2 in_textureCoords;
layout (location = 6) in vec3 in_normal;
layout (location = 11) in vec3 in_tangent;

out vec2 texCoords;
out vec2 lightMapCoords;
out vec4 color;
out vec3 position;
out vec3 normal;
out vec3 tangent;
out vec3 delta;

uniform mat4 projMat, viewMat;
uniform mat4 modelView, lightMapMatrix;
uniform vec4 in_color;
uniform vec2 in_lightMapCoord;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

void main() {	
	vec4 pos = viewMat * vec4(in_position, 1.0);
	color = in_color;
	normal = normalize((viewMat * vec4(in_normal, 0.0)).xyz);
	texCoords = in_textureCoords;
	position = pos.xyz;
	gl_Position = projMat * pos;
	tangent = normalize((viewMat * vec4(in_tangent, 0.0)).xyz);	
	
	lightMapCoords = (lightMapMatrix * vec4(in_lightMapCoord, 0.0, 1.0)).xy;
	delta = (modelView * vec4(directLight[0].dir, 1.0)).xyz;
}