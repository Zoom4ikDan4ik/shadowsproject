//gBuffer shader
//Krogenit 
#version 120

attribute vec3 in_position;
attribute vec2 in_textureCoords;
attribute vec3 in_normal;
attribute vec3 in_tangent;

varying vec2 texCoords;
varying vec2 lightMapCoords;
varying vec3 normal;
varying vec3 position;
varying vec4 color;
varying vec3 tangent;

void main() {
	vec4 pos = gl_ModelViewMatrix * gl_Vertex;
	normal = normalize(gl_NormalMatrix * gl_Normal);
	texCoords = gl_MultiTexCoord0.xy;
	
	lightMapCoords = (gl_TextureMatrix[1] * gl_MultiTexCoord1).xy;
	color = gl_Color;
	tangent = gl_NormalMatrix * in_tangent;
	position = pos.xyz;
	gl_Position = gl_ProjectionMatrix * pos;
}