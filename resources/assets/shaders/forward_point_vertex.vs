//Vertex Shader specially written for ShadowS Project
//Copyright © 2019-2020 by Krogenit
#version 120

#define POINT_LIGHTS 1

attribute vec3 in_position;
attribute vec2 in_textureCoords;
attribute vec3 in_normal;
attribute vec3 in_tangent;

varying vec2 texCoords;
varying vec2 lightMapCoords;
varying vec4 color;
varying vec3 lightColor;

uniform mat4 modelView;

uniform bool oldRender;
uniform float shininess;
uniform float specularUnit;

uniform struct PointLight {
   vec3 position;
   float attenuation;
   vec3 color;
   float specular;
} pointLights[POINT_LIGHTS];

vec3 calculatePointLight(in PointLight light, in vec3 E, 
in vec3 delta, in float dist, in vec3 normal) {
	vec3 L = normalize(delta);
	vec3 R = normalize(-reflect(L,normal));
	float nDotL = max(dot(normal,L), 0.0);
	
	float att = 1.0 - dist / light.attenuation;
	
	if(att > 0) {
		vec3 colorL = light.color;

		float rDotE = max(dot(R,E),0.0);
		float power = pow(rDotE, 128.0 * shininess);
		vec3 amb = colorL * att / 2.0;		
		vec3 dif = colorL * nDotL * att * 2.0; 
		vec3 spec = 6 * colorL * power * att * specularUnit * light.specular;

		vec3 final = amb + dif + spec;
			
		return final;
	} else {
		return vec3(0,0,0);
	}
}

void main() {	
	if(oldRender) {
		vec4 pos = gl_ModelViewMatrix * gl_Vertex;
		vec3 lightPos = (modelView * vec4(pointLights[0].position, 1.0)).xyz;
		vec3 delta = (lightPos - pos.xyz);
		float dist = length(delta);
		if(dist <= pointLights[0].attenuation) {
			vec3 normal = gl_NormalMatrix * gl_Normal;
			lightColor = calculatePointLight(pointLights[0], normalize(-pos.xyz), delta, dist, normal);
		} else {
			lightColor = vec3(0.0, 0.0, 0.0);
		}
		
		color = gl_Color;
		texCoords = gl_MultiTexCoord0.xy;
		gl_Position = gl_ProjectionMatrix * pos;
	} else {
		vec4 pos = gl_ModelViewMatrix * vec4(in_position, 1.0);
		vec3 lightPos = (modelView * vec4(pointLights[0].position, 1.0)).xyz;
		vec3 delta = (lightPos - pos.xyz);
		float dist = length(delta);
		if(dist <= pointLights[0].attenuation) {
			vec3 normal = gl_NormalMatrix * in_normal;
			lightColor = calculatePointLight(pointLights[0], normalize(-pos.xyz), delta, dist, normal);
		} else {
			lightColor = vec3(0.0, 0.0, 0.0);
		}

		color = gl_Color;
		texCoords = in_textureCoords;
		gl_Position = gl_ProjectionMatrix * pos;
	}
	
	lightMapCoords = (gl_TextureMatrix[1] * gl_MultiTexCoord1).xy;
}