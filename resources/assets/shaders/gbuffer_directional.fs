//Krogenit
#version 330
out vec4 outputColor;

#define DIRECTIONAL_LIGHTS 1

uniform sampler2D gColor;
uniform sampler2D gLightMap;
uniform sampler2D gPosition;
uniform sampler2D gNormal;

uniform sampler2D lightmap;

uniform bool usePointLights;

in vec2 texCoords;
in vec3 dirPos;

uniform struct DirLight {
	vec3 dir;
	vec3 color;
	float specular;
} directLight[DIRECTIONAL_LIGHTS];

vec3 calculateDirectLight(in DirLight light, in vec3 E, in vec3 normal, in float specularUnit, in float shininess) {
	vec3 L = normalize(dirPos);
	float nDotL = max(dot(normal,L), 0.0);

	vec3 R = normalize(-reflect(L,normal));
	float rDotE = max(dot(R,E),0.0);
	float power = pow(rDotE, 1.0 + 127.0 * shininess);
	vec3 amb = light.color / 6.0;		
	vec3 dif = light.color * nDotL; 
	vec3 spec = light.color * power * specularUnit * light.specular;

	vec3 final = amb + dif + spec;
			
	return final;
}

void main() {
	vec4 position = texture(gPosition, texCoords);
	vec3 pos = position.xyz;

	vec4 colorAlbedo = texture(gColor, texCoords);
	vec4 gLightMapCoord = texture(gLightMap, texCoords);
	if(usePointLights) gLightMapCoord.x = 0.05;
	vec3 colorLightmap = texture(lightmap, gLightMapCoord.xy).xyz;

	float specularUnit = position.w;
	vec4 gNormalIn = texture(gNormal, texCoords);
	vec3 normal = gNormalIn.xyz;
	float shininess = gNormalIn.w;
	
	vec3 lightValue = colorLightmap * calculateDirectLight(directLight[0], normalize(-pos), normal, specularUnit, shininess);

	outputColor.w = colorAlbedo.w;
	outputColor.rgb = colorAlbedo.rgb * lightValue;
}