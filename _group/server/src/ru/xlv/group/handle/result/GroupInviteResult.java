package ru.xlv.group.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.group.XlvsGroupMod;

@Getter
@RequiredArgsConstructor
public enum GroupInviteResult {

    SUCCESS(XlvsGroupMod.INSTANCE.getLocalization().responseInviteSuccessMessage),
    PLAYER_NOT_FOUND(XlvsGroupMod.INSTANCE.getLocalization().responseInvitePlayerNotFoundMessage),
    PLAYER_ALREADY_INVITED(XlvsGroupMod.INSTANCE.getLocalization().responseInvitePlayerAlreadyInvitedMessage),
    PLAYER_ALREADY_GROUPED(XlvsGroupMod.INSTANCE.getLocalization().responseInvitePlayerAlreadyGroupedMessage),
    GROUP_IS_FULL(XlvsGroupMod.INSTANCE.getLocalization().responseInviteGroupIsFullMessage),
    UNKNOWN(XlvsGroupMod.INSTANCE.getLocalization().responseInviteUnknownMessage);

    private final String responseMessage;

}
