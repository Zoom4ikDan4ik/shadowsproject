package ru.xlv.group.util;

import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Configurable
public class GroupLocalization extends Localization {

    public String responseInviteSuccessMessage = "responseInviteSuccessMessage";
    public String responseInviteUnknownMessage = "responseInviteUnknownMessage";
    public String responseInvitePlayerAlreadyInvitedMessage = "responseInvitePlayerAlreadyInvitedMessage";
    public String responseInvitePlayerAlreadyGroupedMessage = "responseInvitePlayerAlreadyGroupedMessage";
    public String responseInvitePlayerNotFoundMessage = "responseInvitePlayerNotFoundMessage";
    public String responseInviteGroupIsFullMessage = "responseInviteGroupIsFullMessage";

    public String responseSetLeaderSuccessMessage = "responseSetLeaderSuccessMessage";
    public String responseSetLeaderPlayerNotFoundMessage = "responseSetLeaderPlayerNotFoundMessage";
    public String responseSetLeaderPlayerAlreadyLeaderMessage = "responseSetLeaderPlayerAlreadyLeaderMessage";
    public String responseSetLeaderUnknownMessage = "responseSetLeaderUnknownMessage";

    public String responseKickSuccessMessage = "responseKickSuccessMessage";
    public String responseKickPlayerNotFoundMessage = "responseKickPlayerNotFoundMessage";
    public String responseKickUnknownMessage = "responseKickUnknownMessage";

    public String responseAcceptInviteSuccessMessage = "responseAcceptInviteSuccessMessage";
    public String responseAcceptInviteGroupIsFullMessage = "responseAcceptInviteGroupIsFullMessage";
    public String responseAcceptInviteInviterNotFoundOrOfflineMessage = "responseAcceptInviteInviterNotFoundOrOfflineMessage";
    public String responseAcceptInviteUnknownErrorMessage = "responseAcceptInviteUnknownErrorMessage";

    public String inviteNotificationMessage = "Hello, you have an invitation to a group from {0}.";
    public String inviteNotificationGroupMessage = "{0} has been invited to the group.";
    public String kickNotificationMessage = "You have been kicked from the group.";
    public String kickNotificationGroupMessage = "{0} has been kicked out from the group.";
    public String changeLeaderNotificationMessage = "You have been assigned as leader of current group!";
    public String changeLeaderNotificationGroupMessage = "{0} is now the leader of the group.";
    public String inviteAcceptNotificationMessage = "You joined the group.";
    public String inviteAcceptNotificationGroupMessage = "{0} joined the group.";

    @Override
    public File getConfigFile() {
        return new File("config/group/localization.json");
    }
}
