package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;

import java.io.IOException;

@NoArgsConstructor
@AllArgsConstructor
public class PacketGroupSync implements IPacketCallbackOnServer, IPacketOutServer {

    private GroupServer group;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {}

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        GroupServer group = this.group == null ? XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(entityPlayer.getCommandSenderName()) : this.group;
        bbos.writeBoolean(group != null);
        if (group != null) {
            writeGroup(group, bbos);
        }
    }

    private static void writeGroup(GroupServer group, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeUTF(group.getLeader().getPlayerName());
        byteBufOutputStream.writeInt(group.getPlayers().size());
        for (ServerPlayer serverPlayer : group.getPlayers()) {
            byteBufOutputStream.writeUTF(serverPlayer.getPlayerName());
        }
    }
}
