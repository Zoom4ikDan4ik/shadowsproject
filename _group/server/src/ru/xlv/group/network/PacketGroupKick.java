package ru.xlv.group.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.result.GroupKickResult;

import java.io.IOException;

@NoArgsConstructor
public class PacketGroupKick implements IPacketCallbackOnServer {

    private GroupKickResult groupKickResult;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        String targetName = bbis.readUTF();
        groupKickResult = XlvsGroupMod.INSTANCE.getGroupHandler().handleKick(entityPlayer.getCommandSenderName(), targetName);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeUTF(groupKickResult.getResponseMessage());
    }
}
