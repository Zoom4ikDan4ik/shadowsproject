package ru.xlv.container;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.container.network.PacketContainerInteract;
import ru.xlv.container.network.PacketContainerSync;
import ru.xlv.container.network.PacketContainerSyncInteract;
import ru.xlv.container.render.RenderContainer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;

import static ru.xlv.container.XlvsContainerMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsContainerMod {

    public static final String MODID = "xlvscntrs";

    @Mod.Instance(MODID)
    public static XlvsContainerMod INSTANCE;

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        EntityRegistry.registerGlobalEntityID(EntityContainer.class, "EntityContainer", 71);
        RenderingRegistry.registerEntityRenderingHandler(EntityContainer.class, new RenderContainer());
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketContainerInteract(),
                new PacketContainerSync(),
                new PacketContainerSyncInteract()
        );
        XlvsCoreCommon.EVENT_BUS.register(new InteractContainerEventListener());
    }
}
