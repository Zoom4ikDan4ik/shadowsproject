package ru.xlv.container.network;

import lombok.NoArgsConstructor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import ru.xlv.container.common.entity.EntityContainer;
import ru.xlv.container.gui.GuiContainer;
import ru.xlv.core.common.inventory.MatrixInventoryIO;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketContainerSyncInteract implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        int entityId = bbis.readInt();
        Minecraft mc = Minecraft.getMinecraft();
        Entity entityByID = mc.thePlayer.worldObj.getEntityByID(entityId);
        if (entityByID instanceof EntityContainer) {
            ((EntityContainer) entityByID).setPlayerName(bbis.readUTF());
            ((EntityContainer) entityByID).setCharacterType(CharacterType.values()[bbis.readInt()]);
            ((EntityContainer) entityByID).setPlayerLevel(bbis.readInt());
//            MatrixInventoryIO.read(((EntityContainer) entityByID).getMatrixInventory(), bbis);
//            MatrixInventoryIO.read(((EntityContainer) entityByID).getMatrixInventory(), bbis);
            GuiScreen currentScreen = mc.currentScreen;
            if(currentScreen instanceof GuiContainer) {
                GuiContainer guiContainer = (GuiContainer) currentScreen;
                guiContainer.sync();
            }
        }
    }
}
