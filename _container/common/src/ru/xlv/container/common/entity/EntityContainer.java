package ru.xlv.container.common.entity;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;
import ru.xlv.core.common.item.rarity.EnumItemRarity;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.storage.NBTLoader;

import javax.annotation.Nonnull;

public class EntityContainer extends EntityAnimal implements IMatrixInventoryProvider {

    private static final String MATRIX_INVENTORY_KEY = "matrixInventory";

    private float scale;
    private int rotationID, rarityID, growingID;

    private final MatrixInventory matrixInventory = MatrixInventoryFactory.create();
    @Getter
    @Setter
    private String playerName = "";
    @Getter
    @Setter
    private CharacterType characterType = CharacterType.MEDIC;
    @Getter
    @Setter
    private int playerLevel;

    public EntityContainer(World p_i1582_1_) {
        super(p_i1582_1_);
        this.setSize(1F, 0.6F);
        this.isImmuneToFire = true;
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        getDataWatcher().addObject(rarityID = 31, EnumItemRarity.COMMON.ordinal());
        getDataWatcher().addObject(rotationID = 30, 0);
        getDataWatcher().addObject(growingID = 29, 0);
    }

    @Override
    public EntityAgeable createChild(EntityAgeable p_90011_1_) {
        return null;
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();
        if(isGrowing()) {
            if(scale < 1) {
                scale += 0.1F;
            }
        } else {
            scale = 1;
        }
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(1000D);
        getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.0000000001D);
    }

    public void setRarity(EnumItemRarity enumRarity) {
        getDataWatcher().updateObject(31, enumRarity.ordinal());
    }

    public void setRotation(float f) {
        getDataWatcher().updateObject(30, (int) f);
    }

    public void setGrowing(boolean b) {
        getDataWatcher().updateObject(29, b ? 1 : 0);
    }

    public float getRotation() {
        return getDataWatcher().getWatchableObjectInt(rotationID);
    }

    public boolean isGrowing() {
        return getDataWatcher().getWatchableObjectInt(growingID) != 0;
    }

    public EnumItemRarity getRarity() {
        return EnumItemRarity.values()[getDataWatcher().getWatchableObjectInt(rarityID)];
    }

    public float getScale() {
        return scale;
    }

    @Override
    protected void jump() {}

    @Override
    public void knockBack(Entity p_70653_1_, float p_70653_2_, double p_70653_3_, double p_70653_5_) {}

    @Override
    protected String getHurtSound() {
        return null;
    }

    @Override
    protected String getDeathSound() {
        return null;
    }

    @Override
    protected String getLivingSound() {
        return null;
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound p_70037_1_) {
        super.readEntityFromNBT(p_70037_1_);
        if(p_70037_1_.hasKey("playerName"))
            this.playerName = p_70037_1_.getString("playerName");
        if(p_70037_1_.hasKey("scale"))
            scale = p_70037_1_.getFloat("scale");

        if(p_70037_1_.hasKey("rotation"))
            setRotation(p_70037_1_.getFloat("rotation"));
        if(p_70037_1_.hasKey("rarity"))
            setRarity(EnumItemRarity.values()[p_70037_1_.getInteger("rarity")]);

        matrixInventory.readFromNBT(p_70037_1_.getCompoundTag(MATRIX_INVENTORY_KEY), new NBTLoader(p_70037_1_));
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound p_70014_1_) {
        super.writeEntityToNBT(p_70014_1_);
        p_70014_1_.setString("playerName", this.playerName);
        p_70014_1_.setFloat("scale", scale);

        p_70014_1_.setFloat("rotation", getRotation());
        p_70014_1_.setInteger("rarity", getRarity().ordinal());

        NBTTagCompound compoundTag = new NBTTagCompound();
        this.matrixInventory.writeToNBT(compoundTag, new NBTLoader(p_70014_1_));
        p_70014_1_.setTag(MATRIX_INVENTORY_KEY, compoundTag);
    }

    @Override
    public MatrixInventory getMatrixInventory() {
        return matrixInventory;
    }

    @Override
    public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
        return entityPlayer.getDistanceToEntity(this) < 4D;
    }
}
