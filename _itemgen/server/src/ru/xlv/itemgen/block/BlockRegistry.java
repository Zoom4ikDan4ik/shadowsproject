package ru.xlv.itemgen.block;

import cpw.mods.fml.common.registry.GameRegistry;
import lombok.SneakyThrows;
import net.minecraft.block.Block;

import java.lang.reflect.Field;

public class BlockRegistry {

    public static final BlockLootSpawn LOOT_SPAWN = new BlockLootSpawn("lootSpawn", 0);
    public static final BlockLootSpawn LOOT_SPAWN1 = new BlockLootSpawn("lootSpawn1", 1);
    public static final BlockLootSpawn LOOT_SPAWN2 = new BlockLootSpawn("lootSpawn2", 2);
    public static final BlockLootSpawn LOOT_SPAWN3 = new BlockLootSpawn("lootSpawn3", 3);
    public static final BlockLootSpawn LOOT_SPAWN4 = new BlockLootSpawn("lootSpawn4", 4);
    public static final BlockLootSpawn LOOT_SPAWN5 = new BlockLootSpawn("lootSpawn5", 5);
    public static final BlockLootSpawn LOOT_SPAWN6 = new BlockLootSpawn("lootSpawn6", 6);
    public static final BlockLootSpawn LOOT_SPAWN7 = new BlockLootSpawn("lootSpawn7", 7);
    public static final BlockLootSpawn LOOT_SPAWN8 = new BlockLootSpawn("lootSpawn8", 8);
    public static final BlockLootSpawn LOOT_SPAWN9 = new BlockLootSpawn("lootSpawn9", 9);
    public static final BlockLootSpawn LOOT_SPAWN10 = new BlockLootSpawn("lootSpawn10", 10);
    public static final BlockLootSpawn LOOT_SPAWN11 = new BlockLootSpawn("lootSpawn11", 11);
    public static final BlockLootSpawn LOOT_SPAWN12 = new BlockLootSpawn("lootSpawn12", 12);
    public static final BlockLootSpawn LOOT_SPAWN13 = new BlockLootSpawn("lootSpawn13", 13);
    public static final BlockLootSpawn LOOT_SPAWN14 = new BlockLootSpawn("lootSpawn14", 14);
    public static final BlockLootSpawn LOOT_SPAWN15 = new BlockLootSpawn("lootSpawn15", 15);
    public static final BlockLootSpawn LOOT_SPAWN16 = new BlockLootSpawn("lootSpawn16", 16);
    public static final BlockLootSpawn LOOT_SPAWN17 = new BlockLootSpawn("lootSpawn17", 17);
    public static final BlockLootSpawn LOOT_SPAWN18 = new BlockLootSpawn("lootSpawn18", 18);
    public static final BlockLootSpawn LOOT_SPAWN19 = new BlockLootSpawn("lootSpawn19", 19);
    public static final BlockLootSpawn LOOT_SPAWN20 = new BlockLootSpawn("lootSpawn20", 20);
    public static final BlockLootSpawn LOOT_SPAWN21 = new BlockLootSpawn("lootSpawn21", 21);
    public static final BlockLootSpawn LOOT_SPAWN22 = new BlockLootSpawn("lootSpawn22", 22);
    public static final BlockLootSpawn LOOT_SPAWN23 = new BlockLootSpawn("lootSpawn23", 23);
    public static final BlockLootSpawn LOOT_SPAWN24 = new BlockLootSpawn("lootSpawn24", 24);
    public static final BlockLootSpawn LOOT_SPAWN25 = new BlockLootSpawn("lootSpawn25", 25);
    public static final BlockLootSpawn LOOT_SPAWN26 = new BlockLootSpawn("lootSpawn26", 26);
    public static final BlockLootSpawn LOOT_SPAWN27 = new BlockLootSpawn("lootSpawn27", 27);
    public static final BlockLootSpawn LOOT_SPAWN28 = new BlockLootSpawn("lootSpawn28", 28);
    public static final BlockLootSpawn LOOT_SPAWN29 = new BlockLootSpawn("lootSpawn29", 29);
    public static final BlockLootSpawn LOOT_SPAWN30 = new BlockLootSpawn("lootSpawn30", 30);
    public static final BlockLootSpawn LOOT_SPAWN31 = new BlockLootSpawn("lootSpawn31", 31);
    public static final BlockLootSpawn LOOT_SPAWN32 = new BlockLootSpawn("lootSpawn32", 32);
    public static final BlockLootSpawn LOOT_SPAWN33 = new BlockLootSpawn("lootSpawn33", 33);
    public static final BlockLootSpawn LOOT_SPAWN34 = new BlockLootSpawn("lootSpawn34", 34);
    public static final BlockLootSpawn LOOT_SPAWN35 = new BlockLootSpawn("lootSpawn35", 35);
    public static final BlockLootSpawn LOOT_SPAWN36 = new BlockLootSpawn("lootSpawn36", 36);
    public static final BlockLootSpawn LOOT_SPAWN37 = new BlockLootSpawn("lootSpawn37", 37);
    public static final BlockLootSpawn LOOT_SPAWN38 = new BlockLootSpawn("lootSpawn38", 38);
    public static final BlockLootSpawn LOOT_SPAWN39 = new BlockLootSpawn("lootSpawn39", 39);
    public static final BlockLootSpawn LOOT_SPAWN40 = new BlockLootSpawn("lootSpawn40", 40);
    public static final BlockLootSpawn LOOT_SPAWN41 = new BlockLootSpawn("lootSpawn41", 41);
    public static final BlockLootSpawn LOOT_SPAWN42 = new BlockLootSpawn("lootSpawn42", 42);
    public static final BlockLootSpawn LOOT_SPAWN43 = new BlockLootSpawn("lootSpawn43", 43);
    public static final BlockLootSpawn LOOT_SPAWN44 = new BlockLootSpawn("lootSpawn44", 44);
    public static final BlockLootSpawn LOOT_SPAWN45 = new BlockLootSpawn("lootSpawn45", 45);
    public static final BlockLootSpawn LOOT_SPAWN46 = new BlockLootSpawn("lootSpawn46", 46);
    public static final BlockLootSpawn LOOT_SPAWN47 = new BlockLootSpawn("lootSpawn47", 47);
    public static final BlockLootSpawn LOOT_SPAWN48 = new BlockLootSpawn("lootSpawn48", 48);
    public static final BlockLootSpawn LOOT_SPAWN49 = new BlockLootSpawn("lootSpawn49", 49);

    @SneakyThrows
    public static void register() {
        for (Field declaredField : BlockRegistry.class.getDeclaredFields()) {
            Object o = declaredField.get(null);
            register((Block) o);
        }
    }

    private static void register(Block block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
    }
}
