package ru.xlv.friend.handle.result;

import ru.xlv.friend.XlvsFriendMod;

public enum FriendRemoveResult {

    SUCCESS(XlvsFriendMod.INSTANCE.getLocalization().responseFriendRemoveSuccessMessage),
    NOT_FOUND(XlvsFriendMod.INSTANCE.getLocalization().responseFriendRemoveNotFoundMessage);

    private String responseMessage;

    FriendRemoveResult(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
