package ru.xlv.friend.database;

import ru.xlv.core.database.IDatabaseEventListener;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.common.FriendRelation;

public class DatabaseEventListener implements IDatabaseEventListener<FriendRelation> {
    @Override
    public void onInsert(FriendRelation object) {
        synchronized (XlvsFriendMod.INSTANCE.getFriendHandler().getRelations()) {
            if (XlvsFriendMod.INSTANCE.getFriendHandler().getRelation(object.getInitiator(), object.getTarget()) == null) {
                XlvsFriendMod.INSTANCE.getFriendHandler().getRelations().add(object);
            }
        }
    }

    @Override
    public void onUpdate(FriendRelation object) {
        synchronized (XlvsFriendMod.INSTANCE.getFriendHandler().getRelations()) {
            FriendRelation friendRelation = XlvsFriendMod.INSTANCE.getFriendHandler().getRelation(object.getInitiator(), object.getTarget());
            if (friendRelation != null) {
                friendRelation.setState(object.getState());
            }
        }
    }

    @Override
    public void onDelete(FriendRelation object) {
        XlvsFriendMod.INSTANCE.getFriendHandler().getRelations().removeIf(friendRelation ->
                friendRelation.getInitiator().equals(object.getInitiator()) && friendRelation.getTarget().equals(object.getTarget()));
    }
}
