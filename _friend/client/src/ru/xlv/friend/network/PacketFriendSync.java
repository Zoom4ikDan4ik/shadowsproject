package ru.xlv.friend.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketCallbackEffective;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.friend.common.FriendIO;
import ru.xlv.friend.common.FriendRelation;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public class PacketFriendSync implements IPacketCallbackEffective<PacketFriendSync.Result> {

    @Getter
    public static class Result {
        private boolean success;
        private String responseMessage;
        private List<FriendRelation> friendRelations;
    }

    private Result result;

    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        result = new Result();
        result.success = bbis.readBoolean();
        if(result.success) {
            result.friendRelations = FriendIO.readFriendRelationList(bbis);
        } else {
            result.responseMessage = bbis.readUTF();
        }
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {}

    @Nullable
    @Override
    public Result getResult() {
        return result;
    }
}
