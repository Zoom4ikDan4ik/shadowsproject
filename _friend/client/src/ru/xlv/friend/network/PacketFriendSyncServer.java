package ru.xlv.friend.network;

import lombok.NoArgsConstructor;
import ru.xlv.core.common.network.IPacketIn;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.friend.XlvsFriendMod;
import ru.xlv.friend.common.FriendIO;

import java.io.IOException;

@NoArgsConstructor
public class PacketFriendSyncServer implements IPacketIn {
    @Override
    public void read(ByteBufInputStream bbis) throws IOException {
        XlvsFriendMod.INSTANCE.getFriendHandler().sync(FriendIO.readFriendRelationList(bbis));
    }
}
