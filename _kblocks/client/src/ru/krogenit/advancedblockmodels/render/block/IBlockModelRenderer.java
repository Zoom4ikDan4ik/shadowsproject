package ru.krogenit.advancedblockmodels.render.block;

import net.minecraft.util.AxisAlignedBB;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.pbr.IPBR;

public interface IBlockModelRenderer {
    void renderTileEntity(TEModelClient part);
    void renderTileEntityPost(TEModelClient part);
    void renderPreview(IPBR shader);
    void renderInventory();
    void renderAsEntity();
    void renderAsEntityPost();
    void render3rdPerson();
    void render3rdPersonPost();
    void renderFirstPerson();
    String getModelName();
    boolean isModelsLoaded();
    AxisAlignedBB getAABB(Vector3f rotation, Vector3f scale);
    AxisAlignedBB getRenderAABB(TEModelClient part);
    Vector3f getScale();
    Vector3f getTranslate();
    Model getModel();
    BlockModel.ModelRenderType getType();
    boolean isPostRender();
}
