package ru.krogenit.advancedblockmodels.render.block.add;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModelClient;
import ru.krogenit.client.renderer.IReloadableRenderer;
import ru.krogenit.client.renderer.ReloadableRendererManager;
import ru.krogenit.dds_loader.TextureDDS;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;

public class BlockPropsRenderer extends AbstractBlockModelRenderer implements IReloadableRenderer {

    private TextureDDS textureNormal;
    private TextureDDS textureDiffuse;
    private TextureDDS textureSpecular;
    private TextureDDS textureEmission;
    private TextureDDS textureGloss;

    private final boolean hasEmission;
    private final boolean hasGloss;
    private final float emissionPower;
    private final boolean hasSpecular;
    private final boolean hasNormal;
    private final String name;

    @Deprecated
    public BlockPropsRenderer(String modelName, String name, BlockModel.ModelRenderType renderType, boolean hasEmission, float emissionPower, boolean hasGloss) {
        super(renderType, new Model(new ResourceLocation(modelName)));
        this.name = name;
        this.hasEmission = hasEmission;
        this.emissionPower = emissionPower;
        this.hasGloss = hasGloss;
        this.hasNormal = true;
        this.hasSpecular = true;
        this.textureDiffuse = new TextureDDS(new ResourceLocation("advancedblocks", "textures/props/" + name + "/Diffuse.dds"));
        this.textureNormal = new TextureDDS(new ResourceLocation("advancedblocks", "textures/props/" + name + "/Normal.dds"));
        this.textureSpecular = new TextureDDS(new ResourceLocation("advancedblocks", "textures/props/" + name + "/Specular.dds"));
        if(hasEmission) this.textureEmission = new TextureDDS(new ResourceLocation("advancedblocks", "textures/props/" + name + "/Emissive.dds"));
        if(hasGloss) this.textureGloss = new TextureDDS(new ResourceLocation("advancedblocks", "textures/props/" + name + "/Gloss.dds"));
//        ResourceLocationStateful.Builder builder = ResourceLocationStateful.Builder.builder().withDomain("advancedblocks").temporary();
//        this.textureDiffuse = builder.of("textures/props/" + name + "/Diffuse.dds");
//        this.textureNormal = builder.of("textures/props/" + name + "/Normal.dds");
//        this.textureSpecular = builder.of("textures/props/" + name + "/Specular.dds");
//        if(hasEmission) this.textureEmission = builder.of("textures/props/" + name + "/Emissive.dds");
//        if(hasGloss) this.textureGloss = builder.of("textures/props/" + name + "/Gloss.dds");
    }

    public BlockPropsRenderer(String name, BlockModel.ModelRenderType renderType, boolean hasNormal, boolean hasEmission, float emissionPower, boolean hasGloss, boolean hasSpecular) {
        super(renderType, new Model(new ResourceLocation("advancedblocks:models/" + name + ".obj")));
        this.name = name;
        this.hasEmission = hasEmission;
        this.emissionPower = emissionPower;
        this.hasGloss = hasGloss;
        this.hasSpecular = hasSpecular;
        this.hasNormal = hasNormal;
        reload();
        ReloadableRendererManager.registerRenderer(this);
    }

    public BlockPropsRenderer(String name, BlockModel.ModelRenderType renderType, boolean hasEmission, float emissionPower, boolean hasGloss, boolean hasSpecular) {
        super(renderType, new Model(new ResourceLocation("advancedblocks:models/" + name + ".obj")));
        this.name = name;
        this.hasEmission = hasEmission;
        this.emissionPower = emissionPower;
        this.hasGloss = hasGloss;
        this.hasSpecular = hasSpecular;
        this.hasNormal = true;
        reload();
        ReloadableRendererManager.registerRenderer(this);
    }

    @Deprecated
    public BlockPropsRenderer(String modelName, String name, BlockModel.ModelRenderType renderType, boolean hasEmission, float emissionPower) {
        this(modelName, name, renderType, hasEmission, emissionPower, true);
    }

    @Override
    public void reload() {
        String textureName = name.substring(name.indexOf("/"));
        this.textureDiffuse = new TextureDDS(new ResourceLocation("advancedblocks", "textures" + textureName + "_d."+mc.gameSettings.textureSize+".dds"));
        if(hasNormal) this.textureNormal = new TextureDDS(new ResourceLocation("advancedblocks","textures" + textureName + "_n."+mc.gameSettings.textureSize+".dds"));
        if(hasSpecular) this.textureSpecular = new TextureDDS(new ResourceLocation("advancedblocks", "textures" + textureName + "_s."+mc.gameSettings.textureSize+".dds"));
        if(hasEmission) this.textureEmission = new TextureDDS(new ResourceLocation("advancedblocks", "textures" + textureName + "_e."+mc.gameSettings.textureSize+".dds"));
        if(hasGloss) this.textureGloss = new TextureDDS(new ResourceLocation("advancedblocks", "textures" + textureName + "_g."+mc.gameSettings.textureSize+".dds"));
//        ResourceLocationStateful.Builder builder = ResourceLocationStateful.Builder.builder().withDomain("advancedblocks").temporary();
//        this.textureDiffuse = builder.of("textures" + textureName + "_d."+mc.gameSettings.textureSize+".dds");
//        if(hasNormal) this.textureNormal = builder.of("textures" + textureName + "_n."+mc.gameSettings.textureSize+".dds");
//        if(hasSpecular) this.textureSpecular = builder.of("textures" + textureName + "_s."+mc.gameSettings.textureSize+".dds");
//        if(hasEmission) this.textureEmission = builder.of("textures" + textureName + "_e."+mc.gameSettings.textureSize+".dds");
//        if(hasGloss) this.textureGloss = builder.of("textures" + textureName + "_g."+mc.gameSettings.textureSize+".dds");
    }

    private void setShaderState(IPBR shader) {
        TextureLoaderDDS.bindTexture(textureDiffuse);

        if(hasNormal) {
            TextureLoaderDDS.bindNormalMap(textureNormal, shader);
        }

        if(hasSpecular) {
            TextureLoaderDDS.bindSpecularMap(textureSpecular, shader);
        }

        if(hasGloss) {
            TextureLoaderDDS.bindGlossMap(textureGloss, shader);
        }

        if(hasEmission) {
            TextureLoaderDDS.bindEmissionMap(textureEmission, shader, emissionPower);
        }
    }

    private void endShaderState(IPBR shader) {
        if(hasEmission) shader.setEmissionMapping(false);
        if(hasSpecular) shader.setSpecularMapping(false);
        if(hasNormal) shader.setNormalMapping(false);
        if(hasGloss) shader.setGlossMapping(false);
        KrogenitShaders.finishCurrentShader();
        TextureLoaderDDS.unbind();
    }

    @Override
    public void renderPost(TEModelClient part) {

    }

    @Override
    public void render(TEModelClient part) {
        IPBR shader = KrogenitShaders.getCurrentPBRShader(false);
        setShaderState(shader);
        model.render(shader);
        endShaderState(shader);
    }
}
