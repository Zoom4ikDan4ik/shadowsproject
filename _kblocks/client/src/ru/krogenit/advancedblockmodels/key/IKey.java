package ru.krogenit.advancedblockmodels.key;

import net.minecraft.client.settings.KeyBinding;

public abstract class IKey {
	private KeyBinding key;

	IKey(KeyBinding key) {
		this.key = key;
	}

	public KeyBinding getKeyBinding() {
		return this.key;
	}

	public abstract void keyDown();

	public abstract void keyUp();
}
