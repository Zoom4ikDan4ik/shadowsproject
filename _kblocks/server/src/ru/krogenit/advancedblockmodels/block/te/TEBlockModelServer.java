package ru.krogenit.advancedblockmodels.block.te;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel;
import ru.krogenit.advancedblockmodels.block.BlockModelsServer;
import ru.krogenit.advancedblockmodels.block.tileentity.TEBlockModel;
import ru.krogenit.advancedblockmodels.block.tileentity.TEModel;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.List;

public class TEBlockModelServer extends TEBlockModel {

    public void addNewModelOnServer(String model, Vector3f pos, Vector3f rot, Vector3f scale, AxisAlignedBB aabb, BlockModel.ModelCollideType type) {
        parts.add(new TEModelServer(model, this, pos, rot, scale, aabb, type));
    }

    public void removeHelpBlocks(World w, int basex, int basey, int basez, float newX, float newY, float newZ, TEModelServer part) {
        AxisAlignedBB aabb = part.getAabb();
        boolean needHelp = aabb != null;

        if (needHelp) {
            int minx = MathHelper.floor_double(aabb.minX + newX);
            int miny = MathHelper.floor_double(aabb.minY + newY);
            int minz = MathHelper.floor_double(aabb.minZ + newZ);
            int maxx = MathHelper.floor_double(aabb.maxX + newX) + 1;
            int maxy = MathHelper.floor_double(aabb.maxY + newY) + 1;
            int maxz = MathHelper.floor_double(aabb.maxZ + newZ) + 1;

            for (int x = minx; x < maxx; x++)
                for (int y = miny; y < maxy; y++)
                    for (int z = minz; z < maxz; z++) {
                        if (x != basex || y != basey || z != basez) {
                            removeTEHelp(w, part, x, y, z);
                        }
                    }
        }
    }

    protected void removeTEHelp(World w, TEModelServer teModel, int x, int y, int z) {
        TileEntity tile = w.getTileEntity(x, y, z);

        if (tile instanceof TEBlockModel) {
            TEBlockModel te = (TEBlockModel) tile;
            List<TEModel> child_parts = te.getChildParts();
            child_parts.remove(teModel);
        }
    }

    @Override
    public Packet getDescriptionPacket() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setByteArray("M", writeDescriptionBytes());
        return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 0, tag);
    }

    private byte[] writeDescriptionBytes() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream data = new DataOutputStream(bos);
        try {
            int count = parts.size();
            data.writeInt(count);
            for (TEModel part : parts) {
                ((TEModelServer)part).writePacketData(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bos.toByteArray();
    }

    @Override
    public void readFromNBT(NBTTagCompound tag) {
        super.readFromNBT(tag);
        parts.clear();
        NBTTagList l = tag.getTagList("TEModels", 10);
        if (l == null) return;

        World w = MinecraftServer.getServer().worldServers[0];

        for (int k = 0; k < l.tagCount(); k++) {
            TEModelServer p = new TEModelServer(l.removeTag(k), this);
            String modelName = p.getModelName();
            if (!BlockModelsServer.hasModelInRegistry(modelName)) {
                System.out.println("Модель " + modelName + " более не зарегистрирована и была удалена из мира");
                synchronized (partsToRemove) {
                    partsToRemove.add(p);
                }
                continue;
            }

            Vector3f position = p.getPosition();
            placeHelpBlocks(w, xCoord, yCoord, zCoord, xCoord + position.x, yCoord + position.y, zCoord + position.z, p, HelpPlaceType.LOAD);
            parts.add(p);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound tag) {
        super.writeToNBT(tag);
        NBTTagList l = new NBTTagList();
        for (TEModel p : parts)
            l.appendTag(((TEModelServer)p).writeToNBT());
        tag.setTag("TEModels", l);
    }
}
