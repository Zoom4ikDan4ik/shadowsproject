package ru.krogenit.advancedblockmodels.block.tileentity;

import lombok.Getter;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.advancedblockmodels.block.BlockModel.ModelCollideType;

@Getter
public class TEModel {
	protected Vector3f rotation, position, scale;
	protected String modelName;
	protected TEBlockModel te;
	protected final AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(Float.MAX_VALUE,Float.MAX_VALUE,Float.MAX_VALUE,Float.MAX_VALUE,Float.MAX_VALUE,Float.MAX_VALUE);
	protected boolean aabbCalculated;
	protected final AxisAlignedBB aabbHelper = AxisAlignedBB.getBoundingBox(0,0,0,0,0,0);
	protected ModelCollideType type;

	protected boolean hasAABB;

	protected int xCoord, yCoord, zCoord;
	protected World worldObj;

	public AxisAlignedBB getBoundingBoxForCollision(int x, int y, int z) {
		if(aabbCalculated) {
			aabbHelper.minX = x + aabb.minX + this.position.x;
			aabbHelper.minY = y + aabb.minY + this.position.y;
			aabbHelper.minZ = z + aabb.minZ + this.position.z;
			aabbHelper.maxX = x + aabb.maxX + this.position.x;
			aabbHelper.maxY = y + aabb.maxY + this.position.y;
			aabbHelper.maxZ = z + aabb.maxZ + this.position.z;
		}

		return aabbHelper;
	}

	public AxisAlignedBB getBoundingBox(int x, int y, int z) {
		if(aabbCalculated) {
			aabbHelper.minX = x + aabb.minX + this.position.x;
			aabbHelper.minY = y + aabb.minY + this.position.y;
			aabbHelper.minZ = z + aabb.minZ + this.position.z;
			aabbHelper.maxX = x + aabb.maxX + this.position.x;
			aabbHelper.maxY = y + aabb.maxY + this.position.y;
			aabbHelper.maxZ = z + aabb.maxZ + this.position.z;
		}

		return aabbHelper;
	}

	public AxisAlignedBB getBoundingBoxLocal() {
		if(aabbCalculated) {
			aabbHelper.minX = aabb.minX + this.position.x;
			aabbHelper.minY = aabb.minY + this.position.y;
			aabbHelper.minZ = aabb.minZ + this.position.z;
			aabbHelper.maxX = aabb.maxX + this.position.x;
			aabbHelper.maxY = aabb.maxY + this.position.y;
			aabbHelper.maxZ = aabb.maxZ + this.position.z;
		}

		return aabbHelper;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TEModel) {
			TEModel part = (TEModel) obj;
			return part.position.equals(position) && part.te.xCoord == te.xCoord && part.te.yCoord == te.yCoord && part.te.zCoord == te.zCoord;
		}

		return false;
	}
}