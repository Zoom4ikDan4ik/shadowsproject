package ru.xlv.collideblocks.common.tile;

import lombok.NoArgsConstructor;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

@NoArgsConstructor
public class TileEntityBlockCollide extends TileEntity {

	public double minX = 0, maxX = 1, minZ = 0, maxZ = 1, minY = 0, maxY = 1;

	public void readFromNBT(NBTTagCompound tag) {
		super.readFromNBT(tag);
		safeRead(tag);
	}

	public void safeRead(NBTTagCompound tag) {
		minX = tag.getDouble("minX");
		maxX = tag.getDouble("maxX");
		minZ = tag.getDouble("minZ");
		maxZ = tag.getDouble("maxZ");
		minY = tag.getDouble("minY");
		maxY = tag.getDouble("maxY");
	}

	public void writeToNBT(NBTTagCompound tag) {
		super.writeToNBT(tag);
		safeWrite(tag);
	}

	public void safeWrite(NBTTagCompound tag) {
		tag.setDouble("minX", minX);
		tag.setDouble("maxX", maxX);
		tag.setDouble("minZ", minZ);
		tag.setDouble("maxZ", maxZ);
		tag.setDouble("maxY", maxY);
		tag.setDouble("minY", minY);
	}

	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound nbttagcompound = new NBTTagCompound();
		writeToNBT(nbttagcompound);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 5, nbttagcompound);
	}

	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet) {
		readFromNBT(packet.func_148857_g());
	}
}
