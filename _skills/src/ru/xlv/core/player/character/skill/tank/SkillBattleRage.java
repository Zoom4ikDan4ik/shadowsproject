package ru.xlv.core.player.character.skill.tank;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.Set;

public class SkillBattleRage extends ActivableSkill {

    private final double healthAdd;
    private final double healthAddPerPlayer;
    private final double radius;

    public SkillBattleRage(SkillType skillType) {
        super(skillType);
        healthAdd = skillType.getCustomParam("hp_add_self_value", double.class);
        healthAddPerPlayer = skillType.getCustomParam("hp_add_self_for_each_enemy_value", double.class);
        radius = skillType.getCustomParam("range", double.class);
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        Set<EntityPlayer> entityPlayerSet = SkillUtils.getEnemyPlayersAround(serverPlayer.getEntityPlayer(), radius);
        if (!entityPlayerSet.isEmpty()) {
            serverPlayer.getEntityPlayer().heal((float) (healthAdd + healthAddPerPlayer * entityPlayerSet.size()));
        }
    }

    @Nonnull
    @Override
    public SkillBattleRage clone() {
        return new SkillBattleRage(getSkillType());
    }
}
