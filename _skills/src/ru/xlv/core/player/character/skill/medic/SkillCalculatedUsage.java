package ru.xlv.core.player.character.skill.medic;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.event.PlayerConsumeManaEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillCalculatedUsage extends Skill {

    private final double manaMod;

    public SkillCalculatedUsage(SkillType skillType) {
        super(skillType);
        manaMod = 1D + skillType.getCustomParam("skill_usage_cost_add_value", double.class) / 100D;
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        XlvsCoreCommon.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(PlayerConsumeManaEvent event) {
        event.setAmount(event.getAmount() * manaMod);
    }

    @Nonnull
    @Override
    public SkillCalculatedUsage clone() {
        return new SkillCalculatedUsage(getSkillType());
    }
}
