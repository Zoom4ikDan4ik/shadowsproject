package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Random;

public class SkillReaper extends Skill {

    private final double skillCount;

    private ServerPlayer serverPlayer;

    public SkillReaper(SkillType skillType) {
        super(skillType);
        skillCount = skillType.getCustomParam("skill_reset_count", double.class);
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        this.serverPlayer = serverPlayer;
        MinecraftForge.EVENT_BUS.register(this);
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        MinecraftForge.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(LivingDeathEvent event) {
        if(event.source.getSourceOfDamage() instanceof EntityPlayer) {
            if (event.source.getSourceOfDamage() == serverPlayer.getEntityPlayer()) {
                Random rand = serverPlayer.getEntityPlayer().worldObj.rand;
                int i = rand.nextInt((int) skillCount);
                List<ActivableSkill> selectedActiveSkills = serverPlayer.getSelectedCharacter().getSkillManager().getSelectedActiveSkills();
                for (int j = 0; j < i; j++) {
                    ActivableSkill activableSkill = selectedActiveSkills.get(rand.nextInt(selectedActiveSkills.size()));
                    if(activableSkill != null) {
                        activableSkill.breakCooldown();
                        continue;
                    }
                    i--;
                }
            }
        }
    }

    @Nonnull
    @Override
    public SkillReaper clone() {
        return new SkillReaper(getSkillType());
    }
}
