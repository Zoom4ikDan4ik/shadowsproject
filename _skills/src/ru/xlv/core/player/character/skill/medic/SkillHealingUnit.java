package ru.xlv.core.player.character.skill.medic;

import net.minecraft.entity.SharedMonsterAttributes;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.entity.EntityExecutableOnUpdate;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.group.XlvsGroupMod;
import ru.xlv.group.handle.GroupServer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class SkillHealingUnit extends ActivableSkill {

    private final double unitHealth;

    private final ScheduledConsumableTask<EntityExecutableOnUpdate> scheduledTask;

    public SkillHealingUnit(SkillType skillType) {
        super(skillType);
        unitHealth = skillType.getCustomParam("durability", double.class);
        double healthRegen = skillType.getCustomParam("hp_add_ally_all_value", double.class);
        long healPeriod = (long) (skillType.getCustomParam("delta_time", double.class) * 1000);
        double radius = skillType.getCustomParam("hp_add_ally_all_range", double.class);
        scheduledTask = new ScheduledConsumableTask<>(healPeriod, entityExecutableOnUpdate -> {
            GroupServer playerGroup = XlvsGroupMod.INSTANCE.getGroupHandler().getPlayerGroup(entityExecutableOnUpdate.getOwnerPlayerName());
            List<ServerPlayer> players = new ArrayList<>();
            if (playerGroup != null) {
                players = playerGroup.getPlayers();
            } else {
                players.add(XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityExecutableOnUpdate.getOwnerPlayerName()));
            }
            for (ServerPlayer player : players) {
                if(!player.isOnline()) continue;
                if(player.getEntityPlayer().getDistance(entityExecutableOnUpdate.posX, entityExecutableOnUpdate.posY, entityExecutableOnUpdate.posZ) >= radius) continue;
                player.getEntityPlayer().heal((float) healthRegen);
            }
        });
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        EntityExecutableOnUpdate entityExecutableOnUpdate = new EntityExecutableOnUpdate(serverPlayer.getEntityPlayer().worldObj, serverPlayer.getPlayerName(), scheduledTask);
        entityExecutableOnUpdate.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.maxHealth).setBaseValue(unitHealth);
        entityExecutableOnUpdate.setPosition(serverPlayer.getEntityPlayer().posX, serverPlayer.getEntityPlayer().posY, serverPlayer.getEntityPlayer().posZ);
        serverPlayer.getEntityPlayer().worldObj.spawnEntityInWorld(entityExecutableOnUpdate);
    }

    @Nonnull
    @Override
    public SkillHealingUnit clone() {
        return new SkillHealingUnit(getSkillType());
    }
}
