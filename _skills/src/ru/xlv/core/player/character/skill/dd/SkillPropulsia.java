package ru.xlv.core.player.character.skill.dd;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillPropulsia extends ActivableSkill {

    private final double speedMod;

    private ServerPlayer serverPlayer;

    public SkillPropulsia(SkillType skillType) {
        super(skillType);
        speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100D;
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(getSkillType().getDuration())
                .build());
        this.serverPlayer = serverPlayer;
        MinecraftForge.EVENT_BUS.register(this);
    }

    @Override
    protected void onDeactivated(ServerPlayer serverPlayer) {
        super.onDeactivated(serverPlayer);
        MinecraftForge.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(LivingHurtEvent event) {
        if(event.entityLiving instanceof EntityPlayer && event.entityLiving.getCommandSenderName().equals(serverPlayer.getPlayerName())) {
            deactivateSkill(serverPlayer);
        }
    }

    @Nonnull
    @Override
    public SkillPropulsia clone() {
        return new SkillPropulsia(getSkillType());
    }
}
