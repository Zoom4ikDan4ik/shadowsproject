package ru.xlv.core.player.character.skill.medic;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledConsumableTask;
import ru.xlv.mochar.player.character.skill.ISkillUpdatable;
import ru.xlv.mochar.player.character.skill.Skill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;
import java.util.UUID;

public class SkillReactiveJerk extends Skill implements ISkillUpdatable {

    private static final UUID MOD_UUID = UUID.randomUUID();

    private final double jumpMod;

    private final ScheduledConsumableTask<ServerPlayer> scheduledConsumableTask;

    private EntityPlayer entityPlayer;

    public SkillReactiveJerk(SkillType skillType) {
        super(skillType);
        double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100;
        CharacterAttributeMod characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(1000L)
                .uuid(MOD_UUID)
                .build();
        scheduledConsumableTask = new ScheduledConsumableTask<>(1000L, serverPlayer -> {
            serverPlayer.setDisableBlockMovingTimer((int) getSkillType().getDuration());
            serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod, true);
        });
        jumpMod = 1D + skillType.getCustomParam("jump_add_self_value", double.class) / 100;
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        scheduledConsumableTask.update(serverPlayer);
    }

    @Override
    public void onSelected(ServerPlayer serverPlayer) {
        super.onSelected(serverPlayer);
        MinecraftForge.EVENT_BUS.register(this);
        entityPlayer = serverPlayer.getEntityPlayer();
    }

    @Override
    public void onDeselected(ServerPlayer serverPlayer) {
        super.onDeselected(serverPlayer);
        MinecraftForge.EVENT_BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(LivingEvent.LivingJumpEvent event) {
        if(event.entityLiving == entityPlayer) {
            event.entityLiving.motionY *= jumpMod;
        }
    }

    @Nonnull
    @Override
    public SkillReactiveJerk clone() {
        return new SkillReactiveJerk(getSkillType());
    }
}
