package ru.xlv.core.player.character.skill.dd;

import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.player.character.skill.SkillUtils;
import ru.xlv.core.util.DamageUtils;
import ru.xlv.mochar.player.character.skill.ActivableSkill;
import ru.xlv.mochar.player.character.skill.SkillType;

import javax.annotation.Nonnull;

public class SkillInstantDissection extends ActivableSkill {

    private final CharacterAttributeMod characterAttributeMod;

    private final double damage;

    public SkillInstantDissection(SkillType skillType) {
        super(skillType);
        damage = 1D + skillType.getCustomParam("outcome_cut_damage_value", double.class) / 100D;
        double speedMod = 1D + skillType.getCustomParam("speed_add_self_value", double.class) / 100;
        characterAttributeMod = CharacterAttributeMod.builder()
                .attributeType(CharacterAttributeType.MOVE_SPEED)
                .valueMod(speedMod)
                .period(skillType.getDuration())
                .build();
    }

    @Override
    public void onActivate(ServerPlayer serverPlayer) {
        serverPlayer.getSelectedCharacter().addAttributeMod(characterAttributeMod);
    }

    @Override
    public void update(ServerPlayer serverPlayer) {
        super.update(serverPlayer);
        if(isActive()) {
            SkillUtils.getEnemyServerPlayersAround(serverPlayer, 0.5D)
                    .forEach(serverPlayer1 -> DamageUtils.damageEntity(serverPlayer.getEntityPlayer(), serverPlayer1.getEntityPlayer(), CharacterAttributeType.CUT_DAMAGE, damage));
        }
    }

    @Nonnull
    @Override
    public SkillInstantDissection clone() {
        return new SkillInstantDissection(getSkillType());
    }
}
