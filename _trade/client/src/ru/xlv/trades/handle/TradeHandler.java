package ru.xlv.trades.handle;

import lombok.Getter;
import lombok.Setter;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.trade.common.SimpleTradeRequest;
import ru.xlv.trades.network.PacketTradeCancel;
import ru.xlv.trades.network.PacketTradeConfirm;
import ru.xlv.trades.network.PacketTradeInvite;

@Getter
@Setter
public class TradeHandler {

    private SimpleTradeRequest<String> currentTradeRequest;

    public SyncResultHandler<PacketTradeInvite.Result> invitePlayer(String playerName) {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketTradeInvite(playerName));
    }

    public void cancelTrade() {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketTradeCancel());
    }

    public void confirmTrade() {
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(new PacketTradeConfirm());
    }
}
