package ru.xlv.trades;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.trades.handle.TradeHandler;
import ru.xlv.trades.network.PacketTradeCancel;
import ru.xlv.trades.network.PacketTradeConfirm;
import ru.xlv.trades.network.PacketTradeInvite;
import ru.xlv.trades.network.PacketTradeSync;
import ru.xlv.trades.util.TradeLocalization;

import static ru.xlv.trades.XlvsTradeMod.MODID;

@Mod(
        name = MODID,
        modid = MODID,
        version = "1.0"
)
public class XlvsTradeMod {

    static final String MODID = "xlvstrades";

    @Mod.Instance(MODID)
    public static XlvsTradeMod INSTANCE;

    @Getter
    private final TradeHandler tradeHandler = new TradeHandler();

    @Getter
    private final TradeLocalization localization = new TradeLocalization();

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        localization.load();
        CommonUtils.registerFMLEvents(tradeHandler);

        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID,
                new PacketTradeCancel(),
                new PacketTradeConfirm(),
                new PacketTradeInvite(),
                new PacketTradeSync()
        );
    }
}
