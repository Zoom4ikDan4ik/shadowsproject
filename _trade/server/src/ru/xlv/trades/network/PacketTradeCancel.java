package ru.xlv.trades.network;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.trades.XlvsTradeMod;

import java.io.IOException;

@NoArgsConstructor
public class PacketTradeCancel implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        XlvsTradeMod.INSTANCE.getTradeHandler().cancelTrade(entityPlayer);
    }
}
