package ru.xlv.trades.handle;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.ScheduledTask;
import ru.xlv.trades.XlvsTradeMod;
import ru.xlv.trades.handle.result.TradeInviteResult;
import ru.xlv.trades.network.PacketTradeSync;

import java.util.ArrayList;
import java.util.List;

public class TradeHandler {

    private static final long COOLDOWN = 10000L;

    private final List<TradeRequest> activeTradeRequestList = new ArrayList<>();
    private final TObjectLongMap<EntityPlayer> cooldownMap = new TObjectLongHashMap<>();
    private final List<TradeInvite> tradeInviteList = new ArrayList<>();

    private final ScheduledTask updateScheduledTask = new ScheduledTask(200L, () -> {
        synchronized (activeTradeRequestList) {
            activeTradeRequestList.forEach(tradeRequest -> {
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(tradeRequest.getInitiator().getEntityPlayer(), new PacketTradeSync(tradeRequest));
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(tradeRequest.getTarget().getEntityPlayer(), new PacketTradeSync(tradeRequest));
            });
        }
    });
    private final ScheduledTask inviteRemovalScheduledTask = new ScheduledTask(500L, () -> {
        synchronized (tradeInviteList) {
            tradeInviteList.removeIf(tradeInvite -> System.currentTimeMillis() - tradeInvite.getCreationTimeMills() >= TradeInvite.REMOVAL_TIME_MILLS);
        }
    });

    @SubscribeEvent
    public void event(TickEvent.ServerTickEvent event) {
        updateScheduledTask.update();
        inviteRemovalScheduledTask.update();
    }

    public void confirmTrade(EntityPlayer entityPlayer) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer == null) {
            return;
        }
        TradeRequest tradeRequest = getTradeRequest(serverPlayer);
        if (tradeRequest != null) {
            if(serverPlayer == tradeRequest.getInitiator()) {
                tradeRequest.setInitiatorConfirmedTrade(!tradeRequest.isInitiatorConfirmedTrade());
            } else if(serverPlayer == tradeRequest.getTarget()) {
                tradeRequest.setTargetConfirmedTrade(tradeRequest.isTargetConfirmedTrade());
            }
            if(tradeRequest.isInitiatorConfirmedTrade() && tradeRequest.isTargetConfirmedTrade()) {
                tradeRequest.getInitiatorTradeItems().forEach(itemStack -> tradeRequest.getTarget().getSelectedCharacter().getMatrixInventory().addItem(itemStack));
                tradeRequest.getTargetTradeItems().forEach(itemStack -> tradeRequest.getInitiator().getSelectedCharacter().getMatrixInventory().addItem(itemStack));
            }
        }
    }

    public void cancelTrade(EntityPlayer entityPlayer) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer == null) {
            return;
        }
        TradeRequest tradeRequest = getTradeRequest(serverPlayer);
        if (tradeRequest != null) {
            boolean remove;
            synchronized (activeTradeRequestList) {
                remove = activeTradeRequestList.remove(tradeRequest);
            }
            if (remove) {
                if (XlvsCore.INSTANCE.getNotificationService() != null) {
                    boolean isInitiator = serverPlayer == tradeRequest.getInitiator();
                    ServerPlayer initiator = isInitiator ? tradeRequest.getInitiator() : tradeRequest.getTarget();
                    XlvsCore.INSTANCE.getNotificationService().sendNotification(initiator, XlvsTradeMod.INSTANCE.getLocalization().getYouCancelledTradeMessage());
                    XlvsCore.INSTANCE.getNotificationService().sendNotification(isInitiator ? tradeRequest.getTarget() : tradeRequest.getInitiator(),
                            XlvsTradeMod.INSTANCE.getLocalization().getFormatted(XlvsTradeMod.INSTANCE.getLocalization().getPlayerCancelledTradeMessage(), initiator.getPlayerName()));
                }
            }
        }
    }

    private TradeRequest getTradeRequest(ServerPlayer serverPlayer) {
        synchronized (activeTradeRequestList) {
            for (TradeRequest tradeRequest : activeTradeRequestList) {
                if(tradeRequest.getInitiator() == serverPlayer || tradeRequest.getTarget() == serverPlayer) {
                    return tradeRequest;
                }
            }
        }
        return null;
    }

    public TradeInviteResult handleInvite(EntityPlayer entityPlayer, String targetName) {
        if(System.currentTimeMillis() - cooldownMap.get(entityPlayer) < COOLDOWN) {
            return TradeInviteResult.COOLDOWN;
        }
        synchronized (cooldownMap) {
            cooldownMap.put(entityPlayer, System.currentTimeMillis());
        }
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if (serverPlayer == null) {
            return TradeInviteResult.UNKNOWN;
        }
        ServerPlayer targetPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(targetName);
        if (targetPlayer == null) {
            return TradeInviteResult.PLAYER_NOT_FOUND;
        }
        TradeInvite tradeInvite = getTradeInvite(serverPlayer);
        if (tradeInvite == null) {
            tradeInvite = new TradeInvite(serverPlayer, targetPlayer);
            synchronized (tradeInviteList) {
                tradeInviteList.add(tradeInvite);
            }
        } else {
            TradeRequest tradeRequest = new TradeRequest(tradeInvite.getInitiator(), tradeInvite.getTarget());
            synchronized (activeTradeRequestList) {
                activeTradeRequestList.add(tradeRequest);
            }
            synchronized (cooldownMap) {
                cooldownMap.remove(serverPlayer.getEntityPlayer());
                cooldownMap.remove(targetPlayer.getEntityPlayer());
            }
        }
        return TradeInviteResult.SUCCESS;
    }

    private TradeInvite getTradeInvite(ServerPlayer target) {
        synchronized (tradeInviteList) {
            for (TradeInvite tradeInvite : tradeInviteList) {
                if(tradeInvite.getTarget() == target) {
                    return tradeInvite;
                }
            }
        }
        return null;
    }
}
