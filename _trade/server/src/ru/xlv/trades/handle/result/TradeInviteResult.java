package ru.xlv.trades.handle.result;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.trades.XlvsTradeMod;

@Getter
@RequiredArgsConstructor
public enum TradeInviteResult {
    SUCCESS(XlvsTradeMod.INSTANCE.getLocalization().getResponseTradeInviteResultSuccessMessage()),
    PLAYER_NOT_FOUND(XlvsTradeMod.INSTANCE.getLocalization().getResponseTradeInviteResultPlayerNotFoundMessage()),
    UNKNOWN(XlvsTradeMod.INSTANCE.getLocalization().getResponseTradeInviteResultUnknownMessage()),
    COOLDOWN(XlvsTradeMod.INSTANCE.getLocalization().getResponseTradeInviteResultCooldownMessage());

    private final String responseMessage;
}
