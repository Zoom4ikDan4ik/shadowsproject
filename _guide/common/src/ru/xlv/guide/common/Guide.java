package ru.xlv.guide.common;

import lombok.Data;

@Data
public class Guide {

    private final String article;
    private final String text;
    private final String imageURL;
    private final GuideCategory category;

}
