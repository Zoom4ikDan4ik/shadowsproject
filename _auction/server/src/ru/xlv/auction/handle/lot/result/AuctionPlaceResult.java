package ru.xlv.auction.handle.lot.result;

import ru.xlv.auction.XlvsAuctionMod;

public enum AuctionPlaceResult {

    SUCCESS(XlvsAuctionMod.INSTANCE.getLocalization().responseSellSuccess),
    DATABASE_ERROR(XlvsAuctionMod.INSTANCE.getLocalization().responseSellDatabaseError),
    UNKNOWN_ERROR(XlvsAuctionMod.INSTANCE.getLocalization().responseSellUnknownError),
    NOT_FOUND_ITEM(XlvsAuctionMod.INSTANCE.getLocalization().responseSellNotFoundItem),
    NOT_ENOUGH_MONEY(XlvsAuctionMod.INSTANCE.getLocalization().responseSellNotEnoughMoney);

    private final String responseMessage;

    AuctionPlaceResult(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
