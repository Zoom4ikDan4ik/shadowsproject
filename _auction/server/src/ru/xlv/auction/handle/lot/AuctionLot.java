package ru.xlv.auction.handle.lot;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.minecraft.item.ItemStack;
import ru.xlv.auction.common.lot.SimpleAuctionLot;

import java.util.UUID;

@Getter
@ToString
public class AuctionLot extends SimpleAuctionLot {

    @Setter
    private String _id;

    public AuctionLot(String seller, ItemStack itemStack, int cost, long timestamp) {
        super(seller, itemStack, cost, timestamp);
    }

    public AuctionLot(UUID uuid, String seller, ItemStack itemStack, int cost, long timestamp) {
        super(uuid, seller, itemStack, cost, timestamp);
    }
}
