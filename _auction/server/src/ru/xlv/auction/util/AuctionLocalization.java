package ru.xlv.auction.util;

import ru.xlv.core.util.CoreLocalization;
import ru.xlv.core.common.util.config.Configurable;

import java.io.File;

@Configurable
public class AuctionLocalization extends CoreLocalization {

//    public String RESPONSE_BUY_UNKNOWN = "Неизвестная ошибка.";
//    public String RESPONSE_BUY_NOT_ENOUGH_MONEY = "Недостаточно диняг.";
//    public String RESPONSE_BUY_DATABASE_ERROR = "DB err.";
//    public String RESPONSE_BUY_NOT_ACTIVE_LOT = "lot isnt active.";
//    public String RESPONSE_BUY_SUCCESS = "OK.";
//    public String RESPONSE_BUY_BUYER_NOT_FOUND = "no buyer.";
//    public String RESPONSE_BUY_NO_TARGET_CATEGORY = "no category.";
//    public String RESPONSE_BUY_CANNOT_REMOVE_LOT = "cant remove lot.";
//    public String RESPONSE_BUY_SELLER_NOT_FOUND = "no seller.";
//    public String RESPONSE_BUY_WITHDRAW_ERROR = "withdraw err.";
//    public String RESPONSE_BUY_DEPOSIT_ERROR = "depo err.";

    public String responseSellUnknownError = "Неизвестная ошибка.";
    public String responseSellNotEnoughMoney = "Недостаточно диняг.";
    public String responseSellDatabaseError = "DB error.";
    public String responseSellSuccess = "OK.";
    public String responseSellNotFoundItem = "no item.";

    public String postAccountName = "Shadows Project";
    public String postDefaultArticle = "Auction notification.";
    public String postLotReturnMessage = "Hello, {0}. We inform you that your lot {1}, unfortunately, did not find its buyer and was returned to you with this mail.";
    public String postLotSoldMessage = "Hello, {0}. We inform you that your lot {1} has found a buyer for {2} credits. Credits attached to this message.";
    public String postLotBetReturnMessage = "Hello, {0}. We inform you that your bid for the lot {1} has been broken. Your bid has been returned with this message.";
    public String postLotBoughtMessage = "Hello, {0}. We inform you that your bid was the highest bid for the lot {1}. The bought item attached to this message.";

    public String responseBetSuccess = "responseBetSuccess";
    public String responseBetTooLowBid = "responseBetTooLowBid";
    public String responseBetPlayerNotFound = "responseBetPlayerNotFound";
    public String responseBetNotEnoughMoney = "responseBetNotEnoughMoney";
    public String responseBetDatabaseError = "responseBetDatabaseError";
    public String responseBetNotActiveLot = "responseBetNotActiveLot";
    public String responseBetLotNOtFound = "responseBetLotNOtFound";
    public String responseBetUnknown = "responseBetUnknown";

    @Override
    public File getConfigFile() {
        return new File("config/auction/localization.json");
    }
}
