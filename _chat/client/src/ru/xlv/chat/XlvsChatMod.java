package ru.xlv.chat;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.chat.handle.ChannelChatManager;
import ru.xlv.chat.network.PacketChatMessage;
import ru.xlv.core.XlvsCore;

import static ru.xlv.chat.XlvsChatMod.MODID;

@Mod(
        name = "XlvsChatMod",
        modid = MODID,
        version = "1.0"
)
public class XlvsChatMod {

    static final String MODID = "xlvschat";

    @Mod.Instance(MODID)
    public static XlvsChatMod INSTANCE;

    private ChannelChatManager channelChatManager;

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLInitializationEvent event) {
        channelChatManager = new ChannelChatManager();
        XlvsCore.INSTANCE.getPacketHandler().getPacketRegistry().register(MODID, new PacketChatMessage());
        MinecraftForge.EVENT_BUS.register(new EventListener());
    }

    public ChannelChatManager getChannelChatManager() {
        return channelChatManager;
    }
}
