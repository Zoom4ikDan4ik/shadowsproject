package ru.xlv.chat.handle;

import net.minecraft.util.ChatComponentText;
import ru.xlv.chat.common.ChatChannelType;
import ru.xlv.chat.network.PacketChatMessage;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.service.INotificationService;

public class ChatNotificationService implements INotificationService {
    @Override
    public void sendNotification(ServerPlayer serverPlayer, String message) {
        //todo change to notification system on the screen
        ChatComponentText chatComponentText = new ChatComponentText(message);
        String compressedMessage = ChatComponentText.Serializer.func_150696_a(chatComponentText);
        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketChatMessage(compressedMessage, ChatChannelType.LOCATION.ordinal()));
    }
}
