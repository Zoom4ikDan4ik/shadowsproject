package noppes.npcs.blocks.tiles;

import net.minecraft.util.AxisAlignedBB;

public class TileWallBanner extends TileBanner
{
    public AxisAlignedBB getRenderBoundingBox()
    {
        return AxisAlignedBB.getBoundingBox(this.xCoord, this.yCoord - 1, this.zCoord, this.xCoord + 1, this.yCoord + 1, this.zCoord + 1);
    }
}
