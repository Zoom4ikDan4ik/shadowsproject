package noppes.npcs.blocks.tiles;

public class TileCrate extends TileNpcContainer
{
    public String getName()
    {
        return "tile.npcCrate.name";
    }

    @Override
    public boolean hasCustomInventoryName() {
        return false;
    }
}
