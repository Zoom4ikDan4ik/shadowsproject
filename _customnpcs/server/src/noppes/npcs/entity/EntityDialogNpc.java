package noppes.npcs.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class EntityDialogNpc extends EntityNPCInterface
{
    public EntityDialogNpc(World world)
    {
        super(world);
    }

    /**
     * Only used by renderer in EntityLivingBase subclasses.\nDetermines if an entity is visible or not to a specfic
     * player, if the entity is normally invisible.\nFor EntityLivingBase subclasses, returning false when invisible
     * will render the entity semitransparent.
     */
    public boolean isInvisibleToPlayer(EntityPlayer player)
    {
        return true;
    }

    public boolean isInvisible()
    {
        return true;
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate() {}

    /**
     * Called when a player interacts with a mob. e.g. gets milk from a cow, gets into the saddle on a pig.
     */
    public boolean interact(EntityPlayer player)
    {
        return false;
    }
}
