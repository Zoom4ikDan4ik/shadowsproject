package noppes.npcs.entity.old;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import noppes.npcs.ModelData;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityNPCElfFemale extends EntityNPCInterface
{
    public EntityNPCElfFemale(World world)
    {
        super(world);
        this.display.texture = "customnpcs:textures/entity/elffemale/ElfFemale.png";
        this.scaleX = 0.8F;
        this.scaleY = 1.0F;
        this.scaleZ = 0.8F;
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        this.isDead = true;

        if (!this.worldObj.isRemote)
        {
            NBTTagCompound compound = new NBTTagCompound();
            this.writeToNBT(compound);
            EntityCustomNpc npc = new EntityCustomNpc(this.worldObj);
            npc.readFromNBT(compound);
            ModelData data = npc.modelData;
            data.breasts = 2;
            data.legs.setScale(0.8F, 1.05F);
            data.arms.setScale(0.8F, 1.05F);
            data.body.setScale(0.8F, 1.05F);
            data.head.setScale(0.8F, 0.85F);
            this.worldObj.spawnEntityInWorld(npc);
        }

        super.onUpdate();
    }
}
