package noppes.npcs.constants;

public enum EnumQuestExperienceType {
    COMBAT, RESEARCH, SURVIVE;
}
