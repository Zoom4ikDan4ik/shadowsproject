package noppes.npcs.ai;

import net.minecraft.entity.ai.EntityAIBase;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityAIJob extends EntityAIBase
{
    private EntityNPCInterface npc;

    public EntityAIJob(EntityNPCInterface npc)
    {
        this.npc = npc;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
        return !this.npc.isKilled() && this.npc.jobInterface != null ? this.npc.jobInterface.aiShouldExecute() : false;
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
        this.npc.jobInterface.aiStartExecuting();
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
        return !this.npc.isKilled() && this.npc.jobInterface != null ? this.npc.jobInterface.aiContinueExecute() : false;
    }

    /**
     * Updates the task
     */
    public void updateTask()
    {
        this.npc.jobInterface.aiUpdateTask();
    }

    /**
     * Resets the task
     */
    public void resetTask()
    {
        if (this.npc.jobInterface != null)
        {
            this.npc.jobInterface.resetTask();
        }
    }
}
