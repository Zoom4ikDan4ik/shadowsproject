package noppes.npcs.ai;

import net.minecraft.entity.ai.EntityAIBase;
import noppes.npcs.entity.EntityNPCInterface;

public class EntityAIRole extends EntityAIBase
{
    private EntityNPCInterface npc;

    public EntityAIRole(EntityNPCInterface npc)
    {
        this.npc = npc;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
        return !this.npc.isKilled() && this.npc.roleInterface != null ? this.npc.roleInterface.aiShouldExecute() : false;
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
        this.npc.roleInterface.aiStartExecuting();
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
        return !this.npc.isKilled() && this.npc.roleInterface != null ? this.npc.roleInterface.aiContinueExecute() : false;
    }

    /**
     * Updates the task
     */
    public void updateTask()
    {
        if (this.npc.roleInterface != null)
        {
            this.npc.roleInterface.aiUpdateTask();
        }
    }
}
