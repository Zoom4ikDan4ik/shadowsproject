package noppes.npcs.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class EntityMagicProjectile extends EntityProjectile
{
    private EntityPlayer player;
    private ItemStack equiped;

    public EntityMagicProjectile(World par1World)
    {
        super(par1World);
    }

    public EntityMagicProjectile(World par1World, EntityPlayer player, ItemStack item, boolean isNPC)
    {
        super(par1World, player, item, isNPC);
        this.player = player;
        this.equiped = player.inventory.getCurrentItem();
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        if (!this.worldObj.isRemote && (this.player == null || this.player.inventory.getCurrentItem() != this.equiped))
        {
            this.setDead();
        }

        super.onUpdate();
    }

    /**
     * Gets the name of this command sender (usually username, but possibly "Rcon")
     */
    public String getCommandSenderName()
    {
        return StatCollector.translateToLocal("entity.throwableitem.name");
    }
}
