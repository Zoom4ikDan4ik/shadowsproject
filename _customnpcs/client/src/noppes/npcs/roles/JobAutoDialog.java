package noppes.npcs.roles;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobAutoDialog extends JobInterface {

    public int dialogRange;

    public JobAutoDialog(EntityNPCInterface npc) {
        super(npc);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound nbt) {
        nbt.setInteger("DialogRange", dialogRange);
        return nbt;
    }

    @Override
    public void readFromNBT(NBTTagCompound var1) {
        this.dialogRange = var1.getInteger("DialogRange");
    }

    private Map<String, Boolean> openedDialogsForPlayers = new HashMap<>();

    public void onLivingUpdate() {
        if (!this.npc.isRemote())
        {
            List list = this.npc.worldObj.getEntitiesWithinAABB(EntityPlayer.class, this.npc.boundingBox.expand(this.dialogRange, (this.dialogRange / 2f), this.dialogRange));
            for(Object obj : list) {
                EntityPlayer p = (EntityPlayer) obj;
                Boolean hasOpen = openedDialogsForPlayers.get(p.getDisplayName());
                if(hasOpen != null && hasOpen) {
                    if(p.getDistanceToEntity(npc) > dialogRange) {
                        openedDialogsForPlayers.put(p.getDisplayName(), false);
                    }
                } else {
                    if(p.getDistanceToEntity(npc) <= dialogRange) {
                        openDialog(p);
                    }
                }
            }
        }
    }

    @SideOnly(Side.CLIENT)
    private void openDialog(EntityPlayer p) {
        openedDialogsForPlayers.put(p.getDisplayName(), true);
        npc.interact(p);
    }
}
