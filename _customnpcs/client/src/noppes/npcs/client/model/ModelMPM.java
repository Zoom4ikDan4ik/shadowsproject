package noppes.npcs.client.model;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import noppes.npcs.ModelPartConfig;
import noppes.npcs.ModelPartData;
import noppes.npcs.client.ClientProxy;
import noppes.npcs.client.model.animation.AniCrawling;
import noppes.npcs.client.model.animation.AniHug;
import noppes.npcs.client.model.part.*;
import noppes.npcs.client.model.util.ModelPartInterface;
import noppes.npcs.client.model.util.ModelScaleRenderer;
import noppes.npcs.constants.EnumAnimation;
import noppes.npcs.constants.EnumJobType;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.roles.JobPuppet;
import org.lwjgl.opengl.GL11;

import java.util.Random;

public class ModelMPM extends ModelNPCMale
{
    private ModelPartInterface wings;
    private ModelPartInterface mohawk;
    private ModelPartInterface hair;
    private ModelPartInterface beard;
    private ModelPartInterface breasts;
    private ModelPartInterface snout;
    private ModelPartInterface ears;
    private ModelPartInterface fin;
    private ModelPartInterface clawsR;
    private ModelPartInterface clawsL;
    private ModelLegs legs;
    private ModelScaleRenderer headwear;
    private ModelTail tail;
    public ModelBase entityModel;
    public EntityLivingBase entity;
    public boolean currentlyPlayerTexture;
    public boolean isArmor;
    public float alpha = 1.0F;

    public ModelMPM(float par1)
    {
        super(par1);
        this.isArmor = par1 > 0.0F;
        float par2 = 0.0F;
        this.bipedCloak = new ModelRenderer(this, 0, 0);
        this.bipedCloak.addBox(-5.0F, 0.0F, -1.0F, 10, 16, 1, par1);
        this.bipedEars = new ModelRenderer(this, 24, 0);
        this.bipedEars.addBox(-3.0F, -6.0F, -1.0F, 6, 6, 1, par1);
        this.bipedHead = new ModelScaleRenderer(this, 0, 0);
        this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, par1);
        this.bipedHead.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
        this.bipedHeadwear = new ModelScaleRenderer(this, 32, 0);
        this.bipedHeadwear.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, par1 + 0.5F);
        this.bipedHeadwear.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
        this.bipedBody = new ModelScaleRenderer(this, 16, 16);
        this.bipedBody.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, par1);
        this.bipedBody.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
        this.bipedRightArm = new ModelScaleRenderer(this, 40, 16);
        this.bipedRightArm.addBox(-3.0F, -2.0F, -2.0F, 4, 12, 4, par1);
        this.bipedRightArm.setRotationPoint(-5.0F, 2.0F + par2, 0.0F);
        this.bipedLeftArm = new ModelScaleRenderer(this, 40, 16);
        this.bipedLeftArm.mirror = true;
        this.bipedLeftArm.addBox(-1.0F, -2.0F, -2.0F, 4, 12, 4, par1);
        this.bipedLeftArm.setRotationPoint(5.0F, 2.0F + par2, 0.0F);
        this.bipedRightLeg = new ModelScaleRenderer(this, 0, 16);
        this.bipedRightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
        this.bipedRightLeg.setRotationPoint(-1.9F, 12.0F + par2, 0.0F);
        this.bipedLeftLeg = new ModelScaleRenderer(this, 0, 16);
        this.bipedLeftLeg.mirror = true;
        this.bipedLeftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
        this.bipedLeftLeg.setRotationPoint(1.9F, 12.0F + par2, 0.0F);
        this.headwear = new ModelHeadwear(this);
        this.legs = new ModelLegs(this, (ModelScaleRenderer)this.bipedRightLeg, (ModelScaleRenderer)this.bipedLeftLeg);
        this.breasts = new ModelBreasts(this);
        this.bipedBody.addChild(this.breasts);

        if (!this.isArmor)
        {
            this.ears = new ModelEars(this);
            this.bipedHead.addChild(this.ears);
            this.mohawk = new ModelMohawk(this);
            this.bipedHead.addChild(this.mohawk);
            this.hair = new ModelHair(this);
            this.bipedHead.addChild(this.hair);
            this.beard = new ModelBeard(this);
            this.bipedHead.addChild(this.beard);
            this.snout = new ModelSnout(this);
            this.bipedHead.addChild(this.snout);
            this.tail = new ModelTail(this);
            this.wings = new ModelWings(this);
            this.bipedBody.addChild(this.wings);
            this.fin = new ModelFin(this);
            this.bipedBody.addChild(this.fin);
            this.clawsL = new ModelClaws(this, false);
            this.bipedLeftArm.addChild(this.clawsL);
            this.clawsR = new ModelClaws(this, true);
            this.bipedRightArm.addChild(this.clawsR);
        }
    }

    private void setPlayerData(EntityCustomNpc entity)
    {
        if (!this.isArmor)
        {
            this.mohawk.setData(entity.modelData, entity);
            this.beard.setData(entity.modelData, entity);
            this.hair.setData(entity.modelData, entity);
            this.snout.setData(entity.modelData, entity);
            this.tail.setData(entity);
            this.fin.setData(entity.modelData, entity);
            this.wings.setData(entity.modelData, entity);
            this.ears.setData(entity.modelData, entity);
            this.clawsL.setData(entity.modelData, entity);
            this.clawsR.setData(entity.modelData, entity);
        }

        this.breasts.setData(entity.modelData, entity);
        this.legs.setData(entity);
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
    {
        EntityCustomNpc npc = (EntityCustomNpc)par1Entity;

        if (this.entityModel != null)
        {
            if (!this.isArmor)
            {
                this.entityModel.isChild = this.entity.isChild();
                this.entityModel.onGround = this.onGround;
                this.entityModel.isRiding = this.isRiding;

                if (this.entityModel instanceof ModelBiped)
                {
                    ModelBiped job = (ModelBiped)this.entityModel;
                    job.aimedBow = this.aimedBow;
                    job.heldItemLeft = this.heldItemLeft;
                    job.heldItemRight = this.heldItemRight;
                    job.isSneak = this.isSneak;
                }

                this.entityModel.render(this.entity, par2, par3, par4, par5, par6, par7);
            }
        }
        else
        {
            this.alpha = npc.isInvisible() && !npc.isInvisibleToPlayer(Minecraft.getMinecraft().thePlayer) ? 0.15F : 1.0F;
            this.setPlayerData(npc);
            this.currentlyPlayerTexture = true;
            this.setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);

            if (npc.advanced.job == EnumJobType.Puppet)
            {
                JobPuppet job1 = (JobPuppet)npc.jobInterface;

                if (job1.isActive())
                {
                    float pi = (float)Math.PI;

                    if (!job1.head.disabled)
                    {
                        this.bipedHeadwear.rotateAngleX = this.bipedHead.rotateAngleX = job1.head.rotationX * pi;
                        this.bipedHeadwear.rotateAngleY = this.bipedHead.rotateAngleY = job1.head.rotationY * pi;
                        this.bipedHeadwear.rotateAngleZ = this.bipedHead.rotateAngleZ = job1.head.rotationZ * pi;
                    }

                    if (!job1.body.disabled)
                    {
                        this.bipedBody.rotateAngleX = job1.body.rotationX * pi;
                        this.bipedBody.rotateAngleY = job1.body.rotationY * pi;
                        this.bipedBody.rotateAngleZ = job1.body.rotationZ * pi;
                    }

                    if (!job1.larm.disabled)
                    {
                        this.bipedLeftArm.rotateAngleX = job1.larm.rotationX * pi;
                        this.bipedLeftArm.rotateAngleY = job1.larm.rotationY * pi;
                        this.bipedLeftArm.rotateAngleZ = job1.larm.rotationZ * pi;

                        if (!npc.display.disableLivingAnimation)
                        {
                            this.bipedLeftArm.rotateAngleZ -= MathHelper.cos(par3 * 0.09F) * 0.05F + 0.05F;
                            this.bipedLeftArm.rotateAngleX -= MathHelper.sin(par3 * 0.067F) * 0.05F;
                        }
                    }

                    if (!job1.rarm.disabled)
                    {
                        this.bipedRightArm.rotateAngleX = job1.rarm.rotationX * pi;
                        this.bipedRightArm.rotateAngleY = job1.rarm.rotationY * pi;
                        this.bipedRightArm.rotateAngleZ = job1.rarm.rotationZ * pi;

                        if (!npc.display.disableLivingAnimation)
                        {
                            this.bipedRightArm.rotateAngleZ += MathHelper.cos(par3 * 0.09F) * 0.05F + 0.05F;
                            this.bipedRightArm.rotateAngleX += MathHelper.sin(par3 * 0.067F) * 0.05F;
                        }
                    }

                    if (!job1.rleg.disabled)
                    {
                        this.bipedRightLeg.rotateAngleX = job1.rleg.rotationX * pi;
                        this.bipedRightLeg.rotateAngleY = job1.rleg.rotationY * pi;
                        this.bipedRightLeg.rotateAngleZ = job1.rleg.rotationZ * pi;
                    }

                    if (!job1.lleg.disabled)
                    {
                        this.bipedLeftLeg.rotateAngleX = job1.lleg.rotationX * pi;
                        this.bipedLeftLeg.rotateAngleY = job1.lleg.rotationY * pi;
                        this.bipedLeftLeg.rotateAngleZ = job1.lleg.rotationZ * pi;
                    }
                }
            }

            this.renderHead(npc, par7);
            this.renderArms(npc, par7, false);
            this.renderBody(npc, par7);
            this.renderLegs(npc, par7);
        }
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity entity)
    {
        EntityCustomNpc npc = (EntityCustomNpc)entity;
        this.isRiding = npc.isRiding();

        if (this.isSneak && (npc.currentAnimation == EnumAnimation.CRAWLING || npc.currentAnimation == EnumAnimation.LYING))
        {
            this.isSneak = false;
        }

        this.bipedBody.rotationPointX = 0.0F;
        this.bipedBody.rotationPointY = 0.0F;
        this.bipedBody.rotationPointZ = 0.0F;
        this.bipedBody.rotateAngleX = 0.0F;
        this.bipedBody.rotateAngleY = 0.0F;
        this.bipedBody.rotateAngleZ = 0.0F;
        this.bipedHead.rotateAngleZ = 0.0F;
        this.bipedHeadwear.rotateAngleZ = 0.0F;
        this.bipedLeftLeg.rotateAngleX = 0.0F;
        this.bipedLeftLeg.rotateAngleY = 0.0F;
        this.bipedLeftLeg.rotateAngleZ = 0.0F;
        this.bipedRightLeg.rotateAngleX = 0.0F;
        this.bipedRightLeg.rotateAngleY = 0.0F;
        this.bipedRightLeg.rotateAngleZ = 0.0F;
        this.bipedLeftArm.rotationPointY = 2.0F;
        this.bipedLeftArm.rotationPointZ = 0.0F;
        this.bipedRightArm.rotationPointY = 2.0F;
        this.bipedRightArm.rotationPointZ = 0.0F;
        super.setRotationAngles(par1, par2, par3, par4, par5, par6, entity);

        if (!this.isArmor)
        {
            this.hair.setRotationAngles(par1, par2, par3, par4, par5, par6, entity);
            this.beard.setRotationAngles(par1, par2, par3, par4, par5, par6, entity);
            this.wings.setRotationAngles(par1, par2, par3, par4, par5, par6, entity);
            this.tail.setRotationAngles(par1, par2, par3, par4, par5, par6, entity);
        }

        this.legs.setRotationAngles(par1, par2, par3, par4, par5, par6, entity);

        if (this.isSleeping(entity))
        {
            if (this.bipedHead.rotateAngleX < 0.0F)
            {
                this.bipedHead.rotateAngleX = 0.0F;
                this.bipedHeadwear.rotateAngleX = 0.0F;
            }
        }
        else if (npc.currentAnimation == EnumAnimation.CRY)
        {
            this.bipedHeadwear.rotateAngleX = this.bipedHead.rotateAngleX = 0.7F;
        }
        else if (npc.currentAnimation == EnumAnimation.HUG)
        {
            AniHug.setRotationAngles(par1, par2, par3, par4, par5, par6, entity, this);
        }
        else if (npc.currentAnimation == EnumAnimation.CRAWLING)
        {
            AniCrawling.setRotationAngles(par1, par2, par3, par4, par5, par6, entity, this);
        }
        else if (npc.currentAnimation == EnumAnimation.WAVING)
        {
            this.bipedRightArm.rotateAngleX = -0.1F;
            this.bipedRightArm.rotateAngleY = 0.0F;
            this.bipedRightArm.rotateAngleZ = (float)(2.141592653589793D - Math.sin((double)((float)entity.ticksExisted * 0.27F)) * 0.5D);
        }
        else if (this.isSneak)
        {
            this.bipedBody.rotateAngleX = 0.5F / npc.modelData.body.scaleY;
        }
    }

    /**
     * Used for easily adding entity-dependent animations. The second and third float params here are the same second
     * and third as in the setRotationAngles method.
     */
    public void setLivingAnimations(EntityLivingBase par1EntityLivingBase, float par2, float par3, float par4)
    {
        if (this.entityModel != null)
        {
            this.entityModel.setLivingAnimations(this.entity, par2, par3, par4);
        }
        else
        {
            EntityCustomNpc npc = (EntityCustomNpc)par1EntityLivingBase;

            if (!this.isArmor)
            {
                ModelPartData partData = npc.modelData.getPartData("tail");

                if (partData != null)
                {
                    this.tail.setLivingAnimations(partData, par1EntityLivingBase, par2, par3, par4);
                }
            }
        }
    }

    public void loadPlayerTexture(EntityCustomNpc npc)
    {
        if (!this.isArmor && !this.currentlyPlayerTexture)
        {
            ClientProxy.bindTexture(npc.textureLocation);
            this.currentlyPlayerTexture = true;
        }
    }

    private void renderHead(EntityCustomNpc entity, float f)
    {
        this.loadPlayerTexture(entity);
        float x = 0.0F;
        float y = entity.modelData.getBodyY();
        float z = 0.0F;
        GL11.glPushMatrix();

        if (entity.currentAnimation == EnumAnimation.DANCING)
        {
            float head = (float)entity.ticksExisted / 4.0F;
            GL11.glTranslatef((float)Math.sin((double)head) * 0.075F, (float)Math.abs(Math.cos((double)head)) * 0.125F - 0.02F, (float)(-Math.abs(Math.cos((double)head))) * 0.075F);
        }

        ModelPartConfig head1 = entity.modelData.head;

        if (this.bipedHeadwear.showModel && !this.bipedHeadwear.isHidden)
        {
            if (entity.modelData.headwear != 1 && !this.isArmor)
            {
                if (entity.modelData.headwear == 2)
                {
                    this.headwear.rotateAngleX = this.bipedHeadwear.rotateAngleX;
                    this.headwear.rotateAngleY = this.bipedHeadwear.rotateAngleY;
                    this.headwear.rotateAngleZ = this.bipedHeadwear.rotateAngleZ;
                    this.headwear.rotationPointX = this.bipedHeadwear.rotationPointX;
                    this.headwear.rotationPointY = this.bipedHeadwear.rotationPointY;
                    this.headwear.rotationPointZ = this.bipedHeadwear.rotationPointZ;
                    this.headwear.setConfig(head1, x, y, z);
                    this.headwear.render(f);
                }
            }
            else
            {
                ((ModelScaleRenderer)this.bipedHeadwear).setConfig(head1, x, y, z);
                ((ModelScaleRenderer)this.bipedHeadwear).render(f);
            }
        }

        ((ModelScaleRenderer)this.bipedHead).setConfig(head1, x, y, z);
        ((ModelScaleRenderer)this.bipedHead).render(f);
        GL11.glPopMatrix();
    }

    private void renderBody(EntityCustomNpc entity, float f)
    {
        this.loadPlayerTexture(entity);
        float x = 0.0F;
        float y = entity.modelData.getBodyY();
        float z = 0.0F;
        GL11.glPushMatrix();

        if (entity.currentAnimation == EnumAnimation.DANCING)
        {
            float body = (float)entity.ticksExisted / 4.0F;
            GL11.glTranslatef((float)Math.sin((double)body) * 0.015F, 0.0F, 0.0F);
        }

        ModelPartConfig body1 = entity.modelData.body;
        ((ModelScaleRenderer)this.bipedBody).setConfig(body1, x, y, z);
        ((ModelScaleRenderer)this.bipedBody).render(f);
        GL11.glPopMatrix();
    }

    public void renderArms(EntityCustomNpc entity, float f, boolean bo)
    {
        this.loadPlayerTexture(entity);
        ModelPartConfig arms = entity.modelData.arms;
        float x = (1.0F - entity.modelData.body.scaleX) * 0.25F + (1.0F - arms.scaleX) * 0.075F;
        float y = entity.modelData.getBodyY() + (1.0F - arms.scaleY) * -0.1F;
        float z = 0.0F;
        GL11.glPushMatrix();

        if (entity.currentAnimation == EnumAnimation.DANCING)
        {
            float dancing = (float)entity.ticksExisted / 4.0F;
            GL11.glTranslatef((float)Math.sin((double)dancing) * 0.025F, (float)Math.abs(Math.cos((double)dancing)) * 0.125F - 0.02F, 0.0F);
        }

        if (!bo)
        {
            ((ModelScaleRenderer)this.bipedLeftArm).setConfig(arms, -x, y, z);
            ((ModelScaleRenderer)this.bipedLeftArm).render(f);
            ((ModelScaleRenderer)this.bipedRightArm).setConfig(arms, x, y, z);
            ((ModelScaleRenderer)this.bipedRightArm).render(f);
        }
        else
        {
            ((ModelScaleRenderer)this.bipedRightArm).setConfig(arms, 0.0F, 0.0F, 0.0F);
            ((ModelScaleRenderer)this.bipedRightArm).render(f);
        }

        GL11.glPopMatrix();
    }

    private void renderLegs(EntityCustomNpc entity, float f)
    {
        this.loadPlayerTexture(entity);
        ModelPartConfig legs = entity.modelData.legs;
        float x = (1.0F - legs.scaleX) * 0.125F;
        float y = entity.modelData.getLegsY();
        float z = 0.0F;
        GL11.glPushMatrix();
        this.legs.setConfig(legs, x, y, z);
        this.legs.render(f);

        if (!this.isArmor)
        {
            this.tail.setConfig(legs, 0.0F, y, z);
            this.tail.render(f);
        }

        GL11.glPopMatrix();
    }

    public ModelRenderer getRandomModelBox(Random par1Random)
    {
        int random = par1Random.nextInt(5);

        switch (random)
        {
            case 0:
                return this.bipedRightLeg;

            case 1:
                return this.bipedHead;

            case 2:
                return this.bipedLeftArm;

            case 3:
                return this.bipedRightArm;

            case 4:
                return this.bipedLeftLeg;

            default:
                return this.bipedBody;
        }
    }
}
