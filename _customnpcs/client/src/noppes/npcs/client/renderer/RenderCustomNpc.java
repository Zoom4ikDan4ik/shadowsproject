package noppes.npcs.client.renderer;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.NPCRendererHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import noppes.npcs.client.model.ModelMPM;
import noppes.npcs.client.model.util.ModelRenderPassHelper;
import noppes.npcs.controllers.PixelmonHelper;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.opengl.GL11;

import java.lang.reflect.Method;

public class RenderCustomNpc extends RenderNPCHumanMale
{
    private RendererLivingEntity renderEntity;
    private EntityLivingBase entity;
    private ModelRenderPassHelper renderpass = new ModelRenderPassHelper();

    public RenderCustomNpc()
    {
        super(new ModelMPM(0.0F), new ModelMPM(1.0F), new ModelMPM(0.5F));
    }

    public void renderPlayer(EntityNPCInterface npcInterface, double d, double d1, double d2, float f, float f1)
    {
        EntityCustomNpc npc = (EntityCustomNpc)npcInterface;
        this.entity = npc.modelData.getEntity(npc);
        ModelBase model = null;
        this.renderEntity = null;

        if (this.entity != null)
        {
            this.renderEntity = (RendererLivingEntity)RenderManager.instance.getEntityRenderObject(this.entity);
            model = NPCRendererHelper.getMainModel(this.renderEntity);

            if (PixelmonHelper.isPixelmon(this.entity))
            {
                try
                {
                    Class e = Class.forName("com.pixelmonmod.pixelmon.entities.pixelmon.Entity2HasModel");
                    Method breed = e.getMethod("getModel", new Class[0]);
                    model = (ModelBase)breed.invoke(this.entity, new Object[0]);
                }
                catch (Exception var15)
                {
                    var15.printStackTrace();
                }
            }

            if (EntityList.getEntityString(this.entity).equals("doggystyle.Dog"))
            {
                try
                {
                    Method e1 = this.entity.getClass().getMethod("getBreed", new Class[0]);
                    Object breed1 = e1.invoke(this.entity, new Object[0]);
                    e1 = breed1.getClass().getMethod("getModel", new Class[0]);
                    model = (ModelBase)e1.invoke(breed1, new Object[0]);
                    model.getClass().getMethod("setPosition", new Class[] {Integer.TYPE}).invoke(model, new Object[] {Integer.valueOf(0)});
                }
                catch (Exception var14)
                {
                    var14.printStackTrace();
                }
            }

            this.renderPassModel = this.renderpass;
            this.renderpass.renderer = this.renderEntity;
            this.renderpass.entity = this.entity;
        }

        ((ModelMPM)this.modelArmor).entityModel = model;
        ((ModelMPM)this.modelArmor).entity = this.entity;
        ((ModelMPM)this.modelArmorChestplate).entityModel = model;
        ((ModelMPM)this.modelArmorChestplate).entity = this.entity;
        ((ModelMPM)this.mainModel).entityModel = model;
        ((ModelMPM)this.mainModel).entity = this.entity;
        super.renderPlayer(npc, d, d1, d2, f, f1);
    }

    protected void renderEquippedItems(EntityLivingBase entityliving, float f)
    {
        if (this.renderEntity != null)
        {
            NPCRendererHelper.renderEquippedItems(this.entity, f, this.renderEntity);
        }
        else
        {
            super.renderEquippedItems(entityliving, f);
        }
    }

    /**
     * Queries whether should render the specified pass or not.
     */
    protected int shouldRenderPass(EntityLivingBase par1EntityLivingBase, int par2, float par3)
    {
        return this.renderEntity != null ? NPCRendererHelper.shouldRenderPass(this.entity, par2, par3, this.renderEntity) : this.func_130006_a((EntityLiving)par1EntityLivingBase, par2, par3);
    }

    /**
     * Allows the render to do any OpenGL state modifications necessary before the model is rendered. Args:
     * entityLiving, partialTickTime
     */
    protected void preRenderCallback(EntityLivingBase entityliving, float f)
    {
        if (this.renderEntity != null)
        {
            EntityNPCInterface npc = (EntityNPCInterface)entityliving;
            int size = npc.display.modelSize;

            if (this.entity instanceof EntityNPCInterface)
            {
                ((EntityNPCInterface)this.entity).display.modelSize = 5;
            }

            NPCRendererHelper.preRenderCallback(this.entity, f, this.renderEntity);
            npc.display.modelSize = size;
            GL11.glScalef(0.2F * (float)npc.display.modelSize, 0.2F * (float)npc.display.modelSize, 0.2F * (float)npc.display.modelSize);
        }
        else
        {
            super.preRenderCallback(entityliving, f);
        }
    }

    /**
     * Defines what float the third param in setRotationAngles of ModelBase is
     */
    protected float handleRotationFloat(EntityLivingBase par1EntityLivingBase, float par2)
    {
        return this.renderEntity != null ? NPCRendererHelper.handleRotationFloat(this.entity, par2, this.renderEntity) : super.handleRotationFloat(par1EntityLivingBase, par2);
    }
}
