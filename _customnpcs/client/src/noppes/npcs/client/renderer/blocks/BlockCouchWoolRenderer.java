package noppes.npcs.client.renderer.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import noppes.npcs.CustomItems;
import noppes.npcs.blocks.BlockCouchWool;
import noppes.npcs.blocks.tiles.TileCouchWool;
import noppes.npcs.client.model.blocks.*;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class BlockCouchWoolRenderer extends BlockRendererInterface
{
    private final ModelBase model = new ModelCouchMiddle();
    private final ModelBase model2 = new ModelCouchMiddleWool();
    private final ModelBase modelLeft = new ModelCouchLeft();
    private final ModelBase modelLeft2 = new ModelCouchLeftWool();
    private final ModelBase modelRight = new ModelCouchRight();
    private final ModelBase modelRight2 = new ModelCouchRightWool();
    private final ModelBase modelCorner = new ModelCouchCorner();
    private final ModelBase modelCorner2 = new ModelCouchCornerWool();

    public BlockCouchWoolRenderer()
    {
        ((BlockCouchWool)CustomItems.couchWool).renderId = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(this);
    }

    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float timeTick)
    {
        TileCouchWool tile = (TileCouchWool) tileEntity;
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef((float)(90 * tile.rotation), 0.0F, 1.0F, 0.0F);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        this.setWoodTexture(tileEntity.getBlockMetadata());

        if (tile.hasCornerLeft)
        {
            this.modelCorner.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }
        else if (tile.hasCornerRight)
        {
            GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
            this.modelCorner.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }
        else if (tile.hasLeft && tile.hasRight)
        {
            this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }
        else if (tile.hasLeft)
        {
            this.modelLeft.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }
        else if (tile.hasRight)
        {
            this.modelRight.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }
        else
        {
            this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }

        this.bindTexture(BlockTallLampRenderer.resourceTop);
        float[] color = BlockBannerRenderer.colorTable[tile.color];
        GL11.glColor3f(color[0], color[1], color[2]);

        if (!tile.hasCornerLeft && !tile.hasCornerRight)
        {
            if (tile.hasLeft && tile.hasRight)
            {
                this.model2.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            }
            else if (tile.hasLeft)
            {
                this.modelLeft2.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            }
            else if (tile.hasRight)
            {
                this.modelRight2.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            }
            else
            {
                this.model2.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            }
        }
        else
        {
            this.modelCorner2.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        }

        GL11.glPopMatrix();
    }

    public void renderTileEntityAtPost(TileEntity tileEntity, double x, double y, double z, float timeTick) {

    }

    public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef(0.0F, 0.9F, 0.1F);
        GL11.glScalef(0.9F, 0.9F, 0.9F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        this.setWoodTexture(metadata);
        GL11.glColor3f(1.0F, 1.0F, 1.0F);
        this.model.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        this.bindTexture(BlockTallLampRenderer.resourceTop);
        float[] color = BlockBannerRenderer.colorTable[15 - metadata];
        GL11.glColor3f(color[0], color[1], color[2]);
        this.model2.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
    }

    public int getRenderId()
    {
        return CustomItems.couchWool.getRenderType();
    }
}
