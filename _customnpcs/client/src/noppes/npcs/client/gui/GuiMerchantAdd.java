package noppes.npcs.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.IMerchant;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C17PacketCustomPayload;
import net.minecraft.util.ResourceLocation;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;
import noppes.npcs.ServerEventsHandler;
import noppes.npcs.client.Client;
import noppes.npcs.client.CustomNpcResourceListener;
import noppes.npcs.client.gui.util.GuiNpcButton;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.containers.ContainerMerchantAdd;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

@SideOnly(Side.CLIENT)
public class GuiMerchantAdd extends GuiContainer
{
    private static final ResourceLocation merchantGuiTextures = new ResourceLocation("textures/gui/container/villager.png");
    private IMerchant theIMerchant;
    private GuiMerchantAdd.MerchantButton nextRecipeButtonIndex;
    private GuiMerchantAdd.MerchantButton previousRecipeButtonIndex;
    private int currentRecipeIndex;
    private String field_94082_v;

    public GuiMerchantAdd()
    {
        super(new ContainerMerchantAdd(Minecraft.getMinecraft().thePlayer, ServerEventsHandler.Merchant, Minecraft.getMinecraft().theWorld));
        this.theIMerchant = ServerEventsHandler.Merchant;
        this.field_94082_v = I18n.format("entity.Villager.name", new Object[0]);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.buttonList.add(this.nextRecipeButtonIndex = new GuiMerchantAdd.MerchantButton(1, i + 120 + 27, j + 24 - 1, true));
        this.buttonList.add(this.previousRecipeButtonIndex = new GuiMerchantAdd.MerchantButton(2, i + 36 - 19, j + 24 - 1, false));
        this.buttonList.add(new GuiNpcButton(4, i + this.xSize, j + 20, 60, 20, "gui.remove"));
        this.buttonList.add(new GuiNpcButton(5, i + this.xSize, j + 50, 60, 20, "gui.add"));
        this.nextRecipeButtonIndex.enabled = false;
        this.previousRecipeButtonIndex.enabled = false;
    }

    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        this.fontRendererObj.drawString(this.field_94082_v, this.xSize / 2 - this.fontRendererObj.getStringWidth(this.field_94082_v) / 2, 6, CustomNpcResourceListener.DefaultTextColor);
        this.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 8, this.ySize - 96 + 2, CustomNpcResourceListener.DefaultTextColor);
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
        super.updateScreen();
        Minecraft mc = Minecraft.getMinecraft();
        MerchantRecipeList merchantrecipelist = this.theIMerchant.getRecipes(mc.thePlayer);

        if (merchantrecipelist != null)
        {
            this.nextRecipeButtonIndex.enabled = this.currentRecipeIndex < merchantrecipelist.size() - 1;
            this.previousRecipeButtonIndex.enabled = this.currentRecipeIndex > 0;
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        boolean flag = false;
        Minecraft mc = Minecraft.getMinecraft();

        if (guiButton == this.nextRecipeButtonIndex)
        {
            ++this.currentRecipeIndex;
            flag = true;
        }
        else if (guiButton == this.previousRecipeButtonIndex)
        {
            --this.currentRecipeIndex;
            flag = true;
        }

        if (guiButton.id == 4)
        {
            MerchantRecipeList bytebuf = this.theIMerchant.getRecipes(mc.thePlayer);

            if (this.currentRecipeIndex < bytebuf.size())
            {
                bytebuf.remove(this.currentRecipeIndex);

                if (this.currentRecipeIndex > 0)
                {
                    --this.currentRecipeIndex;
                }

                Client.sendData(EnumPacketServer.MerchantUpdate, new Object[] {Integer.valueOf(ServerEventsHandler.Merchant.getEntityId()), bytebuf});
            }
        }

        if (guiButton.id == 5)
        {
            ItemStack var14 = this.inventorySlots.getSlot(0).getStack();
            ItemStack item2 = this.inventorySlots.getSlot(1).getStack();
            ItemStack sold = this.inventorySlots.getSlot(2).getStack();

            if (var14 == null && item2 != null)
            {
                var14 = item2;
                item2 = null;
            }

            if (var14 != null && sold != null)
            {
                var14 = var14.copy();
                sold = sold.copy();

                if (item2 != null)
                {
                    item2 = item2.copy();
                }

                MerchantRecipe recipe = new MerchantRecipe(var14, item2, sold);
                recipe.func_82783_a(2147483639);
                MerchantRecipeList merchantrecipelist = this.theIMerchant.getRecipes(mc.thePlayer);
                merchantrecipelist.add(recipe);
                Client.sendData(EnumPacketServer.MerchantUpdate, new Object[] {Integer.valueOf(ServerEventsHandler.Merchant.getEntityId()), merchantrecipelist});
            }
        }

        if (flag)
        {
            ((ContainerMerchantAdd)this.inventorySlots).setCurrentRecipeIndex(this.currentRecipeIndex);
            ByteBuf var15 = Unpooled.buffer();

            try
            {
                var15.writeInt(this.currentRecipeIndex);
                this.mc.getNetHandler().addToSendQueue(new C17PacketCustomPayload("MC|TrSel", var15));
            }
            catch (Exception var12)
            {
                ;
            }
            finally
            {
                var15.release();
            }
        }
    }

    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        Minecraft mc = Minecraft.getMinecraft();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        mc.getTextureManager().bindTexture(merchantGuiTextures);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        MerchantRecipeList merchantrecipelist = this.theIMerchant.getRecipes(mc.thePlayer);

        if (merchantrecipelist != null && !merchantrecipelist.isEmpty())
        {
            int i1 = this.currentRecipeIndex;
            MerchantRecipe merchantrecipe = (MerchantRecipe)merchantrecipelist.get(i1);

            if (merchantrecipe.isRecipeDisabled())
            {
                mc.getTextureManager().bindTexture(merchantGuiTextures);
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                GL11.glDisable(GL11.GL_LIGHTING);
                this.drawTexturedModalRect(this.guiLeft + 83, this.guiTop + 21, 212, 0, 28, 21);
                this.drawTexturedModalRect(this.guiLeft + 83, this.guiTop + 51, 212, 0, 28, 21);
            }
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        super.drawScreen(mouseX, mouseY, partialTick);
        Minecraft mc = Minecraft.getMinecraft();
        MerchantRecipeList merchantrecipelist = this.theIMerchant.getRecipes(mc.thePlayer);

        if (merchantrecipelist != null && !merchantrecipelist.isEmpty())
        {
            int k = (this.width - this.xSize) / 2;
            int l = (this.height - this.ySize) / 2;
            int i1 = this.currentRecipeIndex;
            MerchantRecipe merchantrecipe = (MerchantRecipe)merchantrecipelist.get(i1);
            GL11.glPushMatrix();
            ItemStack itemstack = merchantrecipe.getItemToBuy();
            ItemStack itemstack1 = merchantrecipe.getSecondItemToBuy();
            ItemStack itemstack2 = merchantrecipe.getItemToSell();
            RenderHelper.enableGUIStandardItemLighting();
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glEnable(GL12.GL_RESCALE_NORMAL);
            GL11.glEnable(GL11.GL_COLOR_MATERIAL);
            GL11.glEnable(GL11.GL_LIGHTING);
            itemRender.zLevel = 100.0F;
            itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, mc.getTextureManager(), itemstack, k + 36, l + 24);
            itemRender.renderItemOverlayIntoGUI(this.fontRendererObj, mc.getTextureManager(), itemstack, k + 36, l + 24);

            if (itemstack1 != null)
            {
                itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, mc.getTextureManager(), itemstack1, k + 62, l + 24);
                itemRender.renderItemOverlayIntoGUI(this.fontRendererObj, mc.getTextureManager(), itemstack1, k + 62, l + 24);
            }

            itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, mc.getTextureManager(), itemstack2, k + 120, l + 24);
            itemRender.renderItemOverlayIntoGUI(this.fontRendererObj, mc.getTextureManager(), itemstack2, k + 120, l + 24);
            itemRender.zLevel = 0.0F;
            GL11.glDisable(GL11.GL_LIGHTING);

            if (this.func_146978_c(36, 24, 16, 16, mouseX, mouseY))
            {
                this.renderToolTip(itemstack, mouseX, mouseY);
            }
            else if (itemstack1 != null && this.func_146978_c(62, 24, 16, 16, mouseX, mouseY))
            {
                this.renderToolTip(itemstack1, mouseX, mouseY);
            }
            else if (this.func_146978_c(120, 24, 16, 16, mouseX, mouseY))
            {
                this.renderToolTip(itemstack2, mouseX, mouseY);
            }

            GL11.glPopMatrix();
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            RenderHelper.enableStandardItemLighting();
        }
    }

    public IMerchant getIMerchant()
    {
        return this.theIMerchant;
    }

    static ResourceLocation func_110417_h()
    {
        return merchantGuiTextures;
    }

    @SideOnly(Side.CLIENT)
    static class MerchantButton extends GuiButton
    {
        private final boolean field_146157_o;
        //private static final String __OBFID = "CL_00000763";

        public MerchantButton(int par1, int par2, int par3, boolean par4)
        {
            super(par1, par2, par3, 12, 19, "");
            this.field_146157_o = par4;
        }

        public void drawButton(Minecraft mc, int mouseX, int mouseY)
        {
            if (this.visible)
            {
                mc.getTextureManager().bindTexture(GuiMerchantAdd.merchantGuiTextures);
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                boolean flag = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
                int k = 0;
                int l = 176;

                if (!this.enabled)
                {
                    l += this.width * 2;
                }
                else if (flag)
                {
                    l += this.width;
                }

                if (!this.field_146157_o)
                {
                    k += this.height;
                }

                this.drawTexturedModalRect(this.xPosition, this.yPosition, l, k, this.width, this.height);
            }
        }
    }
}
