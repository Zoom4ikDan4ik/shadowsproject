package noppes.npcs.client.gui.mainmenu;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.inventory.Slot;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import noppes.npcs.client.Client;
import noppes.npcs.client.gui.util.*;
import noppes.npcs.constants.EnumPacketServer;
import noppes.npcs.containers.ContainerNPCInv;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import java.util.HashMap;

public class GuiNPCInv extends GuiContainerNPCInterface2 implements ISliderListener, IGuiData
{
    private HashMap<Integer, Integer> chances = new HashMap();
    private ContainerNPCInv container;
    private ResourceLocation slot;

    public GuiNPCInv(EntityNPCInterface npc, ContainerNPCInv container)
    {
        super(npc, container, 3);
        this.setBackground("npcinv.png");
        this.container = container;
        this.ySize = 200;
        this.slot = this.getResource("slot.png");
        Client.sendData(EnumPacketServer.MainmenuInvGet, new Object[0]);
    }

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        super.initGui();
        this.addLabel(new GuiNpcLabel(0, "inv.minExp", this.guiLeft + 118, this.guiTop + 18));
        this.addTextField(new GuiNpcTextField(0, this, this.fontRendererObj, this.guiLeft + 108, this.guiTop + 29, 60, 20, this.npc.inventory.minExp + ""));
        this.getTextField(0).numbersOnly = true;
        this.getTextField(0).setMinMaxDefault(0, 32767, 0);
        this.addLabel(new GuiNpcLabel(1, "inv.maxExp", this.guiLeft + 118, this.guiTop + 52));
        this.addTextField(new GuiNpcTextField(1, this, this.fontRendererObj, this.guiLeft + 108, this.guiTop + 63, 60, 20, this.npc.inventory.maxExp + ""));
        this.getTextField(1).numbersOnly = true;
        this.getTextField(1).setMinMaxDefault(0, 32767, 0);
        this.addButton(new GuiNpcButton(10, this.guiLeft + 88, this.guiTop + 88, 80, 20, new String[] {"stats.normal", "inv.auto"}, this.npc.inventory.lootMode));
        this.addLabel(new GuiNpcLabel(2, "inv.npcInventory", this.guiLeft + 191, this.guiTop + 5));
        this.addLabel(new GuiNpcLabel(3, "inv.inventory", this.guiLeft + 8, this.guiTop + 101));

        for (int i = 0; i < 9; ++i)
        {
            int chance = 100;

            if (this.npc.inventory.dropchance.containsKey(Integer.valueOf(i)))
            {
                chance = ((Integer)this.npc.inventory.dropchance.get(Integer.valueOf(i))).intValue();
            }

            if (chance <= 0 || chance > 100)
            {
                chance = 100;
            }

            this.chances.put(Integer.valueOf(i), Integer.valueOf(chance));
            GuiNpcSlider slider = new GuiNpcSlider(this, i, this.guiLeft + 211, this.guiTop + 14 + i * 21, (float)chance / 100.0F);
            this.addSlider(slider);
        }
    }

    protected void actionPerformed(GuiButton guiButton)
    {
        if (guiButton.id == 10)
        {
            this.npc.inventory.lootMode = ((GuiNpcButton) guiButton).getValue();
        }
    }

    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        super.drawGuiContainerBackgroundLayer(f, i, j);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(this.slot);

        for (int id = 4; id <= 6; ++id)
        {
            Slot slot = this.container.getSlot(id);

            if (slot.getHasStack())
            {
                this.drawTexturedModalRect(this.guiLeft + slot.xDisplayPosition - 1, this.guiTop + slot.yDisplayPosition - 1, 0, 0, 18, 18);
            }
        }
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int mouseX, int mouseY, float partialTick)
    {
        int showname = this.npc.display.showName;
        this.npc.display.showName = 1;
        int l = this.guiLeft + 20;
        int i1 = this.height / 2 - 145;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)(l + 33), (float)(i1 + 131), 50.0F);
        float f1 = 150.0F / (float)this.npc.display.modelSize;
        GL11.glScalef(-f1, f1, f1);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        float f2 = this.npc.renderYawOffset;
        float f3 = this.npc.rotationYaw;
        float f4 = this.npc.rotationPitch;
        float f7 = this.npc.rotationYawHead;
        float f5 = (float)(l + 33) - (float) mouseX;
        float f6 = (float)(i1 + 131 - 50) - (float) mouseY;
        GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
        RenderHelper.enableStandardItemLighting();
        GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-((float)Math.atan((double)(f6 / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
        this.npc.renderYawOffset = (float)Math.atan((double)(f5 / 40.0F)) * 20.0F;
        this.npc.rotationYaw = (float)Math.atan((double)(f5 / 40.0F)) * 40.0F;
        this.npc.rotationPitch = -((float)Math.atan((double)(f6 / 40.0F))) * 20.0F;
        this.npc.rotationYawHead = this.npc.rotationYaw;
        GL11.glTranslatef(0.0F, this.npc.yOffset, 0.0F);
        RenderManager.instance.playerViewY = 180.0F;
        RenderManager.instance.renderEntityWithPosYaw(this.npc, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
        this.npc.renderYawOffset = f2;
        this.npc.rotationYaw = f3;
        this.npc.rotationPitch = f4;
        this.npc.rotationYawHead = f7;
        GL11.glPopMatrix();
        RenderHelper.disableStandardItemLighting();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
        this.npc.display.showName = showname;
        super.drawScreen(mouseX, mouseY, partialTick);
    }

    public void save()
    {
        this.npc.inventory.dropchance = this.chances;
        this.npc.inventory.minExp = this.getTextField(0).getInteger();
        this.npc.inventory.maxExp = this.getTextField(1).getInteger();
        Client.sendData(EnumPacketServer.MainmenuInvSave, new Object[] {this.npc.inventory.writeEntityToNBT(new NBTTagCompound())});
    }

    public void setGuiData(NBTTagCompound compound)
    {
        this.npc.inventory.readEntityFromNBT(compound);
        this.initGui();
    }

    public void mouseDragged(GuiNpcSlider guiNpcSlider)
    {
        guiNpcSlider.displayString = StatCollector.translateToLocal("inv.dropChance") + ": " + (int)(guiNpcSlider.sliderValue * 100.0F) + "%";
    }

    public void mousePressed(GuiNpcSlider guiNpcSlider) {}

    public void mouseReleased(GuiNpcSlider guiNpcSlider)
    {
        this.chances.put(Integer.valueOf(guiNpcSlider.id), Integer.valueOf((int)(guiNpcSlider.sliderValue * 100.0F)));
    }
}
