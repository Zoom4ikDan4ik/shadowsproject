package foxz.command;

import foxz.commandhelper.ChMcLogger;
import foxz.commandhelper.annotations.Command;
import foxz.commandhelper.annotations.SubCommand;
import foxz.commandhelper.permissions.OpOnly;
import foxz.commandhelper.permissions.ParamCheck;
import foxz.commandhelper.permissions.PlayerOnly;
import foxz.utils.Utils;
import net.minecraft.block.Block;
import net.minecraft.command.CommandBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import noppes.npcs.controllers.ServerCloneController;
import noppes.npcs.entity.EntityNPCInterface;

import java.util.Iterator;
import java.util.List;

@Command(
    name = "clone",
    desc = "Clone operation (server side)"
)
public class CmdClone extends ChMcLogger
{
    public CmdClone(Object sender)
    {
        super(sender);
    }

    @SubCommand(
        desc = "Add NPC(s) to clone storage",
        usage = "<npc> <tab> [clonedname]",
        permissions = {OpOnly.class, PlayerOnly.class, ParamCheck.class}
    )
    public Boolean add(String[] args)
    {
        EntityPlayerMP player = (EntityPlayerMP)this.pcParam;
        int tab = 0;

        try
        {
            tab = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException var9)
        {
            ;
        }

        List list = Utils.getNearbeEntityFromPlayer(EntityNPCInterface.class, player, 80);
        Iterator var5 = list.iterator();
        EntityNPCInterface npc;

        do
        {
            if (!var5.hasNext())
            {
                return Boolean.valueOf(true);
            }

            npc = (EntityNPCInterface)var5.next();
        }
        while (!npc.display.name.equalsIgnoreCase(args[0]));

        String name = npc.display.name;

        if (args.length > 2)
        {
            name = args[2];
        }

        NBTTagCompound compound = new NBTTagCompound();

        if (!npc.writeToNBTOptional(compound))
        {
            return Boolean.valueOf(false);
        }
        else
        {
            ServerCloneController.Instance.addClone(compound, name, tab);
            return Boolean.valueOf(true);
        }
    }

    @SubCommand(
        desc = "List NPC from clone storage",
        usage = "<tab>",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public Boolean list(String[] args)
    {
        this.sendmessage("--- Stored NPCs --- (server side)");
        int tab = 0;

        try
        {
            tab = Integer.parseInt(args[0]);
        }
        catch (NumberFormatException var5)
        {
            ;
        }

        Iterator var3 = ServerCloneController.Instance.getClones(tab).iterator();

        while (var3.hasNext())
        {
            String name = (String)var3.next();
            this.sendmessage(name);
        }

        this.sendmessage("------------------------------------");
        return Boolean.valueOf(true);
    }

    @SubCommand(
        desc = "Remove NPC from clone storage",
        usage = "<name> <tab>",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public Boolean del(String[] args)
    {
        String nametodel = args[0];
        int tab = 0;

        try
        {
            tab = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException var7)
        {
            ;
        }

        boolean deleted = false;
        Iterator var5 = ServerCloneController.Instance.getClones(tab).iterator();

        while (var5.hasNext())
        {
            String name = (String)var5.next();

            if (nametodel.equalsIgnoreCase(name))
            {
                ServerCloneController.Instance.removeClone(name, tab);
                deleted = true;
                break;
            }
        }

        if (!ServerCloneController.Instance.removeClone(nametodel, tab))
        {
            this.sendmessage(String.format("Npc \'%s\' wasn\'t found", new Object[] {nametodel}));
            return Boolean.valueOf(false);
        }
        else
        {
            return Boolean.valueOf(true);
        }
    }

    @SubCommand(
        desc = "Spawn cloned NPC",
        usage = "<name> <tab> [[world:]x,y,z]] [newname]",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public boolean spawn(String[] args)
    {
        String name = args[0].replaceAll("%", " ");
        int tab = 0;

        try
        {
            tab = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException var17)
        {
            ;
        }

        String newname = null;
        NBTTagCompound compound = ServerCloneController.Instance.getCloneData(this.pcParam, name, tab);

        if (compound == null)
        {
            this.sendmessage("Unknown npc");
            return false;
        }
        else
        {
            World world = this.pcParam.getEntityWorld();
            double posX = (double)this.pcParam.getPlayerCoordinates().posX;
            double posY = (double)this.pcParam.getPlayerCoordinates().posY;
            double posZ = (double)this.pcParam.getPlayerCoordinates().posZ;

            if (args.length > 2)
            {
                String entity = args[2];
                String[] npc;

                if (entity.contains(":"))
                {
                    npc = entity.split(":");
                    entity = npc[1];
                    world = Utils.getWorld(npc[0]);

                    if (world == null)
                    {
                        this.sendmessage(String.format("\'%s\' is an unknown world", new Object[] {npc[0]}));
                        return false;
                    }
                }

                if (entity.contains(","))
                {
                    npc = entity.split(",");

                    if (npc.length != 3)
                    {
                        this.sendmessage("Location need be x,y,z");
                        return false;
                    }

                    try
                    {
                        posX = CommandBase.func_110666_a(this.pcParam, posX, npc[0]);
                        posY = CommandBase.func_110665_a(this.pcParam, posY, npc[1].trim(), 0, 0);
                        posZ = CommandBase.func_110666_a(this.pcParam, posZ, npc[2]);
                    }
                    catch (NumberFormatException var16)
                    {
                        this.sendmessage("Location should be in numbers");
                        return false;
                    }

                    if (args.length > 3)
                    {
                        newname = args[3];
                    }
                }
                else
                {
                    newname = entity;
                }
            }

            if (posX == 0.0D && posY == 0.0D && posZ == 0.0D)
            {
                this.sendmessage("Location needed");
                return false;
            }
            else
            {
                Entity entity1 = EntityList.createEntityFromNBT(compound, world);
                entity1.setPosition(posX + 0.5D, posY + 1.0D, posZ + 0.5D);

                if (entity1 instanceof EntityNPCInterface)
                {
                    EntityNPCInterface npc1 = (EntityNPCInterface)entity1;
                    npc1.ai.startPos = new int[] {MathHelper.floor_double(posX), MathHelper.floor_double(posY), MathHelper.floor_double(posZ)};

                    if (newname != null && !newname.isEmpty())
                    {
                        npc1.display.name = newname.replaceAll("%", " ");
                    }
                }

                world.spawnEntityInWorld(entity1);
                return true;
            }
        }
    }

    @SubCommand(
        desc = "Spawn cloned NPC",
        usage = "<name> <tab> <lenght> <width> [[world:]x,y,z]] [newname]",
        permissions = {OpOnly.class, ParamCheck.class}
    )
    public boolean grid(String[] args)
    {
        String name = args[0].replaceAll("%", " ");
        int tab = 0;

        try
        {
            tab = Integer.parseInt(args[1]);
        }
        catch (NumberFormatException var26)
        {
            ;
        }

        int width;
        int height;

        try
        {
            width = Integer.parseInt(args[2]);
            height = Integer.parseInt(args[3]);
        }
        catch (NumberFormatException var25)
        {
            this.sendmessage("lenght or width wasnt a number");
            return false;
        }

        String newname = null;
        NBTTagCompound compound = ServerCloneController.Instance.getCloneData(this.pcParam, name, tab);

        if (compound == null)
        {
            this.sendmessage("Unknown npc");
            return false;
        }
        else
        {
            World world = this.pcParam.getEntityWorld();
            double posX = (double)this.pcParam.getPlayerCoordinates().posX;
            double posY = (double)this.pcParam.getPlayerCoordinates().posY;
            double posZ = (double)this.pcParam.getPlayerCoordinates().posZ;

            if (args.length > 4)
            {
                String x = args[4];
                String[] z;

                if (x.contains(":"))
                {
                    z = x.split(":");
                    x = z[1];
                    world = Utils.getWorld(z[0]);

                    if (world == null)
                    {
                        this.sendmessage(String.format("\'%s\' is an unknown world", new Object[] {z[0]}));
                        return false;
                    }
                }

                if (x.contains(","))
                {
                    z = x.split(",");

                    if (z.length != 3)
                    {
                        this.sendmessage("Location need be x,y,z");
                        return false;
                    }

                    try
                    {
                        posX = CommandBase.func_110666_a(this.pcParam, posX, z[0]);
                        posY = CommandBase.func_110665_a(this.pcParam, posY, z[1].trim(), 0, 0);
                        posZ = CommandBase.func_110666_a(this.pcParam, posZ, z[2]);
                    }
                    catch (NumberFormatException var24)
                    {
                        this.sendmessage("Location should be in numbers");
                        return false;
                    }

                    if (args.length > 5)
                    {
                        newname = args[5];
                    }
                }
                else
                {
                    newname = x;
                }
            }

            if (posX == 0.0D && posY == 0.0D && posZ == 0.0D)
            {
                this.sendmessage("Location needed");
                return false;
            }
            else
            {
                for (int var27 = 0; var27 < width; ++var27)
                {
                    int var28 = 0;

                    while (var28 < height)
                    {
                        Entity entity = EntityList.createEntityFromNBT(compound, world);
                        int xx = MathHelper.floor_double(posX) + var27;
                        int yy = Math.max(MathHelper.floor_double(posY) - 2, 1);
                        int zz = MathHelper.floor_double(posZ) + var28;
                        int npc = 0;

                        while (true)
                        {
                            if (npc < 10)
                            {
                                Block b = world.getBlock(xx, yy + npc, zz);
                                Block b2 = world.getBlock(xx, yy + npc + 1, zz);

                                if (b == null || b2 != null && b2.getCollisionBoundingBoxFromPool(world, xx, yy + npc + 1, zz) != null)
                                {
                                    ++npc;
                                    continue;
                                }

                                yy += npc;
                            }

                            entity.setPosition(posX + 0.5D + (double)var27, (double)(yy + 1), posZ + 0.5D + (double)var28);

                            if (entity instanceof EntityNPCInterface)
                            {
                                EntityNPCInterface var29 = (EntityNPCInterface)entity;
                                var29.ai.startPos = new int[] {xx, yy, zz};

                                if (newname != null && !newname.isEmpty())
                                {
                                    var29.display.name = newname.replaceAll("%", " ");
                                }
                            }

                            world.spawnEntityInWorld(entity);
                            ++var28;
                            break;
                        }
                    }
                }

                return true;
            }
        }
    }
}
