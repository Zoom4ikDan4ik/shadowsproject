package ru.xlv.gun.inventory;

import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.gun.item.ItemWeapon;

public class InventoryGunModTable extends InventoryBasic {

	private ItemStack lastGunStack;
	private boolean busy = false;

	public InventoryGunModTable() {
		super("Gun Modification Table", true, 13);
	}

	public void markDirty() {
		if (!this.busy) {
			ItemStack gunStack = this.getStackInSlot(0);
			if (gunStack != null && gunStack.getItem() instanceof ItemWeapon) {
				if (gunStack != this.lastGunStack) {
					this.busy = true;
					NBTTagCompound nbtTag = gunStack.stackTagCompound;
					if (nbtTag.getCompoundTag("stvol") != null) {
						this.setInventorySlotContents(1, ItemStack.loadItemStackFromNBT(nbtTag.getCompoundTag("stvol")));
						this.setInventorySlotContents(2, ItemStack.loadItemStackFromNBT(nbtTag.getCompoundTag("scope")));
						this.setInventorySlotContents(4, ItemStack.loadItemStackFromNBT(nbtTag.getCompoundTag("grip")));
					}
					this.busy = false;
				} else {
					NBTTagCompound gunTags = gunStack.getTagCompound();
					this.writeAttachmentTags(gunTags, this.getStackInSlot(1), "stvol");
					this.writeAttachmentTags(gunTags, this.getStackInSlot(2), "scope");
					this.writeAttachmentTags(gunTags, this.getStackInSlot(4), "grip");
				}
				this.lastGunStack = gunStack;
			}
		}
	}

	private void writeAttachmentTags(NBTTagCompound attachmentTags, ItemStack attachmentStack, String attachmentName) {
		NBTTagCompound tags = new NBTTagCompound();
		if (attachmentStack != null) {
			attachmentStack.writeToNBT(tags);
		}

		attachmentTags.setTag(attachmentName, tags);
	}

	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return false;
	}
}
