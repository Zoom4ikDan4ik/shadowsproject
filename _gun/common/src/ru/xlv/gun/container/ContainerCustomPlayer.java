package ru.xlv.gun.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.*;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import ru.xlv.gun.inventory.InventoryCustomPlayer;

public class ContainerCustomPlayer extends Container {

	public InventoryCrafting craftMatrix = new InventoryCrafting(this, 2, 2);
	public IInventory craftResult = new InventoryCraftResult();
	private final EntityPlayer thePlayer;

    public boolean func_94530_a(ItemStack p_94530_1_, Slot p_94530_2_)
    {
        return p_94530_2_.inventory != this.craftResult && super.func_94530_a(p_94530_1_, p_94530_2_);
    }
    
    public void onCraftMatrixChanged(IInventory p_75130_1_)
    {
        this.craftResult.setInventorySlotContents(0, CraftingManager.getInstance().findMatchingRecipe(this.craftMatrix, this.thePlayer.worldObj));
    }

	/**
	 * Avoid magic numbers! This will greatly reduce the chance of you making errors
	 * in 'transferStackInSlot' method
	 */
	private static final int ARMOR_START = 5, ARMOR_END = ARMOR_START,
			
			INV_START = 6 , INV_END = INV_START + 26, HOTBAR_START = INV_END + 1,
			HOTBAR_END = HOTBAR_START + 8;

	public ContainerCustomPlayer(EntityPlayer player, InventoryPlayer inventoryPlayer,
                                 InventoryCustomPlayer inventoryCustom) {
		int i;
		this.thePlayer = player;
		// Add CUSTOM slots - we'll just add two for now, both of the same type.
		// Make a new Slot class for each different item type you want to add
		// this.addSlotToContainer(new SlotCustom(inventoryCustom, 0, 80, 8));
		// this.addSlotToContainer(new SlotCustom(inventoryCustom, 1, 80, 26));

		// Add ARMOR slots; note you need to make a public version of SlotArmor
		// just copy and paste the vanilla code into a new class and change what you
		// need

		this.addSlotToContainer(
				new SlotCrafting(inventoryPlayer.player, this.craftMatrix, this.craftResult, 0, 191, 50));

		int g;
		int z;
		for (z = 0; z < 2; ++z) {
			for (g = 0; g < 2; ++g) {
				this.addSlotToContainer(new Slot(this.craftMatrix, g + z * 2, 126 + g * 22, 40 + z * 22));
			}
		}

		this.addSlotToContainer(
				new SlotArmor(player, inventoryPlayer, inventoryPlayer.getSizeInventory() - 1 - 1, 69, 77 + 1 * 19, 1));

		// Add vanilla PLAYER INVENTORY - just copied/pasted from vanilla classes

		int j;
		for (i = 0; i < 3; ++i) {
			for (j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(inventoryPlayer, j + (i + 1) * 9, 26 + j * 22, 136 + i * 22));
			}
		}

		// Add ACTION BAR - just copied/pasted from vanilla classes

		for (i = 0; i < 9; ++i) {
			this.addSlotToContainer(new Slot(inventoryPlayer, i, 26 + i * 22, 207));
		}

	}

	/**
	 * This should always return true, since custom inventory can be accessed from
	 * anywhere
	 */
	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return true;
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or you
	 * will crash when someone does that. Basically the same as every other
	 * container I make, since I define the same constant indices for all of them
	 */
	public ItemStack transferStackInSlot(EntityPlayer player, int index)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
			
			// Either armor slot or custom item slot was clicked
			

		if (index < INV_START)
			{
				// try to place in player inventory / action bar
				if (!this.mergeItemStack(itemstack1, INV_START, HOTBAR_END + 1, true))
				{
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			}
		
		
		
			// Item is in inventory / hotbar, try to place either in custom or armor slots
			else
			{
	
				if (itemstack1.getItem() instanceof ItemArmor)
				{
					int type = ((ItemArmor) itemstack1.getItem()).armorType;
					if(player.getEquipmentInSlot(1) == null || player.getEquipmentInSlot(2) == null || player.getEquipmentInSlot(3) == null || player.getEquipmentInSlot(4) == null ){
						if (!this.mergeItemStack(itemstack1, ARMOR_START + type, ARMOR_START + type + 1, true) && slot.getStack() == null)
						{
							return null;
						}
					}
				
			
				
	
				else {
					if (index >= HOTBAR_START && index <=HOTBAR_END)
					{
						if (!this.mergeItemStack(itemstack1, INV_START, INV_END, false))
						{
							return null;
						}
		
					
					} else if (index >= INV_START && index < HOTBAR_START)
					{
						// place in action bar
						if (!this.mergeItemStack(itemstack1, HOTBAR_START, HOTBAR_END +1 , false))
						{
							return null;
						}
					}
					
				}
				
				}
				// item in player's inventory, but not in action bar
				else if (index >= INV_START && index < HOTBAR_START)
				{
					// place in action bar
					if (!this.mergeItemStack(itemstack1, HOTBAR_START, HOTBAR_END +1 , false))
					{
						return null;
					}
				}
				// item in action bar - place in player inventory
				else if (index >= HOTBAR_START && index <=HOTBAR_END)
				{
					if (!this.mergeItemStack(itemstack1, INV_START, INV_END, false))
					{
						return null;
					}
	
				}
			}

			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(player, itemstack1);
		}

		return itemstack;
	}
}