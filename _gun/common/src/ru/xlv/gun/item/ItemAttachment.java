package ru.xlv.gun.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

import java.util.List;

public class ItemAttachment extends Item {

	private String icon;
	public float despread;
	public float derecoil;
	public float dedamage;
	public float zoom;
	public AttachmentType etype;

	public ItemAttachment(String icon, float despread, float derecoil, float dedamage, float zoom, AttachmentType etype) {
		this.icon = icon;
		this.despread = despread;
		this.zoom = zoom;
		this.derecoil = derecoil;
		this.dedamage = dedamage;
		this.etype = etype;
		this.setMaxStackSize(1);
		this.setCreativeTab(Tabs.tabAttachments);
	}

	public void addInformation(ItemStack stack, EntityPlayer player, List lines, boolean advancedTooltips) {
		lines.add(EnumChatFormatting.GREEN + "Место обвеса: " + StatCollector.translateToLocal("attach" + etype.name()));
		if (despread != 0F)
			lines.add(EnumChatFormatting.WHITE + "Снижение разброса на: " + EnumChatFormatting.YELLOW + this.despread);
		if (derecoil != 0F)
			lines.add(EnumChatFormatting.WHITE + "Снижение отдачи на: " + EnumChatFormatting.YELLOW + this.derecoil);
		if (dedamage != 0F) {
			lines.add(EnumChatFormatting.WHITE + "Снижение урона на: " + EnumChatFormatting.YELLOW + this.dedamage);
		}
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list) {
		ItemStack gunStack = new ItemStack(item, 1, 0);
		NBTTagCompound tags = new NBTTagCompound();
		gunStack.stackTagCompound = tags;
		list.add(gunStack);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister icon) {
		itemIcon = icon.registerIcon("batmod:" + this.icon);
	}
}