package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;

public class PacketDetach implements IMessage {

	public String attachname;

	public PacketDetach() {
	}

	public PacketDetach(String attachname) {
		this.attachname = attachname;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, attachname);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		attachname = ByteBufUtils.readUTF8String(buf);
	}
}