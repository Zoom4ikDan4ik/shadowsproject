package ru.xlv.gun.network.packets;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;

public class Message_CloseItems implements IMessage {
    public int entityID;
    public int playerID;

    public Message_CloseItems() {
    }

    public Message_CloseItems(int entityID, int playerID) {
        this.entityID = entityID;
        this.playerID = playerID;
    }

    public void toBytes(ByteBuf out) {
        out.writeInt(this.entityID);
        out.writeInt(this.playerID);
    }

    public void fromBytes(ByteBuf in) {
        this.entityID = in.readInt();
        this.playerID = in.readInt();
    }

    public static class Handler implements IMessageHandler<Message_CloseItems, IMessage> {
        public IMessage onMessage(Message_CloseItems message, MessageContext ctx) {
            if(ctx.side == Side.SERVER) {
                MinecraftServer server = MinecraftServer.getServer();
                if (server.getEntityWorld().getEntityByID(message.playerID) != null && server.getEntityWorld().getEntityByID(message.entityID) != null && server.getEntityWorld().getEntityByID(message.playerID) instanceof EntityPlayer) {
                    EntityPlayer thePlayer = (EntityPlayer) server.getEntityWorld().getEntityByID(message.playerID);
                    EntityItem theItem = (EntityItem) server.getEntityWorld().getEntityByID(message.entityID);
                    theItem.setDead();
                    thePlayer.worldObj.playSoundEffect(thePlayer.posX, thePlayer.posY, thePlayer.posZ, "random.pop", 1.0f, 1.0f);
                    thePlayer.inventory.addItemStackToInventory(theItem.getEntityItem());
                    thePlayer.inventoryContainer.detectAndSendChanges();

                }
            }
            return null;
        }
    }
}
