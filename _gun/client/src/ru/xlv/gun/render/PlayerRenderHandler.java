package ru.xlv.gun.render;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.xlv.gun.ClientEventHandler;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.network.PacketHandler;
import ru.xlv.gun.network.packets.Message_CloseItems;
import ru.xlv.gun.render.armor.IArmorRenderer;
import ru.xlv.gun.util.CPlayerInfo;

import java.util.ArrayList;

public class PlayerRenderHandler {

	private static Minecraft mc = Minecraft.getMinecraft();
	private static int tick;
	public static boolean isKilled = false;
	public static int healthreal;
	public static int healthsmooth;
	private static boolean toggleModeEnabled = false;
	private static boolean isOverviewInitialPress = false;
	private static boolean overviewEnabled = false;
	public static float prevhealth;
	public static float health;
	public static float food;
	public static float thisfood;
	public static RenderItem itemRenderer = new RenderItem();

	public static float scaleCrosshair;
	public static float scaleCrosshairGlobal;
	public static float gunSpread = 0;

	public float scale1 = 0;


	public PlayerRenderHandler() {
	}

	@SubscribeEvent
	public void crossHireCalc(final TickEvent.PlayerTickEvent event) {
		if (event.side == Side.CLIENT) {
			if (scaleCrosshair > 0.1) {
				scaleCrosshair -= 0.1;
			}
			if (Minecraft.getMinecraft().thePlayer.getHeldItem() != null && Minecraft.getMinecraft().thePlayer.getHeldItem().getItem() instanceof ItemWeapon && !ClientEventHandler.aiming) {
				NBTTagCompound nbt = Minecraft.getMinecraft().thePlayer.getHeldItem().getTagCompound();
				if (nbt != null) {
					gunSpread = (((ItemWeapon) Minecraft.getMinecraft().thePlayer.getHeldItem().getItem()).getSpread() - nbt.getCompoundTag("att").getFloat("despread"));
				} else {
					gunSpread = 0;
				}
			}
			scaleCrosshairGlobal = (CPlayerInfo.playerIsRun() ? 1 : 0) + (CPlayerInfo.playerIsCrawl() ? -1 : 0) + (CPlayerInfo.playerIsSneaking() ? -0.4f : 0);
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onInGameUI(RenderGameOverlayEvent e) {
		if (e.type == ElementType.ALL) {
			RenderHelper.disableStandardItemLighting();
			drawItems(e);
			RenderHelper.disableStandardItemLighting();
		}
	}

	private void drawItems(RenderGameOverlayEvent e) {
		if (e.type == ElementType.ALL) {
			if (Minecraft.getMinecraft().objectMouseOver != null && Minecraft.getMinecraft().objectMouseOver.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK) {
				Vec3 hitVec = mc.objectMouseOver.hitVec;
				EntityClientPlayerMP player = mc.thePlayer;
				double playerX = player.posX;
				double playerY = player.posY;
				double playerZ = player.posZ;
				WorldClient world = mc.theWorld;
				double segLen = 0.25;
				double segLend2 = 0.125;
				double dx = hitVec.xCoord - playerX;
				double dy = hitVec.yCoord - playerY;
				double dz = hitVec.zCoord - playerZ;
				double lineLen = Math.sqrt(Math.pow(dx, 2.0) + Math.pow(dy, 2.0) + Math.pow(dz, 2.0));
				double segNumDouble = lineLen / segLen;
				int segNum = (int) segNumDouble;
				ArrayList<EntityItem> items = null;
				int index = 0;
				while (++index <= segNum) {
					double cenX = playerX + dx / segNumDouble * (double) index;
					double cenY = playerY + dy / segNumDouble * (double) index;
					double cenZ = playerZ + dz / segNumDouble * (double) index;
					AxisAlignedBB curAABB = AxisAlignedBB.getBoundingBox(cenX - segLend2, cenY - segLend2, cenZ - segLend2, cenX + segLend2, cenY + segLend2, cenZ + segLend2);
					items = (ArrayList) world.getEntitiesWithinAABB(EntityItem.class, curAABB);
					if (items == null || items.isEmpty()) continue;
					index = segNum + 1;
				}
				if (items != null && !items.isEmpty()) {
					EntityItem closeItem = items.get(0);
					double closeDist = 100.0;
					for (EntityItem item : items) {
						double itemDist = Math.pow(item.posX - playerX, 2.0) + Math.pow(item.posY - playerY, 2.0) + Math.pow(item.posZ - playerZ, 2.0);
						if (itemDist >= closeDist) continue;
						closeDist = itemDist;
						closeItem = item;
					}
					String text = closeItem.getEntityItem().getDisplayName();
					String text2 = Keyboard.getKeyName(Keyboard.KEY_F);
					Minecraft.getMinecraft().fontRenderer.drawString("Чтобы подобрать", e.resolution.getScaledWidth() / 2 + 5, e.resolution.getScaledHeight() / 2 + 4, 0xFFFFFF);
					Minecraft.getMinecraft().fontRenderer.drawString("нажми", e.resolution.getScaledWidth() / 2 + 5, e.resolution.getScaledHeight() / 2 + 14, 0xFFFFFF);
					Minecraft.getMinecraft().fontRenderer.drawString("[ " + text2 + " ]", e.resolution.getScaledWidth() / 2 + 5, e.resolution.getScaledHeight() / 2 + 24, 0xFFFFFF);
					Minecraft.getMinecraft().fontRenderer.drawString(text, (e.resolution.getScaledWidth()) / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(text) - 1, e.resolution.getScaledHeight() / 2 + 24, 0xFFFFFF);
					//      renderInventorySlot(closeItem.getEntityItem(), e.resolution.getScaledWidth() / 2 - 18, e.resolution.getScaledHeight() / 2 + 4, 0f);
				}
			}
		}
	}

	@SubscribeEvent
	public void onWorldRender(RenderWorldLastEvent e) {
		EntityPlayer player = Minecraft.getMinecraft().thePlayer;
		if (RenderManager.debugBoundingBox && !player.capabilities.isCreativeMode) {
			RenderManager.debugBoundingBox = false;
		}
		if (mc != null && mc.objectMouseOver != null && mc.theWorld != null) {
			Block b = mc.theWorld.getBlock(mc.objectMouseOver.blockX, mc.objectMouseOver.blockY, mc.objectMouseOver.blockZ);
			int side = mc.objectMouseOver.sideHit;
			if (b.equals(Blocks.wooden_door)) {
				GL11.glPushMatrix();
				GL11.glTranslated(-RenderManager.renderPosX, -RenderManager.renderPosY, -RenderManager.renderPosZ);
				GL11.glTranslated(mc.objectMouseOver.hitVec.xCoord, mc.objectMouseOver.hitVec.yCoord, mc.objectMouseOver.hitVec.zCoord);
				GL11.glScalef(0.025f, 0.025f, 0.025f);
				GL11.glRotatef(180, 0, 0, 1);
				if (side == 1)
					GL11.glRotatef(-90, 1, 0, 0);
				if (side == 0)
					GL11.glRotatef(90, 1, 0, 0);
				if (side == 4)
					GL11.glRotatef(-90, 0, 1, 0);
				if (side == 5)
					GL11.glRotatef(90, 0, 1, 0);
				if (side == 3)
					GL11.glRotatef(180, 0, 1, 0);
				GL11.glTranslatef(0, 0, -0.1f);
				String out = "[F]";
				mc.fontRenderer.drawString(out, -mc.fontRenderer.getStringWidth(out) / 2 + 1, 3, 0xffffff);
				GL11.glPopMatrix();
			}
		}
	}

	@SubscribeEvent
	public void onClientTick(ClientTickEvent e) {
		EntityClientPlayerMP player = mc.thePlayer;
		if (player != null) {
			EntityClientPlayerMP mcp = mc.thePlayer;
			health = (int) mcp.getHealth() * 100;
			if (health != prevhealth) {
				if (prevhealth < health) {
					prevhealth = prevhealth + 200f;
				}
				if (prevhealth > health) {
					prevhealth = prevhealth - 200f;
				}
			}
			thisfood = (float) mcp.getFoodStats().getFoodLevel() * 10 * 5;
			if (thisfood != food) {
				if (food < thisfood) {
					food = food + 1f;
				}
				if (food > thisfood) {
					food = food - 1f;
				}
			}
		}
	}

	@SubscribeEvent
	public void event(PlayerEvent.PlayerRespawnEvent e) {
		isOverviewInitialPress = false;
		toggleModeEnabled = false;
		overviewEnabled = false;
	}

	@SubscribeEvent
	public void onPlayerRenderTick(RenderPlayerEvent.Specials.Post event) {
		GL11.glColor4f(1f, 1f, 1f, 1f);
		if (event.entityPlayer != null) {
			EntityPlayer player = (EntityPlayer) event.entityLiving;
			if (!player.isPotionActive(Potion.invisibility)) {
				ItemStack armor = player.getCurrentArmor(2);
				if (armor != null) {
					IArmorRenderer iArmorRenderer = IArmorRenderer.registry.get(armor.getItem().getClass());
					if(iArmorRenderer != null) {
						RenderHelper.disableStandardItemLighting();
						iArmorRenderer.render(event.renderer);
						RenderHelper.enableStandardItemLighting();
					}
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST, receiveCanceled = true)
	public void onKeyInput(KeyInputEvent e) {
		if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
			if (mc.theWorld.isRemote && mc.objectMouseOver != null && mc.objectMouseOver.hitVec != null) {
				Vec3 hitVec = mc.objectMouseOver.hitVec;
				EntityClientPlayerMP player = mc.thePlayer;
				double playerX = player.posX;
				double playerY = player.posY;
				double segLen = 0.25;
				double segLend2 = 0.125;
				double playerZ = player.posZ;
				double dx = hitVec.xCoord - playerX;
				double dy = hitVec.yCoord - playerY;
				double dz = hitVec.zCoord - playerZ;
				double lineLen = Math.sqrt(Math.pow(dx, 2.0) + Math.pow(dy, 2.0) + Math.pow(dz, 2.0));
				double segNumDouble = lineLen / segLen;
				int segNum = (int) segNumDouble;
				World world = player.getEntityWorld();
				ArrayList<EntityItem> items = null;
				int index = 0;
				while (++index <= segNum) {
					double cenX = playerX + dx / segNumDouble * (double) index;
					double cenY = playerY + dy / segNumDouble * (double) index;
					double cenZ = playerZ + dz / segNumDouble * (double) index;
					AxisAlignedBB curAABB = AxisAlignedBB.getBoundingBox(cenX - segLend2, cenY - segLend2, cenZ - segLend2, cenX + segLend2, cenY + segLend2, cenZ + segLend2);
					items = (ArrayList) world.getEntitiesWithinAABB(EntityItem.class, curAABB);
					if (items == null || items.isEmpty()) continue;
					index = segNum + 1;
				}
				if (items != null && !items.isEmpty()) {
					EntityItem closeItem = items.get(0);
					double closeDist = 100.0;
					for (EntityItem item : items) {
						double itemDist = Math.pow(item.posX - playerX, 2.0) + Math.pow(item.posY - playerY, 2.0) + Math.pow(item.posZ - playerZ, 2.0);
						if (itemDist >= closeDist) continue;
						closeDist = itemDist;
						closeItem = item;
					}
					PacketHandler.INSTANCE.sendToServer(new Message_CloseItems(closeItem.getEntityId(), player.getEntityId()));
				}
			}
		}
	}
}
