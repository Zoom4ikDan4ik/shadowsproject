package ru.xlv.gun.render.guns.rifle;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.*;
import ru.xlv.gun.util.Utils;

import java.util.Random;

public class OC14Render implements IItemRenderer {
	
	
	
	
	//public static final IModelCustom model = spproject.loaderModel.Modell.loadModel(new ResourceLocation("batmod", "models/guns/ak74.sp"));
	
    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/groza/texture.png");
   
    private boolean livingdown = true;
    private static float run4Progress = 0F, lastRun4Progress = 0F;
    private int list;
    private int magL;
    RenderWeaponThings rWT = new RenderWeaponThings();
    private int gripL;
    private static RenderAttachments renderAtt;
    ModelSmoke model_smoke = new ModelSmoke();
	private static ModelHand model1;
	private ItemWeapon weapon;
	private static boolean aiming;
	private boolean shooting;
	Random rand = new Random();
	Minecraft mc = Minecraft.getMinecraft();
	private boolean reloading;
	private static float runProgress = 0F, lastRunProgress = 0F;
	private static float run1Progress = 0F, lastRun1Progress = 0F;
	private static float run2Progress = 0F, lastRun2Progress = 0F;
	private static float run3Progress = 0F, lastRun3Progress = 0F;
	
	
	
	public OC14Render() {
		model1 = new ModelHand();
		renderAtt = new RenderAttachments();
		IModelCustom model = ModelLoader.loadModel(new ResourceLocation("batmod", "models/guns/groza/model.sp"));
        list = GL11.glGenLists(1);
        GL11.glNewList(list, GL11.GL_COMPILE);
        //ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod", "textures/models/guns/bulletak.png"));
        model.renderAll();
        GL11.glEndList();
       
        
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		if ((FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem() != null) && (FMLClientHandler.instance().getClientPlayerEntity().inventory.getCurrentItem().getItem() instanceof ItemWeapon) && (Mouse.isButtonDown(1)) && (!FMLClientHandler.instance().getClientPlayerEntity().isSprinting() && (FMLClientHandler.instance().getClient().currentScreen == null))) {
			aiming = true;
		} else {
			aiming = false;
		}
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;

		switch (type) {
		
		case EQUIPPED_FIRST_PERSON: {
			doAnimations();
 			if(shootTimer == 1){
			GL11.glPushMatrix();
			GL11.glRotated(1.0F*shootAnim, 1.0, 0.0, 1.0); 
			GL11.glTranslatef(-0.9F * aimAnim, -0.1F * aimAnim, 0.150F * aimAnim);
			GL11.glScalef(0.15F, 0.15F, 0.15F);
			GL11.glRotatef(135, 0, 1, 0);
			GL11.glTranslatef(1.6F, 8.5F, 11F);
			rWT.flash();
			GL11.glPopMatrix();
			}		
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(is.getTagCompound().getString("scope").equals("Прицел ИК ПНВ")){
				gameSettings.fovSetting = 90 - 50 * aimAnim;
				x = 0.134F;
				y = -0.136F;
				z = -0.136F;
				
			}
			if(is.getTagCompound().getString("scope").equals("Коллиматор")){
				gameSettings.fovSetting = 90 -20 * aimAnim;
				x = 0.134F;
				y = -0.154F;
				z = -0.136F;
				
			}
			if(is.getTagCompound().getString("scope").equals("4х опт. прицел")){
				gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = 0.104F;
				y = -0.123F;
				z = -0.103F;
				
			}
			float dSWS = 1F;
			float f1 = 1F;
			if((is.getTagCompound().getString("scope") != null || is.getTagCompound().getString("planka") != null) && aiming){
			   dSWS = 0.4F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!aiming)
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(50*runAnim, 0, 1, 0);
			GL11.glRotatef(25*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.2F*runAnim, 0.2F*runAnim, -1.0F*runAnim);
			GL11.glRotated(2F*aimAnim, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.753F * aimAnim+ x*aimAnim, -0.045F * aimAnim + y*aimAnim, 0.004F * aimAnim + z*aimAnim);
			if(aiming){
				GL11.glRotated(2F*shootAnim * dSWS, 1.0, 0.0, 1.0);
				GL11.glTranslatef(0, -0.03F * shootAnim * dSWS, 0);
				}
				else
				GL11.glRotated(1.5F*shootAnim * dSWS, 1.0, 0.0, 1.0);
			GL11.glTranslatef(-0.085F * shootAnim*dSWS, 0.01F*shootAnim, 0.084F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			
			GL11.glScalef(1.05F, 1.05F, 1.05F);
			ru.xlv.core.util.Utils.bindTexture(tex);
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.6F, 0.59F, -0.165F);
			GL11.glDisable(GL11.GL_CULL_FACE);
			GL11.glCallList(list);
			GL11.glDisable(GL11.GL_CULL_FACE);
			
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			rWT.renderRightArm(0.5F, -1.0F, 0.8F, -90, -5, -35, 1.6F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.6F, -0.48F, -0.22F, 10, 25, -80, 2F);
			GL11.glPopMatrix();
			if(is.getTagCompound().getString("stvol").equals("Кустарный глушитель")){
			     GL11.glPushMatrix();
				 GL11.glRotatef(45, 0, 1, 0);
				 GL11.glScalef(0.07F, 0.07F, 0.07F);
				 GL11.glTranslatef(38.7F, -0.3F, -2.8F);
				 renderAtt.renderPBS();
				 GL11.glPopMatrix();
				
			}
		    if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
			     GL11.glPushMatrix();

				 GL11.glRotatef(45, 0, 1, 0);
			    
				 GL11.glScalef(0.07F, 0.07F, 0.07F);
				 GL11.glTranslatef(10.7F, -0.6F, -3.9F);
				 renderAtt.renderLaser(type);
				 GL11.glPopMatrix();
				
			}
			
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.2F, -0.1F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(tex);
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0, 0, 0);
		GL11.glCallList(list);
		
		if(is.getTagCompound().getString("stvol").equals("Кустарный глушитель")){	
			 GL11.glPushMatrix();
			 GL11.glTranslatef(1.45F, -0.79F, -0.01F);
			 GL11.glScalef(0.07F, 0.07F, 0.07F);
			 renderAtt.renderPBS();
			 GL11.glPopMatrix();
			
		}	
		
		if(AttachmentType.checkAttachment(is,"grip", Items.laser)){
			 GL11.glPushMatrix();
			 GL11.glScalef(0.07F, 0.07F, 0.07F);
			 GL11.glTranslatef(13.55F, -9.44F, -1.2F);
			 GL11.glScalef(0.91F, 0.91F, 0.91F);
			 renderAtt.renderLaser(type);
			 GL11.glPopMatrix();
		}
		
		
		GL11.glPopMatrix();
    }
    
    public void doAnimations(){
    	ItemStack stack = this.mc.thePlayer.getCurrentEquippedItem();
    	if(stack != null && stack.getItem() instanceof ItemWeapon){
		
    	ItemWeapon weapon = (ItemWeapon)stack.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		lastRun1Progress = run1Progress;
		if(!FMLClientHandler.instance().getClientPlayerEntity()
				.isSprinting()){
			run1Progress *= 0.72F;
		}
		else if (stack.getTagCompound().getFloat("reloadTime") < 5F){
			run1Progress = 1F - (1F - run1Progress) * 0.85F;
		}
		
		lastRunProgress = runProgress;
		if(!aiming){
			runProgress *= 0.82F;
		}
		else if (stack.getTagCompound().getFloat("reloadTime") < 5F){
			runProgress = 1F - (1F - runProgress) * 0.75F;
		}
		
		lastRun2Progress = run2Progress;
		if(shootTimer == 1){
			
			run2Progress = 1F - (1F - run2Progress) * 0.5F;
		}
		else{
			run2Progress *= 0.5F;
		}
		
		lastRun3Progress = run3Progress;
		if(stack != null && stack.getItem() instanceof ItemWeapon && !(stack.getTagCompound().getFloat("reloadTime") > 6F)){
			run3Progress *= 0.75F;
		}
		else{
			run3Progress = 1F - (1F - run3Progress) * 0.8F;
		}
		lastRun4Progress = run4Progress;
		if(livingdown){
			run4Progress -= 0.008F;
			if(run4Progress < 0.045F){
				livingdown = false;
			}
		}
		else{
			run4Progress += 0.008F;
			if(run4Progress > 0.9F){
				livingdown = true;
			}
		}
    	}
	}
}

