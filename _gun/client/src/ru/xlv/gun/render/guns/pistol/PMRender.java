package ru.xlv.gun.render.guns.pistol;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.WavefrontObject;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;
import ru.xlv.gun.item.AttachmentType;
import ru.xlv.gun.item.ItemWeapon;
import ru.xlv.gun.item.Items;
import ru.xlv.gun.render.ModelWrapperDisplayList;
import ru.xlv.gun.render.RenderAttachments;
import ru.xlv.gun.render.RenderWeaponThings;
import ru.xlv.gun.util.Utils;

public class PMRender implements IItemRenderer {
	
	
	
	
	//public static final IModelCustom model = spproject.loaderModel.Modell.loadModel(new ResourceLocation("batmod", "models/guns/ak74.sp"));
	
    public static final ResourceLocation tex = new ResourceLocation("batmod", "models/guns/pm/texture.png");
   
    private static RenderAttachments renderAtt;
   
	Minecraft mc = Minecraft.getMinecraft();
	RenderWeaponThings rWT = new RenderWeaponThings();
	IModelCustom model;
	
	public PMRender() {
		
		renderAtt = new RenderAttachments();
		this.model = ModelLoader.loadModel(new ResourceLocation("batmod:models/guns/pm/model.sp"));
        this.model = new ModelWrapperDisplayList((WavefrontObject)this.model);
	}
	public boolean handleRenderType(ItemStack is, ItemRenderType type) {
		switch (type) {
		case ENTITY:
			return true;
		case EQUIPPED:
			return true;
		case EQUIPPED_FIRST_PERSON:
			return true;
		default:
			return false;
		}
	}
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack is, ItemRendererHelper helper) {
		switch (type){
		case EQUIPPED_FIRST_PERSON: return true;
		case ENTITY:
			return false;
		case EQUIPPED:
			return false;
		default:
			return false;
		}
		
	}


    
    
	
	
	public void renderItem(ItemRenderType type, ItemStack is, Object ... data) {
		
		GameSettings gameSettings = FMLClientHandler.instance().getClient().gameSettings;
		
		ItemWeapon weapon = (ItemWeapon)is.getItem();
		int shootTimer = (int)weapon.otdachaTimer;
		//анимация прицеливания
		//анимация бега
		//анимация выстрела
		//анимация перезарядки
	    //анимация ливинг
		float aimAnim = rWT.lastRunProgress + (rWT.runProgress - rWT.lastRunProgress) * RenderWeaponThings.renderTicks;
	
		float runAnim = rWT.lastRun1Progress + (rWT.run1Progress - rWT.lastRun1Progress)* RenderWeaponThings.renderTicks;
	
		float shootAnim = rWT.lastRun2Progress + (rWT.run2Progress - rWT.lastRun2Progress)* RenderWeaponThings.renderTicks;

	    float reloadAnim = rWT.lastRun3Progress + (rWT.run3Progress - rWT.lastRun3Progress)* RenderWeaponThings.renderTicks;
	 
	    float livingAnim = rWT.lastRun4Progress + (rWT.run4Progress - rWT.lastRun4Progress)* RenderWeaponThings.renderTicks;

		switch (type) {
		
		case EQUIPPED_FIRST_PERSON: {
			
			rWT.doAnimations();
 			
			float x = 0;
			float y = 0;
			float z = 0;
			GL11.glPushMatrix();
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
			if(AttachmentType.checkAttachment(is,"scope", Items.pkas)){
				gameSettings.fovSetting = 90 - 20 * aimAnim;
				x = -0.055F;
				y = -0.162F;
				z = 0.063F;
				
			}
			if(is.getTagCompound().getString("scope").equals("Коллиматор")){
				gameSettings.fovSetting = 90 -20 * aimAnim;
				x = 0.134F;
				y = -0.154F;
				z = -0.136F;
				
			}
			if(is.getTagCompound().getString("scope").equals("4х опт. прицел")){
				gameSettings.fovSetting = 90 - 60 * aimAnim;
				x = 0.104F;
				y = -0.123F;
				z = -0.103F;
				
			}
			float dSWS = 1F;
			float f1 = 1F;
			if(Utils.isPlayerAiming()){
			   dSWS = 0.4F;
			   f1 = 1F;
			}
			float fov = gameSettings.fovSetting/10000;
			if(!Utils.isPlayerAiming())
				GL11.glTranslatef(0,0.05F*livingAnim - aimAnim/30,0);
			else
				GL11.glTranslatef(0,-0.6F*livingAnim*fov,0);
			GL11.glRotatef(65*reloadAnim, 0, 0, 1);
			GL11.glRotatef(-25*runAnim, 1, 0, 0);
			GL11.glRotatef(-15*runAnim, 0, 0, 1);
			GL11.glTranslatef(-0.4F*runAnim, -0.8F*runAnim, 0.5F*runAnim);
			GL11.glRotated(0F*aimAnim, 0.0, 1.0, 0.0);
			GL11.glTranslatef(-0.6F * aimAnim+ x*aimAnim, 0.27F * aimAnim + y*aimAnim, 0.041F * aimAnim + z*aimAnim);
			GL11.glRotated(4.0F*shootAnim*dSWS, 1.0, 0.0, 1.0);
			GL11.glRotated(0.1F*shootAnim*dSWS, 0.0, 1.0, 0.0);
			GL11.glTranslatef(-0.12F * shootAnim*dSWS, 0.14F*shootAnim*dSWS, 0.12F * shootAnim*dSWS);
			
			GL11.glPushMatrix();
			GL11.glScalef(1.08F, 1.08F, 1.08F);
			
			GL11.glRotated(0.0, 1.0, 0.0, 0.0);
			GL11.glRotated(45.0, 0.0, 1.0, 0.0);
			GL11.glRotated(0.0, 0.0, 0.0, 1.0);
			//GL11.glTranslatef(-0.4F * aimAnim, 0.203F * aimAnim, -0.570F * aimAnim);
			GL11.glTranslatef(-0.17F, 0.72F, -0.275F);
			renderAttachments(type, is);
			ru.xlv.core.util.Utils.bindTexture(tex);
			model.renderAll();
			if(shootTimer == weapon.shootSpeed-1){
				GL11.glPushMatrix();
				GL11.glScalef(0.1F, 0.1F, 0.1F);
				GL11.glRotatef(90, 0, 1, 0);
				GL11.glTranslatef(0.5F, 6F, 10F);
				rWT.flash();
				GL11.glPopMatrix();
			}	
			//if(CheckAttachments.checkAttachment(is,"scope",Items.pkas)){renderAtt.renderPKAS(aiming);}
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); 
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			
			GL11.glPopMatrix();
			
			GL11.glPushMatrix();
			rWT.renderRightArm(0.64F, -1.05F, 1.06F, -78, -15, -25, 2.3F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			rWT.renderLeftArm(-0.8F, -0.64F, -0.02F, 10, 5, -50, 2.3F);
			GL11.glPopMatrix();
			
			GL11.glPopMatrix();
		}
		break;
		case EQUIPPED: {
			GL11.glRotatef(35.0F, 1.0F, 0.0F, 0.0F);
		    GL11.glRotatef(-73.0F, 0.0F, 1.0F, 0.0F);
		    GL11.glRotatef(21.0F, 0.0F, 0.0F, 1.0F);
		    GL11.glTranslatef(0.15F, -0.08F, -0.5F);
	        GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
	        
			renderEntityAndEquipped(type, is);
		}
		break;
		case ENTITY: {
			
			renderEntityAndEquipped(type, is);
		}
		break;
		default:
			break;
		}
		
		
	}
	
	public void renderAttachments(ItemRenderType type, ItemStack is){
		if(AttachmentType.checkAttachment(is,"stvol", Items.osprey)){
			 GL11.glPushMatrix();
			 GL11.glScalef(0.75F, 0.75F, 0.75F);
			 GL11.glTranslatef(-1.45F, 0.145F, -0.04F);
			 renderAtt.renderOsprey();
			 GL11.glPopMatrix();
			
		}	
	}
	
    public void renderEntityAndEquipped(ItemRenderType type, ItemStack is){
    	
    	GL11.glPushMatrix();
		
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
		
		GL11.glTranslatef(0, 0, 0);
		NBTTagCompound tagz = is.getTagCompound();
		if(tagz != null) {	
			if(tagz.getString("scope").length() > 0 || tagz.getString("stvol").length() > 0 || tagz.getString("laser").length() > 0) {
				renderAttachments(type, is);
			}
		}
		ru.xlv.core.util.Utils.bindTexture(tex);
		model.renderAll();
		
		GL11.glPopMatrix();
    }

}
