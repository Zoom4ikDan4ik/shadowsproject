package ru.xlv.gun.render.armor;

import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.krogenit.dds_loader.TextureLoaderDDS;
import ru.krogenit.model_loader.Model;
import ru.krogenit.model_loader.ModelLoader;
import ru.krogenit.shaders.KrogenitShaders;

public class RenderTestArmor implements IArmorRenderer {

    private static int pantsLeftLegArmor1 = -1, pantsRightLegArmor1 = -1;
    private static int helmetArmor1 = -1;
    private static int bootsLeftBootArmor1 = -1, bootsRightBootArmor1 = -1;
    private static int chestplateBodyArmor1 = -1, chestplateLeftHandArmor1 = -1, chestplateRightHandArmor1 = -1;

    private static Model model;

    public static ResourceLocation texture_normal_dds, texture, specular, emission;

    @Override
    public void init() {
        texture_normal_dds = new ResourceLocation("textures/models/armor/test/normal.dds");
        texture = new ResourceLocation("textures/models/armor/test/2048.dds");
        specular = new ResourceLocation("textures/models/armor/test/specular.dds");
        emission = new ResourceLocation("textures/models/armor/test/emission4.dds");
//        TextureLoaderDDS.loadTexture(texture_normal_dds = new ResourceLocation("textures/models/armor/test/normal.dds"));
//        TextureLoaderDDS.loadTexture(texture = new ResourceLocation("textures/models/armor/test/2048.dds"));
//        TextureLoaderDDS.loadTexture(specular = new ResourceLocation("textures/models/armor/test/specular.dds"));
//        TextureLoaderDDS.loadTexture(emission = new ResourceLocation("textures/models/armor/test/emission4.dds"));
        model = new Model(new ResourceLocation("sp:models/armor/medic.sp"));
        ModelLoader.loadModels();
    }

    @Override
    public void render(RenderPlayer renderPlayer) {
//		ResourceLocation normalh = new ResourceLocation("sp", "models/armor/Helmet_N.png");
//		ResourceLocation colorh = new ResourceLocation("sp", "models/armor/Helmet-ID-1k.png");

//		if(Minecraft.getMinecraft().thePlayer.isSneaking() || model == null) {//assets/sp/models/armor/medic.sp
//			System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
//			model = new Model(new ResourceLocation("sp:models/armor/medic.sp"));
//			ModelLoader.loadModels();
//			pantsLeftLegArmor1 = -1;
//			pantsRightLegArmor1 = -1;
//			helmetArmor1 = -1;
//			bootsLeftBootArmor1 = -1;
//			bootsRightBootArmor1 = -1;
//			chestplateBodyArmor1 = -1;
//			chestplateLeftHandArmor1 = -1;
//			chestplateRightHandArmor1 = -1;
//		}
        if((Keyboard.isKeyDown(Keyboard.KEY_B))) {
            System.out.println("ASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
            init();
            pantsLeftLegArmor1 = -1;
            pantsRightLegArmor1 = -1;
            helmetArmor1 = -1;
            bootsLeftBootArmor1 = -1;
            bootsRightBootArmor1 = -1;
            chestplateBodyArmor1 = -1;
            chestplateLeftHandArmor1 = -1;
            chestplateRightHandArmor1 = -1;
        }
        if (pantsLeftLegArmor1 == -1) {
//			pantsLeftLegArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(pantsLeftLegArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("Lleg");
//			model.renderPart("Lleg");
//			GL11.glEndList();
        }
        if (pantsRightLegArmor1 == -1) {
//			pantsRightLegArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(pantsRightLegArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("Rleg");
//			model.renderPart("Rleg");
//			GL11.glEndList();
        }
        GL11.glPushMatrix();

        renderPlayer.modelBipedMain.bipedLeftLeg.postRender(0.0625F);
        GL11.glRotatef(180.0F, 0.1F, 0.0F, 0.0F);
        GL11.glTranslatef(-0.13F, -0.75F, 0.0F);
        GL11.glScalef(1.05F, 1F, 1.05F);

        KrogenitShaders.forwardDirect.enable();
        KrogenitShaders.enableBloom();
        KrogenitShaders.forwardDirect.setOldRender(false);
        KrogenitShaders.forwardDirect.setNormalMapping(true);
        KrogenitShaders.forwardDirect.setSpecularMapping(true);
        KrogenitShaders.forwardDirect.setEmissionMapping(true);
        KrogenitShaders.forwardDirect.setEmissionPower(5f);

//		glActiveTexture(GL13.GL_TEXTURE0);
//		ru.xlv.core.util.Utils.bindTexture(texture);
//		glActiveTexture(GL_TEXTURE2);
//		ru.xlv.core.util.Utils.bindTexture(texture_normal_dds);
        TextureLoaderDDS.bindTexture(texture);
        TextureLoaderDDS.bindNormalMap(texture_normal_dds);
        TextureLoaderDDS.bindSpecularMap(specular);
        TextureLoaderDDS.bindEmissionMap(emission);
        model.renderPart("LLeg");
//		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("sp", "textures/models/armor/medic/BBS_cbkjdfz_Normal.png"));


//		model.render();
//		GL11.glCallList(pantsLeftLegArmor1);
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        GL11.glDisable(GL11.GL_BLEND);
        renderPlayer.modelBipedMain.bipedRightLeg.postRender(0.0625F);
        GL11.glRotatef(180.0F, 0.1F, 0.0F, 0.0F);
        GL11.glTranslatef(0.13F, -0.75F, 0.0F);
        GL11.glScalef(1.05F, 1F, 1.05F);
//		GL11.glCallList(pantsRightLegArmor1);
        model.renderPart("RLeg");
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glPopMatrix();

        if (helmetArmor1 == -1) {
//			helmetArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(helmetArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("Head");
//			model.renderPart("Head");
//			GL11.glEndList();
        }
        GL11.glPushMatrix();
        renderPlayer.modelBipedMain.bipedHead.postRender(0.0625F);
        GL11.glRotatef(180.0F, 0.1F, 0.0F, 0F);
        GL11.glTranslatef(0, -1.73f, 0);
        GL11.glScalef(1.12F, 1.13F, 1.15F);
        model.renderPart("Head");
//		glActiveTexture(GL13.GL_TEXTURE0);
//		ru.xlv.core.util.Utils.bindTexture(colorh);
//		glActiveTexture(GL_TEXTURE2);
//		ru.xlv.core.util.Utils.bindTexture(normalh);
//		model.renderPart("Helmet");
//		glActiveTexture(GL13.GL_TEXTURE0);
//		ru.xlv.core.util.Utils.bindTexture(texture);
//		glActiveTexture(GL_TEXTURE2);
//		ru.xlv.core.util.Utils.bindTexture(texture_normal_dds);
//		GL11.glCallList(helmetArmor1);
        GL11.glPopMatrix();
        if (bootsLeftBootArmor1 == -1) {
//			bootsLeftBootArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(bootsLeftBootArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("Lboot");
//			model.renderPart("Lboot");
//			GL11.glEndList();
        }
        if (bootsRightBootArmor1 == -1) {
//			bootsRightBootArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(bootsRightBootArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("Rboot");
//			model.renderPart("Rboot");
//			GL11.glEndList();
        }
        GL11.glPushMatrix();
        renderPlayer.modelBipedMain.bipedLeftLeg.postRender(0.0625F);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glTranslatef(-0.125F, -0.75F, 0.0F);
        GL11.glScalef(1.0F, 1.0F, 1.0F);
//		GL11.glCallList(bootsLeftBootArmor1);
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        renderPlayer.modelBipedMain.bipedRightLeg.postRender(0.0625F);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glTranslatef(0.125F, -0.75F, 0.0F);
        GL11.glScalef(1.0F, 1.0F, 1.0F);
//		GL11.glCallList(bootsRightBootArmor1);
        GL11.glPopMatrix();

        if (chestplateBodyArmor1 == -1) {
//			chestplateBodyArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(chestplateBodyArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("Torso");
//			model.renderPart("Torso");
//			GL11.glEndList();
        }
        if (chestplateLeftHandArmor1 == -1) {
//			chestplateLeftHandArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(chestplateLeftHandArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("LArm");
//			model.renderPart("LArm");
//			GL11.glEndList();
        }

        if (chestplateRightHandArmor1 == -1) {
//			chestplateRightHandArmor1 = GL11.glGenLists(1);
//			GL11.glNewList(chestplateRightHandArmor1, GL11.GL_COMPILE);
//			ResourcesSPLocation.medic.renderPart("RArm");
//			model.renderPart("RArm");
//			GL11.glEndList();
        }
        GL11.glPushMatrix();
        renderPlayer.modelBipedMain.bipedBody.postRender(0.0625F);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glTranslatef(0, -1.5f, 0);
        GL11.glScalef(1F, 1F, 1F);
//		model.renderPart("Torso_Torso.002");
        model.renderPart("Torso");
//		GL11.glCallList(chestplateBodyArmor1);
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        renderPlayer.modelBipedMain.bipedLeftArm.postRender(0.0625F);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glTranslatef(-0.325F, -1.38F, 0.0F);
        GL11.glScalef(1.05F, 1.0F, 1.05F);
        model.renderPart("LArm");
//		GL11.glCallList(chestplateLeftHandArmor1);
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        renderPlayer.modelBipedMain.bipedRightArm.postRender(0.0625F);
        GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
        GL11.glTranslatef(0.325F, -1.38F, 0.0F);
        GL11.glScalef(1.05F, 1.0F, 1.05F);
        model.renderPart("RArm");
//		GL11.glCallList(chestplateRightHandArmor1);
        GL11.glPopMatrix();

        KrogenitShaders.forwardDirect.setEmissionMapping(false);
        KrogenitShaders.forwardDirect.setSpecularMapping(false);
        KrogenitShaders.forwardDirect.setNormalMapping(false);
        KrogenitShaders.forwardDirect.setOldRender(true);
        KrogenitShaders.disableBloom();
//		Shaders.baseShader.disable();
        TextureLoaderDDS.unbind();
    }
}
