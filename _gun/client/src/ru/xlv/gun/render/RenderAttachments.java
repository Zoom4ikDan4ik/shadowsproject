package ru.xlv.gun.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;
import ru.xlv.core.model.ModelLoader;


public class RenderAttachments {

	public static final IModelCustom scope = ModelLoader.loadModel(new ResourceLocation("batmod", "models/attachments/reflex/model.sp"));
	private final static ResourceLocation trActex = new ResourceLocation("batmod:models/attachments/trAc/texture.png");
	private static final ResourceLocation lasertex = new ResourceLocation("batmod", "models/attachments/laser/laser.png");
	private static final ResourceLocation redtex = new ResourceLocation("batmod", "textures/red.png");
	private final static ResourceLocation kobratex = new ResourceLocation("batmod:models/attachments/kobra/texture.png");
	private final static ResourceLocation pbstex = new ResourceLocation("batmod:models/attachments/silencer/pbs1.png");
	private static final ResourceLocation susattex = new ResourceLocation("batmod", "models/attachments/susat/texture.png");
	private static final ResourceLocation pbs4tex = new ResourceLocation("batmod", "models/attachments/pbs4/texture.png");
	private static final ResourceLocation pkastex = new ResourceLocation("batmod", "models/attachments/pkas/texture.png");
	private static final ResourceLocation psotex = new ResourceLocation("batmod", "models/attachments/pso/texture.png");
	private static final ResourceLocation leupoldtex = new ResourceLocation("batmod", "models/attachments/leupold/texture.png");
	private static final ResourceLocation barskatex = new ResourceLocation("batmod", "models/attachments/barska/texture.png");
	private static final ResourceLocation posptex = new ResourceLocation("batmod", "models/attachments/posp/texture.png");
	private static final ResourceLocation ospreytex = new ResourceLocation("batmod", "models/attachments/osprey/texture.png");
	private static final ResourceLocation aacsdntex = new ResourceLocation("batmod", "models/attachments/aacsdn/texture.png");
	private static final ResourceLocation acogtex = new ResourceLocation("batmod", "models/attachments/acog/texture.png");
	private Minecraft mc = Minecraft.getMinecraft();
	private ModelLaser laserred = new ModelLaser();

	//L на конце - List
	private int acogL;
	private int linzaL;
	private int glushL;
	private int psoL;
	private int laserL;
	private int leupoldL;
	private int pkasL;
	private int pbs4L;
	private int susatL;
	private int kobraL;
	private int barskaL;
	private int trAcL;
	private int pospL;
	private int ospreyL;
	private int aacsdnL;
	private int aspeedL;
	private int aspeedLensa;

	public RenderAttachments() {
		IModelCustom Lens_model = ModelLoader.loadModel(new ResourceLocation("batmod:textures/models/attachments/IK_PNV/Lens.sp"));
		linzaL = GL11.glGenLists(1);
		GL11.glNewList(linzaL, GL11.GL_COMPILE);
		Lens_model.renderAll();
		GL11.glEndList();

		IModelCustom aspeed_lens = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/aspeed/aspeed_lens.sp"));
		aspeedLensa = GL11.glGenLists(1);
		GL11.glNewList(aspeedLensa, GL11.GL_COMPILE);
		aspeed_lens.renderAll();
		GL11.glEndList();

		IModelCustom Lens_aspeed_model = ModelLoader.loadModel(new ResourceLocation("batmod:textures/models/attachments/IK_PNV/Lens.sp"));
		linzaL = GL11.glGenLists(1);
		GL11.glNewList(linzaL, GL11.GL_COMPILE);
		Lens_model.renderAll();
		GL11.glEndList();

		IModelCustom acog = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/acog/model.sp"));
		acogL = GL11.glGenLists(1);
		GL11.glNewList(acogL, GL11.GL_COMPILE);
		acog.renderAll();
		GL11.glEndList();

		IModelCustom laser = ModelLoader.loadModel(new ResourceLocation("batmod", "models/attachments/laser/laser.sp"));
		laserL = GL11.glGenLists(1);
		GL11.glNewList(laserL, GL11.GL_COMPILE);
		laser.renderAll();
		GL11.glEndList();


		IModelCustom pbs = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/silencer/pbs1.sp"));
		glushL = GL11.glGenLists(1);
		GL11.glNewList(glushL, GL11.GL_COMPILE);
		pbs.renderAll();
		GL11.glEndList();


		IModelCustom pkas = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/pkas/model.sp"));
		pkasL = GL11.glGenLists(1);
		GL11.glNewList(pkasL, GL11.GL_COMPILE);
		pkas.renderAll();
		GL11.glEndList();

		IModelCustom leupold = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/leupold/model.sp"));
		leupoldL = GL11.glGenLists(1);
		GL11.glNewList(leupoldL, GL11.GL_COMPILE);
		leupold.renderAll();
		GL11.glEndList();

		IModelCustom pso = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/pso/model.sp"));
		psoL = GL11.glGenLists(1);
		GL11.glNewList(psoL, GL11.GL_COMPILE);
		pso.renderAll();
		GL11.glEndList();

		IModelCustom pbs4 = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/pbs4/model.sp"));
		pbs4L = GL11.glGenLists(1);
		GL11.glNewList(pbs4L, GL11.GL_COMPILE);
		pbs4.renderAll();
		GL11.glEndList();

		IModelCustom susat = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/susat/model.sp"));
		susatL = GL11.glGenLists(1);
		GL11.glNewList(susatL, GL11.GL_COMPILE);
		susat.renderAll();
		GL11.glEndList();

		IModelCustom kobra = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/kobra/model.sp"));
		kobraL = GL11.glGenLists(1);
		GL11.glNewList(kobraL, GL11.GL_COMPILE);
		kobra.renderAll();
		GL11.glEndList();

		IModelCustom barska = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/barska/model.sp"));
		barskaL = GL11.glGenLists(1);
		GL11.glNewList(barskaL, GL11.GL_COMPILE);
		barska.renderAll();
		GL11.glEndList();

		IModelCustom trAc = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/trAc/model.sp"));
		trAcL = GL11.glGenLists(1);
		GL11.glNewList(trAcL, GL11.GL_COMPILE);
		trAc.renderAll();
		GL11.glEndList();

		IModelCustom posp = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/posp/model.sp"));
		pospL = GL11.glGenLists(1);
		GL11.glNewList(pospL, GL11.GL_COMPILE);
		posp.renderAll();
		GL11.glEndList();

		IModelCustom osprey = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/osprey/model.sp"));
		ospreyL = GL11.glGenLists(1);
		GL11.glNewList(ospreyL, GL11.GL_COMPILE);
		osprey.renderAll();
		GL11.glEndList();

		IModelCustom aacsdn = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/aacsdn/model.sp"));
		aacsdnL = GL11.glGenLists(1);
		GL11.glNewList(aacsdnL, GL11.GL_COMPILE);
		aacsdn.renderAll();
		GL11.glEndList();

		IModelCustom aspeed = ModelLoader.loadModel(new ResourceLocation("batmod:models/attachments/aspeed/aspeed.sp"));
		aspeedL = GL11.glGenLists(1);
		GL11.glNewList(aspeedL, GL11.GL_COMPILE);
		aspeed.renderAll();
		GL11.glEndList();
	}

	public void renderPBS() {
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(pbstex);
		GL11.glCallList(glushL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();

	}

	public void renderPBS4() {
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(pbs4tex);
		GL11.glCallList(pbs4L);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();

	}

	public void renderAAC762SDN() {
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(aacsdntex);
		GL11.glCallList(aacsdnL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();

	}

	public void renderOsprey() {
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(ospreytex);
		GL11.glCallList(ospreyL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();

	}

	public void renderLaser(ItemRenderType type) {
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		GL11.glScalef(3F, 3F, 3F);
		GL11.glTranslatef(0F, 6.4F, 0.18F);
		ru.xlv.core.util.Utils.bindTexture(lasertex);
		GL11.glCallList(laserL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		ru.xlv.core.util.Utils.bindTexture(redtex);
		GL11.glRotatef(-90F, 0F, 1F, 0F);
		GL11.glTranslatef(-0.6F, 0.4F, 0F);
		GL11.glScalef(0.2F, 0.2F, 5F);
		if (type == ItemRenderType.EQUIPPED_FIRST_PERSON) {
			laserred.renderLaser(0.625f, 128f);
		} else {
			GL11.glScalef(0.25F, 0.25F, 0.0018F);
			laserred.renderLaser(0.625f, 128f);
		}


		GL11.glPopMatrix();

	}


	public void renderLeupold(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;

		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 4.5F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(1.4F, 1.4F, 1.4F);
		GL11.glTranslatef(-0.02F, -0.229F, 0.001F);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/hamr.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(leupoldtex);
		GL11.glCallList(leupoldL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderACOG(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);

		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;

		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.55F;
		if (aiming) {
			zoom = 4F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(1.4F, 1.4F, 1.4F);
		GL11.glTranslatef(-0.087F, -0.229F, 0.001F);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/aimacog.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);

		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(acogtex);
		GL11.glCallList(acogL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}


	public void renderTRAC(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;

		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 4.2F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(1.8F, 1.8F, 1.8F);
		GL11.glTranslatef(-0.0F, -0.382F, 0.001F);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/sigsight.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(trActex);
		GL11.glCallList(trAcL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderAspeed(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 50.5F;
		if (aiming) {
			zoom = 9F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(0.1f, 1f, 1F);
		GL11.glTranslatef(0f, 3.12F, 0F);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glRotatef(180, 1, 0, 0);
		GL11.glCallList(aspeedLensa);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/aspeed.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(aspeedLensa);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(AspeedRender.tex);
		GL11.glCallList(aspeedL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderOnlyLens(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 10F;
		if (aiming) {
			zoom = 8F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(0.1f, 1.08f, 1.5F);
		GL11.glTranslatef(5.59f, 2.46F, 0F);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glRotatef(180, 1, 0, 0);
		GL11.glCallList(aspeedLensa);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/smg.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(aspeedLensa);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}


	public void renderSUSAT(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;

		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 3.5F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(1.4F, 1.4F, 1.4F);
		GL11.glTranslatef(0.11F, -0.177F, -0.0015F);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/susat.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(susattex);
		GL11.glCallList(susatL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderBarska(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);

		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;

		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 2.5F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glScalef(1.1F, 1.1F, 1.1F);
		GL11.glTranslatef(0.41F, -0.042F, -0.0015F);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		//GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/barska.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(barskatex);
		GL11.glCallList(barskaL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderPSO(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 4.5F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glScalef(1.3F, 1.3F, 1.3F);
		GL11.glTranslatef(-0.14F, -0.182F, 0.0055F);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/aimpso.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);

		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(psotex);
		GL11.glCallList(psoL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderPOSP(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 4.5F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glScalef(1.3F, 1.3F, 1.3F);
		GL11.glTranslatef(-0.14F, -0.188F, 0.0034F);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/scope2.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(posptex);
		GL11.glCallList(pospL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderKobra(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 7.65F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glScalef(1F, 1F, 1F);
		GL11.glTranslatef(0.27F, -0.02F, 0.0065F);
		//GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/pkas.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(kobratex);
		GL11.glCallList(kobraL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}

	public void renderPKAS(boolean aiming) {
		GL11.glPushMatrix();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
		//GL11.glMatrixMode(GL11.GL_TEXTURE);
		GL11.glPushMatrix();
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png"));
		int tex = mc.renderEngine.getTexture(new ResourceLocation("batmod:textures/models/attachments/reflex/blank.png")).getGlTextureId();
		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		int width = mc.displayWidth;
		int height = mc.displayHeight;
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		GL11.glDisable(GL11.GL_LIGHTING);
		float zoom = 250.5F;
		if (aiming) {
			zoom = 2F;
		}
		int size = (int) (Math.min(width, height) / zoom);
		GL11.glCopyTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width / 2 - size / 2, height / 2 - size / 2, size, size, 0);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glColor4f(1, 1, 1, 0.49F);
		ru.xlv.core.util.Utils.bindTexture(new ResourceLocation("batmod:textures/gui/pkas.png"));
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glCallList(linzaL);
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		RenderHelper.disableStandardItemLighting();
		GL11.glColor4f(0.9f, 0.9f, 0.9f, 1f);
		int i = this.mc.theWorld.getLightBrightnessForSkyBlocks(MathHelper.floor_double(mc.thePlayer.posX),
				MathHelper.floor_double(mc.thePlayer.posY), MathHelper.floor_double(mc.thePlayer.posZ), 0);
		int j = i % 65536;
		int k = i / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) j / 1.0F, (float) k / 1.0F);
		GL11.glDisable(GL11.GL_CULL_FACE);
		ru.xlv.core.util.Utils.bindTexture(pkastex);
		GL11.glCallList(pkasL);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glPopMatrix();
	}
}
