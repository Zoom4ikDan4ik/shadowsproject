package ru.krogenit.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.AbstractTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import ru.krogenit.png_loader.PNGTextureLoader;
import ru.xlv.core.api.CoreAPI;
import ru.xlv.core.util.CoreUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;

public class Utils {

    private static final Minecraft mc = Minecraft.getMinecraft();

    public static boolean resourceExists(ResourceLocation loc) {
        File file;

        if (CoreUtils.isStartedFromGradle()) {
            file = new File("../lib/resources.zip");
        } else {
            file = new File("resources");
        }
        if (file.exists()) {
            try {
                ZipFile zipFile = new ZipFile(file);
                ZipEntry zipEntry = zipFile.getEntry("assets/" + loc.getResourceDomain() + "/" + loc.getResourcePath());
                if (zipEntry != null) {
                    return true;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    /**
     * gets InputStream decrypting it before
     */
    public static InputStream getInputStreamFromZip(ResourceLocation loc) {
        return CoreUtils.getResourceInputStream(loc.getResourceDomain() + "/" + loc.getResourcePath());
    }

    /**
     * binds the texture with decrypting before
     */
    public static void bindTexture(ResourceLocation resourceLocation) {
        bindTexture(resourceLocation, true);
    }

    public static void bindTexture(ResourceLocation resourceLocation, boolean decryptBefore) {
        bindTexture(resourceLocation, decryptBefore, GL_CLAMP_TO_BORDER);
    }

    public static void bindTexture(ResourceLocation loc, boolean decryptBefore, int wrapFormat) {
        TextureManager textureManager = mc.getTextureManager();
        if (textureManager.getTexture(loc) == null) {
            textureManager.loadTexture(loc, new AbstractTexture() {
                @Override
                public void loadTexture(IResourceManager p_110551_1_) throws IOException {
                    this.deleteGlTexture();
                    InputStream inputStream = CoreAPI.getResourceInputStream(loc.getResourceDomain() + "/" + loc.getResourcePath(), decryptBefore);
                    if (inputStream != null) {
                        BufferedImage bufferedimage = ImageIO.read(inputStream);
                        PNGTextureLoader.loadTexture(this, PNGTextureLoader.loadImageData(bufferedimage, wrapFormat));
                        inputStream.close();
                    }
                }
            });
        }
        textureManager.bindTexture(loc);
    }
}
