package ru.krogenit.dds_loader;

import java.io.IOException;
import java.nio.ByteBuffer;

class DDSPixelFormat {

	private static final int DDPF_ALPHAPIXELS = 0x00001;
	private static final int DDPF_ALPHA = 0x00002;
	private static final int DDPF_FOURCC = 0x00004;
	private static final int DDPF_RGB = 0x00040;
	private static final int DDPF_YUV = 0x00200;
	private static final int DDPF_LUMINANCE = 0x20000;

	private int dwSize;

	private int dwFlags;

	private int dwFourCC;

	private int dwRGBBitCount;

	private int dwRBitMask;

	private int dwGBitMask;

	private int dwBBitMask;

	private int dwABitMask;

	String sFourCC;

	private boolean isCompressed;

	private boolean hasFlagAlphaPixels;
	private boolean hasFlagAlpha;
	private boolean hasFlagFourCC;
	private boolean hasFlagRgb;
	private boolean hasFlagYuv;
	private boolean hasFlagLuminance;

	DDSPixelFormat(ByteBuffer header, boolean printDebug) throws IOException {
		dwSize = header.getInt();
		dwFlags = header.getInt();
		dwFourCC = header.getInt();
		dwRGBBitCount = header.getInt();
		dwRBitMask = header.getInt();
		dwGBitMask = header.getInt();
		dwBBitMask = header.getInt();
		dwABitMask = header.getInt();

		sFourCC = createFourCCString(dwFourCC);

		if (dwSize != 32)
			if (printDebug)
				System.err.println("Size is not 32!");

		hasFlagAlphaPixels = (dwFlags & DDPF_ALPHAPIXELS) == DDPF_ALPHAPIXELS;
		hasFlagAlpha = (dwFlags & DDPF_ALPHA) == DDPF_ALPHA;
		hasFlagFourCC = (dwFlags & DDPF_FOURCC) == DDPF_FOURCC;
		hasFlagRgb = (dwFlags & DDPF_RGB) == DDPF_RGB;
		hasFlagYuv = (dwFlags & DDPF_YUV) == DDPF_YUV;
		hasFlagLuminance = (dwFlags & DDPF_LUMINANCE) == DDPF_LUMINANCE;

		if (hasFlagFourCC) {
			isCompressed = true;
		} else if (hasFlagRgb) {
			isCompressed = false;
		}

		String sysout = "DDSPixelFormat properties:\n" + "\tdwSize \t\t\t= " + dwSize + "\n" + "\tFlags:\t\t\t";

		if (hasFlagAlphaPixels)
			sysout += "DDPF_ALPHAPIXELS | ";
		if (hasFlagAlpha)
			sysout += "DDPF_ALPHA | ";
		if (hasFlagFourCC)
			sysout += "DDPF_FOURCC | ";
		if (hasFlagRgb)
			sysout += "DDPF_RGB | ";
		if (hasFlagYuv)
			sysout += "DDPF_YUV | ";
		if (hasFlagLuminance)
			sysout += "DDPF_LUMINANCE";

		if (hasFlagFourCC)
			sysout += "\n\tsFourCC \t\t= " + sFourCC;
		if (hasFlagRgb) {
			sysout += "\n\tdwRGBBitCount \t= " + dwRGBBitCount + "\n\tdwRBitMask \t\t= " + dwRBitMask
					+ "\n\tdwGBitMask \t\t= " + dwGBitMask + "\n\tdwBBitMask \t\t= " + dwBBitMask;
		}
		sysout += "\n\tisCompressed \t= " + isCompressed;

		if (printDebug)
			System.out.println("\n" + sysout);
	}

	private String createFourCCString(int fourCC) {
		byte[] fourCCString = new byte[DDPF_FOURCC];
		for (int i = 0; i < fourCCString.length; i++)
			fourCCString[i] = (byte) (fourCC >> (i * 8));
		return new String(fourCCString);
	}

}
