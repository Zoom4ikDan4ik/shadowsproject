package ru.krogenit.model_loader;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;

@Mod(
        name = "ModelLoaderCore",
        modid = ModelLoaderCore.MODID,
        version = "1.0"
)
public class ModelLoaderCore {
    public static final String MODID = "modelloadercore";

    @Mod.Instance(MODID)
    public static ModelLoaderCore instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e) {
        if(e.getSide() == Side.CLIENT) {
            FMLCommonHandler.instance().bus().register(new ModelLoaderGuiEvents());

        }
    }
}
