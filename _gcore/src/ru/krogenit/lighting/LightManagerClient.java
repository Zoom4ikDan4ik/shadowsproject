package ru.krogenit.lighting;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.culling.Frustrum;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.math.MathRotateHelper;
import ru.krogenit.shaders.EnumShaderLightingType;
import ru.krogenit.utils.AnimationHelper;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LightManagerClient {

    @ToString
    @RequiredArgsConstructor
    private static class PositionPointLight {
        private final float x, y, z;

        @Override
        @SuppressWarnings("all")
        public boolean equals(Object o) {
            PositionPointLight that = (PositionPointLight) o;
            return Float.compare(that.x, x) == 0 && Float.compare(that.y, y) == 0 && Float.compare(that.z, z) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y, z);
        }
    }

    private static final Minecraft mc = Minecraft.getMinecraft();
    private static ArrayList<PointLight>[][] lights;
    private static int gridX = 32, gridZ = 32;
    private static Vector3f lastMins, lastMaxs;
    private static final Sorter lightSorter = new Sorter();
    private static final ExecutorService service = Executors.newFixedThreadPool(1);
    private static final List<PointLight> getLights = new ArrayList<>();
    private static final List<PointLight> nearestLights = new ArrayList<>();
    private static int lastX = -1, lastZ = -1, xPos, zPos;
    private static boolean forceUpdate;
    private static final Map<PositionPointLight, PointLight> helpMap = new HashMap<>();
    private static final List<PointLight> nearestLightsInFrustum = new ArrayList<>();
    private static final Map<Object, TickableLight> tickableLights = Collections.synchronizedMap(new HashMap<>());

    public static final float BLOCK_OFFSET = 0.55f;
    protected static Map<PositionPointLight, PointLight> allLights = new HashMap<>();
    static HashMap<Block, PointLight> blockLightTypes = new HashMap<>();

    static {
        lastMaxs = new Vector3f(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
        lastMins = new Vector3f(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public static void createWeaponFlashLight(EntityPlayer p) {
        int x = MathHelper.floor_double(p.posX);
        int y = MathHelper.floor_double(p.posY);
        int z = MathHelper.floor_double(p.posZ);
        if(mc.gameSettings.lighting == EnumShaderLightingType.ALL) {
            Vector3f addVec = new Vector3f(0, -0.0f, 1.5f);
            MathRotateHelper.rotateAboutX(addVec, Math.toRadians(p.rotationPitch));
            MathRotateHelper.rotateAboutY(addVec, Math.toRadians(0 - p.rotationYaw));
            addTickableLight(new PointLight(new Vector3f((float) p.posX + addVec.x, (float) p.posY + p.getEyeHeight() + addVec.y, (float) p.posZ + addVec.z), new Vector3f(8.0f, 6.4f, 4f), 6f), 0, EnumTickableLightType.None, p);
        } else {
            LightValue.addTickableLight(x, y+1, z, 2);
            p.worldObj.setLightValue(EnumSkyBlock.Block, x, y, z, 10);
            p.worldObj.updateLightByType(EnumSkyBlock.Block, x, y + 1, z);
            p.worldObj.updateLightByType(EnumSkyBlock.Block, x, y - 1, z);
            p.worldObj.updateLightByType(EnumSkyBlock.Block, x - 1, y, z);
            p.worldObj.updateLightByType(EnumSkyBlock.Block, x + 1, y, z);
            p.worldObj.updateLightByType(EnumSkyBlock.Block, x, y, z - 1);
            p.worldObj.updateLightByType(EnumSkyBlock.Block, x, y, z + 1);
        }
    }

    public static void addTickableLight(PointLight light, int lifetime, EnumTickableLightType type, Object creator) {
        TickableLight l = tickableLights.get(creator);
        if(l != null) {
            PointLight light1 = l.light;
            removeLight(light1.pos.x, light1.pos.y, light1.pos.z);
            createLight(light);
            tickableLights.put(creator, new TickableLight(light, lifetime, type));
            return;
        }

        createLight(light);
        tickableLights.put(creator, new TickableLight(light, lifetime, type));
    }

    public static void update() {
        float animSpeed = AnimationHelper.getAnimationSpeed();
        Iterator<Object> it = tickableLights.keySet().iterator();
        while(it.hasNext()) {
            Object key = it.next();
            TickableLight l = tickableLights.get(key);
            PointLight light = l.light;
            light.color.scale(0.9f * animSpeed);
            light.power -= 1.0f * animSpeed;

            if(light.power <= 0) {
                removeLight(light.pos.x, light.pos.y, light.pos.z);
                it.remove();
            }
        }
    }

    public static void checkIfLightBlock(Block b, float x, float y, float z) {
        service.submit(new LightAddingForBlock(b, x, y, z));
    }

    public static synchronized void removeLight(float x, float y, float z) {
        PointLight l = LightManagerClient.allLights.get(new PositionPointLight(x, y, z));
        if (l != null) {
            removeLight(l);
        }
    }

    public static synchronized void removeLight(int x, int y, int z) {
        PointLight l = LightManagerClient.allLights.get(new PositionPointLight(x + LightManagerClient.BLOCK_OFFSET, y + LightManagerClient.BLOCK_OFFSET, z + LightManagerClient.BLOCK_OFFSET));
        if (l != null) {
            removeLight(l);
        }
    }

    public static synchronized void updateLight(int x, int y, int z, float r, float g, float b, float power) {
        PointLight p = LightManagerClient.allLights.get(new PositionPointLight(x + LightManagerClient.BLOCK_OFFSET, y + LightManagerClient.BLOCK_OFFSET, z + LightManagerClient.BLOCK_OFFSET));
        if(p != null) {
            p.color.x = r;
            p.color.y = g;
            p.color.z = b;
            p.power = power;
            p.refreshAABB();
        } else {
            p = new PointLight(r, g, b, power);
            p.pos.x = x + LightManagerClient.BLOCK_OFFSET;
            p.pos.y = y + LightManagerClient.BLOCK_OFFSET;
            p.pos.z = z + LightManagerClient.BLOCK_OFFSET;
            p.refreshAABB();
            service.submit(new LightAddingForBlock(p, x, y, z));
        }
    }

    public static synchronized PointLight getLight(int x, int y, int z) {
        return LightManagerClient.allLights.get(new PositionPointLight(x + LightManagerClient.BLOCK_OFFSET, y + LightManagerClient.BLOCK_OFFSET, z + LightManagerClient.BLOCK_OFFSET));
    }

    public static List<PointLight> getLightsInFrustum(float maxDistance, Frustrum frustum) {
        nearestLightsInFrustum.clear();
        List<PointLight> ponits = getLightsNearest(maxDistance);
        if(ponits != null)
            for(PointLight p : ponits) {
                if(frustum.isBoundingBoxInFrustum(p.getAabb())) {
                    nearestLightsInFrustum.add(p);
                }
            }

        return nearestLightsInFrustum;
    }

    public static List<PointLight> getNearestLightsInFrustum() {
        return nearestLightsInFrustum;
    }

    public static List<PointLight> getLightsNearest(float maxDistance) {
        if (lights == null) return null;

        Minecraft mc = Minecraft.getMinecraft();
        int x = MathHelper.floor_double(mc.thePlayer.posX);
        int z = MathHelper.floor_double(mc.thePlayer.posZ);

        xPos = (int) (x / (float) gridX);
        zPos = (int) (z / (float) gridZ);

        if (forceUpdate) {
            forceUpdate = false;
            getAllLights(maxDistance);
        } else if (xPos == lastX && zPos == lastZ) {
            getSectorLights(maxDistance);
        } else {
            getAllLights(maxDistance);
        }

        nearestLights.sort(lightSorter);

        return nearestLights;
    }

    private static void getSectorLights(float maxDistance) {
        nearestLights.clear();
        Minecraft mc = Minecraft.getMinecraft();

        for (PointLight getLight : getLights) {
            Vector3f vec = new Vector3f((float) mc.thePlayer.posX, (float) mc.thePlayer.posY, (float) mc.thePlayer.posZ);

            float dist = (float) Math.sqrt(Math.pow(vec.x - getLight.pos.x, 2) + Math.pow(vec.y - getLight.pos.y, 2) + Math.pow(vec.z - getLight.pos.z, 2));

            if (dist <= maxDistance) {
                nearestLights.add(getLight);
            }
        }
    }

    private static void getAllLights(float maxDistance) {
        getLights.clear();
        helpMap.clear();
        nearestLights.clear();

        getLights(xPos, zPos, maxDistance);

        getLights(xPos + 1, zPos, maxDistance);
        getLights(xPos, zPos + 1, maxDistance);
        getLights(xPos + 1, zPos + 1, maxDistance);
        getLights(xPos - 1, zPos, maxDistance);
        getLights(xPos, zPos - 1, maxDistance);
        getLights(xPos - 1, zPos - 1, maxDistance);
        getLights(xPos + 1, zPos - 1, maxDistance);
        getLights(xPos - 1, zPos + 1, maxDistance);

        lastX = xPos;
        lastZ = zPos;
    }

    private static synchronized void getLights(int x, int z, float maxDistance) {
        Minecraft mc = Minecraft.getMinecraft();
        x = Math.max(0, x);
        x = Math.min(lights.length - 1, x);
        z = Math.max(0, z);
        z = Math.min(lights[0].length - 1, z);

        if (lights[x][z] != null) for (int i = 0; i < lights[x][z].size(); i++) {
            PointLight l = lights[x][z].get(i);
            PositionPointLight pos = new PositionPointLight(l.pos.x, l.pos.y, l.pos.z);
            if (!helpMap.containsKey(pos)) {
                helpMap.put(pos, l);
                getLights.add(l);
                Vector3f vec = new Vector3f((float) mc.thePlayer.posX, (float) mc.thePlayer.posY, (float) mc.thePlayer.posZ);

                float dist = (float) Math.sqrt(Math.pow(vec.x - l.pos.x, 2) + Math.pow(vec.y - l.pos.y, 2) + Math.pow(vec.z - l.pos.z, 2));

                if (dist <= maxDistance) {
                    nearestLights.add(l);
                }
            }
        }
    }

    static synchronized void createLight(PointLight l) {
//		if (l.power == blockLightTypes.get(Blocks.glowstone).power) {
//			int minX = MathHelper.floor_double(l.pos.x) - 2;
//			int minY = MathHelper.floor_double(l.pos.y) - 2;
//			int minZ = MathHelper.floor_double(l.pos.z) - 2;
//
//			int maxX = minX + 5;
//			int maxY = minY + 5;
//			int maxZ = minZ + 5;
//
//			for (int x = minX; x < maxX; x++)
//				for (int y = minY; y < maxY; y++)
//					for (int z = minZ; z < maxZ; z++) {
//						if (allLights.get((x + BLOCK_OFFSET) + ":" + (y + BLOCK_OFFSET) + ":" + (z + BLOCK_OFFSET)) != null) return;
//					}
//		}

        int minX = (int) Math.min(MathHelper.floor_double(l.pos.x), lastMins.x);
        int minZ = (int) Math.min(MathHelper.floor_double(l.pos.z), lastMins.z);
        int maxX = (int) Math.max(MathHelper.floor_double(l.pos.x), lastMaxs.x);
        int maxZ = (int) Math.max(MathHelper.floor_double(l.pos.z), lastMaxs.z);

        lastMins.x = minX;
        lastMins.z = minZ;
        lastMaxs.x = maxX;
        lastMaxs.z = maxZ;

        int gridSizeX = maxX - minX;
        int gridSizeZ = maxZ - minZ;

        if (Minecraft.getMinecraft().gameSettings.lightingDeferredDistance >= 128) {
            gridX = 64;
            gridZ = 64;
        } else {
            gridX = 32;
            gridZ = 32;
        }

        if (gridSizeX < gridX) gridSizeX = gridX;
        if (gridSizeZ < gridZ) gridSizeZ = gridZ;

        lights = new ArrayList[gridSizeX / gridX][gridSizeZ / gridZ];

        Map<PositionPointLight, PointLight> bloc = LightManagerClient.allLights;
        LightManagerClient.allLights = new HashMap<>();
        for (PointLight l1 : bloc.values()) {
            addLight(l1);
        }

        if (LightManagerClient.allLights.get(new PositionPointLight(l.pos.x, l.pos.y, l.pos.z)) == null) {
            addLight(l);
        }
    }

    private static void addLight(PointLight l) {
        LightManagerClient.allLights.put(new PositionPointLight(l.pos.x, l.pos.y, l.pos.z), l);
        Vector3f point = l.pos;
        int xPos = (int) (point.x / (float) gridX);
        int zPos = (int) (point.z / (float) gridZ);
        xPos = Math.max(0, xPos);
        xPos = Math.min(lights.length - 1, xPos);
        zPos = Math.max(0, zPos);
        zPos = Math.min(lights[0].length - 1, zPos);

        if (lights[xPos][zPos] == null) lights[xPos][zPos] = new ArrayList<>();
        lights[xPos][zPos].add(l);
        forceUpdate = true;
    }

    private static synchronized void removeLight(PointLight l) {
        LightManagerClient.allLights.remove(new PositionPointLight(l.pos.x, l.pos.y, l.pos.z));
        Vector3f point = l.pos;
        int xPos = (int) (point.x / (float) gridX);
        int zPos = (int) (point.z / (float) gridZ);
        xPos = Math.max(0, xPos);
        xPos = Math.min(lights.length - 1, xPos);
        zPos = Math.max(0, zPos);
        zPos = Math.min(lights[0].length - 1, zPos);

        if (lights[xPos][zPos] != null) lights[xPos][zPos].remove(l);
        forceUpdate = true;
    }

    public static void clear() {
        lights = null;
        LightManagerClient.allLights.clear();
        lastMaxs = new Vector3f(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
        lastMins = new Vector3f(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }


    public static class Sorter implements Comparator<PointLight> {
        @Override
        public int compare(PointLight l1, PointLight l2) {
            Minecraft mc = Minecraft.getMinecraft();
            double d0 = mc.thePlayer.getDistanceSq(l1.pos.x, l1.pos.y, l1.pos.z);
            double d1 = mc.thePlayer.getDistanceSq(l2.pos.x, l2.pos.y, l2.pos.z);
            return Double.compare(d0, d1);
        }
    }
}
