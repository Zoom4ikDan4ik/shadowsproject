package ru.krogenit.lighting;

import net.minecraft.client.Minecraft;
import net.minecraft.world.EnumSkyBlock;

import java.util.*;

public class LightValue {
    public static Set<LightValuePoint> list = new HashSet<>();
    public static HashMap<String, LightValuePoint> list2 = new HashMap<>();
    public static Set<LightValuePoint> list3 = new HashSet<>();

    public static ArrayList<LightValuePoint> tickableLightPoints = new ArrayList<>();
    private static final Minecraft mc = Minecraft.getMinecraft();

    public static void addTickableLight(int var0, int var1, int var2, int lifetime) {
        tickableLightPoints.add(new LightValuePoint(var0, var1, var2, lifetime));
    }

    public static void addData(int var0, int var1, int var2) {
        list.add(new LightValuePoint(var0, var1, var2, 0));
    }

    public static void addData2(String name, int var0, int var1, int var2) {
        list2.put(name, new LightValuePoint(var0, var1, var2, 0));
    }

    public static void addData3(int var0, int var1, int var2) {
        list3.add(new LightValuePoint(var0, var1, var2, 0));
    }

    public static boolean isAlready(int x, int y, int z, Set<LightValuePoint> list) {
        if (list.size() != 0) {

            for (LightValuePoint var1 : list) {
                if (var1.x == x && var1.y == y && var1.z == z)
                    return true;
            }
        }
        return false;
    }

    public static boolean isAlready(int x, int y, int z, Map<String, LightValuePoint> list) {
        if (list.size() != 0) {

            for (String s : list.keySet()) {
                LightValuePoint var1 = list.get(s);
                if (var1.x == x && var1.y == y && var1.z == z)
                    return true;
            }
        }
        return false;
    }

    public static void update() {
        if (tickableLightPoints.size() != 0) {
            for (int i = 0; i < tickableLightPoints.size(); i++) {
                LightValuePoint l = tickableLightPoints.get(i);
                l.lifetime--;
                if (l.lifetime < 0) {
                    tickableLightPoints.remove(i);
                    i--;
                    mc.theWorld.setLightValue(EnumSkyBlock.Block, l.x, l.y, l.z, 0);
                    mc.theWorld.updateLightByType(EnumSkyBlock.Block, l.x, l.y + 1, l.z);
                    mc.theWorld.updateLightByType(EnumSkyBlock.Block, l.x, l.y - 1, l.z);
                    mc.theWorld.updateLightByType(EnumSkyBlock.Block, l.x - 1, l.y, l.z);
                    mc.theWorld.updateLightByType(EnumSkyBlock.Block, l.x + 1, l.y, l.z);
                    mc.theWorld.updateLightByType(EnumSkyBlock.Block, l.x, l.y, l.z - 1);
                    mc.theWorld.updateLightByType(EnumSkyBlock.Block, l.x, l.y, l.z + 1);
                }
            }
        }
    }

    public static void resetAll() {
        if (list.size() != 0) {
            for (LightValuePoint var1 : list) {
                Minecraft.getMinecraft().theWorld.updateLightByType(EnumSkyBlock.Block, var1.x, var1.y, var1.z);
            }
        }
    }

    public static void reset2(String name) {
        LightValuePoint point = list2.remove(name);
        if (point != null)
            Minecraft.getMinecraft().theWorld.updateLightByType(EnumSkyBlock.Block, point.x, point.y, point.z);
    }

    public static void resetAll3() {
        if (list3.size() != 0) {
            for (LightValuePoint var1 : list3) {
                Minecraft.getMinecraft().theWorld.updateLightByType(EnumSkyBlock.Block, var1.x, var1.y, var1.z);
            }
        }
    }

    public static void emptyData() {
        list.clear();
    }

    public static void emptyData2() {
        list2.clear();
    }

    public static void emptyData3() {
        list3.clear();
    }
}
