package ru.krogenit.client.gui.api;

import org.lwjgl.input.Mouse;
import ru.xlv.customfont.FontType;
import ru.xlv.customfont.StringCache;

public class GuiTextFieldChat extends GuiTextFieldAdvanced {
    public GuiTextFieldChat(float x, float y, float width, float height, float fontScale) {
        super(FontType.FUTURA_PT_MEDIUM, x, y, width, height, fontScale);
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        mouseX = Mouse.getEventX();
        mouseY = mc.displayHeight - Mouse.getEventY() - 1;
        boolean flag = mouseX >= this.xPosition && mouseX < this.xPosition + this.width && mouseY >= this.yPosition && mouseY < this.yPosition + this.height;

        if (this.canLoseFocus) {
            this.setFocused(flag);
        }

        if (this.isFocused && mouseButton == 0) {
            float x = ScaleGui.getCenterX(120);
            float l = (mouseX - x) / ScaleGui.get(1.64f);
            StringCache stringCache = fontContainer.getTextFont();
            String s = stringCache.trimStringToWidth(this.text.substring(this.lineScrollOffset), getWidth(), false);
            this.setCursorPosition(stringCache.trimStringToWidth(s, l, false).length() + this.lineScrollOffset);
        }
    }
}
