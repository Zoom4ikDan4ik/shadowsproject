package ru.krogenit.client.event;

import cpw.mods.fml.common.eventhandler.Event;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.item.ItemStack;

@Getter
@AllArgsConstructor
public class EventUpdateItemInSlot extends Event {

    private final ItemStack newItemStack;
    private final int slotIndex;

    @Override
    public boolean isCancelable() {
        return true;
    }
}
