package ru.xlv.core.resource;

public interface IResource {
    void loadFromFile();
    void setFrom(AbstractResource resource);
    void loadToMemory();
    void unload();
}
