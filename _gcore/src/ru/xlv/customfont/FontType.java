package ru.xlv.customfont;

import net.minecraft.util.ResourceLocation;

public enum FontType {

    DEFAULT(new FontContainer("Yeager", 22)),
    FUTURA_PT_MEDIUM(new FontContainer("FuturaPT_medium", 22, new ResourceLocation("font", "FuturaPT-Medium.ttf"))),
    FUTURA_PT_BOLD(new FontContainer("FuturaPT_bold", 22, new ResourceLocation("font", "FuturaPT-Bold.ttf"))),
    FUTURA_PT_DEMI(new FontContainer("FuturaPT_demi", 22, new ResourceLocation("font", "FuturaPT-Demi.ttf"))),
    ROBOTO_REGULAR(new FontContainer("Roboto-Regular", 22, new ResourceLocation("font", "Roboto-Regular.ttf"))),
	OpenSans_Italic(new FontContainer("OpenSans-Italic", 22, new ResourceLocation("font", "OpenSans-Italic.ttf"))),

    HATTEN(new FontContainer("HATTEN", 22, new ResourceLocation("font", "HATTEN.ttf"))),
    Marske(new FontContainer("Marske", 22, new ResourceLocation("font", "Marske.ttf"))),

    HelveticaNeueCyrLight(new FontContainer("HelveticaNeueCyr-Light", 32, new ResourceLocation("font", "HelveticaNeueCyr-Light.ttf"))),
    HelveticaNeueCyrBold(new FontContainer("HelveticaNeueCyr-Bold", 32, new ResourceLocation("font", "HelveticaNeueCyr-Bold.ttf"))),
    HelveticaNeueCyrMedium(new FontContainer("HelveticaNeueCyr-Medium", 32, new ResourceLocation("font", "HelveticaNeueCyr-Medium.ttf"))),
    HelveticaNeueCyrRoman(new FontContainer("HelveticaNeueCyr-Roman", 32, new ResourceLocation("font", "HelveticaNeueCyr-Roman.ttf"))),
    HelveticaNeueCyrBlack(new FontContainer("HelveticaNeueCyr-Black", 32, new ResourceLocation("font", "HelveticaNeueCyr-Black.ttf"))),

    DeadSpace(new FontContainer("DeadSpace", 32, new ResourceLocation("font", "DeadSpace.ttf")));

    private final FontContainer fontContainer;

    FontType(FontContainer fontContainer) {
        this.fontContainer = fontContainer;
    }

    public FontContainer getFontContainer() {
        return fontContainer;
    }
}
