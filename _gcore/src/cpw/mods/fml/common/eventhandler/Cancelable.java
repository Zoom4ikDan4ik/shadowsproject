package cpw.mods.fml.common.eventhandler;

import ru.xlv.core.util.obf.IgnoreObf;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(value = RUNTIME)
@Target(value = TYPE)
@IgnoreObf
public @interface Cancelable{}