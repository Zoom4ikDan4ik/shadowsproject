package net.minecraft.client.renderer.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lombok.Getter;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.init.Blocks;
import net.minecraft.src.Config;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;
import ru.krogenit.shaders.KrogenitShaders;
import ru.krogenit.shaders.pbr.IPBR;
import shadersmod.client.Shaders;

@SideOnly(Side.CLIENT)
public abstract class Render
{
    private static final ResourceLocation shadowTextures = new ResourceLocation("textures/misc/shadow.png");
    protected RenderManager renderManager;
    protected RenderBlocks field_147909_c = new RenderBlocks();
    protected float shadowSize;
    /** Determines the darkness of the object's shadow. Higher value makes a darker shadow. */
    protected float shadowOpaque = 1.0F;
    private boolean staticEntity = false;
    @Getter
    protected boolean postRender = false;

    /**
     * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
     * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
     * (Render<T extends Entity) and this method has signature public void func_76986_a(T entity, double d, double d1,
     * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
     */
    public abstract void doRender(Entity entity, double x, double y, double z, float rotationYaw, float tickTime);

    public abstract void doRenderPost(Entity entity, double x, double y, double z, float rotationYaw, float tickTime);

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected abstract ResourceLocation getEntityTexture(Entity entity);

    public boolean isStaticEntity()
    {
        return this.staticEntity;
    }

    protected void bindEntityTexture(Entity entity)
    {
        this.bindTexture(this.getEntityTexture(entity));
    }

    protected void bindTexture(ResourceLocation resourceLocation) {
        this.renderManager.renderEngine.bindTexture(resourceLocation);
    }

    /**
     * Renders fire on top of the entity. Args: entity, x, y, z, partialTickTime
     */
    private void renderEntityOnFire(Entity entity, double x, double y, double z, float tickTime) {
        IPBR currentPBRShader = KrogenitShaders.getCurrentPBRShader(true);
        currentPBRShader.setLightMapping(false);
        currentPBRShader.useLighting(false);
        IIcon iicon = Blocks.fire.getFireIcon(0);
        IIcon iicon1 = Blocks.fire.getFireIcon(1);
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x, (float) y, (float) z);
        float f1 = entity.width * 1.4F;
        GL11.glScalef(f1, f1, f1);
        Tessellator tessellator = Tessellator.instance;
        float f2 = 0.5F;
        float f3 = 0.0F;
        float f4 = entity.height / f1;
        float f5 = (float) (entity.posY - entity.boundingBox.minY);
        GL11.glRotatef(-this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
        GL11.glTranslatef(0.0F, 0.0F, -0.3F + (float) ((int) f4) * 0.02F);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        float f6 = 0.0F;
        int i = 0;
        tessellator.startDrawingQuads();

        while (f4 > 0.0F) {
            IIcon iicon2 = i % 2 == 0 ? iicon : iicon1;
            this.bindTexture(TextureMap.locationBlocksTexture);
            float f7 = iicon2.getMinU();
            float f8 = iicon2.getMinV();
            float f9 = iicon2.getMaxU();
            float f10 = iicon2.getMaxV();

            if (i / 2 % 2 == 0) {
                float f11 = f9;
                f9 = f7;
                f7 = f11;
            }

            tessellator.addVertexWithUV(f2 - f3, 0.0F - f5, f6, f9, f10);
            tessellator.addVertexWithUV(-f2 - f3, 0.0F - f5, f6, f7, f10);
            tessellator.addVertexWithUV(-f2 - f3, 1.4F - f5, f6, f7, f8);
            tessellator.addVertexWithUV(f2 - f3, 1.4F - f5, f6, f9, f8);
            f4 -= 0.45F;
            f5 -= 0.45F;
            f2 *= 0.9F;
            f6 += 0.03F;
            ++i;
        }

        tessellator.draw();
        GL11.glPopMatrix();
        currentPBRShader.setLightMapping(true);
        currentPBRShader.useLighting(true);
    }

    /**
     * Renders the entity shadows at the position, shadow alpha and partialTickTime. Args: entity, x, y, z, shadowAlpha,
     * partialTickTime
     */
    private void renderShadow(Entity entity, double x, double y, double z, float shadowAlpha, float tickTime) {
        if (!Config.isShaders() || !Shaders.shouldSkipDefaultShadow) {
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            this.renderManager.renderEngine.bindTexture(shadowTextures);
            World var10 = this.getWorldFromRenderManager();
            GL11.glDepthMask(false);
            float var11 = this.shadowSize;

            if (entity instanceof EntityLiving) {
                EntityLiving var35 = (EntityLiving) entity;
                var11 *= var35.getRenderSizeModifier();

                if (var35.isChild()) {
                    var11 *= 0.5F;
                }
            }

            double var351 = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * (double) tickTime;
            double var14 = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * (double) tickTime + (double) entity.getShadowSize();
            double var16 = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * (double) tickTime;
            int var18 = MathHelper.floor_double(var351 - (double) var11);
            int var19 = MathHelper.floor_double(var351 + (double) var11);
            int var20 = MathHelper.floor_double(var14 - (double) var11);
            int var21 = MathHelper.floor_double(var14);
            int var22 = MathHelper.floor_double(var16 - (double) var11);
            int var23 = MathHelper.floor_double(var16 + (double) var11);
            double var24 = x - var351;
            double var26 = y - var14;
            double var28 = z - var16;
            Tessellator var30 = Tessellator.instance;
            var30.startDrawingQuads();

            for (int var31 = var18; var31 <= var19; ++var31) {
                for (int var32 = var20; var32 <= var21; ++var32) {
                    for (int var33 = var22; var33 <= var23; ++var33) {
                        Block var34 = var10.getBlock(var31, var32 - 1, var33);

                        if (var34.getMaterial() != Material.air && var10.getBlockLightValue(var31, var32, var33) > 3) {
                            this.func_147907_a(var34, x, y + (double) entity.getShadowSize(), z, var31, var32, var33, shadowAlpha, var11, var24, var26 + (double) entity.getShadowSize(), var28);
                        }
                    }
                }
            }

            var30.draw();
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glDepthMask(true);
        }
    }

    /**
     * Returns the render manager's world object
     */
    private World getWorldFromRenderManager()
    {
        return this.renderManager.worldObj;
    }

    private void func_147907_a(Block p_147907_1_, double p_147907_2_, double p_147907_4_, double p_147907_6_, int p_147907_8_, int p_147907_9_, int p_147907_10_, float p_147907_11_, float p_147907_12_, double p_147907_13_, double p_147907_15_, double p_147907_17_) {
        Tessellator tessellator = Tessellator.instance;

        if (p_147907_1_.renderAsNormalBlock()) {
            double d6 = ((double) p_147907_11_ - (p_147907_4_ - ((double) p_147907_9_ + p_147907_15_)) / 2.0D) * 0.5D * (double) this.getWorldFromRenderManager().getLightBrightness(p_147907_8_, p_147907_9_, p_147907_10_);

            if (d6 >= 0.0D) {
                if (d6 > 1.0D) {
                    d6 = 1.0D;
                }

                tessellator.setColorRGBA_F(1.0F, 1.0F, 1.0F, (float) d6);
                double d7 = (double) p_147907_8_ + p_147907_1_.getBlockBoundsMinX() + p_147907_13_;
                double d8 = (double) p_147907_8_ + p_147907_1_.getBlockBoundsMaxX() + p_147907_13_;
                double d9 = (double) p_147907_9_ + p_147907_1_.getBlockBoundsMinY() + p_147907_15_ + 0.015625D;
                double d10 = (double) p_147907_10_ + p_147907_1_.getBlockBoundsMinZ() + p_147907_17_;
                double d11 = (double) p_147907_10_ + p_147907_1_.getBlockBoundsMaxZ() + p_147907_17_;
                float f2 = (float) ((p_147907_2_ - d7) / 2.0D / (double) p_147907_12_ + 0.5D);
                float f3 = (float) ((p_147907_2_ - d8) / 2.0D / (double) p_147907_12_ + 0.5D);
                float f4 = (float) ((p_147907_6_ - d10) / 2.0D / (double) p_147907_12_ + 0.5D);
                float f5 = (float) ((p_147907_6_ - d11) / 2.0D / (double) p_147907_12_ + 0.5D);
                tessellator.addVertexWithUV(d7, d9, d10, f2, f4);
                tessellator.addVertexWithUV(d7, d9, d11, f2, f5);
                tessellator.addVertexWithUV(d8, d9, d11, f3, f5);
                tessellator.addVertexWithUV(d8, d9, d10, f3, f4);
            }
        }
    }

    /**
     * Renders a white box with the bounds of the AABB translated by the offset. Args: aabb, x, y, z
     */
    public static void renderOffsetAABB(AxisAlignedBB axisAlignedBB, double x, double y, double z) {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        Tessellator tessellator = Tessellator.instance;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        tessellator.startDrawingQuads();
        tessellator.setTranslation(x, y, z);
        tessellator.setNormal(0.0F, 0.0F, -1.0F);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.setNormal(0.0F, 0.0F, 1.0F);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.setNormal(0.0F, -1.0F, 0.0F);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.setNormal(-1.0F, 0.0F, 0.0F);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.setNormal(1.0F, 0.0F, 0.0F);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.setTranslation(0.0D, 0.0D, 0.0D);
        tessellator.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    /**
     * Adds to the tesselator a box using the aabb for the bounds. Args: aabb
     */
    public static void renderAABB(AxisAlignedBB axisAlignedBB) {
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.minX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.minZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.maxY, axisAlignedBB.maxZ);
        tessellator.addVertex(axisAlignedBB.maxX, axisAlignedBB.minY, axisAlignedBB.maxZ);
        tessellator.draw();
    }

    /**
     * Sets the RenderManager.
     */
    public void setRenderManager(RenderManager renderManager)
    {
        this.renderManager = renderManager;
    }

    /**
     * Renders the entity's shadow and fire (if its on fire). Args: entity, x, y, z, yaw, partialTickTime
     */
    public void doRenderShadowAndFire(Entity e, double x, double y, double z, float yaw, float tickTime) {
        if (this.renderManager.options.fancyGraphics && this.shadowSize > 0.0F && !e.isInvisible()) {
            double d3 = this.renderManager.getDistanceToCamera(e.posX, e.posY, e.posZ);
            float f2 = (float) ((1.0D - d3 / 256.0D) * (double) this.shadowOpaque);

            if (f2 > 0.0F) {
                this.renderShadow(e, x, y, z, f2, tickTime);
            }
        }

        if (e.canRenderOnFire()) {
            this.renderEntityOnFire(e, x, y, z, tickTime);
        }
    }

    /**
     * Returns the font renderer from the set render manager
     */
    public FontRenderer getFontRendererFromRenderManager()
    {
        return this.renderManager.getFontRenderer();
    }

    public void updateIcons(IIconRegister p_94143_1_) {}

    protected void func_147906_a(Entity entity, String s, double x, double y, double z, int distance) {
        double d3 = entity.getDistanceSqToEntity(this.renderManager.livingPlayer);

        if (d3 <= (double) (distance * distance)) {
            IPBR shader = KrogenitShaders.getCurrentPBRShader(true);
            FontRenderer fontrenderer = this.getFontRendererFromRenderManager();
            float f = 1.6F;
            float f1 = 0.016666668F * f;
            GL11.glPushMatrix();
            GL11.glTranslatef((float) x + 0.0F, (float) y + entity.height + 0.5F, (float) z);
            GL11.glNormal3f(0.0F, 1.0F, 0.0F);
            GL11.glRotatef(-this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
            GL11.glScalef(-f1, -f1, f1);
            shader.useLighting(false);
            GL11.glDepthMask(false);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            Tessellator tessellator = Tessellator.instance;
            byte b0 = 0;

            if (s.equals("deadmau5")) {
                b0 = -10;
            }

            shader.setUseTexture(false);
            tessellator.startDrawingQuads();
            int j = fontrenderer.getStringWidth(s) / 2;
            tessellator.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
            tessellator.addVertex(-j - 1, -1 + b0, 0.0D);
            tessellator.addVertex(-j - 1, 8 + b0, 0.0D);
            tessellator.addVertex(j + 1, 8 + b0, 0.0D);
            tessellator.addVertex(j + 1, -1 + b0, 0.0D);
            tessellator.draw();
            shader.setUseTexture(true);
            fontrenderer.drawString(s, -fontrenderer.getStringWidth(s) / 2, b0, 553648127);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            fontrenderer.drawString(s, -fontrenderer.getStringWidth(s) / 2, b0, -1);
            shader.useLighting(true);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glPopMatrix();
        }
    }
}