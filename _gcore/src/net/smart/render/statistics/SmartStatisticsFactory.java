// ==================================================================
// This file is part of Smart Render.
//
// Smart Render is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Smart Render is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Smart Render. If not, see <http://www.gnu.org/licenses/>.
// ==================================================================

package net.smart.render.statistics;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

import java.util.Hashtable;
import java.util.Iterator;

public class SmartStatisticsFactory {
	private static final SmartStatisticsFactory SMART_STATISTICS_FACTORY = new SmartStatisticsFactory();

	private Hashtable<Integer, SmartStatisticsOther> otherStatistics;

	public static void handleMultiPlayerTick(Minecraft minecraft) {
		SMART_STATISTICS_FACTORY.doHandleMultiPlayerTick(minecraft);
	}

	public static SmartStatistics getInstance(EntityPlayer entityPlayer) {
		return SMART_STATISTICS_FACTORY.doGetInstance(entityPlayer);
	}

	public static SmartStatisticsOther getOtherStatistics(int entityId) {
		return SMART_STATISTICS_FACTORY.doGetOtherStatistics(entityId);
	}

	public static SmartStatisticsOther getOtherStatistics(EntityOtherPlayerMP entity) {
		return SMART_STATISTICS_FACTORY.doGetOtherStatistics(entity);
	}

	protected void doHandleMultiPlayerTick(Minecraft minecraft) {
		for (Object o : minecraft.theWorld.playerEntities) {
			Entity player = (Entity) o;
			if (player instanceof EntityOtherPlayerMP) {
				EntityOtherPlayerMP otherPlayer = (EntityOtherPlayerMP) player;
				SmartStatisticsOther statistics = doGetOtherStatistics(otherPlayer);
				statistics.calculateAllStats(true);
				statistics.foundAlive = true;
			}
		}

		if (otherStatistics == null || otherStatistics.isEmpty())
			return;

		Iterator<Integer> entityIds = otherStatistics.keySet().iterator();
		while (entityIds.hasNext()) {
			Integer entityId = entityIds.next();
			SmartStatisticsOther statistics = otherStatistics.get(entityId);
			if (statistics.foundAlive)
				statistics.foundAlive = false;
			else
				entityIds.remove();
		}
	}

	protected SmartStatistics doGetInstance(EntityPlayer entityPlayer) {
		if (entityPlayer instanceof EntityOtherPlayerMP)
			return doGetOtherStatistics(entityPlayer.getEntityId());
		else if (entityPlayer instanceof EntityPlayerSP)
			return ((EntityPlayerSP) entityPlayer).getStatistics();
		return null;
	}

	protected SmartStatisticsOther doGetOtherStatistics(int entityId) {
		SmartStatisticsOther statistics = tryGetOtherStatistics(entityId);
		if (statistics == null) {
			Entity entity = Minecraft.getMinecraft().theWorld.getEntityByID(entityId);
			if (entity instanceof EntityOtherPlayerMP)
				statistics = addOtherStatistics((EntityOtherPlayerMP) entity);
		}
		return statistics;
	}

	protected SmartStatisticsOther doGetOtherStatistics(EntityOtherPlayerMP entity) {
		SmartStatisticsOther statistics = tryGetOtherStatistics(entity.getEntityId());
		if (statistics == null)
			statistics = addOtherStatistics(entity);
		return statistics;
	}

	protected final SmartStatisticsOther tryGetOtherStatistics(int entityId) {
		if (otherStatistics == null)
			otherStatistics = new Hashtable<>();
		return otherStatistics.get(entityId);
	}

	protected final SmartStatisticsOther addOtherStatistics(EntityOtherPlayerMP entity) {
		SmartStatisticsOther statistics = new SmartStatisticsOther(entity);
		otherStatistics.put(entity.getEntityId(), statistics);
		return statistics;
	}
}