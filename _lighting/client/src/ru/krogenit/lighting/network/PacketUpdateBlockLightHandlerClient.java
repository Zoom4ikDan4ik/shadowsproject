package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import ru.krogenit.lighting.LightManagerClient;

public class PacketUpdateBlockLightHandlerClient extends AbstractClientMessageHandler<PacketUpdateBlockLight> {

    @Override
    public IMessage handleMessageOnClientSide(PacketUpdateBlockLight msg, MessageContext ctx) {
        LightManagerClient.updateLight(msg.getX(), msg.getY(), msg.getZ(), msg.getR(), msg.getG(), msg.getB(), msg.getPower());
        return null;
    }
}
