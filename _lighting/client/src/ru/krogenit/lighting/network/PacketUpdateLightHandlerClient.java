package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.block.Block;
import ru.krogenit.lighting.EnumLightEvent;
import ru.krogenit.lighting.LightManagerClient;

public class PacketUpdateLightHandlerClient extends AbstractClientMessageHandler<PacketUpdateLight> {

    @Override
    public IMessage handleMessageOnClientSide(PacketUpdateLight msg, MessageContext ctx) {
        int x = msg.getX(), y = msg.getY(), z = msg.getZ(), id = msg.getId();
        EnumLightEvent event = msg.getEvent();

        if (event == EnumLightEvent.Add) LightManagerClient.checkIfLightBlock(Block.getBlockById(id), x, y, z);
        else if (event == EnumLightEvent.RemoveStandardModel) {
            LightManagerClient.removeLight(x + LightManagerClient.BLOCK_OFFSET, y + LightManagerClient.BLOCK_OFFSET, z + LightManagerClient.BLOCK_OFFSET);
        } else LightManagerClient.removeLight(x, y, z);

        return null;
    }
}
