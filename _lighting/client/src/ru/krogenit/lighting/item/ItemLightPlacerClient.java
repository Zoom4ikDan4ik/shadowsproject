package ru.krogenit.lighting.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import ru.krogenit.lighting.CoreLightingCommon;
import ru.krogenit.lighting.LightPlacer;

public class ItemLightPlacerClient extends ItemLightPlacer {

    public ItemLightPlacerClient() {
        setCreativeTab(CreativeTabs.tabTools);
        this.iconString = "itemLightPlacer";
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World w, EntityPlayer p) {
        if(w.isRemote) {
            LightPlacer.placeLight();
        }

        return itemStack;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(CoreLightingCommon.MODID + ":" + iconString);
    }
}
