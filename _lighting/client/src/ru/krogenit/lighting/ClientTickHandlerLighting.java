package ru.krogenit.lighting;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

public class ClientTickHandlerLighting {

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(TickEvent.ClientTickEvent event) {
        LightValue.update();
    }
}
