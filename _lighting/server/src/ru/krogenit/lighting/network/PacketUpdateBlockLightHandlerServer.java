package ru.krogenit.lighting.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import ru.krogenit.lighting.LightManagerServer;
import ru.krogenit.lighting.PointLightServer;

public class PacketUpdateBlockLightHandlerServer extends AbstractServerMessageHandler<PacketUpdateBlockLight> {

    @Override
    public IMessage handleMessageOnServerSide(PacketUpdateBlockLight msg, MessageContext ctx) {
        PointLightServer p = LightManagerServer.getLight(msg.getX(), msg.getY(), msg.getZ());
        if(p== null) p = new PointLightServer(msg.getR(), msg.getG(), msg.getB(), msg.getPower());

        LightManagerServer.updateLightServer(msg.getX(), msg.getY(), msg.getZ(), msg.getR(), msg.getG(), msg.getB(), msg.getPower());
        PacketDispatcherLighting.sendToAllAround(new PacketUpdateBlockLight(p.power, p.color.x, p.color.y, p.color.z, msg.getX(), msg.getY(), msg.getZ()), 0, msg.getX(), msg.getY(), msg.getZ(), 128);

//			TileEntity tile = w.getTileEntity(msg.x, msg.y, msg.z);
//			if(tile instanceof TEBlockLight) {
//				TEBlockLight te = (TEBlockLight) tile;
//				te.setR(msg.r);
//				te.setG(msg.g);
//				te.setB(msg.b);
//				te.setPower(msg.power);
//				te.markDirty();
//				w.markBlockForUpdate(msg.x, msg.y, msg.z);
//			}

        return null;
    }

}