package ru.krogenit.lighting;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.common.util.config.IConfigGson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Configurable
public class ConfigPointLightServer implements IConfigGson {

    @Getter
    @Configurable
    @RequiredArgsConstructor
    public static class PointLightServerWrap {
        private final float x, y, z;
        private final float r, g, b;
        private final float power;
    }

    //не юзать напрямую для добавления
    @Configurable
    private final List<PointLightServerWrap> pointLightServers = new ArrayList<>();

    public void addPointLightServer(PointLightServer pointLightServer) {
        pointLightServers.add(new PointLightServerWrap(
                pointLightServer.pos.x, pointLightServer.pos.y, pointLightServer.pos.z,
                pointLightServer.color.x, pointLightServer.color.y, pointLightServer.color.z,
                pointLightServer.power
        ));
    }

    @Override
    public File getConfigFile() {
        return new File("point_lights.json");
    }
}
