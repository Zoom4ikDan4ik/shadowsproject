package ru.xlv.navigator.handle;

import lombok.Getter;
import ru.xlv.navigator.common.portal.Portal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PortalManager {

    @Getter
    private final List<Portal> portals = Collections.synchronizedList(new ArrayList<>());

    public void init() {

    }

    public void handle() {}
}
