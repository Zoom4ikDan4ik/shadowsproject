package ru.xlv.shop.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Localization;

import java.io.File;

@Getter
@Configurable
public class ShopLocalization extends Localization {

    private final String notEnoughInvSpaceMessage = "notEnoughInvSpaceMessage";
    private final String notEnoughInvSpacePostTitle = "notEnoughInvSpacePostTitle";
    private final String notEnoughInvSpacePostText = "an item you have bought {0} has been attached to this message, bcs you didn't have a free inventory space at the moment.";

    private final String responseShopBuyResultSuccessMessage = "responseShopBuyResultSuccessMessage";
    private final String responseShopBuyResultNotEnoughInvSpaceMessage = "responseShopBuyResultNotEnoughInvSpaceMessage";
    private final String responseShopBuyResultNotEnoughMoneyMessage = "responseShopBuyResultNotEnoughMoneyMessage";
    private final String responseShopBuyResultUnknownErrorMessage = "responseShopBuyResultUnknownErrorMessage";
    private final String responseShopBuyResultItemNotFoundMessage = "responseShopBuyResultItemNotFoundMessage";

    private transient final File configFile = new File("config/shop/localization.json");
}
