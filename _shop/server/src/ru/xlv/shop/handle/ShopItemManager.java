package ru.xlv.shop.handle;

import lombok.RequiredArgsConstructor;
import ru.xlv.core.util.Flex;
import ru.xlv.shop.common.ShopItem;

import javax.annotation.Nullable;
import java.util.List;

@RequiredArgsConstructor
public class ShopItemManager {

    private final List<ShopItem> shopItems;

    @Nullable
    public synchronized ShopItem getShopItemById(int shopItemId) {
        return Flex.getCollectionElement(shopItems, shopItem -> shopItem.getId() == shopItemId);
    }

    public synchronized List<ShopItem> getShopItems() {
        return shopItems;
    }
}
