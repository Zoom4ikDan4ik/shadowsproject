package ru.xlv.shop.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.network.RequestController;
import ru.xlv.shop.XlvsShopMod;
import ru.xlv.shop.common.ShopItemCategory;

import java.io.IOException;

@NoArgsConstructor
public class PacketShopSyncCategory implements IPacketCallbackOnServer {

    private static final RequestController<EntityPlayer> REQUEST_CONTROLLER = new RequestController.Periodic<>(1000L);

    private ShopItemCategory shopItemCategory;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int id = bbis.readInt();
        if(id >= 0 && id < ShopItemCategory.values().length) {
            shopItemCategory = ShopItemCategory.values()[id];
            REQUEST_CONTROLLER.doCompletedRequestAsync(entityPlayer, packetCallbackSender::send);
        }
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        writeCollection(bbos, XlvsShopMod.INSTANCE.getShopHandler().getShopItemMap().get(shopItemCategory), this::writeComposable);
    }

    @Override
    public boolean handleCallback() {
        return true;
    }
}
