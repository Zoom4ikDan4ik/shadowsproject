package ru.xlv.shop.handle;

import lombok.experimental.UtilityClass;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.shop.common.ShopItemCost;
import ru.xlv.shop.network.PacketShopBuy;

@UtilityClass
public class ShopHandler {

    public SyncResultHandler<PacketShopBuy.Result> buy(int shopItemId, ShopItemCost.Type type) {
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketShopBuy(type, shopItemId));
    }
}
