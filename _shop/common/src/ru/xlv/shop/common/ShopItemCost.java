package ru.xlv.shop.common;

import io.netty.buffer.ByteBufOutputStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.xlv.core.common.network.IPacketComposable;
import ru.xlv.core.common.util.config.Configurable;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class ShopItemCost implements IPacketComposable {

    public enum Type {
        CREDITS,
        PLATINUM
    }

    @Configurable
    private final Type type;
    @Configurable
    private final int amount;

    @Override
    public void writeDataToPacket(List<Object> writableList, ByteBufOutputStream byteBufOutputStream) {
        writableList.add(type.ordinal());
        writableList.add(amount);
    }
}
