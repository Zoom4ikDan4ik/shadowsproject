package ru.krogenit.inventory;

import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.customfont.FontType;

public class GuiHotSlot extends GuiMatrixSlotSpecial {

    public GuiHotSlot(float x, float y, float width, float height, ItemStack itemStack, MatrixInventory.SlotType slotType, MatrixInventory matrixInventory) {
        super(x, y, width, height, itemStack, slotType, matrixInventory);
    }

    @Override
    public void render(int mx, int my, boolean invHovered) {
        float fs = 1.13f;
        GL11.glColor4f(1f,1f,1f,1f);
        GuiDrawUtils.drawRect(TextureRegister.textureInvBorderForm, x, y, width, height);
        if(getSlotType() != null) GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, ""  + (getSlotType().ordinal() - 9), x + width / 1.27f, y + height / 1.19f, fs, 0x000000);
        GL11.glColor4f(1f,1f,1f,1f);
        if(itemStack != null) {
            renderItem(mx, my);
        } else if(slotTexture != null) {
            GL11.glColor4f(1f,1f,1f,1f);
            GuiDrawUtils.drawRect(slotTexture, x, y, width, height);
        }
    }

    @Override
    public void renderItem(int mx, int my) {
        GuiDrawUtils.renderItem(itemStack,0.8f, 0f, 0f, x, y, width, height, 0f);
    }

    @Override
    public GuiMatrixSlot copyEmpty() {
        return new GuiHotSlot(x, y, width, height, null, slotType, matrixInventory);
    }
}
