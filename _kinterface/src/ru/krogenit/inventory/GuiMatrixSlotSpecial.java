package ru.krogenit.inventory;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.util.ColorUtils;
import ru.xlv.core.common.inventory.MatrixInventory;

public class GuiMatrixSlotSpecial extends GuiMatrixSlot {

    private ResourceLocation bg, gradient, frame;

    public GuiMatrixSlotSpecial(float x, float y, float width, float height, ItemStack itemStack, MatrixInventory.SlotType slotType, MatrixInventory matrixInventory) {
        super(x, y, width, height, itemStack, slotType, matrixInventory);
    }

    @Override
    public void render(int mx, int my, boolean invHovered) {
        if(itemStack != null) {
            GL11.glColor4f(1f,1f,1f,0.9f);
            GuiDrawUtils.drawRect(bg, x, y, width, height);
            Vector3f color = ColorUtils.getColorByRarity(itemStack.getItem());
            if(invHovered && isHovered(mx, my)) GL11.glColor4f(color.x, color.y, color.z,1f);
            else GL11.glColor4f(color.x, color.y, color.z,0.5f);
            GuiDrawUtils.drawRect(gradient, x, y, width, height);
            GL11.glColor4f(1f,1f,1f,1f);
            GuiDrawUtils.drawRect(frame, x, y, width, height);
            renderItem(mx, my);
        } else if(slotTexture != null) {
            GL11.glColor4f(1f,1f,1f,1f);
            GuiDrawUtils.drawRect(slotTexture, x, y, width, height);
        }
    }

    @Override
    public void renderItem(int mx, int my) {
        GuiDrawUtils.renderItem(itemStack, 1.0f, 0f, 0f, x, y, width, height, 0f);
    }

    @Override
    public GuiMatrixSlot copyEmpty() {
        GuiMatrixSlotSpecial slot = new GuiMatrixSlotSpecial(x, y, width, height, null, slotType, matrixInventory);
        slot.setSlotTexture(slotTexture);
        slot.setBg(bg);
        slot.setFrame(frame);
        slot.setGradient(gradient);
        return slot;
    }

    public void setBg(ResourceLocation bg) {
        this.bg = bg;
    }

    public void setFrame(ResourceLocation frame) {
        this.frame = frame;
    }

    public void setGradient(ResourceLocation gradient) {
        this.gradient = gradient;
    }
}
