package ru.krogenit.npc;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.utils.AnimationHelper;

public class GuiNPCButtonSwitcher extends GuiButtonAdvanced {

    private ResourceLocation maskTexture;
    private float maskAnim;

    public GuiNPCButtonSwitcher(int id, float x, float y, float width, float height) {
        super(id, x, y, width, height, "");
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovering = isHovered(mouseX, mouseY);
        if(isHovering) {
            maskAnim = AnimationHelper.updateAnim(maskAnim, 1.0f, 0.006f);
            if(maskAnim >= 1f) {
                maskAnim -= 1f;
            }

            GuiDrawUtils.clearMaskBuffer(0, 0, mc.displayWidth, mc.displayHeight);
            float off = this.height / 8f;
            float x = xPosition - off;
            float y = yPosition - off;
            float width = this.width + off * 2;
            float height = this.height + off * 2;
            GuiDrawUtils.drawMaskingButtonEffect(maskTexture, x, y, width, height, this.xPosition - this.width / 2f, this.yPosition + this.height / 8f, this.width*2f, this.height/1.25f, maskAnim * 360f);
        }

        super.drawButton(mouseX, mouseY);
    }

    @Override
    protected void drawText() {
        super.drawText();
    }

    public void setMaskTexture(ResourceLocation maskTexture) {
        this.maskTexture = maskTexture;
    }
}
