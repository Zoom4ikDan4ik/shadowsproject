package ru.krogenit.npc;

import ru.krogenit.inventory.GuiMatrixSlot;
import ru.xlv.core.common.inventory.MatrixInventory;

public interface IInventoryWithDraggedSlot {
    GuiMatrixSlot getDraggedSlot();
    void setDraggedSlot(GuiMatrixSlot slot);
    void setDraggedSlotXY(float x, float y);
    float getDraggedSlotX();
    float getDraggedSlotY();
    boolean canTakeItem(MatrixInventory matrixInventory, GuiMatrixSlot slot);
    boolean canPutItem(MatrixInventory matrixInventory, GuiMatrixSlot slot);
    boolean isPlayerInventory(MatrixInventory matrixInventory);
    void putItem(int x, int y, MatrixInventory toInventory);
}
