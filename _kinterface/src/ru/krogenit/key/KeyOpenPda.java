package ru.krogenit.key;

import net.minecraft.client.settings.KeyBinding;
import ru.krogenit.client.key.AbstractKey;
import ru.krogenit.pda.gui.GuiPda;

public class KeyOpenPda extends AbstractKey {
    private boolean keyDown = false;
    private boolean keyUp = true;

    public KeyOpenPda(KeyBinding keyBindings) {
        super(keyBindings);
    }

    @Override
    public void keyDown() {
        if (mc.currentScreen == null && !keyDown && mc.thePlayer != null) {
            mc.displayGuiScreen(new GuiPda());
        }
    }

    @Override
    public void keyUp() {
        if (!keyUp) {
            keyDown = false;
            keyUp = true;
        }
    }
}
