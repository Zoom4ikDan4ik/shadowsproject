package ru.krogenit.settings.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.xlv.customfont.FontType;

public class GuiSettings extends AbstractGuiScreenAdvanced {

    private final GuiScreen parent;

    public GuiSettings(GuiScreen parent) {
        super(ScaleGui.SXGA);
        this.parent = parent;
    }

    @Override
    public void initGui() {
        super.initGui();
        this.buttonList.clear();

        float buttonWidth = 271;
        float buttonHeight = 40;
        float y = 457;
        float x = 960;
        float buttonOffsetY = 19 + buttonHeight;
        GuiButtonAnimated button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАСТРОЙКИ ЗВУКА");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
        y+=buttonOffsetY;
        button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАСТРОЙКИ ГРАФИКИ");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
        y+=buttonOffsetY;
        button = new GuiButtonAnimated(2, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАСТРОЙКИ УПРАВЛЕНИЯ");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
        y+=buttonOffsetY;
        y+=buttonOffsetY;
        button = new GuiButtonAnimated(3, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАЗАД");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 0) {
            mc.displayGuiScreen(new GuiAudioSettings(this));
        } else if(guiButton.id == 1) {
            mc.displayGuiScreen(new GuiGraphicsSettings(this));
        } else if(guiButton.id == 2) {
            mc.displayGuiScreen(new GuiControlSettings(this));
        } else if(guiButton.id == 3) {
            mc.displayGuiScreen(parent);
        }
    }

    private void drawMiscElements() {
        float fsx = 2.4f;
        String s = "НАСТРОЙКИ";
        float x = 960;
        float y = 345f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0x626262);

        fsx = 0.8f;
        x = 1866;
        y = 937;
        s = "VERSION 0.163 BETA";
        GuiDrawUtils.drawRightStringRightBot(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0xffffff);
        s = "Shadows Project - an original MMORPG-FPS\n" +
                "Massively Multiplayer Online Role-Playing First\n" +
                "Person Shooter) in a fantasy anti-utopian hi-tec future where the horrors\n" +
                "of space and all the “charms” of interplanetary travel await you.";
        fsx = 0.7f;
        y = 971;
        GuiDrawUtils.drawSplittedStringRightBot(FontType.DeadSpace, s, x, y, fsx, 1000f, -1f, 0x626262, EnumStringRenderType.RIGHT);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureSettingsBorderTop, 960, 291, 821, 78);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureSettingsBorderBot, 960, 771, 821, 78);
        GuiDrawUtils.drawBotCentered(TextureRegister.textureESCLogo, 137, 960, 184, 53);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawDefaultBackground();
        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);
        drawMiscElements();
        drawButtons(mouseX, mouseY, partialTick);
    }
}
