package ru.krogenit.settings.gui.button;

import net.minecraft.client.settings.GameSettings;
import org.lwjgl.opengl.GL11;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

public class GuiButtonControl extends GuiButtonAdvanced {

    private final GameSettings.Options enumOptions;
    private float blending;

    public GuiButtonControl(int id, float x, float y, float width, float height, GameSettings.Options enumOptions, String text) {
        super(id, x, y, width, height, text);
        this.enumOptions = enumOptions;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            boolean hovered = isHovered(mouseX, mouseY);
            if (hovered) {
                blending += AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending > 1) blending = 1;
            } else {
                blending -= AnimationHelper.getAnimationSpeed() * 0.1f;
                if (blending < 0) blending = 0f;
            }

            Utils.bindTexture(texture);
            if(enabled) GL11.glColor4f(1f, 1f, 1f, 1f);
            else GL11.glColor4f(0.3f, 0.3f, 0.3f, 1f);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

            if (blending > 0) {
                GL11.glColor4f(1f, 1f, 1f, blending);
                GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
            }

            GL11.glColor4f(1f, 1f, 1f, 1f);
            drawText();
        }
    }

    protected void drawText() {
        float offsetY = mc.displayHeight / 400f;
        float textScale = 0.9f;

        String s = displayString.toUpperCase();
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, s, this.xPosition + this.width / 2f,
                this.yPosition + this.height / 2.0f - offsetY, textScale, enabled ? 0xffffff : 0x444444);
    }

    public GameSettings.Options returnEnumOptions()
    {
        return this.enumOptions;
    }
}
