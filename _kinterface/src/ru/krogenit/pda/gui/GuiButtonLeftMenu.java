package ru.krogenit.pda.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

import java.util.ArrayList;
import java.util.List;

public class GuiButtonLeftMenu extends GuiButtonAdvanced {

    private ResourceLocation activeTexture, inactiveTexture, inactiveHoverTexture;
    public float xPositionBase, yPositionBase;
    public float widthBase;
    private final List<GuiButtonLeftMenu> subButtons = new ArrayList<>();
    private float animation, sideAnim;
    private boolean isPressed;
    private float subButtonsBlockHeight;
    private final boolean info;
    private String infoString;
    private final EnumPdaLeftMenuSection enumId;
    private GuiButtonLeftMenu parent;

    public GuiButtonLeftMenu(EnumPdaLeftMenuSection buttonId, float x, float y, float widthIn, float heightIn, String buttonText, boolean info) {
        super(buttonId.ordinal(), x, y, widthIn, heightIn, buttonText);
        this.xPositionBase = x;
        this.yPositionBase = y;
        this.info = info;
        this.widthBase = widthIn;
        this.enumId = buttonId;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            if(enabled) {
                if(isPressed) {
                    animation = AnimationHelper.updateAnim(animation, 1, 0.075f);
                } else {
                    animation = AnimationHelper.updateAnim(animation, 0, -0.075f);
                }

                if(animation > 0) {
                    if(subButtons.size() > 0) drawSubButtons(mc, mouseX, mouseY);
                } else {
                    subButtonsBlockHeight = 0;
                }

                if(isPressed) {
                    Utils.bindTexture(activeTexture);
                } else {
                    Utils.bindTexture(texture);
                }
            } else {
                Utils.bindTexture(inactiveTexture);
            }

            boolean hovered = isHovered(mouseX, mouseY);
            if(hovered && this.hovered != hovered) {
                SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
            }
            this.hovered = hovered;
            if(!isPressed && hovered) {
                if(enabled) {
                    Utils.bindTexture(textureHover);
                } else {
                    Utils.bindTexture(inactiveHoverTexture);
                }
            }
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height, 1f, 1f, 1f, 1f);
            drawText();

            if(info) {
                float iconWidth = this.width / 3.84375f;
                float iconHeight = this.height / 2f;
                float x = this.xPosition + this.width / 1.225f - iconWidth / 2f;
                float y = this.yPosition + this.height / 2f - iconHeight / 2f;
                GuiDrawUtils.drawRect(TextureRegister.texturePDAButtonSubInfo, x, y, iconWidth, iconHeight, 1f, 1f, 1f, 1f);

                float offsetY = mc.displayHeight / 800f;
                float textScale = 0.7f;
                GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrRoman, infoString, x + iconWidth / 2.25f,
                        y - offsetY + iconHeight / 2f, textScale * (width / widthBase), 0xffffff);
            }
        }
    }

    private void drawSubButtons(Minecraft mc, int mouseX, int mouseY) {
        float yOffset = this.height + this.height / 4f;
        float x = xPosition + this.width / 5.1f;
        float y = yPosition + yOffset;
        float buttonWidthAnim = animation > 0.66f ? (animation - 0.66f) * 3.33f : 0;
        if(buttonWidthAnim > 1) buttonWidthAnim = 1f;
        for (GuiButtonLeftMenu b : subButtons) {
            b.xPosition = x;
            b.yPosition = y;
            b.width = b.widthBase * buttonWidthAnim;
            b.drawButton(mc, mouseX, mouseY);
            y += yOffset;
        }

        float lineStartX = xPosition + this.width / 10.2f;
        float lineStartY = yPosition + this.height;
        float lineEndY = y - yOffset + this.height / 2f;
        float downLineAnim = animation * 2f;
        if(downLineAnim > 1f) downLineAnim = 1f;
        float toRightLinesAnim = animation > 0.33f ? ((animation - 0.33f) * 3.33f) : 0f;
        if(toRightLinesAnim > 1f) toRightLinesAnim = 1f;
        subButtonsBlockHeight = (y - yPosition - yOffset) * downLineAnim;

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glColor4f(1f,1f,1f,1f);
        GL11.glLineWidth(2f);
        GL11.glBegin(GL11.GL_LINES);
        GL11.glVertex2f(lineStartX, lineStartY);
        GL11.glVertex2f(lineStartX, lineStartY + (lineEndY - lineStartY)*downLineAnim);
        for (GuiButtonLeftMenu b : subButtons) {
            GL11.glVertex2f(lineStartX, b.yPosition + b.height / 2f);
            GL11.glVertex2f(lineStartX + (b.xPosition - lineStartX) * toRightLinesAnim, b.yPosition + b.height / 2f);
        }
        GL11.glEnd();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    @Override
    protected void drawText() {
        float offsetY = mc.displayHeight / 800f;
        float textScale = 0.84375f;

        if(widthBase != 0) {
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman, displayString, this.xPosition + this.width / 8.0f,
                    this.yPosition + this.height / 2.0f - offsetY, textScale * (width / widthBase), 0xffffff);
        } else {
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman, displayString, this.xPosition + this.width / 8.0f,
                    this.yPosition + this.height / 2.0f - offsetY, textScale, 0xffffff);
        }

    }

    public GuiButtonLeftMenu mousePressed(int mouseX, int mouseY) {
        if(enabled && isHovered(mouseX, mouseY)) {
            return this;
        }

        if(isPressed) {
            for(GuiButtonLeftMenu b : subButtons) {
                GuiButtonLeftMenu b1 = b.mousePressed(mouseX, mouseY);
                if(b1 != null) {
                    return b1;
                }
            }
        }

        return null;
    }

    @Override
    protected boolean isHovered(int mouseX, int mouseY) {
        return this.field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
    }

    public void setIsPressed(boolean isPressed) {
        this.isPressed = isPressed;
    }

    public boolean isPressed() {
        return this.isPressed;
    }

    public void addYPosition(float y) {
        this.yPosition = yPositionBase + y;
    }

    public void addXPosition(float x) {
        this.xPosition = xPositionBase + x;
    }

    public void updateSideAnimation() {
        if(sideAnim < 1) {
            float speed = (1f - sideAnim);
            if(speed < 0.0002f) speed = 0.0002f;
            sideAnim += AnimationHelper.getAnimationSpeed() * 0.25f * speed;
            if(sideAnim > 1) {
                sideAnim = 1f;
            }
        }
    }

    public float getSideAnim() {
        return sideAnim;
    }

    public void setActiveTexture(ResourceLocation activeTexture) {
        this.activeTexture = activeTexture;
    }

    public void setInactiveHoverTexture(ResourceLocation inactiveHoverTexture) {
        this.inactiveHoverTexture = inactiveHoverTexture;
    }

    public void setInactiveTexture(ResourceLocation inactiveTexture) {
        this.inactiveTexture = inactiveTexture;
    }

    public void addSubButton(GuiButtonLeftMenu b) {
        b.setTexture(TextureRegister.texturePDAButtonSub);
        b.setTextureHover(TextureRegister.texturePDAButtonSubHover);
        b.setActiveTexture(TextureRegister.texturePDAButtonSubActive);
        b.setInactiveTexture(TextureRegister.texturePDAButtonSubInactive);
        b.setInactiveHoverTexture(TextureRegister.texturePDAButtonSubInactiveHover);
        b.setParent(this);

        this.subButtons.add(b);
    }

    public void setTexture(EnumPdaLeftMenuSection leftMenuSection) {
        ResourceLocation[] textures = TextureRegister.getPDAButtonTexture(leftMenuSection);
        setTexture(textures[0]);
        setTextureHover(textures[1]);
        setActiveTexture(textures[2]);
        setInactiveTexture(textures[3]);
        setInactiveHoverTexture(textures[4]);
    }

    public void setInfoString(String infoString) {
        this.infoString = infoString;
    }

    public float getSubButtonsBlockHeight() {
        return subButtonsBlockHeight;
    }

    public List<GuiButtonLeftMenu> getSubButtons() {
        return subButtons;
    }

    public EnumPdaLeftMenuSection getEnumId() {
        return enumId;
    }

    public void setParent(GuiButtonLeftMenu parent) {
        this.parent = parent;
    }

    public GuiButtonLeftMenu getParent() {
        return parent;
    }
}
