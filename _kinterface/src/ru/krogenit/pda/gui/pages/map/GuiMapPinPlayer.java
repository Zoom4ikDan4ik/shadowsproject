package ru.krogenit.pda.gui.pages.map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;

public class GuiMapPinPlayer extends GuiMapPin {

    private final PlayerMapPin playerPin;
    private final String playerClass;

    public GuiMapPinPlayer(float x, float y, float widthIn, float heightIn, PlayerMapPin pin) {
        super(x, y, widthIn, heightIn, pin, pin.getPinName());
        this.playerPin = pin;
        setTexture(TextureRegister.texturePDAMapIconPlayer);
        this.playerClass = playerPin.getCharacterType().getDisplayName().toUpperCase();
    }

    public void drawPopup(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        if(isHovered) {
            float x1 = xPosition + ScaleGui.get(45f);
            float yRightUp = yPosition - ScaleGui.get(13f);
            float yRightDown = yRightUp + ScaleGui.get(80f);

            float fsMain = 26 / 32f;
            float fsMainScaled = ScaleGui.get(26 / 32f);
            float fsDesc = 19 / 32f;
            float fsDescScaled = ScaleGui.get(19 / 32f);
            FontContainer fontContainer = FontType.HelveticaNeueCyrRoman.getFontContainer();
            float sl = fontContainer.width(displayString) * fsMainScaled;

            float sl1 = fontContainer.width(playerClass) * fsDescScaled;
            if(sl1 > sl) sl = sl1;
            sl1 = fontContainer.width("LVL " + playerPin.getPlayerLvl()) * fsDescScaled;
            if(sl1 > sl) sl = sl1;
            sl1 = fontContainer.width(playerPin.getPlayerLocation()) * fsDescScaled;
            if(sl1 > sl) sl = sl1;

            float x2 = x1 + sl + ScaleGui.get(63f);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(12/255f, 76/255f, 91/255f, 0.9f);
            tessellator.addVertex(x1, yRightDown,0f);
            tessellator.addVertex(x2, yRightDown, 0);
            tessellator.addVertex(x2, yRightUp, 0f);
            tessellator.addVertex(x1, yRightUp, 0f);
            tessellator.draw();

            GL11.glColor4f(55/255f, 140/255f, 187/255f, 1f);
            float lineWidth = ScaleGui.get(2f);
            GL11.glLineWidth(lineWidth);
            GL11.glBegin(GL11.GL_LINE_STRIP);
            GL11.glVertex2f(x1, yRightDown);
            GL11.glVertex2f(x2, yRightDown);
            GL11.glVertex2f(x2, yRightUp);
            GL11.glVertex2f(x1, yRightUp);
            GL11.glVertex2f(x1, yRightDown);
            GL11.glEnd();
            GL11.glEnable(GL11.GL_TEXTURE_2D);

            GL11.glColor4f(1f,1f,1f,1f);
            float x = x1 + ScaleGui.get(24f);
            float y = yRightUp + ScaleGui.get(24f);
            float iconSize = ScaleGui.get(28f);
            GuiDrawUtils.drawRectCentered(TextureRegister.getClassIcon(playerPin.getCharacterType()), x, y, iconSize, iconSize);
            if(playerPin.isLeader()) {
                y+= iconSize + ScaleGui.get(4f);
                GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAGroupIconLeader, x, y, iconSize, iconSize);
            }

            float textOffsetY = 0;
            x += iconSize / 2f + ScaleGui.get(8f);
            y = yRightUp + ScaleGui.get(24f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman, displayString, x, y, fsMain, 0xffffff);
            textOffsetY += ScaleGui.get(22f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  playerClass, x, y + textOffsetY, fsDesc, 0xffffff);
            textOffsetY += ScaleGui.get(10f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  "LVL " + playerPin.getPlayerLvl(), x, y + textOffsetY, fsDesc, 0xffffff);
            textOffsetY += ScaleGui.get(10f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  playerPin.getPlayerLocation(), x, y + textOffsetY, fsDesc, 0xffffff);
        }
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        if(isHovered) {
            float x = xPosition + width / 2f;
            float x1 = xPosition + ScaleGui.get(45f);
            float yLeftUp = yPosition + ScaleGui.get(4f);
            float yRightUp = yPosition - ScaleGui.get(13f);
            float yRightDown = yRightUp + ScaleGui.get(80f);
            float yLeftDown = yPosition + height - ScaleGui.get(4f);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(0f, 180 / 255f, 219 / 255f, 0.5f);
            tessellator.addVertex(x, yLeftDown, 0f);
            tessellator.setColorRGBA_F(12 / 255f, 76 / 255f, 91 / 255f, 0.63f);
            tessellator.addVertex(x1, yRightDown, 0);
            tessellator.addVertex(x1, yRightUp, 0f);
            tessellator.setColorRGBA_F(0f, 180 / 255f, 219 / 255f, 0.5f);
            tessellator.addVertex(x, yLeftUp, 0f);
            tessellator.draw();
            GL11.glShadeModel(GL11.GL_FLAT);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
        }

        GL11.glColor4f(1f,1f,1f,1f);
        Utils.bindTexture(texture);
        GuiDrawUtils.drawRect(xPosition, yPosition, width, height);

        Vector3f playerColor = playerPin.getPlayerColor();
        GL11.glColor4f(playerColor.x, playerColor.y, playerColor.z, 1.0f);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_CULL_FACE);
        drawFilledCircle(xPosition + width / 2f, yPosition + height / 2f, ScaleGui.get(7f), 10);
        GL11.glColor4f(1f,1f,1f,1f);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_CULL_FACE);
        if(!isHovered) GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  displayString, xPosition + width + ScaleGui.get(2f), yPosition + height /2f, 20 / 32f, 0xffffff);
    }


    private void drawFilledCircle(float x, float y, float radius, int count){
        int i;
        double twicePi = 2.0 * 3.142;
        GL11.glBegin(GL11.GL_TRIANGLE_FAN);
        GL11.glVertex2f(x, y);
        for (i = 0; i <= count; i++)   {
            GL11.glVertex2f((x + (radius * (float)Math.cos(i * twicePi / (float)count))), (y + (radius * (float)Math.sin(i * twicePi / (float)count))));
        }
        GL11.glEnd();
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        return false;
    }
}
