package ru.krogenit.pda.gui.pages.map;

import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;

public class GuiMapPinQuestKill extends GuiMapPin {

    private final QuestMapPin questPin;

    public GuiMapPinQuestKill(float x, float y, float widthIn, float heightIn, QuestMapPin pin) {
        super(x, y, widthIn, heightIn, pin, pin.getPinName());
        this.questPin = pin;
        setTexture(TextureRegister.texturePDAMapPinQuest);
    }

    public void drawPopup(int mouseX, int mouseY) {
        boolean isHovered = isHovered(mouseX, mouseY);
        if(isHovered) {
            float descIconSize = ScaleGui.get(10f);
            float descOffsetY = descIconSize + ScaleGui.get(9f);
            float x1 = xPosition + ScaleGui.get(55f);
            float yRightUp = yPosition - ScaleGui.get(13f);
            float totalDescOffset = questPin.getDescription().size() * descOffsetY;
            float yRightDown = yRightUp + totalDescOffset + (ScaleGui.get(98f));

            float textOffsetX = ScaleGui.get(14f);
            float textOffsetY = ScaleGui.get(19f);
            float fsMain = 30 / 32f;
            float fsMainScaled = ScaleGui.get(fsMain);
            float fsDesc = 23 / 32f;
            float fsDescScaled = ScaleGui.get(fsDesc);
            FontContainer fontContainer = FontType.HelveticaNeueCyrRoman.getFontContainer();
            float sl = fontContainer.width(displayString) * fsMainScaled;

            for(String desc : questPin.getDescription()) {
                float sl1 = fontContainer.width(desc) * fsDescScaled;
                if(sl1 > sl) {
                    sl = sl1;
                }
            }

            float x2 = x1 + sl + ScaleGui.get(27f);
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(12/255f, 76/255f, 91/255f, 0.9f);
            tessellator.addVertex(x1, yRightDown,0f);
            tessellator.addVertex(x2, yRightDown, 0);
            tessellator.addVertex(x2, yRightUp, 0f);
            tessellator.addVertex(x1, yRightUp, 0f);
            tessellator.draw();

            GL11.glColor4f(55/255f, 140/255f, 187/255f, 1f);
            float lineWidth = ScaleGui.get(2f);
            float rectHeight = ScaleGui.get(2f);
            GL11.glLineWidth(lineWidth);
            GL11.glBegin(GL11.GL_LINE_STRIP);
            GL11.glVertex2f(x1, yRightDown);
            GL11.glVertex2f(x2, yRightDown);
            GL11.glVertex2f(x2, yRightUp);
            GL11.glVertex2f(x1, yRightUp);
            GL11.glVertex2f(x1, yRightDown);
            GL11.glEnd();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glColor4f(1f,1f,1f,1f);

            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman, displayString, x1 + textOffsetX, yRightUp + textOffsetY, fsMain, 0xffffff);
            textOffsetY += ScaleGui.get(18f);
            float borderWidth = ScaleGui.get(162f);
            GuiDrawUtils.drawRect(TextureRegister.texturePDAMapBorder, x1 + textOffsetX, yRightUp + textOffsetY, borderWidth, rectHeight);
            textOffsetY += ScaleGui.get(16f);

            for(String desc : questPin.getDescription()) {
                GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAMapIconKill,x1 + textOffsetX + descIconSize / 2f, yRightUp + textOffsetY, descIconSize, descIconSize);
                GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  desc, x1 + textOffsetX + descIconSize + ScaleGui.get(5f), yRightUp + textOffsetY, fsDesc, 0xffffff);
                textOffsetY += descOffsetY;
            }

            textOffsetY -= ScaleGui.get(4f);
            GuiDrawUtils.drawRect(TextureRegister.texturePDAMapBorder,x1 + textOffsetX, yRightUp + textOffsetY, borderWidth, rectHeight);
            textOffsetY += ScaleGui.get(19f);
            float iconWidth = ScaleGui.get(11f);
            float iconHeight = ScaleGui.get(20f);
            GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAMapLeftMouse,x1 + textOffsetX + iconWidth / 2f, yRightUp + textOffsetY + iconWidth /2f, iconWidth, iconHeight);
            textOffsetY += ScaleGui.get(4f);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrRoman,  "ПОДРОБНЕЕ", x1 + textOffsetX + iconWidth + ScaleGui.get(5f), yRightUp + textOffsetY, fsDesc, 0xffffff);
        }
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if(isHovered(mouseX, mouseY)) {
            float descIconSize = ScaleGui.get(10f);
            float descOffsetY = descIconSize + ScaleGui.get(9f);
            float x = xPosition + width / 1.07f;
            float x1 = xPosition + ScaleGui.get(55f);
            float yLeftUp = yPosition + ScaleGui.get(2f);
            float yRightUp = yPosition - ScaleGui.get(13f);
            float totalDescOffset = questPin.getDescription().size() * descOffsetY;
            float yRightDown = yRightUp + totalDescOffset + (ScaleGui.get(98f));
            float yLeftDown = yPosition + (ScaleGui.get(33f));
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            tessellator.setColorRGBA_F(0f, 180 / 255f, 219 / 255f, 0.5f);
            tessellator.addVertex(x, yLeftDown, 0f);
            tessellator.setColorRGBA_F(12 / 255f, 76 / 255f, 91 / 255f, 0.63f);
            tessellator.addVertex(x1, yRightDown, 0);
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0f);
            tessellator.addVertex(x1, yRightUp, 0f);
            tessellator.setColorRGBA_F(0f, 180 / 255f, 219 / 255f, 0.5f);
            tessellator.addVertex(x, yLeftUp, 0f);
            tessellator.draw();
            GL11.glShadeModel(GL11.GL_FLAT);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
        }

        GL11.glColor4f(1f,1f,1f,1f);
        Utils.bindTexture(texture);
        GuiDrawUtils.drawRect(xPosition, yPosition, width, height);
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAMapIconQuestion, xPosition + width / 2f,
                yPosition + ScaleGui.get(18f), ScaleGui.get(13f), ScaleGui.get(17f));
    }
}
