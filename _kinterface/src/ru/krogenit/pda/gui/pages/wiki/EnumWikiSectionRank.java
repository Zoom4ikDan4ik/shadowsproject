package ru.krogenit.pda.gui.pages.wiki;

public enum EnumWikiSectionRank {
    NOVICE("НОВИЧОК");

    private String localized;

    EnumWikiSectionRank(String localized) {
        this.localized = localized;
    }

    public String getLocalizedName() {
        return localized;
    }
}
