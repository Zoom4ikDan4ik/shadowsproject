package ru.krogenit.pda.gui.pages.wiki;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.util.Utils;
import ru.xlv.customfont.FontType;

@Getter
@Setter
public class GuiSlotWiki extends GuiButtonAdvanced {

    protected EnumWikiSection wikiSection;
    private int opened, total;
    private int totalToNextRank;
    private EnumWikiSectionRank wikiSectionRank;
    protected float firstPoint, fs1;

    public GuiSlotWiki(EnumWikiSection wikiSection, float x, float y, float widthIn, float heightIn) {
        super(0, x, y, widthIn, heightIn, wikiSection.getLocalizedName());
        this.wikiSection = wikiSection;
    }

    protected void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
        }
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        if (this.visible) {
            updateAnimation();
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glColor4f(0f,0f,0f,0.6f);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
            GL11.glColor4f(1f,1f,1f,1f);
            GL11.glEnable(GL11.GL_TEXTURE_2D);

            boolean isHovered = isHovered(mouseX, mouseY);

            Utils.bindTexture(texture);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height / (275 / 202f));

            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_SMOOTH);
            Tessellator tessellator = Tessellator.instance;
            tessellator.startDrawingQuads();
            if(isHovered) tessellator.setColorRGBA_F(0f, 174/255f, 1f, 0.4f);
            else tessellator.setColorRGBA_F(0f, 174/255f, 1f, 0.2f);
            tessellator.addVertex(xPosition, yPosition + height,0f);
            tessellator.addVertex(xPosition + width, yPosition + height, 0);
            tessellator.setColorRGBA_F(0f, 0f, 0f, 0.1f);
            tessellator.addVertex(xPosition + width, yPosition, 0f);
            tessellator.addVertex(xPosition, yPosition, 0f);
            tessellator.draw();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glShadeModel(GL11.GL_FLAT);

            drawText();
        }
    }

    protected void drawText() {
        float textScale = 46.24f / 32f;
        GuiDrawUtils.drawCenteredStringNoXYScale(FontType.HelveticaNeueCyrLight, displayString, this.xPosition + this.width / 2.0f, yPosition + ScaleGui.get(172f), textScale, 0xffffff);

        float x = xPosition + ScaleGui.get(43f);
        float y = yPosition + ScaleGui.get(239f);
        float iconWidth = ScaleGui.get(37f);
        float stringMinusYOffset = ScaleGui.get(12f);
        float stringPositiveYOffset = ScaleGui.get(8f);
        float stringXOffset = ScaleGui.get(27f);
        float fs = (36f / 32f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        if(wikiSection == EnumWikiSection.MECHANICS) {
            String s = "СТАТЕЙ: ";
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  "" + total, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
            s = "ПРОЧИТАНО: ";
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "" + opened, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        } else {
            String s = "ОТКРЫТО: ";
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  "" + opened + "/" + total, x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
            s = "РАНГ: ";
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight,  s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
            GuiDrawUtils.drawStringNoXYScale(FontType.HelveticaNeueCyrLight, "" + wikiSectionRank.getLocalizedName() + " (" + opened + "/" + totalToNextRank + ")", x + stringXOffset + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        }

        GL11.glColor4f(1f, 1f, 1f, 1f);
    }
}
