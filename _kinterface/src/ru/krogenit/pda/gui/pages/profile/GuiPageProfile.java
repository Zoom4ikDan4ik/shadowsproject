package ru.krogenit.pda.gui.pages.profile;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderPlayerEvent;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.AbstractGuiScreenAdvanced;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.GuiProfileDescription;
import ru.krogenit.pda.gui.windows.achievements.GuiSlotAchieve;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import static org.lwjgl.opengl.GL11.glColor4f;

@Getter
public class GuiPageProfile extends AbstractGuiScreenAdvanced {

    private GuiProfileDescription field;
    @Setter private int playerLvl;
    @Setter private String playerName, registryDate;
    private CharacterType characterType;
    @Setter private ResourceLocation playerAvatar;
    @Setter private EnumOnlineStatus status;
    @Setter private String description;
    private String characterName;
    private final GuiPda pda;

    private float firstPoint, fs1;

    private final Vector3f lightPosition = new Vector3f(0f, -40f, 100f);
    private final Vector3f lightColor = new Vector3f(1.5f, 1.5f, 2f);
    @Setter private AbstractClientPlayer entityPlayer;

    public GuiPageProfile(float minAspect, GuiPda pda) {
        super(minAspect);
        this.pda = pda;
        setPlayerLvl(XlvsMainMod.INSTANCE.getClientMainPlayer().getLvl());
        setPlayerName(mc.thePlayer.getDisplayName().toUpperCase());
        setDescription("Описание профиля");
        setRegistryDate("01.01.1970");
        setPlayerAvatar(TextureRegister.texturePDAProfileAva);
        setCharacterType(XlvsMainMod.INSTANCE.getCurrentCharacterType());
        setStatus(EnumOnlineStatus.ONLINE);
        entityPlayer = mc.thePlayer;
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();

        float buttonWidth = 170;
        float buttonHeight = 40;
        float x = ScaleGui.getCenterX(705, buttonWidth);
        float y = ScaleGui.getCenterY(406, buttonHeight);
        GuiButtonAnimated b = new GuiButtonAnimated(0, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИЗМЕНИТЬ");
        b.setTexture(TextureRegister.textureTreeButtonCancel);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);

        buttonWidth = 526;
        buttonHeight = 89;
        x = ScaleGui.getCenterX(1445, buttonWidth);
        y = ScaleGui.getCenterY(414, buttonHeight);
        field = new GuiProfileDescription(x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), ScaleGui.get(1.25f));
        field.setText(description);

        buttonWidth = 170;
        buttonHeight = 40;
        x = ScaleGui.getCenterX(1447, buttonWidth);
        y = ScaleGui.getCenterY(496, buttonHeight);
        b = new GuiButtonAnimated(1, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИЗМЕНИТЬ");
        b.setTexture(TextureRegister.textureTreeButtonCancel);
        b.setTextureHover(TextureRegister.textureTreeButtonHover);
        b.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(b);

        buttonWidth = 231;
        buttonHeight = 40;
        x = ScaleGui.getCenterX(869, buttonWidth);
        y = ScaleGui.getCenterY(888, buttonHeight);
        b = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ДРУЗЬЯ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1158, buttonWidth);
        b = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ЗАЯВКИ В ДРУЗЬЯ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1446, buttonWidth);
        b = new GuiButtonAnimated(4, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ИГРОКИ СЕРВЕРА");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        x = ScaleGui.getCenterX(1446, buttonWidth);
        y = ScaleGui.getCenterY(782, buttonHeight);
        b = new GuiButtonAnimated(5, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВСЕ ДОСТИЖЕНИЯ");
        b.setTexture(TextureRegister.textureESCButton);
        b.setTextureHover(TextureRegister.textureESCButtonHover);
        b.setMaskTexture(TextureRegister.textureESCButtonMask);
        buttonList.add(b);

        buttonWidth = 170;
        buttonHeight = 195;
        x = 1267;
        y = ScaleGui.getCenterY(643, buttonHeight);
        for(int i=0;i<3;i++) {
            GuiSlotAchieveProfile a = new GuiSlotAchieveProfile(6 + i, ScaleGui.getCenterX(x, buttonWidth), y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЫБЕРИТЕ ДОСТИЖЕНИЕ");
            a.setTexture(TextureRegister.texturePDAProfileSlotAchieve);
            buttonList.add(a);
            x+=8+buttonWidth;
        }
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 0) {
            pda.changeAvatar();
        } else if(b.id == 1) {
            pda.saveProfileDescription(field.getText());
        } else if(b.id == 2) {
            pda.showFriends();
        } else if(b.id == 3) {
            pda.showIncomingFriendRequests();
        } else if(b.id == 4) {
            pda.showServerPlayers();
        } else if(b.id == 5) {
//            pda.showAchievements(null, null);
        } else if(b.id >= 6) {
//            pda.showAchievements(this, (GuiSlotAchieveProfile) b);
        }
    }

    public void setAchievementToSlot(GuiSlotAchieveProfile b, GuiSlotAchieve a) {
        b.setAchievement(a.getAchievement());
    }

    private void updateAnimation() {
        firstPoint = AnimationHelper.updateSlowEndAnim(firstPoint, 1f, 0.1f, 0.0001f);
        if(firstPoint > 0.9f) {
            fs1 = AnimationHelper.updateSlowEndAnim(fs1, 1f, 0.1f, 0.0001f);
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        updateAnimation();
        GL11.glColor4f(1f, 1f, 1f, 1f);
        float scale = 1.5f;
        float iconWidth = 1366 / scale;
        float iconHeight = 356 / scale;
        float x = 976;
        float y = 720;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor, x, y - ((1f - fs1) * 1000f), iconWidth, iconHeight);
        iconWidth = 1803 / scale;
        iconHeight = 493 / scale;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor1, x, y - ((1f - fs1) * 1000f), iconWidth, iconHeight);
        iconWidth = 542 / scale;
        iconHeight = 1167 / scale;
        y = 360;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerLight, x, y - ((1f - fs1) * 1000f), iconWidth, iconHeight);
        iconWidth = 1385 / scale;
        iconHeight = 369 / scale;
        y = 720;
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureInvPlayerFloor2, x, y - ((1f - fs1) * 1000f), iconWidth, iconHeight);

        glColor4f(1f, 1f, 1f, 1f);
        x = ScaleGui.getCenterX(976, 0);
        y = ScaleGui.getCenterY(426 - ((1f - fs1) * 1000f), 0);
        scale = ScaleGui.get(168.75f) * 1.05f;
        renderPlayer(x, y, scale);

        GL11.glColor4f(1f, 1f, 1f,1f);
        GuiDrawUtils.drawCenterCentered(TextureRegister.getClassIcon(characterType), 1164, 210, 55 * firstPoint, 56 * firstPoint);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderDevider, 1160, 828, 1096 * firstPoint, 2);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileBorderTop, 1389, 273, 505 * fs1, 1);
        GuiDrawUtils.drawCenterCentered(playerAvatar, 706, 276, 188 * fs1, 188 * fs1);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAIconCircle, 1155, 312, 37 * firstPoint, 36 * firstPoint);
        GuiDrawUtils.drawCenterCentered(TextureRegister.texturePDAProfileFaceBio, 705, 625, 187 * fs1, 312 * fs1);

        x = 1212;
        y = 190;
        float fs = 1.875f * fs1;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerName, x, y, fs, 0xffffff);
        fs = 0.9375f * fs1;
        y+=41;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, characterName, x, y, fs, 0xffffff);
        y+=19;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, playerLvl + " УР.", x, y, fs, 0xffffff);

        x = 1183;
        y+=49;
        fs = 1.1159375f * fs1;
        String s = "СТАТУС: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x, y, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, this.status.toString(), x+ FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y, fs, 0x1BC3EC);
        y+=22;
        s = "ДАТА РЕГИСТРАЦИИ: ";
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, s, x, y, fs, 0xffffff);
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "" + registryDate, x + FontType.HelveticaNeueCyrLight.getFontContainer().width(s) * fs, y, fs, 0x1BC3EC);
        fs = 0.9375f * fs1;
        y+=32;
        GuiDrawUtils.drawStringCenter(FontType.HelveticaNeueCyrLight, "СЛОГАН", x, y, fs, 0xffffff);

        drawButtons(mouseX, mouseY, partialTick);
        field.drawTextBox(mouseX, mouseY);
    }

    private void renderPlayer(float x, float y, float scale) {
        if (entityPlayer != null)
            GuiDrawUtils.drawPlayer(entityPlayer, x, y, 50f, scale, 180f, 1f, 1f, 1f, lightPosition, lightColor, new RenderPlayerEvent.Specials.Post(entityPlayer, (RenderPlayer) RenderManager.instance.getEntityRenderObject(entityPlayer), 0f));
        else {
            //TODO: получение информации о профиле онлайн игрока или за пределами видимости;
            GuiDrawUtils.drawPlayer(entityPlayer, x, y, 50f, scale, 180f, 1f, 1f, 1f, lightPosition, lightColor, new RenderPlayerEvent.Specials.Post(entityPlayer, (RenderPlayer) RenderManager.instance.getEntityClassRenderObject(EntityPlayer.class), 0f));
        }
    }

    @Override
    public void updateScreen() {
        field.updateCursorCounter();
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        firstPoint = fs1 = 1f;
        field.mouseClicked(mouseX, mouseY, mouseButton);
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void keyTyped(char character, int key) {
        super.keyTyped(character, key);
        field.textboxKeyTyped(character, key);
    }

    public void setCharacterType(CharacterType characterType) {
        this.characterType = characterType;
        this.characterName = characterType.getDisplayName().toUpperCase();
    }
}
