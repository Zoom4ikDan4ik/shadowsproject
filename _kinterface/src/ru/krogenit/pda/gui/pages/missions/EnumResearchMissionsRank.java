package ru.krogenit.pda.gui.pages.missions;

public enum EnumResearchMissionsRank {
    LEARNER("УЧЕНИК"), CONNOISSEUR("ЗНАТОК");

    String localized;

    EnumResearchMissionsRank(String localized) {
        this.localized = localized;
    }

    public String getLocalizedName() {
        return localized;
    }
}
