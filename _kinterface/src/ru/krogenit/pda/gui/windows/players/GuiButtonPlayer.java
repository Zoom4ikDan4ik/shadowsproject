package ru.krogenit.pda.gui.windows.players;

import lombok.Getter;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.EnumStringRenderType;
import ru.krogenit.pda.gui.windows.ButtonContent;
import ru.krogenit.pda.gui.windows.GuiWinButtonWithSubs;
import ru.krogenit.pda.gui.windows.IGuiWindowWithSubActions;
import ru.krogenit.util.TimeUtils;
import ru.xlv.core.common.player.character.CharacterType;

@Getter
public class GuiButtonPlayer extends GuiWinButtonWithSubs {

    private final String playerName;
    private final CharacterType characterType;
    private final int level;
    private final long lastOnline;
    private final boolean isFriend;
    private final boolean isInGroup;

    public GuiButtonPlayer(int id, float x, float y, float width, float height, IGuiWindowWithSubActions guiWindow, String playerName,
                           CharacterType characterType, int level, long lastOnline, boolean isFriend, boolean isInGroup, boolean isBlocked) {
        super(id, x, y, width, height, guiWindow);
        this.playerName = playerName;
        this.characterType = characterType;
        this.level = level;
        this.lastOnline = lastOnline;
        this.isFriend = isFriend;
        this.isInGroup = isInGroup;
        addContent(ButtonContent.newBuilder().setString("" + id).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString(playerName).setWidth(283).build());
        addContent(ButtonContent.newBuilder().setTexture(TextureRegister.getClassIcon(characterType)).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString("" + level).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString(TimeUtils.getLastOnlineTime(lastOnline)).setStringRenderType(EnumStringRenderType.CENTERED).setWidth(38).build());
        addContent(ButtonContent.newBuilder().setString("Написать").setStringRenderType(EnumStringRenderType.CENTERED).setId(0).setWidth(86).build());
        addContent(ButtonContent.newBuilder().setString("Профиль").setStringRenderType(EnumStringRenderType.CENTERED).setId(1).setWidth(83).build());
        addContent(ButtonContent.newBuilder().setString("В группу").setStringRenderType(EnumStringRenderType.CENTERED).setId(2).setWidth(81).build());
        addContent(ButtonContent.newBuilder().setString(isFriend ? "Из друзей" : "В друзья").setStringRenderType(EnumStringRenderType.CENTERED).setId(3).setWidth(81).build());
        addContent(ButtonContent.newBuilder().setString(isBlocked ? "Разблокировать" : "В черный список").setStringRenderType(EnumStringRenderType.CENTERED).setId(4).setWidth(138).build());
    }

    public void setFriend() {
        contents.get(8).setInfo("Из друзей");
    }

    public void setInGroup() {
        contents.get(7).setInfo("Из группы");
    }

    public void setCharacterType(CharacterType characterType) {
        contents.get(2).setTexture(TextureRegister.getClassIcon(characterType));
    }

    public void setLevel(int lvl) {
        contents.get(3).setInfo("" + lvl);
    }
}
