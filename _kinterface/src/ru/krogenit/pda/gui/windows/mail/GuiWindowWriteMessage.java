package ru.krogenit.pda.gui.windows.mail;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWindow;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;
import ru.xlv.post.network.PacketPostSend;

import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiWindowWriteMessage extends GuiWindow {

    private GuiTextFieldAdvanced targetField, subjectField;
    private GuiTextFieldScrollable textField;
    private String target, subject;

    public GuiWindowWriteMessage(GuiPda pda, String target, String subject) {
        this(pda);
        this.subject = subject;
        this.target = target;
    }

    public GuiWindowWriteMessage(GuiPda pda) {
        super(EnumWindowType.MAIL_WRITE, pda, 743, 854);
        setHeader("НАПИСАТЬ ПИСЬМО");
        setLeftCornerDesc("FILE №89065012_NEW_MAIL");
        setBackgroundTexture(TextureRegister.texturePDAWinMailBg);
        target = "";
        subject = "";
    }

    @Override
    public void initGui() {
        super.initGui();
        Keyboard.enableRepeatEvents(true);
        initButtons();
        targetField = new GuiTextFieldAdvanced(FontType.HelveticaNeueCyrLight, ScaleGui.get(158), ScaleGui.get(73), ScaleGui.get(535), ScaleGui.get(35), ScaleGui.get(35 / 32f), 0x1AC3EA, 50);
        targetField.setText(target);
        subjectField = new GuiTextFieldAdvanced(FontType.HelveticaNeueCyrLight, ScaleGui.get(158), ScaleGui.get(114), ScaleGui.get(535), ScaleGui.get(35), ScaleGui.get(35 / 32f), 0x1AC3EA, 50);
        subjectField.setText(subject);
        textField = new GuiTextFieldScrollable(FontType.HelveticaNeueCyrLight, ScaleGui.get(45), ScaleGui.get(202), ScaleGui.get(649), ScaleGui.get(508), ScaleGui.get(28 / 32f), 0xffffff, 4096);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketPostSend(targetField.getText(), subjectField.getText(), textField.getText(), new ArrayList<>())).thenAcceptSync(result -> {
                if(result.isSuccess()) {
                    pda.closeWindow(this);
                    pda.setPopup(new GuiPopup(pda,"СООБЩЕНИЕ УСПЕШНО ОТПРАВЛЕНО", "", GuiPopup.green));
                    SoundUtils.playGuiSound(SoundType.LETTER_SENT);
                } else {
                    pda.setPopup(new GuiPopup(pda,"НЕ УДАЛОСЬ ОТПРАВИТЬ СООБЩЕНИЕ", result.getResponseMessage(), GuiPopup.red));
                }
            });
            return;
        } else if(b.id == 3) {
            pda.closeWindow(this); return;
        }
        super.actionPerformed(b);
    }

    protected void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(84);
        float y = ScaleGui.get(752);
        GuiButtonAnimated button = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТПРАВИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
        x += ScaleGui.get(398);
        button = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТМЕНИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
    }

    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(174), ScaleGui.get(651) * secondPoint, ScaleGui.get(2));
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(731), ScaleGui.get(651) * secondPoint, ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(3);
        float stringPositiveYOffset = ScaleGui.get(36);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = "КОМУ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        s = "ТЕМА: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
    }

    protected void drawInputFields(int mouseX, int mouseY) {
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        scissorX = (int) (windowXAnim);
        float height = ScaleGui.get(854);
        float height1 = ScaleGui.get(windowHeight);
        scissorHeight = (int) (height * firstPoint * collapseAnim);
        scissorY = (int) (mc.displayHeight - (windowCenterYAnim + halfHeight) + height1 * (1.0f - firstPoint) + height1 * (1.0f - collapseAnim));
        scissorWidth = (int) scaledWidth;
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        targetField.addXPosition(windowXAnim);
        targetField.addYPosition(windowYAnim);
        targetField.drawTextBox();
        subjectField.addXPosition(windowXAnim);
        subjectField.addYPosition(windowYAnim);
        subjectField.drawTextBox();
        textField.addXPosition(windowXAnim);
        textField.addYPosition(windowYAnim);
        textField.drawTextBox(mouseX, mouseY);
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawBackgroundThings();
        drawInputFields(mouseX, mouseY);
    }

    @Override
    public boolean mouseClickedWindow(int mouseX, int mouseY, int mouseButton) {
        subjectField.mouseClicked(mouseX, mouseY, mouseButton);
        targetField.mouseClicked(mouseX, mouseY, mouseButton);
        textField.mouseClicked(mouseX, mouseY, mouseButton);

        return super.mouseClickedWindow(mouseX, mouseY, mouseButton);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        subjectField.updateCursorCounter();
        targetField.updateCursorCounter();
        textField.updateCursorCounter();
    }

    @Override
    public void keyTyped(char character, int key) {
        super.keyTyped(character, key);
        subjectField.textboxKeyTyped(character, key);
        targetField.textboxKeyTyped(character, key);
        textField.textboxKeyTyped(character, key);
    }

    @Override
    public void setCollapsed(boolean collapsed) {
        super.setCollapsed(collapsed);
        if(!collapsed) {
            Keyboard.enableRepeatEvents(true);
        }
    }

    @Override
    public void onWindowClosed() {
        Keyboard.enableRepeatEvents(false);
        pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, false);
    }

    @Override
    public void onWindowCollapsed() {
        Keyboard.enableRepeatEvents(false);
    }
}
