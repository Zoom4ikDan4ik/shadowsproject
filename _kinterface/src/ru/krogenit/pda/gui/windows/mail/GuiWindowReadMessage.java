package ru.krogenit.pda.gui.windows.mail;

import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAnimated;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.GuiTextFieldScrollable;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.pda.EnumPdaLeftMenuSection;
import ru.krogenit.pda.gui.GuiPda;
import ru.krogenit.pda.gui.windows.EnumWindowType;
import ru.krogenit.pda.gui.windows.GuiWindow;
import ru.xlv.customfont.FontContainer;
import ru.xlv.customfont.FontType;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.lwjgl.opengl.GL11.glScissor;

public class GuiWindowReadMessage extends GuiWindow {

    private final String from, subject, date, text;
    private GuiTextFieldScrollable textField;
    private final boolean asOutgoing;

    public GuiWindowReadMessage(GuiPda pda, String from, String subject, long date, String text, boolean asOutgoing) {
        super(EnumWindowType.MAIL_READ, pda, 743, 854);
        setHeader(subject.toUpperCase());
        setLeftCornerDesc("FILE №89065012_CODE_MAIL");
        setBackgroundTexture(TextureRegister.texturePDAWinMailBg);
        this.from = from;
        this.subject = subject;
        this.date = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date(date));
        this.text = text;
        this.asOutgoing = asOutgoing;
    }

    protected void initButtons() {
        float buttonWidth = 159;
        float buttonHeight = 39;
        float x = ScaleGui.get(84);
        float y = ScaleGui.get(752);
        GuiButtonAnimated button = new GuiButtonAnimated(2, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ОТВЕТИТЬ");
        button.setTexture(TextureRegister.textureTreeButtonAccept);
        button.setTextureHover(TextureRegister.textureTreeButtonHover);
        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
        buttonList.add(button);
//        x += ScaleGui.get(398);
//        button = new GuiButtonAnimated(3, x, y, ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "УДАЛИТЬ");
//        button.setTexture(TextureRegister.textureTreeButtonAccept);
//        button.setTextureHover(TextureRegister.textureTreeButtonHover);
//        button.setMaskTexture(TextureRegister.textureTreeButtonMask);
//        buttonList.add(button);
    }

    @Override
    public void initGui() {
        super.initGui();
        initButtons();
        textField = new GuiTextFieldScrollable(FontType.HelveticaNeueCyrLight, ScaleGui.get(45), ScaleGui.get(202), ScaleGui.get(649), ScaleGui.get(508), ScaleGui.get(28 / 32f), 0xffffff, 4096);
        textField.setCanEdit(false);
        textField.setText(text);
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        if(b.id == 2) {
            pda.showOrCreateWindow(EnumWindowType.MAIL_WRITE, new GuiWindowWriteMessage(pda,from,subject));
            pda.setLeftMenuSubButtonPressed(EnumPdaLeftMenuSection.MAIL_WRITE, true);
            return;
        } else if(b.id == 3) {
            pda.closeWindow(this);
            return;
        }
        super.actionPerformed(b);
    }

    protected void drawBackgroundThings() {
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(174), ScaleGui.get(651) * secondPoint, ScaleGui.get(2));
        GuiDrawUtils.drawRect(TextureRegister.texturePDAWinPlayersDevider, windowXAnim + ScaleGui.get(44), windowYAnim + ScaleGui.get(731), ScaleGui.get(651) * secondPoint, ScaleGui.get(2));

        float x = windowXAnim + ScaleGui.get(62);
        float y = windowYAnim + ScaleGui.get(91);
        float iconWidth = ScaleGui.get(37);
        float stringMinusYOffset = ScaleGui.get(14);
        float stringPositiveYOffset = ScaleGui.get(10);
        float stringXOffset = ScaleGui.get(27);
        float fs = ScaleGui.get(1.2f) * fs1;
        GuiDrawUtils.drawRectCentered(TextureRegister.texturePDAIconCircle, x, y, iconWidth * firstPoint, iconWidth * firstPoint);
        String s = asOutgoing ?"КОМУ: " :  "ОТ: ";
        FontContainer fontContainer = FontType.HelveticaNeueCyrLight.getFontContainer();
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y - stringMinusYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, from, x + stringXOffset + fontContainer.width(s) * fs, y - stringMinusYOffset, fs, 0x1BC3EC);
        s = "ТЕМА: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, subject, x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
        stringPositiveYOffset += ScaleGui.get(43);
        s = "ДАТА: ";
        GuiDrawUtils.drawStringNoScale(fontContainer, s, x + stringXOffset, y + stringPositiveYOffset, fs, 0xffffff);
        GuiDrawUtils.drawStringNoScale(fontContainer, date, x + stringXOffset + fontContainer.width(s) * fs, y + stringPositiveYOffset, fs, 0x1BC3EC);
    }

    protected void drawInputFields(int mouseX, int mouseY) {
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        scissorX = (int) (windowXAnim);
        float height = ScaleGui.get(854);
        float height1 = ScaleGui.get(windowHeight);
        scissorHeight = (int) (height * firstPoint * collapseAnim);
        scissorY = (int) (mc.displayHeight - (windowCenterYAnim + halfHeight) + height1 * (1.0f - firstPoint) + height1 * (1.0f - collapseAnim));
        scissorWidth = (int) scaledWidth;
        glScissor(scissorX, scissorY, scissorWidth, scissorHeight);
        textField.addXPosition(windowXAnim);
        textField.addYPosition(windowYAnim);
        textField.drawTextBox(mouseX, mouseY);
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawBackgroundThings();
        drawInputFields(mouseX, mouseY);
    }

    @Override
    public boolean mouseClickedWindow(int mouseX, int mouseY, int mouseButton) {
        textField.mouseClicked(mouseX, mouseY, mouseButton);

        return super.mouseClickedWindow(mouseX, mouseY, mouseButton);
    }
}
