package ru.krogenit.util;

import net.minecraft.util.ResourceLocation;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.item.ItemStat;
import ru.krogenit.client.gui.item.ItemStatSmall;
import ru.krogenit.client.gui.item.ItemTag;
import ru.krogenit.client.gui.item.WeaponStat;
import ru.krogenit.guns.item.ItemGunClient;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.item.tag.EnumItemTag;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import java.util.ArrayList;
import java.util.List;

public class ItemStatUtils {

    private static final CharacterAttributeType[] weaponDamageStats = new CharacterAttributeType[] {
            CharacterAttributeType.BALLISTIC_DAMAGE,
            CharacterAttributeType.CUT_DAMAGE,
            CharacterAttributeType.ENERGY_DAMAGE,
            CharacterAttributeType.THERMAL_DAMAGE,
            CharacterAttributeType.FIRE_DAMAGE,
            CharacterAttributeType.ELECTRIC_DAMAGE,
            CharacterAttributeType.TOXIC_DAMAGE,
            CharacterAttributeType.RADIATION_DAMAGE,
            CharacterAttributeType.EXPLOSION_DAMAGE,
    };

    public static ResourceLocation getTextureByDamageType(CharacterAttributeType type) {
        switch (type) {
            case BALLISTIC_DAMAGE:
            case CUT_DAMAGE:
            case ENERGY_DAMAGE:
            case THERMAL_DAMAGE:
            case FIRE_DAMAGE:
            case ELECTRIC_DAMAGE:
            case TOXIC_DAMAGE:
            case RADIATION_DAMAGE:
            case EXPLOSION_DAMAGE:
            default:
                return TextureRegister.textureModifyThermalDamage;
        }
    }

    public static ItemStat[] getWeaponDamageStats(ItemGunClient itemGun) {
        List<ItemStat> stats = new ArrayList<>();
        for(CharacterAttributeType att : weaponDamageStats) {
            double v = itemGun.getCharacterAttributeBoost(att);
            if(v > 0) {
                stats.add(new ItemStat(getTextureByDamageType(att), DecimalUtils.getFormattedStringWithOneDigit(v), "Урон в секунду"));
            }
        }

        return stats.toArray(new ItemStat[0]);
    }

    public static ItemTag[] getItemTags(ItemBase item) {
        List<ItemTag> tags = new ArrayList<>();

        for(EnumItemTag tag : item.getItemTags()) {
            tags.add(new ItemTag(tag.getDisplayName(), ColorUtils.getColorByTag(tag)));
        }

        return tags.toArray(new ItemTag[0]);
    }

    public static WeaponStat[] getWeaponStats(ItemGunClient itemGun) {
        List<WeaponStat> stats = new ArrayList<>();
        stats.add(new WeaponStat((int) itemGun.getDamage() + "", "ед. урона"));
        stats.add(new WeaponStat(itemGun.getRate() + "", "скорострельность"));

        return stats.toArray(new WeaponStat[0]);
    }

    public static ItemStatSmall[] getWeaponMiscStats(ItemGunClient itemGun) {
        List<ItemStatSmall> stats = new ArrayList<>();
        stats.add(new ItemStatSmall(TextureRegister.textureModifyAccuracy, DecimalUtils.getFormattedStringWithOneDigit(itemGun.getAccuracy()) + "°"));
        stats.add(new ItemStatSmall(TextureRegister.textureModifyKnockBack, DecimalUtils.getFormattedStringWithOneDigit(itemGun.getKnockback()) + ""));
        stats.add(new ItemStatSmall(TextureRegister.textureModifyClip, itemGun.getAmmo().maxAmmo + ""));
        stats.add(new ItemStatSmall(TextureRegister.textureModifyDurability, itemGun.getMaxDamage() + ""));
        return stats.toArray(new ItemStatSmall[0]);
    }
}
