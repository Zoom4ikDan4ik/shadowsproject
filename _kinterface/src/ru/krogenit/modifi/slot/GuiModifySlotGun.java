package ru.krogenit.modifi.slot;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiButtonAdvanced;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.client.gui.api.ScaleGui;
import ru.krogenit.shaders.KrogenitShaders;

public class GuiModifySlotGun extends GuiButtonAdvanced {

    @Getter
    @Setter
    private ItemStack itemStack;
    private static final Vector3f lightColor = new Vector3f(4f, 4f, 5f);
    private static final Vector3f lightPos = new Vector3f(0,10,10f);

    public GuiModifySlotGun(int id, float x, float y, float width, float height, ItemStack itemStack) {
        super(id, x, y, width, height, "");
        this.itemStack = itemStack;
    }

    @Override
    public void drawButton(int mouseX, int mouseY) {
        boolean isHovering = isHovered(mouseX, mouseY);
        if(isHovering) GL11.glColor4f(1f, 1f, 1f, 1f);
        else GL11.glColor4f(0.75f, 0.75f, 0.75f, 1f);
        GuiDrawUtils.drawRect(TextureRegister.textureModifySlotGun, xPosition, yPosition, width, height);
        if(itemStack != null) {
            float offset = ScaleGui.get(0);
            KrogenitShaders.forwardPBRDirectionalShaderOld.enable();
            KrogenitShaders.forwardPBRDirectionalShaderOld.setLightPos(lightPos.x, lightPos.y, lightPos.z);
            KrogenitShaders.forwardPBRDirectionalShaderOld.setLightColor(lightColor.x, lightColor.y, lightColor.z);
            GL11.glPushMatrix();
            GL11.glLoadIdentity();
            KrogenitShaders.forwardPBRDirectionalShaderOld.setModelView();
            GL11.glPopMatrix();
            GuiDrawUtils.renderItem(itemStack, 1.0f, offset, offset, xPosition, yPosition, width, height, 0f);
            KrogenitShaders.forwardPBRDirectionalShaderOld.disable();
        }
    }
}
