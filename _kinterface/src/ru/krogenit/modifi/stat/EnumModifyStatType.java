package ru.krogenit.modifi.stat;

public enum EnumModifyStatType {
    ACCURACY, DURABILITY, CLIP_SIZE, KNOCKBACK;
}
