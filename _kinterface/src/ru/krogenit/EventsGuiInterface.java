package ru.krogenit;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.GuiDownloadTerrain;
import net.minecraft.client.gui.GuiGameOver;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraftforge.client.event.GuiOpenEvent;
import noppes.npcs.client.gui.player.GuiDialogInteract;
import noppes.npcs.client.gui.player.GuiNPCTrader;
import ru.krogenit.client.event.KeyboardNoRepeatEvent;
import ru.krogenit.connecting.GuiDownloadTerrainCustom;
import ru.krogenit.deathscreen.GuiDeathScreen;
import ru.krogenit.escmenu.gui.GuiEscMenu;
import ru.krogenit.inventory.GuiMatrixPlayerInventory;
import ru.krogenit.inventory.chest.GuiInventoryChest;
import ru.krogenit.npc.dialog.GuiNPCDialog;
import ru.krogenit.npc.trade.GuiNPCTrade;
import ru.krogenit.settings.gui.GuiControlSettings;
import ru.krogenit.settings.gui.GuiSettings;
import ru.xlv.core.event.ContainerMatrixInventoryOpenEvent;
import ru.xlv.core.event.PlayerKilledClientEvent;
import ru.xlv.core.event.PreloadableResourcesLoadPostEvent;
import ru.xlv.core.gui.GuiCharacterMainMenu;

public class EventsGuiInterface {

    private static final Minecraft mc = Minecraft.getMinecraft();
    private static boolean isLoaded = false;

    @SubscribeEvent
    public void event(ContainerMatrixInventoryOpenEvent event) {
        mc.displayGuiScreen(new GuiInventoryChest(event.getX(), event.getY(), event.getZ()));
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(GuiOpenEvent event) {
        if(event.gui instanceof GuiGameOver) {
            event.setCanceled(true);
            mc.displayGuiScreen(new GuiDeathScreen());
        } else if(event.gui instanceof GuiOptions) {
            event.setCanceled(true);
            mc.displayGuiScreen(new GuiSettings(mc.currentScreen));
        } else if(event.gui instanceof GuiNPCTrader) {
            event.setCanceled(true);
            mc.displayGuiScreen(new GuiNPCTrade((GuiNPCTrader)event.gui));
        } else if(event.gui instanceof GuiDialogInteract) {
            event.setCanceled(true);
            if(mc.currentScreen instanceof GuiNPCDialog) {
                ((GuiNPCDialog)mc.currentScreen).appendDialog(((GuiDialogInteract) event.gui).getDialog());
            } else {
                mc.displayGuiScreen(new GuiNPCDialog((GuiDialogInteract)event.gui));
            }
        } else if(event.gui instanceof GuiIngameMenu) {
            event.setCanceled(true);
            mc.displayGuiScreen(new GuiEscMenu());
        } else if(event.gui instanceof GuiDownloadTerrain && !(event.gui instanceof GuiDownloadTerrainCustom)) {
            event.setCanceled(true);
            mc.displayGuiScreen(new GuiDownloadTerrainCustom(((GuiDownloadTerrain) event.gui).getField_146594_a()));
        } else if(event.gui instanceof GuiInventory && !Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) {
            event.gui = new GuiMatrixPlayerInventory();
        }
//        else if(event.gui instanceof GuiChest) {
//            IInventory lowerChestInventory = ((GuiChest) event.gui).getLowerChestInventory();
//            if(lowerChestInventory instanceof TileEntityChest) {
//                TileEntityChest tileEntityChest = (TileEntityChest) lowerChestInventory;
//                event.setCanceled(true);
//                mc.displayGuiScreen(new GuiInventoryChest(tileEntityChest));
//            }
//
//            IInventory upperChestInventory = ((GuiChest) event.gui).getUpperChestInventory();
//            if(upperChestInventory instanceof TileEntityChest) {
//                TileEntityChest tileEntityChest = (TileEntityChest) upperChestInventory;
//                event.setCanceled(true);
//                mc.displayGuiScreen(new GuiInventoryChest(tileEntityChest));
//            }
//        }
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(PlayerKilledClientEvent event) {
        GuiDeathScreen.setKillerPlayer((AbstractClientPlayer)event.getEntityPlayer());
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(KeyboardNoRepeatEvent event) {
        event.shouldProcess = !(mc.currentScreen instanceof GuiControlSettings) || ((GuiControlSettings) mc.currentScreen).time <= Minecraft.getSystemTime() - 20L;
    }

    @SubscribeEvent
    @SuppressWarnings("unused")
    public void event(PreloadableResourcesLoadPostEvent event) {
       if(!isLoaded) {
           mc.displayGuiScreen(new GuiCharacterMainMenu());
           isLoaded = true;
       }
    }
}
