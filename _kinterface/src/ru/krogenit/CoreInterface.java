package ru.krogenit;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import org.lwjgl.input.Keyboard;
import ru.krogenit.hud.EventsHUD;
import ru.krogenit.hud.HudOverlayElement;
import ru.krogenit.inventory.GuiMatrixPlayerInventory;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.util.KeyBindingsExecutive;
import ru.xlv.event.UsernameEventListener;
import ru.xlv.mochar.XlvsMainMod;

import static ru.xlv.core.event.KeyRegistry.KEY_INVENTORY_CATEGORY;

@Mod(
        modid = CoreInterface.MODID,
        name = "Interface",
        version = "1.0.0"
)
public class CoreInterface {
    public static final String MODID = "interface";

    @Mod.Instance(CoreInterface.MODID)
    public static CoreInterface instance;

    @Getter private HudOverlayElement hud;

    @Mod.EventHandler
    @SuppressWarnings("unused")
    public void event(FMLPostInitializationEvent e) {
        CommonUtils.registerFMLEvents(new KeyBindingsInterface().init());
        EventsGuiInterface event = new EventsGuiInterface();
        CommonUtils.registerEvents(event);
        XlvsCoreCommon.EVENT_BUS.register(event);
        XlvsCoreCommon.EVENT_BUS.register(new EventsHUD());
        CommonUtils.registerEvents(new UsernameEventListener());

        hud = new HudOverlayElement();
        XlvsCore.INSTANCE.getGameOverlayManager().registerElement(hud);

        if(e.getSide().isClient()) {
            XlvsMainMod.INSTANCE.getKeyRegistry().register(new KeyBindingsExecutive("Open Inventory", Keyboard.KEY_E, KEY_INVENTORY_CATEGORY, () -> {
                if(Minecraft.getMinecraft().thePlayer != null) {
                    if(!Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) {
                        Minecraft.getMinecraft().displayGuiScreen(new GuiMatrixPlayerInventory());
                    } else {
                        Minecraft.getMinecraft().displayGuiScreen(new GuiContainerCreative(Minecraft.getMinecraft().thePlayer));
                    }
                }
            }));
        }
    }
}
