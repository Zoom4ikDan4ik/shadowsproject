package ru.krogenit.skilltree.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.GuiDrawUtils;
import ru.krogenit.skilltree.type.EnumSlotSkillPassiveType;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.core.util.Utils;

public class GuiSlotSkillPassive extends GuiButton {

    private GuiSkill skill;
    private EnumSlotSkillPassiveType type;
    private final float radius;

    private boolean hovered;

    public GuiSlotSkillPassive(int id, float xPos, float yPos, float width, float height, EnumSlotSkillPassiveType type) {
        super(id, xPos, yPos, width, height, "");
        this.type = type;
        this.radius = (width + height) / 4.0f;
    }

    @Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY) {
        boolean hovered = isHovered(mouseX, mouseY);
        if(hovered && !this.hovered) {
            SoundUtils.playGuiSound(SoundType.BUTTON_HOVER);
        }
        this.hovered = hovered;
        if(type == EnumSlotSkillPassiveType.LOCKED) {
            Utils.bindTexture(TextureRegister.textureSkillPssiveLock);
        } else {
            Utils.bindTexture(TextureRegister.textureSkillPssiveEmpty);
        }
        GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);

        if(skill != null) {
            Utils.bindTexture(skill.getSkillLearnedTexture());
            GuiDrawUtils.drawRect(this.xPosition + height / 50f, this.yPosition + height / 50f, this.width / 1.06f, this.height / 1.08f);
            Utils.bindTexture(TextureRegister.textureSkillPssiveBorder);
            GuiDrawUtils.drawRect(this.xPosition, this.yPosition, this.width, this.height);
        }
    }

    @Override
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
        return this.enabled && this.visible && Math.sqrt(Math.pow(mouseX - xPosition - width / 2f, 2) + Math.pow(mouseY - yPosition - height / 2f, 2)) <= radius;
    }

    private boolean isHovered(int mouseX, int mouseY) {
        return this.field_146123_n = Math.sqrt(Math.pow(mouseX - xPosition - width / 2f, 2) + Math.pow(mouseY - yPosition - height / 2f, 2)) <= radius;
    }

    public EnumSlotSkillPassiveType getType() {
        return type;
    }

    public void setType(EnumSlotSkillPassiveType type) {
        this.type = type;
    }

    public void setSkill(GuiSkill skill) {
        this.skill = skill;
    }

    public GuiSkill getSkill() {
        return skill;
    }
}
