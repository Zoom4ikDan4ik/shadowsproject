package ru.krogenit.skilltree.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.smart.moving.SmartMovingContext;
import net.smart.moving.SmartMovingFactory;
import net.smart.moving.SmartMovingSelf;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.skilltree.SkillAttribute;
import ru.krogenit.skilltree.SkillTreeConnection;
import ru.krogenit.skilltree.SkillValue;
import ru.krogenit.skilltree.type.*;
import ru.krogenit.utils.AnimationHelper;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.player.character.ExperienceType;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.core.network.skill.PacketSkillBuildChange;
import ru.xlv.core.network.skill.PacketSkillLearn;
import ru.xlv.core.network.skill.PacketSkillSelect;
import ru.xlv.core.player.ClientMainPlayer;
import ru.xlv.core.skill.Skill;
import ru.xlv.core.util.FlexArrays;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class GuiSkillTree extends AbstractGuiScreenAdvanced implements IGuiWithPopup {
    private final List<GuiSkill> skills = new ArrayList<>();
    private final List<SkillTreeConnection> connections = new ArrayList<>();

    private final List<GuiButtonBuildSkillTree> buttonsBuild = new ArrayList<>();
    private final List<GuiSlotSkillActive> slotsActive = new ArrayList<>();
    private final List<GuiSlotSkillPassive> slotsPassive = new ArrayList<>();
    private GuiSkill movedSkill;
    private int previousBuildId, currentBuild;

    private AbstractGuiScreenAdvanced popup;

    private float leftSideAnim;
    private float treeAnim;
    private boolean leftSideOpen;
    private float scale, scaleAnim;
    private final float MAX_SCALE = 2.0f, MIN_SCALE = 0.35f;

    private static final float WIDTH_ACTIVE = 46;
    private static final float HEIGHT_ACTIVE = 46f;
    private static final float WIDTH_PASSIVE = 38;
    private static final float HEIGHT_PASSIVE = 42;

    private final Vector2f mouseLastClick = new Vector2f(0, 0);
    private final Vector2f treePos = new Vector2f(0, 0);
    private final Vector2f treePosAnim = new Vector2f(0, 0);
    private boolean treeMovingByMouse;
    private AxisAlignedBB scissorAABB;
    private boolean wantToCloseGui;
    private final Vector2f recenter = new Vector2f();

    public GuiSkillTree() {
        super();
        this.scale = 1.0f;
        this.currentBuild = XlvsMainMod.INSTANCE.getClientMainPlayer().getCurrentSkillBuildIndex();
        this.mc = Minecraft.getMinecraft();
    }

    private boolean isSkillLearned(Skill skill) {
        return XlvsMainMod.INSTANCE.getClientMainPlayer().getLearnedSkillIds().contains(skill.getId());
    }

    private Skill getSkillById(int id) {
        List<Skill> characterSkills = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterSkills();
        for(Skill skill : characterSkills) {
            if(skill.getId() == id)
                return skill;
        }

        return null;
    }

    public GuiSkill getGuiSkillById(int id) {
        for(GuiSkill skill : skills) {
            if(skill.id == id)
                return skill;
        }

        return null;
    }

    private boolean isSkillAvailable(Skill skill) {
        for(int parentId : skill.getParentIds()) {
            if(parentId != -1) {
                Skill parentSkill = getSkillById(parentId);
                if(parentSkill != null && !isSkillLearned(parentSkill)) return false;
            }
        }

        return true;
    }

    private int getSkillCostMana(Skill skill) {
        List<Skill.SkillCost> skillCosts = skill.getSkillCosts();
        for(Skill.SkillCost skillCost : skillCosts) {
            if(skillCost instanceof Skill.SkillCostMana) {
                return skillCost.getAmount();
            }
        }

        return 0;
    }

    private int getSkillCost(Skill skill, ExperienceType type) {
        List<Skill.SkillLearnRule> skillRules = skill.getSkillRules();
        for(Skill.SkillLearnRule skillRole : skillRules) {
            if(skillRole instanceof Skill.SkillLearnRuleExp) {
                Skill.SkillLearnRuleExp skillCost = (Skill.SkillLearnRuleExp) skillRole;
                if(skillCost.getExperienceType() == type) {
                    return (int) skillCost.getAmount();
                }
            }
        }

        return 0;
    }

    private List<Skill> getChildSkills(Skill parent) {
        List<Skill> childSkills = new ArrayList<>();
        List<Skill> characterSkills = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterSkills();
        for(Skill skill : characterSkills) {
            int[] parentIds = skill.getParentIds();
            for(int parentId : parentIds) {
                if(parentId == parent.getId()) childSkills.add(skill);
            }
        }

        return childSkills;
    }

    private void setSkillPositionAndSize(GuiSkill guiSkill, Skill skill) {
        guiSkill.setPositionAndSize(skill.getIconX(), skill.getIconY(), guiSkill.getType() == EnumSkillTreeType.ACTIVE ? WIDTH_ACTIVE : WIDTH_PASSIVE, guiSkill.getType() == EnumSkillTreeType.ACTIVE ? HEIGHT_ACTIVE : HEIGHT_PASSIVE);
    }

    private void createConnections(Skill skill, GuiSkill guiSkill) {
        for(int parentId : skill.getParentIds()) {
            if(parentId != -1) {
                GuiSkill parent = getGuiSkillById(parentId);
                SkillTreeConnection connection = new SkillTreeConnection(parent, guiSkill);
                connections.add(connection);
            }
        }
    }

    private void initTreeSkills() {
        skills.clear();
        connections.clear();

        List<Skill> characterSkills = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterSkills();
        float minX = Float.MAX_VALUE, minY = Float.MAX_VALUE, maxX = Float.MIN_VALUE, maxY = Float.MIN_VALUE;
        for(Skill skill : characterSkills) {
            boolean isActive = skill.isActivable();
            EnumSkillTreeAvailableType availableType = isSkillLearned(skill) ? EnumSkillTreeAvailableType.LEARNED : isSkillAvailable(skill) ? EnumSkillTreeAvailableType.AVAILABLE : EnumSkillTreeAvailableType.LOCKED;
            GuiSkill guiSkill = new GuiSkill(skill.getId(), isActive? EnumSkillTreeType.ACTIVE : EnumSkillTreeType.PASSIVE, availableType, skill.getTextureName());
            List<SkillValue> values = new ArrayList<>();
            if(isActive) {
                values.add(new SkillValue(EnumSkillStatType.MANA, getSkillCostMana(skill)));
                values.add(new SkillValue(EnumSkillStatType.TIME, (int) (skill.getMaxCooldown() / 1000)));
                guiSkill.setValues(values);
                List<SkillAttribute> skillAttributeList = new ArrayList<>();
                guiSkill.setAttributes(skillAttributeList);
            } else {
                guiSkill.setValues(values);
                List<SkillAttribute> skillAttributeList = new ArrayList<>();
                guiSkill.setAttributes(skillAttributeList);
            }

            guiSkill.setName(skill.getName());
            guiSkill.setDescription(skill.getDescription());
            guiSkill.setCost(new Vector3f(getSkillCost(skill, ExperienceType.BATTLE), getSkillCost(skill, ExperienceType.EXPLORE), getSkillCost(skill, ExperienceType.SURVIVE)));
            setSkillPositionAndSize(guiSkill, skill);
            minX = Math.min(minX, guiSkill.xPosition);
            minY = Math.min(minY, guiSkill.yPosition);
            maxX = Math.max(maxX, guiSkill.xPosition);
            maxY = Math.max(maxY, guiSkill.yPosition);
            skills.add(guiSkill);
        }

        this.recenter.x = (minX + maxX) / 2f;
        this.recenter.y = (minY + maxY) / 2f;

        for(GuiSkill skill : skills) {
            skill.setPositionAndSize(skill.getxBase() - recenter.x, skill.getyBase() - recenter.y, skill.getWidthBase(), skill.getHeightBase());
        }

        for (Skill characterSkill : characterSkills) {
            createConnections(characterSkill, getGuiSkillById(characterSkill.getId()));
        }
    }

    @Override
    public void initGui() {
        super.initGui();
        buttonList.clear();
        initTreeSkills();
        initControlButtons();
        initBuildButtons();
        initSlotsPassiveSkills();
        initSlotsActiveSkills();
        initTreeSkillsPositions();
        if(popup != null) popup.initGui();
    }

    private void initControlButtons() {
        GuiButtonAdvanced texturedButton = new GuiButtonAdvanced(0, ScaleGui.get(90, 11), ScaleGui.get(637, 22), ScaleGui.get(11), ScaleGui.get(22), "");
        if(leftSideOpen) {
            texturedButton.setTexture(TextureRegister.textureSkillButtonLeftClose);
        } else {
            texturedButton.setTexture(TextureRegister.textureSkillButtonLeft);
        }

        buttonList.add(texturedButton);

        texturedButton = new GuiButtonAdvanced(10, ScaleGui.getRight(-69 + 1920, 10), ScaleGui.get(284, 10), ScaleGui.get(10), ScaleGui.get(10), "");
        texturedButton.setTexture(TextureRegister.textureSkillButtonPlus);
        texturedButton.setTextureHover(TextureRegister.textureSkillButtonPlusHover);
        buttonList.add(texturedButton);

        texturedButton = new GuiButtonAdvanced(11, ScaleGui.getRight(-69 + 1920, 10), ScaleGui.get(605, 4), ScaleGui.get(10), ScaleGui.get(4), "");
        texturedButton.setTexture(TextureRegister.textureSkillButtonMinus);
        texturedButton.setTextureHover(TextureRegister.textureSkillButtonMinusHover);
        buttonList.add(texturedButton);
    }

    private void initBuildButtons() {
        float width = 60f;
        float height = 60f;
        float xOffset = ScaleGui.get(75f);
        float x = ScaleGui.getRight(-339 + 1920,width);

        for(int i=0;i<4;i++) {
            GuiButtonBuildSkillTree prevB;
            try {
                prevB = buttonsBuild.get(i);
            } catch (Exception ignored) {
                prevB = null;
            }

            EnumButtonBuildType type;
            GuiButtonBuildSkillTree guiButtonBuildSkillTree;

            if(i == currentBuild) {
                type = prevB != null ? prevB.getType() : EnumButtonBuildType.CURRENT;
                guiButtonBuildSkillTree = new GuiButtonBuildSkillTree(i, x, ScaleGui.get(66, height), ScaleGui.get(width), ScaleGui.get(height), "" + (i+1), type, this);
            } else if(i < 2) {
                type = prevB != null ? prevB.getType() : EnumButtonBuildType.AVAILABLE;
                guiButtonBuildSkillTree = new GuiButtonBuildSkillTree(i, x, ScaleGui.get(66, height), ScaleGui.get(width), ScaleGui.get(height), "" + (i+1), type, this);
            } else {
                type = prevB != null ? prevB.getType() : EnumButtonBuildType.LOCKED;
                guiButtonBuildSkillTree = new GuiButtonBuildSkillTree(i, x, ScaleGui.get(66, height), ScaleGui.get(width), ScaleGui.get(height), "" + (i+1), type, this);
            }

            if(buttonsBuild.size() < 4) buttonsBuild.add(guiButtonBuildSkillTree);
            else buttonsBuild.set(i, guiButtonBuildSkillTree);
            x+=xOffset;
        }
    }

    private void initSlotsPassiveSkills() {
        slotsPassive.clear();
        float x = 533f;
        float y = 935.5f;
        float width = 51;
        float height = 59;
        EnumSlotSkillPassiveType type;
        ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        for(int i=0;i<10;i++) {
            if(i < clientMainPlayer.getAvailablePassiveSkillSlotAmount()) {
                type = EnumSlotSkillPassiveType.AVAILABLE;
            } else {
                type = EnumSlotSkillPassiveType.LOCKED;
            }
            GuiSlotSkillPassive slot = new GuiSlotSkillPassive(i, ScaleGui.getCenterX(x, width), ScaleGui.getBot(y, height), ScaleGui.get(width), ScaleGui.get(height), type);
            slotsPassive.add(slot);
            x -= 28f;
            if(i % 2 == 0) {
                y = 886;
            } else {
                y = 935.5f;
            }

            Skill hotSkill = clientMainPlayer.getHotPassiveSkill(i);
            if(hotSkill != null) {
                GuiSkill skill = getGuiSkillById(hotSkill.getId());
                slot.setSkill(skill);
            }
        }
    }

    private void initSlotsActiveSkills() {
        slotsActive.clear();
        float width = 76;
        float height = 77;
        float x = 792;
        float y = 988;
        EnumSlotSkillActiveType type1;
        ClientMainPlayer clientMainPlayer = XlvsMainMod.INSTANCE.getClientMainPlayer();
        for(int i=0;i<4;i++) {
            if(i < clientMainPlayer.getAvailableActiveSkillSlotAmount()) {
                type1 = EnumSlotSkillActiveType.EMPTY;
            } else {
                type1 = EnumSlotSkillActiveType.LOCKED;
            }
            GuiSlotSkillActive slot = new GuiSlotSkillActive(i, ScaleGui.getCenterX(x, width), ScaleGui.getBot(y, height), ScaleGui.get(width), ScaleGui.get(height), type1, minAspect);
            slotsActive.add(slot);
            x += 115;

            Skill hotSkill = clientMainPlayer.getHotActiveSkill(i);
            if(hotSkill != null) {
                GuiSkill skill = getGuiSkillById(hotSkill.getId());
                slot.setSkill(skill);
                slot.setSkillTexture(skill.getTextureName());
            }
        }
    }

    private void initTreeSkillsPositions() {
        for(GuiSkill skill : skills) {
            skill.width = ScaleGui.get(skill.getWidthBase() * scaleAnim);
            skill.height = ScaleGui.get(skill.getHeightBase() * scaleAnim);
            skill.xPosition = ScaleGui.getCenterX(960 + (skill.getxBase() + treePosAnim.x) * scaleAnim);
            skill.yPosition = ScaleGui.getCenterY(540 + (skill.getyBase() + treePosAnim.y) * scaleAnim);
        }
    }

    @Override
    protected void actionPerformed(GuiButton guiButton) {
        if(guiButton.id == 0) {
            GuiButtonAdvanced texturedButton = (GuiButtonAdvanced) guiButton;
            if(leftSideOpen) {
                texturedButton.setTexture(TextureRegister.textureSkillButtonLeft);
            } else {
                texturedButton.setTexture(TextureRegister.textureSkillButtonLeftClose);
            }

            leftSideOpen = !leftSideOpen;
        } else if(guiButton.id == 10) {
            this.scale += 0.25f;
            if(scale > MAX_SCALE) {
                scale = MAX_SCALE;
            }
        } else if(guiButton.id == 11) {
            this.scale -= 0.25f;
            if(scale < MIN_SCALE) {
                scale = MIN_SCALE;
            }
        }
    }

    private void checkConnectionsForAvailability(GuiSkill skill) {
        List<Skill> childSkills = getChildSkills(getSkillById(skill.id));

        for(Skill child : childSkills) {
            GuiSkill guiChildSkill = getGuiSkillById(child.getId());
            if(guiChildSkill.getAvailableType() == EnumSkillTreeAvailableType.LOCKED) {
                boolean canBeAvailable = true;
                int[] parentIds = child.getParentIds();
                for(int parenId : parentIds) {
                    GuiSkill guiSkillById = getGuiSkillById(parenId);
                    if(guiSkillById.getAvailableType() != EnumSkillTreeAvailableType.LEARNED) {
                        canBeAvailable = false;
                        break;
                    }
                }

                if(canBeAvailable) guiChildSkill.setAvailableType(EnumSkillTreeAvailableType.AVAILABLE);
            }
        }
    }

    public void learnSkill(GuiSkill skill) {
        if(skill.getAvailableType() == EnumSkillTreeAvailableType.AVAILABLE) {
            SyncResultHandler<PacketSkillLearn.Result> resultPacketResultHandler = XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketSkillLearn(skill.id));
            resultPacketResultHandler.thenAcceptSync(result -> {
                if(result.isSuccess()) {
                    skill.setAvailableType(EnumSkillTreeAvailableType.LEARNED);
                    checkConnectionsForAvailability(skill);
                    popup = new GuiPopup(this, "СКИЛЛ УСПЕШНО ИЗУЧЕН!", "", GuiPopup.green);
                    SoundUtils.playGuiSound(SoundType.SKILL_LEARNED);
                } else {
                    popup = new GuiPopup(this, "СКИЛЛ ИЗУЧИТЬ НЕ УДАЛОСЬ!", result.getResponseMessage(), GuiPopup.red);
                }
            });
        }
    }

    public void changeBuild(boolean save, int id) {
        if(save) {
            PacketSkillSelect packetSkillSelect = new PacketSkillSelect();
            List<Integer> ids = new ArrayList<>();
            List<Integer> hotSlotIndices = new ArrayList<>();
            boolean[] activeFlags = new boolean[slotsPassive.size() + slotsActive.size()];
            Arrays.fill(activeFlags, 0, slotsPassive.size(), false);
            Arrays.fill(activeFlags, slotsPassive.size(), slotsPassive.size() + slotsActive.size(), true);
            slotsPassive.forEach(guiSlotSkillActive -> {
                ids.add(guiSlotSkillActive.getSkill() != null ? guiSlotSkillActive.getSkill().id : -1);
                hotSlotIndices.add(guiSlotSkillActive.id);
            });
            slotsActive.forEach(guiSlotSkillPassive -> {
                ids.add(guiSlotSkillPassive.getSkill() != null ? guiSlotSkillPassive.getSkill().id : -1);
                hotSlotIndices.add(guiSlotSkillPassive.id);
            });
            packetSkillSelect.setIsActiveSkillArr(activeFlags);
            packetSkillSelect.setIds(FlexArrays.getIntArrayOf(ids));
            packetSkillSelect.setHotSlotIndices(FlexArrays.getIntArrayOf(hotSlotIndices));
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToServer(packetSkillSelect);
        }

        if(wantToCloseGui) {
            closeGui();
        } else {
            buttonsBuild.get(currentBuild).setType(EnumButtonBuildType.AVAILABLE);
            buttonsBuild.get(id).setType(EnumButtonBuildType.CURRENT);
            previousBuildId = currentBuild;
            currentBuild = id;
            SyncResultHandler<Boolean> booleanSyncResultHandler = XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketSkillBuildChange(currentBuild));
            booleanSyncResultHandler.thenAcceptSync(success -> {
                if(success) {
                    initSlotsActiveSkills();
                    initSlotsPassiveSkills();
                } else {
                    buttonsBuild.get(currentBuild).setType(EnumButtonBuildType.AVAILABLE);
                    buttonsBuild.get(previousBuildId).setType(EnumButtonBuildType.CURRENT);
                    currentBuild = previousBuildId;
                }
            });
        }
    }

    private void drawTreeConnections(float lineWidth) {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glLineWidth(lineWidth);
        GL11.glBegin(GL_LINES);
        connections.sort(Comparator.comparingInt(o -> o.getChild().getAvailableType().ordinal()));
        for(SkillTreeConnection connection : connections) {
            connection.drawLines();
        }
        GL11.glEnd();
        for(SkillTreeConnection connection : connections) {
            connection.drawEffects(lineWidth, treePosAnim, scaleAnim);
        }
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    private void drawSkillTree(int mouseX, int mouseY) {
        if(treeAnim < 1f) {
            float speed = 0.225f;
            treeAnim += AnimationHelper.getAnimationSpeed() * speed / 25f;
            if(treeAnim > 1) {
                treeAnim = 0f;
            }
        } else {
            treeAnim = 0f;
        }

        if(treePosAnim.x < treePos.x) {
            float speed = (treePos.x - treePosAnim.x) / 10f;
            if(speed < 0.001f) speed = 0.001f;
            treePosAnim.x += AnimationHelper.getAnimationSpeed() * speed;
            if(treePosAnim.x > treePos.x) {
                treePosAnim.x = treePos.x;
            }
        } else {
            float speed = (treePosAnim.x - treePos.x) / 10f;
            if(speed < 0.001f) speed = 0.001f;
            treePosAnim.x -= AnimationHelper.getAnimationSpeed() * speed;
            if(treePosAnim.x < treePos.x) {
                treePosAnim.x = treePos.x;
            }
        }

        if(treePosAnim.y < treePos.y) {
            float speed = (treePos.y - treePosAnim.y) / 10f;
            if(speed < 0.001f) speed = 0.001f;
            treePosAnim.y += AnimationHelper.getAnimationSpeed() * speed;
            if(treePosAnim.y > treePos.y) {
                treePosAnim.y = treePos.y;
            }
        } else {
            float speed = (treePosAnim.y - treePos.y) / 10f;
            if(speed < 0.001f) speed = 0.001f;
            treePosAnim.y -= AnimationHelper.getAnimationSpeed() * speed;
            if(treePosAnim.y < treePos.y) {
                treePosAnim.y = treePos.y;
            }
        }

        if(scaleAnim < scale) {
            float speed = (scale - scaleAnim) / 10f;
            if(speed < 0.001f) speed = 0.001f;
            scaleAnim += AnimationHelper.getAnimationSpeed() * speed;
            if(scaleAnim > scale) {
                scaleAnim = scale;
            }
        } else {
            float speed = (scaleAnim - scale) / 10f;
            if(speed < 0.001f) speed = 0.001f;
            scaleAnim -= AnimationHelper.getAnimationSpeed() * speed;
            if(scaleAnim < scale) {
                scaleAnim = scale;
            }
        }

        initTreeSkillsPositions();
        scissorAABB = AxisAlignedBB.getBoundingBox(0, 0, -100, ScaleGui.screenWidth, ScaleGui.screenHeight,100);
        drawTreeConnections(ScaleGui.get(2f * scaleAnim));
        glColor4f(1f,1f,1f,1f);
        for(GuiSkill skill : skills) {
            if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(skill.xPosition, skill.yPosition, 0, skill.xPosition + skill.width, skill.yPosition + skill.height, 0))) {
                skill.drawButton(mc, mouseX, mouseY);
            }
        }
        glColor4f(1f,1f,1f,1f);
    }

    private void drawLeftSide() {
        if(leftSideOpen) {
            leftSideAnim = AnimationHelper.updateSlowEndAnim(leftSideAnim, 1.0f, 0.1f, 0.005f);
        } else {
            leftSideAnim = AnimationHelper.updateSlowEndAnim(leftSideAnim, 0.0f, -0.1f, -0.005f);
        }

        float xOffset = 96 * leftSideAnim;
        GuiButtonAdvanced texturedButton = (GuiButtonAdvanced) buttonList.get(0);
        texturedButton.xPosition = ScaleGui.get(90 + xOffset, texturedButton.width);

        GuiDrawUtils.drawCentered(TextureRegister.textureSkillBorderLeft, 70 + xOffset, 502, 73, 682);
        GuiDrawUtils.drawString(FontType.HelveticaNeueCyrLight, "STATS DESCRIPTION", 52 + xOffset, 593, 1.25f, 0x626262);
        glColor4f(1f, 1f, 1f, 1f);

        float x = 83;
        float y = 165;
        float iconWidth = 26;
        GuiDrawUtils.drawCentered(TextureRegister.textureTreeIconResCombat, x + xOffset, y, iconWidth, 25);
        String s = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getExperienceMap().get(ExperienceType.BATTLE);
        float fs = 1.75f;
        x += iconWidth;
        GuiDrawUtils.drawString(FontType.Marske, s, x + xOffset, y - 7, fs, 0x972c38);
        x += FontType.Marske.getFontContainer().width(s) * fs + iconWidth;
        glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCentered(TextureRegister.textureTreeIconResResearch, x + xOffset, y, iconWidth, 25);
        x += iconWidth;
        s = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getExperienceMap().get(ExperienceType.EXPLORE);
        GuiDrawUtils.drawString(FontType.Marske, s, x + xOffset, y - 7, fs, 0x2f85aa);
        x += FontType.Marske.getFontContainer().width(s) * fs + iconWidth;
        glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCentered(TextureRegister.textureTreeIconResSurvive, x + xOffset, y, iconWidth, 25);
        x += iconWidth;
        s = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getExperienceMap().get(ExperienceType.SURVIVE);
        GuiDrawUtils.drawString(FontType.Marske, s, x + xOffset, y - 7, fs, 0x468039);
        s = "Shadows Project - an original MMORPG-FPS";
        fs = 0.875f;
        y = 203;
        GuiDrawUtils.drawString(FontType.DeadSpace, s, 72 + xOffset, y, fs, 0x2e2e2d);
        s = "Massively Multiplayer Online Role-Playing First";
        y += 15;
        GuiDrawUtils.drawString(FontType.DeadSpace, s, 72 + xOffset, y, fs, 0x2e2e2d);

        GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, "ACTIVE PASSIVE NODS", 92 + xOffset, 798, 1.25f, 0x626262);
        glColor4f(1f, 1f, 1f, 1f);

        if(leftSideAnim > 0.05f) {
            float addOffsetX = (1.0f - leftSideAnim) * -20f;
            x = 51 + xOffset + addOffsetX - 96;
            y = 193;
            iconWidth = 31;
            float iconHeight = 31;
            GuiDrawUtils.drawCentered(TextureRegister.textureSkillIconStatHp, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureSkillIconStatMana, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureSkillIconStatStamina, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureSkillIconStatArmor, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureSkillIconStatCutDefence, x, y, iconWidth, iconHeight);
            y += iconHeight * 2f;

            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconEnergy, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconHeat, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconFire, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconElectricity, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconToxins, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconRadiation, x, y, iconWidth, iconHeight);
            y += iconHeight * 1.1f;
            GuiDrawUtils.drawCentered(TextureRegister.textureInvIconExplosion, x, y, iconWidth, iconHeight);

//            y += iconHeight * 2f;
//            drawFromCenter(new ResourceLocation("inventory", "icon_stat_radiation1.png"), x, y, iconWidth, iconHeight);
//            y += iconHeight * 1.1f;
//            drawFromCenter(new ResourceLocation("inventory", "icon_stat_burning.png"), x, y, iconWidth, iconHeight);
//            y += iconHeight * 1.1f;
//            drawFromCenter(new ResourceLocation("inventory", "icon_stat_poisoned.png"), x, y, iconWidth, iconHeight);
//            y += iconHeight * 1.1f;
//            drawFromCenter(new ResourceLocation("inventory", "icon_stat_bleeding.png"), x, y, iconWidth, iconHeight);

            x = 51 + xOffset + addOffsetX - 94 + iconWidth;
            y = 190;

            float ballisticArmorValue = 0;
            float cutProtectionValue = 0;
            float energyProtection = 0;
            float thermalProtection = 0;
            float fireProtection = 0;
            float electricProtection = 0;
            float toxicProtection = 0;
            float radiationProtection = 0;
            float explosionProtection = 0;
            for(int v = MatrixInventory.SlotType.HEAD.ordinal(); v <= MatrixInventory.SlotType.FEET.ordinal();v++) {
                MatrixInventory.SlotType slotType = MatrixInventory.SlotType.values()[v];
                ItemStack itemStack = mc.thePlayer.inventory.getStackInSlot(slotType.getAssociatedSlotIndex());
                if(itemStack != null) {
                    Item item = itemStack.getItem();
                    if(item instanceof ItemArmor) {
                        ItemArmor itemArmor = (ItemArmor) item;
                        ballisticArmorValue += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.BALLISTIC_PROTECTION);
                        cutProtectionValue += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.CUT_PROTECTION);
                        energyProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.ENERGY_PROTECTION);
                        thermalProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.THERMAL_PROTECTION);
                        fireProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.FIRE_PROTECTION);
                        electricProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.ELECT_PROTECTION);
                        toxicProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.TOXIC_PROTECTION);
                        radiationProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.RADIATION_PROTECTION);
                        explosionProtection += itemArmor.getCharacterAttributeValue(itemStack, CharacterAttributeType.EXPLOSION_PROTECTION);
                    }
                }
            }


            for(int i=0;i<12;i++) {
                String value = "";
                switch (i) {
                    case 0: value = "" + (int) mc.thePlayer.getHealth(); break;
                    case 1: value = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.MANA).getValue(); break;
                    case 2:
//                        value = "" + (int) XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterAttribute(CharacterAttributeType.STAMINA).getValue();
                        SmartMovingSelf moving = (SmartMovingSelf) SmartMovingFactory.getInstance(mc.thePlayer);
                        float maxExhaustion = SmartMovingContext.Client.getMaximumExhaustion();
                        float fitness = maxExhaustion - Math.min(moving.exhaustion, maxExhaustion);
                        float stamina = fitness / 100f;
                        value = "" + (int) (stamina * 100);
                        break;
                    case 3: value = "" + (int) ballisticArmorValue; break;
                    case 4: value = "" + (int) cutProtectionValue; break;
                    case 5: value = "" + (int) energyProtection; break;
                    case 6: value = "" + (int) thermalProtection; break;
                    case 7: value = "" + (int) fireProtection; break;
                    case 8: value = "" + (int) electricProtection; break;
                    case 9: value = "" + (int) toxicProtection; break;
                    case 10: value = "" + (int) radiationProtection; break;
                    case 11: value = "" + (int) explosionProtection; break;
                }
                GuiDrawUtils.drawString(FontType.HelveticaNeueCyrMedium, value, x, y, 1.25f, 0xffffff);
                if(i == 4 || i == 11) {
                    y += iconHeight * 2f;
                } else {
                    y += iconHeight * 1.1f;
                }
            }
        }
    }

    private void drawSkillsPassive(int mouseX, int mouseY) {
        GuiDrawUtils.drawCenterXBot(TextureRegister.textureSkillPassiveSkillsBg, -25.5f, 853, 588, 118);
        for(GuiSlotSkillPassive slot : slotsPassive) {
            slot.drawButton(mc, mouseX, mouseY);
        }
    }

    private void drawSkillsActive(int mouseX, int mouseY) {
        for(GuiSlotSkillActive slot : slotsActive) {
            slot.drawButton(mc, mouseX, mouseY);
        }
    }

    private void drawControlButtons() {
        GuiDrawUtils.drawRightCentered(TextureRegister.textureSkillScrollBody, 1851, 446, 6, 150 * scaleAnim);
    }

    private void drawBuildButtons(int mouseX, int mouseY) {
        for(GuiButtonBuildSkillTree b : buttonsBuild) {
            b.drawButton(mc, mouseX, mouseY);
        }

        for(GuiButtonBuildSkillTree b : buttonsBuild) {
            b.drawTooltip(mc, mouseX, mouseY);
        }
    }

    private void drawBackgroundThings(int mouseX, int mouseY) {
        this.drawDefaultBackground();

        glEnable(GL_BLEND);
        glDisable(GL_ALPHA_TEST);

        glColor4f(0.5f,1f,1f,0.5f);
        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);
        GL11.glColor4f(1f, 1f, 1f, 1f);
        drawSkillTree(mouseX, mouseY);
        GuiDrawUtils.drawRect(TextureRegister.textureSkillOverlay, 0, 0, width, height);
        GuiDrawUtils.clearMaskBuffer(0, 0, width, height);
        for(GuiButtonBuildSkillTree b : buttonsBuild) {
            b.drawButtonEffect(mouseX, mouseY);
        }

        String s = "Shadows Project - an original MMORPG-FPS";
        float fs = 0.875f;
        float y = 900;
        float fsy = 12;
        GuiDrawUtils.drawRightStringRightBot(FontType.DeadSpace, s, 1766, y, fs, 0x2e2e2d);
        s = "Massively Multiplayer Online Role-Playing First";
        y -= fsy;
        GuiDrawUtils.drawRightStringRightBot(FontType.DeadSpace, s, 1766, y, fs, 0x2e2e2d);
        s = "Person Shooter) in a fantasy anti-utopian hi-tec future where the horrors";
        y -= fsy;
        GuiDrawUtils.drawRightStringRightBot(FontType.DeadSpace, s, 1766, y, fs, 0x2e2e2d);
        s = "of space and all the “charms” of interplanetary travel await you.";
        y -= fsy;
        GuiDrawUtils.drawRightStringRightBot(FontType.DeadSpace, s, 1766, y, fs, 0x2e2e2d);

        glColor4f(1f, 1f, 1f, 1f);
        GuiDrawUtils.drawCenterXCentered(TextureRegister.textureSkillBorderTop, 960, 94, 1804, 78);
        GuiDrawUtils.drawRightCentered(TextureRegister.textureSkillBorderRight, 1858f, 522, 53, 682);
        GuiDrawUtils.drawCenterXBotCentered(TextureRegister.textureSkillBorderBot, 960, 912, 1813, 154);

        GuiDrawUtils.drawStringCenterXBot(FontType.HelveticaNeueCyrLight, "ACTIVE SKILL NODS", 662, 888, 1.25f, 0x626262);

        s = "НАВЫКИ И УМЕНИЯ";
        fs = 2f;
        GuiDrawUtils.drawString(FontType.HelveticaNeueCyrLight, s, 100, 58, fs, 0x626262);
        GuiDrawUtils.drawString(FontType.HelveticaNeueCyrLight, CharacterType.values()[XlvsMainMod.INSTANCE.getSelectedCharacterIndex()].getDisplayName().toUpperCase(), 412, 65, 0.96f, 0x626262);

        String playerName = mc.thePlayer.getDisplayName().toUpperCase();
        GuiDrawUtils.drawCenteredStringCenterX(FontType.HelveticaNeueCyrLight, playerName, 960, 110, fs, 0x626262);
        GuiDrawUtils.drawStringCenterX(FontType.HelveticaNeueCyrLight, "LVL " + XlvsMainMod.INSTANCE.getClientMainPlayer().getLvl(), 960 + FontType.HelveticaNeueCyrLight.getFontContainer().width(playerName) * (fs) / 2f, 118, 0.96f, 0x626262);
        glColor4f(1f, 1f, 1f, 1f);
    }

    private void drawSkillsTooltip(int mouseX, int mouseY) {
        if(popup == null) {
            for(GuiSkill skill : skills) {
                if(scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(skill.xPosition, skill.yPosition, 0, skill.xPosition + skill.width, skill.yPosition + skill.height, 0))) {
                    skill.drawTooltip(mc, mouseX, mouseY);
                }
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        drawBackgroundThings(mouseX, mouseY);
        drawLeftSide();
        drawControlButtons();
        drawButtons(mouseX, mouseY, partialTick);
        drawSkillsPassive(mouseX, mouseY);
        drawSkillsActive(mouseX, mouseY);
        drawBuildButtons(mouseX, mouseY);
        drawSkillsTooltip(mouseX, mouseY);
        glColor4f(1f,1f,1f,1f);
        if(popup != null) popup.drawScreen(mouseX, mouseY, partialTick);
        if(movedSkill != null) {
            GuiDrawUtils.drawRect(movedSkill.getSkillLearnedTexture(), mouseX - movedSkill.width/2f, mouseY - movedSkill.height/2f, movedSkill.width, movedSkill.height);
        }
    }

    @Override
    public void handleMouseInput() {
        super.handleMouseInput();

        int d = Mouse.getEventDWheel();
        if (d != 0) {
            scale += d / 300f * ((scale + MIN_SCALE) / MAX_SCALE);
            if(scale > MAX_SCALE) {
                scale = MAX_SCALE;
            } else if(scale < MIN_SCALE) {
                scale = MIN_SCALE;
            }
        }
    }

    private boolean needSaveBuild() {
        for(GuiSlotSkillPassive guiSlotSkillPassive : slotsPassive) {
            Skill hotSkill = XlvsMainMod.INSTANCE.getClientMainPlayer().getHotPassiveSkill(guiSlotSkillPassive.id);
            if(hotSkill == null) {
                if (guiSlotSkillPassive.getSkill() != null) return true;
            } else if(guiSlotSkillPassive.getSkill() == null) return true;
            else if(hotSkill.getId() != guiSlotSkillPassive.getSkill().id) return true;
        }

        for(GuiSlotSkillActive guiSlotSkillActive : slotsActive) {
            Skill hotSkill = XlvsMainMod.INSTANCE.getClientMainPlayer().getHotActiveSkill(guiSlotSkillActive.id);
            if(hotSkill == null) {
                if (guiSlotSkillActive.getSkill() != null) return true;
            } else if(guiSlotSkillActive.getSkill() == null) return true;
            else if (hotSkill.getId() != guiSlotSkillActive.getSkill().id) return true;
        }

        return false;
    }

    @Override
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
        if (popup != null) {
            popup.mouseClicked(mouseX, mouseY, mouseButton);
        } else {
            if (mouseButton == 0) {
                for (Object o : this.buttonList) {
                    GuiButton guibutton = (GuiButton) o;
                    if (guibutton.mousePressed(this.mc, mouseX, mouseY)) {
                        this.selectedButton = guibutton;
                        guibutton.playClickSound(this.mc.getSoundHandler());
                        this.actionPerformed(guibutton);
                        return;
                    }
                }

                for (GuiSkill skill : skills) {
                    if (scissorAABB.intersectsWith(AxisAlignedBB.getBoundingBox(skill.xPosition, skill.yPosition, 0, skill.xPosition + skill.width, skill.yPosition + skill.height, 0)) &&
                            skill.mousePressed(this.mc, mouseX, mouseY) && skill.getAvailableType() == EnumSkillTreeAvailableType.AVAILABLE) {
                        this.popup = new GuiConfirmSkillLearn(this, skill);
                        return;
                    }
                }

                for(GuiButtonBuildSkillTree b : buttonsBuild) {
                    if(b.getType() == EnumButtonBuildType.AVAILABLE && b.mousePressed(this.mc, mouseX, mouseY)) {
                        if(needSaveBuild()) popup = new GuiConfirmSaveBuild(this, b);
                        else changeBuild(false, b.id);
                        return;
                    }
                }
            }
            else if(mouseButton == 1) {
                for (GuiSlotSkillActive slot : slotsActive) {
                    if(slot.getType() != EnumSlotSkillActiveType.LOCKED && slot.mousePressed(mc, mouseX, mouseY)) {
                        slot.setSkillTexture(null);
                        slot.setSkill(null);
                        break;
                    }
                }

                for (GuiSlotSkillPassive slot : slotsPassive) {
                    if(slot.getType() != EnumSlotSkillPassiveType.LOCKED && slot.mousePressed(mc, mouseX, mouseY)) {
                        slot.setSkill(null);
                        break;
                    }
                }
            }
        }

        if(movedSkill == null) {
            for (GuiSkill skill : skills) {
                if (skill.mousePressed(this.mc, mouseX, mouseY) && skill.getAvailableType() == EnumSkillTreeAvailableType.LEARNED) {
                    movedSkill = skill;
                    return;
                }
            }

            for(GuiSlotSkillPassive skillPassive : slotsPassive) {
                GuiSkill skill = skillPassive.getSkill();
                if(skill != null && skillPassive.mousePressed(mc, mouseX, mouseY)) {
                    movedSkill = skill;
                    skillPassive.setSkill(null);
                    return;
                }
            }

            for(GuiSlotSkillActive slotSkillActive : slotsActive) {
                GuiSkill skill = slotSkillActive.getSkill();
                if(skill != null && slotSkillActive.mousePressed(mc, mouseX, mouseY)) {
                    movedSkill = skill;
                    slotSkillActive.setSkill(null);
                    slotSkillActive.setSkillTexture(null);
                    return;
                }
            }
        }

        if(!treeMovingByMouse) {
            treeMovingByMouse = true;
            mouseLastClick.x = mouseX;
            mouseLastClick.y = mouseY;
        }
    }

    @Override
    public void mouseClickMove(int mouseX, int mouseY, int lastButtonClicked, long timeSinceMouseClick) {
        super.mouseClickMove(mouseX, mouseY, lastButtonClicked, timeSinceMouseClick);
        if(treeMovingByMouse && movedSkill == null) {
            treePos.x += (mouseX - mouseLastClick.x) / ScaleGui.scaleValue * ScaleGui.DEFAULT_HEIGHT / scaleAnim;
            treePos.y += (mouseY - mouseLastClick.y) / ScaleGui.scaleValue * ScaleGui.DEFAULT_HEIGHT / scaleAnim;
            mouseLastClick.x = mouseX;
            mouseLastClick.y = mouseY;
        }
    }

    private boolean isSkillNotInHotSlot(GuiSkill skill) {
        for (GuiSlotSkillPassive slot : slotsPassive) {
            if(slot.getSkill() != null && slot.getSkill().id == skill.id) return false;
        }

        for (GuiSlotSkillActive slot : slotsActive) {
            if(slot.getSkill() != null && slot.getSkill().id == skill.id) return false;
        }

        return true;
    }

    @Override
    public void mouseMovedOrUp(int mouseX, int mouseY, int which) {
        super.mouseMovedOrUp(mouseX, mouseY, which);
        if(movedSkill != null) {
            if(movedSkill.getType() == EnumSkillTreeType.PASSIVE) {
                for (GuiSlotSkillPassive slot : slotsPassive) {
                    if(slot.getType() != EnumSlotSkillPassiveType.LOCKED && slot.mousePressed(mc, mouseX, mouseY) && isSkillNotInHotSlot(movedSkill)) {
                        slot.setSkill(movedSkill);
                        break;
                    }
                }
            } else {
                for (GuiSlotSkillActive slot : slotsActive) {
                    if(slot.getType() != EnumSlotSkillActiveType.LOCKED && slot.mousePressed(mc, mouseX, mouseY) && isSkillNotInHotSlot(movedSkill)) {
                        slot.setSkill(movedSkill);
                        slot.setSkillTexture(movedSkill.getTextureName());
                        break;
                    }
                }
            }
            movedSkill = null;
            return;
        }

        if(treeMovingByMouse) {
            treeMovingByMouse = false;
        }
    }

    public int getCurrentBuild() {
        return currentBuild;
    }

    private void closeGui() {
        this.mc.displayGuiScreen(null);
    }

    @Override
    public void keyTyped(char character, int key) {
        if (key == 1) {
            if(needSaveBuild()) {
                popup = new GuiConfirmSaveBuild(this, buttonsBuild.get(currentBuild));
                wantToCloseGui = true;
            } else closeGui();
        }
    }

    @Override
    public void popupAction(GuiButton button) {

    }

    @Override
    public void setPopup(GuiPopup popup) {
        this.popup = popup;
    }
}
