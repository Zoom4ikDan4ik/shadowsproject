package ru.krogenit.escmenu.gui;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiShareToLan;
import ru.krogenit.TextureRegister;
import ru.krogenit.client.gui.api.*;
import ru.krogenit.shop.GuiShop;
import ru.xlv.core.XlvsCore;
import ru.xlv.customfont.FontType;
import ru.xlv.mochar.XlvsMainMod;

@SideOnly(Side.CLIENT)
public class GuiEscMenu extends AbstractGuiScreenAdvanced
{

    private final String characterNameAndLevel;

    public GuiEscMenu() {
        super(ScaleGui.SXGA);
        this.characterNameAndLevel = XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterType().getDisplayName().toUpperCase() + " " + XlvsMainMod.INSTANCE.getClientMainPlayer().getLvl() + " УР.";
    }

    public void initGui() {
        super.initGui();
        this.buttonList.clear();

        float buttonWidth = 271;
        float buttonHeight = 40;
        float y = 457;
        float x = 960;
        float buttonOffsetY = 19 + buttonHeight;
        GuiButtonAnimated button = new GuiButtonAnimated(4, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЕРНУТЬСЯ К ИГРЕ");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
        y += buttonOffsetY;
        button = new GuiButtonAnimated(0, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "НАСТРОЙКИ");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
        y += buttonOffsetY;
        button = new GuiButtonAnimated(5, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "МАГАЗИН");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
        y += buttonOffsetY;
        y += buttonOffsetY;
        button = new GuiButtonAnimated(1, ScaleGui.getCenterX(x, buttonWidth), ScaleGui.getCenterY(y, buttonHeight), ScaleGui.get(buttonWidth), ScaleGui.get(buttonHeight), "ВЫХОД");
        button.setTexture(TextureRegister.textureESCButton);
        button.setTextureHover(TextureRegister.textureESCButtonHover);
        button.setMaskTexture(TextureRegister.textureESCButtonMask);
        this.buttonList.add(button);
    }

    protected void actionPerformed(GuiButton guiButton) {
        switch (guiButton.id) {
            case 0:
                this.mc.displayGuiScreen(new GuiOptions(this, mc.gameSettings));
                break;
            case 1:
                guiButton.enabled = false;
                XlvsCore.INSTANCE.saveLastArmorConfig();
                this.mc.theWorld.sendQuittingDisconnectingPacket();
                this.mc.loadWorld(null);
                this.mc.displayGuiScreen(new GuiMainMenu());
            case 2:
            case 3:
            default:
                break;
            case 4:
                this.mc.displayGuiScreen(null);
                this.mc.setIngameFocus();
                break;
            case 5:
                this.mc.displayGuiScreen(new GuiShop());
                break;
            case 7:
                this.mc.displayGuiScreen(new GuiShareToLan(this));
                break;
            case 12:
                FMLClientHandler.instance().showInGameModOptions(this);
                break;
        }
    }

    public void drawScreen(int mouseX, int mouseY, float partialTick) {
        super.drawScreen(mouseX, mouseY, partialTick);
        this.drawDefaultBackground();
        GuiDrawUtils.drawMaskingBackgroundEffect(0, 0, width, height);

        float fsx = 2.4f;
        String playerName = mc.thePlayer.getDisplayName().toUpperCase();
        float x = 960;
        float y = 345f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, playerName, x, y, fsx, 0x626262);
        fsx = 1.1f;
        y += 43f;
        GuiDrawUtils.drawCenteredStringCenter(FontType.HelveticaNeueCyrLight, characterNameAndLevel, x, y, fsx, 0x626262);

        fsx = 0.8f;
        x = 1866;
        y = 937;
        String s = "VERSION 0.163 BETA";
        GuiDrawUtils.drawRightStringRightBot(FontType.HelveticaNeueCyrLight, s, x, y, fsx, 0xffffff);
        s = "Shadows Project - an original MMORPG-FPS\n" +
                "Massively Multiplayer Online Role-Playing First\n" +
                "Person Shooter) in a fantasy anti-utopian hi-tec future where the horrors\n" +
                "of space and all the “charms” of interplanetary travel await you.";
        fsx = 0.7f;
        y = 971;
        GuiDrawUtils.drawSplittedStringRightBot(FontType.DeadSpace, s, x, y, fsx, 1000f, -1f, 0x626262, EnumStringRenderType.RIGHT);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureESCBorderTop, 960, 291, 821, 78);
        GuiDrawUtils.drawCenterCentered(TextureRegister.textureESCBorderBot, 960, 771, 821, 78);
        GuiDrawUtils.drawBotCentered(TextureRegister.textureESCLogo, 137, 960, 184, 53);
        GuiDrawUtils.drawPreAlpha(1400, 50, 300, 60, 1.0f);
        drawButtons(mouseX, mouseY, partialTick);
    }
}