package ru.xlv.core.database;

import lombok.Data;

@Data
public class ServerIndex {

    private final String serverAddress;
    private final int index;
}
