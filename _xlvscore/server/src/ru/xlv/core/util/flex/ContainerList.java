package ru.xlv.core.util.flex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * todo:
 *  добавить все методы из StreamApi
 *  перенести некоторые функции Flex
 * */
public abstract class ContainerList<T extends Container<V>, V> extends Container<Collection<T>> {

    protected Stream<T> stream;

    public ContainerList(Collection<T> object) {
        super(object);
    }

    public ContainerList<T, V> notEmpty() {
        chain = stream.count() != 0;
        return this;
    }

    public ContainerList<T, V> filter(Predicate<V> predicate) {
        stream = stream.filter(serverPlayerFlexPlayer -> predicate.test(serverPlayerFlexPlayer.object));
        return this;
    }

    public ContainerList<T, V> forEach(Consumer<V> consumer) {
        Set<T> collect = stream.collect(Collectors.toSet());
        collect.forEach(serverPlayerFlexPlayer -> consumer.accept(serverPlayerFlexPlayer.object));
        stream = collect.stream();
        return this;
    }

    public ContainerList<T, V> forEachFlex(Consumer<T> consumer) {
        Set<T> collect = stream.collect(Collectors.toSet());
        collect.forEach(consumer);
        stream = collect.stream();
        return this;
    }

    @Override
    protected <R> BiContainer<?, ?, R> apply(Function<Collection<T>, R> function) {
        throw new UnsupportedOperationException();
    }

    public <U> List<U> aggregate(Function<V, U> function) {
        List<U> list = new ArrayList<>();
        forEach(serverPlayer -> list.add(function.apply(serverPlayer)));
        return list;
    }

    public <U, R> R collect(Collector<? super T, U, R> collector) {
        return stream.collect(collector);
    }
}
