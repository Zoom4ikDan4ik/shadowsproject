package ru.xlv.core.util;

public interface IConverter<SOURCE, TARGET> {
    TARGET convertTo(SOURCE source);
    SOURCE convertFrom(TARGET target);
}
