package ru.xlv.core.util;

import lombok.Getter;
import ru.xlv.core.common.util.config.Configurable;

import java.io.File;

@Getter
@Configurable
public class CoreLocalization extends Localization {

    public String responseTooManyRequests = "Too many requests! Try again later.";

    public String responseMatrixInvAddItemError = "Impossible to pick up the item. No free space!";

    @Override
    public File getConfigFile() {
        return new File("config/xlvscore/localization.json");
    }
}
