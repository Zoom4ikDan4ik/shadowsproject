package ru.xlv.core.util;

import lombok.Data;
import ru.xlv.core.common.util.config.Configurable;

@Data
@Configurable
public class ItemStackModel {
    private final int id;
    private final int count;
    private final int damage;
    private final String stringedNbtTagCompound;
}
