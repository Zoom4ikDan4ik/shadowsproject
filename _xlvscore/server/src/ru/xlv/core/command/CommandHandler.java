package ru.xlv.core.command;

import gnu.trove.map.TObjectDoubleMap;
import lombok.experimental.UtilityClass;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.flex.FlexPlayer;
import ru.xlv.mochar.player.kit.KitHandler;
import ru.xlv.mochar.util.Utils;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
@UtilityClass
public class CommandHandler {

    private final Map<String, Boolean> MODES_MAP = new HashMap<>();

    @Command
    public void debugDamage(EntityPlayer entityPlayer, String[] args) {
        Boolean aBoolean = MODES_MAP.get(entityPlayer.getCommandSenderName());
        if (aBoolean == null) {
            aBoolean = true;
        } else {
            aBoolean = !aBoolean;
        }
        MODES_MAP.put(entityPlayer.getCommandSenderName(), aBoolean);
        Utils.sendMessage(entityPlayer, aBoolean ? "Enabled" : "Disabled");
    }

    public void handleDebugDamageMessage(String state, ServerPlayer attacker, ServerPlayer target, DamageSource damageSource, double amount, TObjectDoubleMap<CharacterAttributeType> damageMap) {

    }

    @Command
    public void clearMatrix(EntityPlayer entityPlayer, String[] args) {
        String username = entityPlayer.getCommandSenderName();
        if(args.length > 0) {
            username = args[0];
        }
        FlexPlayer.of(username)
                .notNull()
                .orAccept(serverPlayer -> Utils.sendMessage(serverPlayer, "Player not found."))
                .accept(serverPlayer -> {
                    serverPlayer.getSelectedCharacter().getMatrixInventory().clear();
                    serverPlayer.getEntityPlayer().inventory.clearInventory(null, -1);
                })
                .sendMessage("The inventory of %s cleared.", username);
    }

    @Command("xkit")
    public void kit(EntityPlayer entityPlayer, String[] args) {
        String username = entityPlayer.getCommandSenderName();
        if(args.length > 1) {
            username = args[1];
        }
        FlexPlayer.of(username)
                .notNull()
                .apply(serverPlayer -> KitHandler.getKit(args[0]))
                .notNull()
                .accept((flexPlayer, kit) -> {
                    KitHandler.giveOut(flexPlayer.getPlayer(), kit);
                    flexPlayer.sendMessage("Kit has been successfully gave out.");
                });
    }
}
