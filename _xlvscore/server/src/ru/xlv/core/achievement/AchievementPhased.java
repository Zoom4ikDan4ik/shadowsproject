package ru.xlv.core.achievement;

import lombok.Getter;

public class AchievementPhased extends AchievementProgressive {

    @Getter
    private final int[] maxProgresses;
    @Getter
    protected int phase;

    public AchievementPhased(AchievementType achievementType, int[] progresses) {
        super(achievementType, 0);
        this.maxProgresses = progresses;
        recalculateTotals();
    }

    @Override
    public void updateProgress() {
        if(progress + 1 < maxProgresses[phase] || phase + 1 < maxProgresses.length) {
            if(progress + 1 == maxProgresses[phase]) {
                phase++;
                progress = 0;
            } else {
                progress++;
            }
            recalculateTotals();
        }
    }

    @Override
    public void achieve() {
        phase = maxProgresses.length;
        recalculateTotals();
    }

    private void recalculateTotals() {
        int totalMaxProgress = 0;
        for (int value : maxProgresses) {
            totalMaxProgress += value;
        }
        this.maxProgress = totalMaxProgress;
    }

    public boolean isAchieved(int phase) {
        return progress >= maxProgresses[phase];
    }

    /**
     * Следует использовать isAchieved(int phase), чтобы получить состояние конкретной фазы достижения
     * */
    @Override
    public boolean isAchieved() {
        return phase == maxProgresses.length;
    }

    @Override
    public Achievement clone() {
        AchievementPhased achievementPhased = new AchievementPhased(getAchievementType(), maxProgresses);
        achievementPhased.maxProgress = maxProgress;
        achievementPhased.progress = progress;
        achievementPhased.phase = phase;
        return achievementPhased;
    }
}
