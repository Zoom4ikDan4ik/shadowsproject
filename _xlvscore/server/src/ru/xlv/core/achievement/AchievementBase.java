package ru.xlv.core.achievement;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class AchievementBase extends Achievement {

    protected final AchievementType achievementType;
    protected boolean isAchieved;

    @Override
    public void achieve() {
        isAchieved = true;
    }

    @Override
    public void setAchieved(boolean flag) {
        isAchieved = flag;
    }

    @Override
    public boolean isAchieved() {
        return isAchieved;
    }

    @Override
    public AchievementType getAchievementType() {
        return achievementType;
    }

    @Override
    public Achievement clone() {
        AchievementBase achievementBase = new AchievementBase(achievementType);
        achievementBase.isAchieved = isAchieved;
        return achievementBase;
    }
}
