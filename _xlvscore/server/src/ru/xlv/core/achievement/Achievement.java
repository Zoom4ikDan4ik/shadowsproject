package ru.xlv.core.achievement;

import ru.xlv.core.common.database.DatabaseAdaptedBy;

@DatabaseAdaptedBy(AchievementDatabaseAdapter.class)
public abstract class Achievement {

    public abstract void achieve();

    public abstract void setAchieved(boolean flag);

    public abstract boolean isAchieved();

    public abstract AchievementType getAchievementType();

    @Override
    public abstract Achievement clone();
}
