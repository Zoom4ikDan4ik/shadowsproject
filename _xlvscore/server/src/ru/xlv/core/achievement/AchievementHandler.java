package ru.xlv.core.achievement;

import ru.xlv.core.XlvsCore;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.stat.StatManager;
import ru.xlv.mochar.player.stat.StatType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class AchievementHandler {

    /**
     * Achieves the achievement or moves progress for progressive achievement
     * */
    public void updateAchievement(ServerPlayer serverPlayer, AchievementType achievementType) {
        Achievement achievement = getAchievementByType(serverPlayer, achievementType);
        if (achievement == null) {
            achievement = AchievementFactory.getAchievement(achievementType);
            putAchievement(serverPlayer, achievement);
        }
        if (updateAchievement(serverPlayer, achievement)) {
            StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
            statManager.getStatProvider(StatType.TOTAL_ACHIEVEMENT_AMOUNT).increment();
        }
//        if(serverPlayer.isOnline()) {
//            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketAchievementNew(achievement));
//        }
    }

    private boolean updateAchievement(ServerPlayer serverPlayer, Achievement achievement) {
        if(achievement instanceof AchievementProgressive) {
            ((AchievementProgressive) achievement).updateProgress();
        } else if(achievement instanceof AchievementPredictable) {
            ((AchievementPredictable) achievement).achieve(serverPlayer);
        } else {
            achievement.achieve();
        }
        return achievement.isAchieved();
    }

    private void putAchievement(ServerPlayer serverPlayer, Achievement achievement) {
        List<Achievement> achievements = getAchievements(serverPlayer);
        achievements.add(achievement);
    }

    @Nullable
    public Achievement getAchievementByType(ServerPlayer serverPlayer, AchievementType achievementType) {
        List<Achievement> achievements = getAchievements(serverPlayer);
        if(achievements != null) {
            for (Achievement achievement1 : achievements) {
                if (achievement1.getAchievementType() == achievementType) {
                    return achievement1;
                }
            }
        }
        return null;
    }

    @Nullable
    public List<Achievement> getAchievements(String username) {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(username);
        if (serverPlayer != null) {
            return serverPlayer.getSelectedCharacter().getAchievementManager().getAchievements();
        }
        return null;
    }

    public List<Achievement> getAchievements(@Nonnull ServerPlayer serverPlayer) {
        return serverPlayer.getSelectedCharacter().getAchievementManager().getAchievements();
    }
}
