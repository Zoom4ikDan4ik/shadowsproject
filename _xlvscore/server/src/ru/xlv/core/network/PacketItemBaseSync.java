package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.item.ItemBase;
import ru.xlv.core.common.item.ItemIOUtils;
import ru.xlv.core.common.network.IPacketOutServer;

import java.io.IOException;

@NoArgsConstructor
public class PacketItemBaseSync implements IPacketOutServer {

    private ItemBase itemBase;

    public PacketItemBaseSync(ItemBase itemBase) {
        this.itemBase = itemBase;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        ItemIOUtils.write(itemBase, bbos);
    }
}
