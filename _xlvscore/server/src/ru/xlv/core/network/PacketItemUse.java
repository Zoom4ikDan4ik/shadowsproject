package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.item.IItemUsable;
import ru.xlv.core.common.item.ItemUseResult;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@NoArgsConstructor
public class PacketItemUse implements IPacketCallbackOnServer {

    private int slotIndex;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        slotIndex = bbis.readInt();
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        boolean success = false;
        ItemStack stackInSlot = entityPlayer.inventory.getStackInSlot(slotIndex);
        if(stackInSlot != null && stackInSlot.getItem() instanceof IItemUsable) {
            ItemUseResult itemUseResult = ((IItemUsable) stackInSlot.getItem()).useItem(entityPlayer, stackInSlot, slotIndex);
            success = itemUseResult.isSuccess();
        }
        bbos.writeBoolean(success);
    }
}
