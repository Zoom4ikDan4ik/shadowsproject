package ru.xlv.core.network;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.util.SoundType;

import java.io.IOException;

@NoArgsConstructor
public class PacketPlaySound implements IPacketOutServer {

    private SoundType soundType;
    private String soundKey;
    private boolean isGuiSound;
    private double x, y, z;
    private float volume = 1, pitch = 1;

    public PacketPlaySound(SoundType soundType) {
        this.soundType = soundType;
        this.isGuiSound = true;
    }

    public PacketPlaySound(Entity entity, SoundType soundType) {
        this.soundType = soundType;
        this.isGuiSound = false;
        this.x = entity.posX;
        this.y = entity.posY;
        this.z = entity.posZ;
    }

    public PacketPlaySound(Entity entity, SoundType soundType, float volume, float pitch) {
        this.soundType = soundType;
        this.isGuiSound = false;
        this.x = entity.posX;
        this.y = entity.posY;
        this.z = entity.posZ;
        this.volume = volume;
        this.pitch = pitch;
    }

    public PacketPlaySound(Entity entity, String soundKey) {
        this.soundKey = soundKey;
        this.isGuiSound = false;
        this.x = entity.posX;
        this.y = entity.posY;
        this.z = entity.posZ;
    }

    public PacketPlaySound(double x, double y, double z, SoundType soundType) {
        this.soundType = soundType;
        this.isGuiSound = false;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        bbos.writeBoolean(isGuiSound);
        bbos.writeBoolean(soundKey != null);
        if(soundKey == null) {
            bbos.writeInt(soundType.ordinal());
        } else {
            bbos.writeUTF(soundKey);
        }
        if(!isGuiSound) {
            bbos.writeFloat(volume);
            bbos.writeFloat(pitch);
            bbos.writeDouble(x);
            bbos.writeDouble(y);
            bbos.writeDouble(z);
        }
    }
}
