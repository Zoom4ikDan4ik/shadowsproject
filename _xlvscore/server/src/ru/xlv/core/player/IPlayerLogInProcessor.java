package ru.xlv.core.player;

import net.minecraft.entity.player.EntityPlayer;

public interface IPlayerLogInProcessor {
    void playerAboutToLogIn(EntityPlayer entityPlayer);
    void playerLoggedIn(ServerPlayer serverPlayer);
}
