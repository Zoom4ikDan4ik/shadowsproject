package ru.xlv.core.player;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.player.Player;
import ru.xlv.core.common.player.PlayerManagerBase;
import ru.xlv.core.common.player.character.CharacterAttributeType;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.player.stat.StatIntProvider;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.common.util.WorldPosition;
import ru.xlv.core.database.DatabaseManager;
import ru.xlv.core.event.ServerPlayerLoginEvent;
import ru.xlv.core.event.ServerPlayerLogoutEvent;
import ru.xlv.core.event.ServerPlayerRespawnEvent;
import ru.xlv.core.event.ServerPlayerUpdateEvent;
import ru.xlv.core.util.Flex;
import ru.xlv.core.util.ScheduledTask;
import ru.xlv.core.util.flex.FlexPlayerList;
import ru.xlv.mochar.network.PacketClientPlayerSync;
import ru.xlv.mochar.network.matrix.PacketMatrixPlayerInventorySync;
import ru.xlv.mochar.network.skill.PacketSkillAllList;
import ru.xlv.mochar.network.skill.PacketSkillBuildSync;
import ru.xlv.mochar.network.skill.PacketSkillList;
import ru.xlv.mochar.network.skill.PacketSkillSync;
import ru.xlv.mochar.player.stat.StatManager;
import ru.xlv.mochar.player.stat.StatType;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PlayerManagerServer extends PlayerManagerBase<ServerPlayer> {

    private final List<IPlayerLogInProcessor> playerLoginProcessors = new ArrayList<>();
    private final List<IPlayerTracker> playerTrackers = new ArrayList<>();

    public void init() {
        CommonUtils.registerFMLEvents(this);
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        ScheduledTask scheduledTask = new ScheduledTask(500L, () -> forEach(serverPlayer -> {
            if(serverPlayer.isOnline()) {
                forEach(serverPlayer1 -> {
                    if (serverPlayer1.isOnline() && serverPlayer != serverPlayer1) {
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer1.getEntityPlayer(), new PacketClientPlayerSync(serverPlayer));
                        XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketClientPlayerSync(serverPlayer1));
                    }
                });
                XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketSkillSync(serverPlayer));
            }
            playerTrackers.forEach(iPlayerTracker -> iPlayerTracker.onUpdate(serverPlayer));
        }));
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            try {
                forEach(serverPlayer -> {
                    if(serverPlayer.isOnline()) {
                        serverPlayer.update();
                        XlvsCoreCommon.EVENT_BUS.post(new ServerPlayerUpdateEvent(serverPlayer));
                    }
                });
                scheduledTask.update();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 0L, 50L, TimeUnit.MILLISECONDS);
        PlayerNBTLogInProcessor PlayerNBTLogInProcessor = new PlayerNBTLogInProcessor();
        try {
            XlvsCore.INSTANCE.getDatabaseManager().getAllPlayers()
                    .stream()
                    .peek(serverPlayer -> serverPlayer.setOnline(false))
                    .forEach(this::addPlayer);
            XlvsCore.INSTANCE.getDatabaseManager().getAllPlayerNBTTags().forEach(PlayerNBTLogInProcessor::addLoadedEntityPlayerNBTTag);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        playerLoginProcessors.add(PlayerNBTLogInProcessor);
        playerLoginProcessors.add(new PlayerSpawnLogInProcessor());
        playerTrackers.add(new PlayerSysTimeTracker());
//        playerTrackers.add(new PlayerMoveTracker());
    }

    @SubscribeEvent
    public void event(PlayerEvent.PlayerRespawnEvent event) {
        ServerPlayer player = getPlayer(event.player, true);
        double maxHealth = player.getSelectedCharacter().getAttribute(CharacterAttributeType.MAX_HEALTH).getValue();
        event.player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.maxHealth).setBaseValue(maxHealth);
        event.player.setHealth((float) maxHealth);
        player.getSelectedCharacter().onMatrixInvCleaned();
        player.getSelectedCharacter().calcInvItemMods(player);
        XlvsCoreCommon.EVENT_BUS.post(new ServerPlayerRespawnEvent(player));
    }

    @SubscribeEvent
    public void event(PlayerEvent.PlayerLoggedOutEvent event) {
        ServerPlayer player = getPlayer(event.player, false);
        player.setOnline(false);
        updateTotalOnlineTime(player);
        player.setLastOnlineTimeMills(System.currentTimeMillis());
        if(player.getSelectedCharacter() != null && player.getSelectedCharacter().getSkillManager() != null) {
            player.getSelectedCharacter().getSkillManager().deactivateAllSkills(player);
        }
        XlvsCoreCommon.EVENT_BUS.post(new ServerPlayerLogoutEvent(player));
        uploadPlayer(player);
    }

    @SubscribeEvent
    public void event(PlayerEvent.PlayerLoggedInEvent event) {
        playerLoginProcessors.forEach(iPlayerLogInProcessor -> iPlayerLogInProcessor.playerAboutToLogIn(event.player));
        ServerPlayer serverPlayer = getPlayer(event.player, true);
        serverPlayer.setOnline(true);
        serverPlayer.setLastOnlineTimeMills(System.currentTimeMillis());
        playerLoginProcessors.forEach(iPlayerLogInProcessor -> iPlayerLogInProcessor.playerLoggedIn(serverPlayer));
        XlvsCore.INSTANCE.getPacketHandler().sendPacketsToPlayer(serverPlayer.getEntityPlayer(),
                new PacketSkillList(serverPlayer),
                new PacketSkillAllList(serverPlayer),
                new PacketMatrixPlayerInventorySync(serverPlayer),
                new PacketSkillBuildSync(serverPlayer)
        );
        FlexPlayerList.of(getPlayerList())
                .filter(Player::isOnline)
                .filter(serverPlayer1 -> serverPlayer != serverPlayer1)
                .forEach(serverPlayer1 -> {
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer1.getEntityPlayer(), new PacketClientPlayerSync(serverPlayer));
                    XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(serverPlayer.getEntityPlayer(), new PacketClientPlayerSync(serverPlayer1));
                });
        XlvsCoreCommon.EVENT_BUS.post(new ServerPlayerLoginEvent(serverPlayer));
        serverPlayer.getSelectedCharacter().getSkillManager().activatePassives(serverPlayer);
        uploadPlayer(serverPlayer);
    }

    private void uploadPlayer(@Nonnull ServerPlayer serverPlayer) {
        DatabaseManager databaseManager = XlvsCore.INSTANCE.getDatabaseManager();
        if(serverPlayer.isNewbie()) {
            serverPlayer.setNewbie(false);
            for (CharacterType value : CharacterType.values()) {
                if(serverPlayer.getSelectedCharacter() != null && value == serverPlayer.getSelectedCharacter().getCharacterType()) continue;
                databaseManager.createEmptyPlayerAsync(serverPlayer.getPlayerName(), value).handle((aBoolean, throwable) -> {
                    if(throwable != null || !aBoolean) {
                        System.out.println("An error has occurred during saving the player " + serverPlayer.getPlayerName());
                        if (throwable != null) {
                            throwable.printStackTrace();
                        }
                    }
                    return null;
                });
            }
        }
        databaseManager.updatePlayerAsync(serverPlayer).handle((aBoolean, throwable) -> {
            if(throwable != null || !aBoolean) {
                System.out.println("An error has occurred during saving the player " + serverPlayer.getPlayerName());
                if (throwable != null) {
                    throwable.printStackTrace();
                }
            }
            return null;
        });
    }

    private void updateTotalOnlineTime(@Nonnull ServerPlayer serverPlayer) {
        if(serverPlayer.getSelectedCharacter() != null) {
            long online = System.currentTimeMillis() - serverPlayer.getLastOnlineTimeMills();
            int seconds = (int) (online / 1000L);
            StatManager statManager = serverPlayer.getSelectedCharacter().getStatManager();
            ((StatIntProvider) statManager.getStatProvider(StatType.TOTAL_ONLINE_TIME_SECONDS)).increment(seconds);
        }
    }

    public boolean consumePlatinum(ServerPlayer serverPlayer, double amount) throws SQLException {
        double playerPlatinumSync = XlvsCore.INSTANCE.getDatabaseManager().getPlayerPlatinumSync(serverPlayer.getPlayerName());
        double amount1 = playerPlatinumSync - amount;
        if(amount1 >= 0) {
            return XlvsCore.INSTANCE.getDatabaseManager().setPlayerPlatinumSync(serverPlayer.getPlayerName(), amount1);
        }
        return false;
    }

    public void movePlayer(@Nonnull ServerPlayer serverPlayer, WorldPosition worldPosition) {
        movePlayer(serverPlayer, worldPosition.getDimension(), worldPosition.getX(), worldPosition.getY(), worldPosition.getZ());
    }

    public void movePlayer(@Nonnull ServerPlayer serverPlayer, int dimension, double x, double y, double z) {
        if(serverPlayer.isOnline()) {
            serverPlayer.movePlayer(dimension, x, y, z);
        } else {
            PlayerSpawnLogInProcessor playerLogInProcessor = getPlayerLogInProcessor(PlayerSpawnLogInProcessor.class);
            if (playerLogInProcessor != null) {
                playerLogInProcessor.setPlayerSpawnPosition(serverPlayer, new WorldPosition(dimension, x, y, z));
            }
        }
    }

    public ServerPlayer getPlayer(@Nonnull EntityPlayer entityPlayer) {
        return getPlayer(entityPlayer, false);
    }

    public ServerPlayer getPlayer(@Nonnull EntityPlayer entityPlayer, boolean initPlayer) {
        ServerPlayer serverPlayer = getPlayer(entityPlayer.getCommandSenderName());
        if(serverPlayer == null) addPlayer(serverPlayer = new ServerPlayer(entityPlayer.getCommandSenderName()));
        if(initPlayer) serverPlayer.init(entityPlayer);
        return serverPlayer;
    }

    @Nullable
    public List<ServerPlayer> getPlayersAround(@Nonnull ServerPlayer player, double radius) {
        EntityPlayer entityPlayer = player.getEntityPlayer();
        if (entityPlayer != null) {
            return getPlayersAround(entityPlayer.posX, entityPlayer.posY, entityPlayer.posZ, radius);
        }
        return null;
    }

    public List<ServerPlayer> getPlayersAround(double x, double y, double z, double radius) {
        return getPlayerList().stream()
                .filter(serverPlayer -> serverPlayer.isOnline()
                        && serverPlayer.getEntityPlayer().posX >= x - radius
                        && serverPlayer.getEntityPlayer().posY >= y - radius
                        && serverPlayer.getEntityPlayer().posZ >= z - radius
                )
                .collect(Collectors.toList());
    }

    public List<ServerPlayer> getOnlinePlayers() {
        return getPlayerList().stream().filter(ServerPlayer::isOnline).collect(Collectors.toList());
    }

    @Nullable
    public <T> T getPlayerLogInProcessor(Class<T> tClass) {
        synchronized (playerLoginProcessors) {
            //noinspection unchecked
            return (T) Flex.getCollectionElement(playerLoginProcessors, iPlayerLogInProcessor -> iPlayerLogInProcessor.getClass() == tClass);
        }
    }

    public void registerPlayerLogInProcessor(@Nonnull IPlayerLogInProcessor iPlayerLogInProcessor) {
        synchronized (playerLoginProcessors) {
            playerLoginProcessors.add(iPlayerLogInProcessor);
        }
    }
}
