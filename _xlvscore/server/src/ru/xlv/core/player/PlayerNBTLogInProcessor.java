package ru.xlv.core.player;

import com.mojang.authlib.GameProfile;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.Logger;
import ru.xlv.mochar.XlvsMainMod;

import java.util.HashMap;
import java.util.Map;

public class PlayerNBTLogInProcessor implements IPlayerLogInProcessor {

    private final Logger logger = new Logger(getClass().getSimpleName());

    private final TObjectIntMap<String> playerSelectedCharacterMap = new TObjectIntHashMap<>();
    private final Map<String, NBTTagCompound> loadedEntityPlayerNBTTagMap = new HashMap<>();

    public void userAboutToLogIn(GameProfile gameProfile) {
        logger.info("userAboutToLogIn: " + gameProfile.getName());
        if(XlvsCore.INSTANCE.getPlayerManager().getPlayer(gameProfile.getName()) == null) {
            logger.info("not found serverplayer");
        }
        int selectedCharacterIndex = playerSelectedCharacterMap.get(gameProfile.getName());
        NBTTagCompound nbtTagCompound = XlvsCore.INSTANCE.getDatabaseManager().getEntityPlayerNBTTags(gameProfile.getName(), CharacterType.values()[selectedCharacterIndex]);
        if (nbtTagCompound != null) {
            addLoadedEntityPlayerNBTTag(gameProfile.getName(), nbtTagCompound);
        }
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getDatabaseManager().getPlayer(gameProfile.getName());
        if(serverPlayer == null) {
            serverPlayer = new ServerPlayer(gameProfile.getName());
        }
        serverPlayer.setSelectedCharacterIndex(selectedCharacterIndex);
        logger.info("selected character: " + serverPlayer.getSelectedCharacterIndex());
        synchronized (XlvsCore.INSTANCE.getPlayerManager().getPlayerList()) {
            XlvsCore.INSTANCE.getPlayerManager().getPlayerList().removeIf(serverPlayer1 -> serverPlayer1.getPlayerName().equals(gameProfile.getName()));
            XlvsCore.INSTANCE.getPlayerManager().getPlayerList().add(serverPlayer);
        }
    }

    @Override
    public void playerAboutToLogIn(EntityPlayer entityPlayer) {
        logger.info("userLoggedIn: " + entityPlayer.getCommandSenderName());
        NBTTagCompound nbtTagCompound = getLoadedEntityPlayerNBTTag(entityPlayer.getCommandSenderName());
        if (nbtTagCompound != null) {
            XlvsMainMod.INSTANCE.getCharacterLoader().loadEntityPlayerFromNBT(entityPlayer, nbtTagCompound);
        }
    }

    @Override
    public void playerLoggedIn(ServerPlayer serverPlayer) {

    }

    public void putSelectedCharacter(String username, int characterIndex) {
        synchronized (playerSelectedCharacterMap) {
            playerSelectedCharacterMap.put(username, characterIndex);
        }
    }

    protected void addLoadedEntityPlayerNBTTag(String username, NBTTagCompound nbtTagCompound) {
        synchronized (loadedEntityPlayerNBTTagMap) {
            loadedEntityPlayerNBTTagMap.put(username, nbtTagCompound);
        }
    }

    private NBTTagCompound getLoadedEntityPlayerNBTTag(String username) {
        synchronized (loadedEntityPlayerNBTTagMap) {
            return loadedEntityPlayerNBTTagMap.remove(username);
        }
    }
}
