package ru.xlv.mochar.player;

import net.minecraft.util.DamageSource;
import ru.xlv.core.player.ServerPlayer;

import java.util.function.Function;
import java.util.function.Predicate;

public enum CumulativeEffectType {

    FIRE(1D, 1D, 1000L, 1000L, serverPlayer -> {
        return serverPlayer.getEntityPlayer().isBurning();
    }, serverPlayer -> {
        serverPlayer.getEntityPlayer().attackEntityFrom(DamageSource.inFire, 1F);
        return null;
    });

    private final Predicate<ServerPlayer> predicate;
    private final Function<ServerPlayer, Void> function;

    private final double levelAmpValue, levelDecreaseValue;
    private final long ampPeriod, decreasePeriod;

    CumulativeEffectType(double levelAmpValue, double levelDecreaseValue, long ampPeriod, long decreasePeriod, Predicate<ServerPlayer> predicate, Function<ServerPlayer, Void> function) {
        this.predicate = predicate;
        this.function = function;
        this.levelAmpValue = levelAmpValue;
        this.levelDecreaseValue = levelDecreaseValue;
        this.ampPeriod = ampPeriod;
        this.decreasePeriod = decreasePeriod;
    }

    public double getLevelAmpValue() {
        return levelAmpValue;
    }

    public double getLevelDecreaseValue() {
        return levelDecreaseValue;
    }

    public long getAmpPeriod() {
        return ampPeriod;
    }

    public long getDecreasePeriod() {
        return decreasePeriod;
    }

    public Predicate<ServerPlayer> getPredicate() {
        return predicate;
    }

    public Function<ServerPlayer, Void> getFunction() {
        return function;
    }
}
