package ru.xlv.mochar.player.character.skill;

import ru.xlv.core.player.ServerPlayer;

public interface ISkillUpdatable {

    void update(ServerPlayer serverPlayer);
}
