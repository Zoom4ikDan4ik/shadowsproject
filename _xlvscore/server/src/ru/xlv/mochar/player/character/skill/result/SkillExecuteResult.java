package ru.xlv.mochar.player.character.skill.result;

public enum SkillExecuteResult {

    SKILL_NOT_LEARNED_OR_NOT_FOUND,
    SUCCESS,
    UNKNOWN_ERROR,
    CONDITIONS_NOT_FULFILLED,
    ALREADY_ACTIVE,
    IN_COOLDOWN,
    SILENCED

}
