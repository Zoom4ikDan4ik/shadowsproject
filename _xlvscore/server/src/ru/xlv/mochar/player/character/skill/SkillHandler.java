package ru.xlv.mochar.player.character.skill;

import ru.xlv.core.common.player.character.CharacterType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SkillHandler {

    private static final Map<CharacterType, List<Skill>> SKILL_BLANK_LIST = new HashMap<>();

    public static void init() {
        for (CharacterType value : CharacterType.values()) {
            SKILL_BLANK_LIST.put(value, new ArrayList<>());
        }
        SkillRegistry.REGISTRY.forEach(skillType -> {
            List<Skill> skills = SKILL_BLANK_LIST.get(skillType.getCharacterType());
            skills.add(SkillFactory.createSkill(skillType));
            SKILL_BLANK_LIST.put(skillType.getCharacterType(), skills);
        });
    }

    public synchronized static List<Skill> getAllCharacterSkills(CharacterType characterType) {
        return SKILL_BLANK_LIST.get(characterType);
    }
}
