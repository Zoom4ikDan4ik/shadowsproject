package ru.xlv.mochar.player.character.skill;

import gnu.trove.list.array.TIntArrayList;
import lombok.Getter;
import net.minecraft.nbt.NBTTagCompound;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.database.DatabaseValue;
import ru.xlv.core.common.player.character.CharacterDebuffType;
import ru.xlv.core.common.storage.ISavableNBT;
import ru.xlv.core.common.storage.NBTLoader;
import ru.xlv.core.event.PlayerLearnedSkillEvent;
import ru.xlv.core.event.PlayerSelectSkillEvent;
import ru.xlv.core.event.PlayerUsedSkillEvent;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.core.util.Flex;
import ru.xlv.mochar.player.character.skill.result.SkillBuildCreateResult;
import ru.xlv.mochar.player.character.skill.result.SkillExecuteResult;
import ru.xlv.mochar.player.character.skill.result.SkillLearnResult;
import ru.xlv.mochar.player.character.skill.result.SkillSelectResult;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SkillManager implements ISavableNBT {

    private static final int HOT_ACTIVES_LIST_SIZE = 4;
    private static final int SKILL_BUILD_MAX_AMOUNT = 4;
    private static final int HOT_PASSIVES_LIST_SIZE = 10;

    @Getter
    @DatabaseValue
    private SkillListProvider skillListProvider;

    @Getter
    @DatabaseValue
    private int skillBuildIndex;

    public void init() {
        if(skillListProvider == null) {
            skillListProvider = new SkillListProvider();
            if(skillListProvider.getSkillBuildActivesMap().isEmpty()) {
                for (int i = 0; i < SKILL_BUILD_MAX_AMOUNT; i++) {
                    int[] value = new int[HOT_ACTIVES_LIST_SIZE];
                    Arrays.fill(value, -1);
                    skillListProvider.getSkillBuildActivesMap().put(i, value);
                }
            }
            if(skillListProvider.getSkillBuildPassivesMap().isEmpty()) {
                for (int i = 0; i < SKILL_BUILD_MAX_AMOUNT; i++) {
                    int[] value = new int[HOT_PASSIVES_LIST_SIZE];
                    Arrays.fill(value, -1);
                    skillListProvider.getSkillBuildPassivesMap().put(i, value);
                }
            }
        } else {
            skillListProvider.getLearnedSkillIds().forEach(value -> {
                skillListProvider.getLearnedSkills().add(getNewSkill(value));
                return true;
            });
        }
    }

    public void activatePassives(ServerPlayer serverPlayer) {
        getLearnedSkills().forEach(skill -> {
            if(isSelected(skill)) {
                skill.onSelected(serverPlayer);
            }
        });
    }

    public void deactivateAllSkills(ServerPlayer serverPlayer) {
        getLearnedSkills().forEach(skill -> {
            if (skill instanceof ActivableSkill) {
                ((ActivableSkill) skill).deactivateSkill(serverPlayer);
            }
            skill.onDeselected(serverPlayer);
        });
    }

    private boolean isSelected(Skill skill) {
        for (int i : getSkillListProvider().getSkillBuildActivesMap().get(skillBuildIndex)) {
            if(i == skill.getSkillType().getSkillId()) {
                return true;
            }
        }
        for (int i : getSkillListProvider().getSkillBuildPassivesMap().get(skillBuildIndex)) {
            if(i == skill.getSkillType().getSkillId()) {
                return true;
            }
        }
        return false;
    }

    public SkillLearnResult learn(ServerPlayer serverPlayer, int id) {
        if(getLearnedSkill(id) != null) {
            return SkillLearnResult.ALREADY_LEARNED;
        }
        Skill skill = getNewSkill(id);
        if(skill == null) {
            return SkillLearnResult.NOT_FOUND;
        }
        if(!skill.getSkillType().getParentIds().forEach(value -> value == -1 || getLearnedSkill(value) != null)) {
            return SkillLearnResult.CONDITIONS_NOT_FULFILLED;
        }
        for (SkillLearnRules.SkillLearnRule learnRule : skill.getSkillType().getLearnRules()) {
            if(!learnRule.checkCondition(skill, serverPlayer)) {
                return SkillLearnResult.CONDITIONS_NOT_FULFILLED;
            }
        }
        skill.getSkillType().getLearnRules().forEach(skillLearnRule -> skillLearnRule.onLearn(serverPlayer));
        addLearnedSkill(skill);
        XlvsCoreCommon.EVENT_BUS.post(new PlayerLearnedSkillEvent(serverPlayer, skill));
        return SkillLearnResult.SUCCESS;
    }

    public boolean changeSkillBuild(int index) {
        if(index < 0 || index >= 4) {
            return false;
        }
        skillBuildIndex = index;
        return true;
    }

    private void addLearnedSkill(Skill skill) {
        skillListProvider.getLearnedSkills().add(skill);
        skillListProvider.getLearnedSkillIds().add(skill.getSkillType().getSkillId());
    }

    public SkillSelectResult selectSkill(ServerPlayer serverPlayer, boolean isSelectedSkillActive, int inputSkillId, int inputActiveSkillSlotIndex) {
        int availableSkillSloAmount = isSelectedSkillActive ? getAvailableActiveSkillSlotAmount() : getAvailablePassiveSkillSlotAmount();
        if (inputActiveSkillSlotIndex < 0 || inputActiveSkillSlotIndex + 1 > availableSkillSloAmount) {
            return SkillSelectResult.WRONG_SLOT_INDEX;
        }
        if(inputSkillId == -1) {
            Map<Integer, int[]> skillBuildMap = isSelectedSkillActive ? skillListProvider.getSkillBuildActivesMap() : skillListProvider.getSkillBuildPassivesMap();
            Skill learnedSkill = getLearnedSkill(skillBuildMap.get(skillBuildIndex)[inputActiveSkillSlotIndex]);
            if(learnedSkill != null) {
                callDeselectAtSkill(inputActiveSkillSlotIndex, isSelectedSkillActive, serverPlayer);
                skillBuildMap.get(skillBuildIndex)[inputActiveSkillSlotIndex] = -1;
            }
        } else {
            if (!doesSkillExists(inputSkillId)) {
                return SkillSelectResult.DOES_NOT_EXIST;
            }
            Skill skill = getLearnedSkill(inputSkillId);
            if (skill == null) {
                return SkillSelectResult.NOT_LEARNED;
            }
            boolean isActivable = skill instanceof ActivableSkill;
            Map<Integer, int[]> skillBuildMap = isActivable ? getSkillListProvider().getSkillBuildActivesMap() : getSkillListProvider().getSkillBuildPassivesMap();
            if (skill.getSkillType().getFamilyId() != -1) {
                int prevActiveSkillId = skillBuildMap.get(skillBuildIndex)[inputActiveSkillSlotIndex];
                if(prevActiveSkillId != -1) {
                    Skill prevActiveSkill = getLearnedSkill(prevActiveSkillId);
                    if(prevActiveSkill != null && prevActiveSkill.getSkillType().getFamilyId() == skill.getSkillType().getFamilyId()) {
                        return SkillSelectResult.SUCCESS;
                    }
                }
                for (int i : skillBuildMap.get(skillBuildIndex)) {
                    if (i != -1) {
                        if (i == inputSkillId) {
                            return SkillSelectResult.SUCCESS;
                        }
                        Skill activeSkill = getLearnedSkill(i);
                        if (activeSkill.getSkillType().getFamilyId() == skill.getSkillType().getFamilyId()) {
                            return SkillSelectResult.SAME_FAMILY;
                        }
                    }
                }
            }
            if (isActivable) {
                for (int i : skillListProvider.getSkillBuildActivesMap().get(skillBuildIndex)) {
                    if (i == inputSkillId) {
                        return SkillSelectResult.WRONG_SLOT_INDEX;
                    }
                }
                callDeselectAtSkill(inputActiveSkillSlotIndex, true, serverPlayer);
                skillListProvider.getSkillBuildActivesMap().get(skillBuildIndex)[inputActiveSkillSlotIndex] = inputSkillId;
            } else {
                for (int i : skillListProvider.getSkillBuildPassivesMap().get(skillBuildIndex)) {
                    if (i == inputSkillId) {
                        return SkillSelectResult.WRONG_SLOT_INDEX;
                    }
                }
                callDeselectAtSkill(inputActiveSkillSlotIndex, false, serverPlayer);
                skillListProvider.getSkillBuildPassivesMap().get(skillBuildIndex)[inputActiveSkillSlotIndex] = inputSkillId;
            }
            skill.onSelected(serverPlayer);
            XlvsCoreCommon.EVENT_BUS.post(new PlayerSelectSkillEvent(serverPlayer, skill));
        }
        return SkillSelectResult.SUCCESS;
    }

    private void callDeselectAtSkill(int hotSkillIndex, boolean isActivable, ServerPlayer serverPlayer) {
        if(isActivable) {
            int prevSkillId = skillListProvider.getSkillBuildActivesMap().get(skillBuildIndex)[hotSkillIndex];
            if(prevSkillId != -1) {
                Skill prevSkill = getLearnedSkill(prevSkillId);
                if(prevSkill != null) {
                    prevSkill.onDeselected(serverPlayer);
                }
            }
        } else {
            int prevSkillId = skillListProvider.getSkillBuildPassivesMap().get(skillBuildIndex)[hotSkillIndex];
            if (prevSkillId != -1) {
                Skill prevSkill = getLearnedSkill(prevSkillId);
                if (prevSkill != null) {
                    prevSkill.onDeselected(serverPlayer);
                }
            }
        }
    }

    public SkillExecuteResult execute(ServerPlayer serverPlayer, int id) {
        if(serverPlayer.getSelectedCharacter().isDebuffActive(CharacterDebuffType.SILENCE)) {
            return SkillExecuteResult.SILENCED;
        }
        Skill skill = getLearnedSkill(id);
        if(skill != null) {
            SkillExecuteResult skillExecuteResult = skill.execute(serverPlayer);
            XlvsCoreCommon.EVENT_BUS.post(new PlayerUsedSkillEvent(serverPlayer, skill, skillExecuteResult));
            return skillExecuteResult;
        }
        return SkillExecuteResult.SKILL_NOT_LEARNED_OR_NOT_FOUND;
    }

    public SkillBuildCreateResult createSkillBuild(int index, int... ids) {
        if(index < 0 || index >= SKILL_BUILD_MAX_AMOUNT) {
            return SkillBuildCreateResult.UNKNOWN;
        }
        if(index > 2) {
            int size = getLearnedSkills().size();
            if(size < XlvsCore.INSTANCE.getSkillConfig().getSkillBuildFourthSlotOpenAfterLevel()
                    || size < XlvsCore.INSTANCE.getSkillConfig().getSkillBuildThirdSlotOpenAfterLevel()) {
                return SkillBuildCreateResult.NOT_AVAILABLE_SLOT;
            }
        }
        for (int id : ids) {
            Skill learnedSkill = getLearnedSkill(id);
            if (learnedSkill == null) {
                return SkillBuildCreateResult.UNKNOWN;
            }
        }
        skillListProvider.getSkillBuildActivesMap().put(index, ids);
        return SkillBuildCreateResult.SUCCESS;
    }

    public void removeSkillBuild(int index) {
        if(index < 0 || index >= SKILL_BUILD_MAX_AMOUNT) {
            return;
        }
        Arrays.fill(skillListProvider.getSkillBuildActivesMap().get(index), -1);
    }

    public List<ActivableSkill> getSelectedActiveSkills() {
        TIntArrayList actives = new TIntArrayList(getSkillListProvider().getSkillBuildActivesMap().get(skillBuildIndex));
        return getSkillListProvider().getLearnedSkills()
                .stream()
                .filter(skill -> skill instanceof ActivableSkill && actives.contains(skill.getSkillType().getSkillId()))
                .map(skill -> ((ActivableSkill) skill))
                .collect(Collectors.toList());
    }

    public List<Skill> getSelectedPassiveSkills() {
        return Arrays.stream(getSkillListProvider().getSkillBuildPassivesMap().get(skillBuildIndex))
                .mapToObj(this::getLearnedSkill)
                .collect(Collectors.toList());
    }

    /**
     * @return a skill with specified id or null if skill doesn't exists.
     * */
    public Skill getNewSkill(int id) {
        SkillType arrayElement = Flex.getCollectionElement(SkillRegistry.REGISTRY, skillType -> skillType.getSkillId() == id);
        return arrayElement != null ? SkillFactory.createSkill(arrayElement) : null;
    }

    public boolean doesSkillExists(int id) {
        return Flex.getCollectionElement(SkillRegistry.REGISTRY, skillType -> skillType.getSkillId() == id) != null;
    }

    /**
     * @return a skill with specified id or null of skill isn't learned.
     * */
    public Skill getLearnedSkill(int id) {
        return Flex.getCollectionElement(skillListProvider.getLearnedSkills(), skill -> skill.getSkillType().getSkillId() == id);
    }

    public List<Skill> getLearnedSkills() {
        return skillListProvider.getLearnedSkills();
    }

    public int getAvailableActiveSkillSlotAmount() {
        int size = getLearnedSkills().size();
        if(size >= XlvsCore.INSTANCE.getSkillConfig().getActiveSkillFourthSlotOpenAfterLevel()) {
            return 4;
        } else if(size >= XlvsCore.INSTANCE.getSkillConfig().getActiveSkillThirdSlotOpenAfterLevel()) {
            return 3;
        } else return 2;
    }

    public int getAvailablePassiveSkillSlotAmount() {
        int size = getLearnedSkills().size();
        SkillConfig skillConfig = XlvsCore.INSTANCE.getSkillConfig();
        if(size >= skillConfig.getPassiveSkillTenthSlotOpenAfterLevel()) {
            return 10;
        } else if(size >= skillConfig.getPassiveSkillNinthSlotOpenAfterLevel()) {
            return 9;
        } else if(size >= skillConfig.getPassiveSkillEighthSlotOpenAfterLevel()) {
            return 8;
        } else if(size >= skillConfig.getPassiveSkillSeventhSlotOpenAfterLevel()) {
            return 7;
        } else if(size >= skillConfig.getPassiveSkillSixthSlotOpenAfterLevel()) {
            return 6;
        } else if(size >= skillConfig.getPassiveSkillFifthSlotOpenAfterLevel()) {
            return 5;
        } else if(size >= skillConfig.getPassiveSkillFourthSlotOpenAfterLevel()) {
            return 4;
        } else if(size >= skillConfig.getPassiveSkillThirdSlotOpenAfterLevel()) {
            return 3;
        } else return 2;
    }

    public int getAvailableSkillBuildAmount() {
        int size = getLearnedSkills().size();
        if(size >= XlvsCore.INSTANCE.getSkillConfig().getSkillBuildFourthSlotOpenAfterLevel()) {
            return 4;
        } else if(size >= XlvsCore.INSTANCE.getSkillConfig().getSkillBuildThirdSlotOpenAfterLevel()) {
            return 3;
        }
        return 2;
    }

//    private static final String LEARNED_SKILLS_KEY = "learnedSkills";
//    private static final String HOT_SKILLS_KEY = "hotSkills";

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
//        StringBuilder string = new StringBuilder();
//        for (Skill skill : getLearnedSkills()) {
//            string.append(skill.getSkillType().getId()).append(NBTLoader.ARRAY_DELIMITER);
//        }
//        if(string.length() > 0) {
//            string = new StringBuilder(string.substring(0, string.length() - 1));
//        }
//        nbtTagCompound.setString(LEARNED_SKILLS_KEY, string.toString());
//        string = new StringBuilder();
//        for (int index : skillListProvider.getHotSkills().keys()) {
//            string.append(index).append(NBTLoader.ARRAY_DELIMITER).append(skillListProvider.getHotSkills().get(index)).append(NBTLoader.ARRAY_DELIMITER);
//        }
//        if (string.length() > 0) {
//            string = new StringBuilder(string.substring(0, string.length() - 1));
//        }
//        nbtTagCompound.setString(HOT_SKILLS_KEY, string.toString());
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound, NBTLoader nbtLoader) {
//        String string = nbtTagCompound.getString(LEARNED_SKILLS_KEY);
//        if(!string.isEmpty()) {
//            String[] split = string.split(NBTLoader.ARRAY_DELIMITER);
//            int[] skills = new int[split.length];
//            for (int i = 0; i < split.length; i++) {
//                skills[i] = Integer.parseInt(split[i]);
//            }
//            setLoadedSkills(serverPlayer, characterType, skills);
//        }
//        string = nbtTagCompound.getString(HOT_SKILLS_KEY);
//        if (!string.isEmpty()) {
//            String[] split = string.split(NBTLoader.ARRAY_DELIMITER);
//            for (int i = 0; i < split.length; i += 2) {
//                skillListProvider.getHotSkills().put(Integer.parseInt(split[i]), Integer.parseInt(split[i + 1]));
//            }
//        }
    }
}