package ru.xlv.mochar.tile;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;

import javax.annotation.Nonnull;

public class TileEntityMatrixInventoryTest extends TileEntity implements IMatrixInventoryProvider {

    private MatrixInventory matrixInventory;

    public TileEntityMatrixInventoryTest() {
        this.matrixInventory = MatrixInventoryFactory.create(20, 10);
    }

    @Override
    public MatrixInventory getMatrixInventory() {
        return matrixInventory;
    }

    @Override
    public boolean canInteractWith(@Nonnull EntityPlayer entityPlayer) {
        return true;
    }
}
