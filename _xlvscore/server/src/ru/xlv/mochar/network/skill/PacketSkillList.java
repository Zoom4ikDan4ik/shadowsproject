package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketOutServer;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.player.character.skill.SkillManager;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillList implements IPacketOutServer {

    private ServerPlayer serverPlayer;

    public PacketSkillList(ServerPlayer serverPlayer) {
        this.serverPlayer = serverPlayer;
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        ServerPlayer serverPlayer = this.serverPlayer == null ? XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer) : this.serverPlayer;
        SkillManager skillManager = serverPlayer.getSelectedCharacter().getSkillManager();
        writeCollection(bbos, skillManager.getLearnedSkills(), (byteBufOutputStream, skill) -> byteBufOutputStream.writeInt(skill.getSkillType().getSkillId()));
        PacketSkillBuildSync.writeSkillBuilds(skillManager, bbos);
    }
}