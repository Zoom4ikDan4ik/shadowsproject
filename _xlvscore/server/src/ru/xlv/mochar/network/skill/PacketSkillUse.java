package ru.xlv.mochar.network.skill;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.IPacketCallbackOnServer;
import ru.xlv.core.common.network.PacketCallbackSender;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;

import java.io.IOException;

@NoArgsConstructor
public class PacketSkillUse implements IPacketCallbackOnServer {

    private ServerPlayer serverPlayer;

    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, PacketCallbackSender packetCallbackSender) throws IOException {
        int id = bbis.readInt();
        serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        serverPlayer.getSelectedCharacter().getSkillManager().execute(serverPlayer, id);
    }

    @Override
    public void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {
        PacketSkillBuildSync.writeSkillBuilds(serverPlayer.getSelectedCharacter().getSkillManager(), bbos);
    }
}
