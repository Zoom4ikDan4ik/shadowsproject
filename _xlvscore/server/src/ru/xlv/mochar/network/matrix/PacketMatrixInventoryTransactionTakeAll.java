package ru.xlv.mochar.network.matrix;

import lombok.NoArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.inventory.IMatrixInventoryProvider;
import ru.xlv.core.common.network.IPacketInOnServer;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.player.ServerPlayer;
import ru.xlv.mochar.inventory.MatrixInventoryController;
import ru.xlv.mochar.inventory.MatrixInventoryTransaction;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventoryTransactionTakeAll implements IPacketInOnServer {
    @Override
    public void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {
        ServerPlayer serverPlayer = XlvsCore.INSTANCE.getPlayerManager().getPlayer(entityPlayer);
        if(serverPlayer != null) {
            IMatrixInventoryProvider transactionMatrixInventory = MatrixInventoryController.INSTANCE.getTransactionMatrixInventoryProvider(entityPlayer);
            if (transactionMatrixInventory == null) {
                transactionMatrixInventory = serverPlayer.getSelectedCharacter();
            }
            MatrixInventoryTransaction matrixInventoryTransaction = new MatrixInventoryTransaction(serverPlayer.getSelectedCharacter(), transactionMatrixInventory);
            matrixInventoryTransaction.moveAll();
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixPlayerInventorySync(serverPlayer));
            XlvsCore.INSTANCE.getPacketHandler().sendPacketToPlayer(entityPlayer, new PacketMatrixInventoryTransactionSync(transactionMatrixInventory.getMatrixInventory()));
        }
    }
}
