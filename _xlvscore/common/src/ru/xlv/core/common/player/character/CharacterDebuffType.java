package ru.xlv.core.common.player.character;

public enum CharacterDebuffType {
    SILENCE,
    STUN,
    BLOCK_MOVE
}
