package ru.xlv.core.common.network;

import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

public interface IPacketInOnServer extends IPacket {

    void read(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException;
}
