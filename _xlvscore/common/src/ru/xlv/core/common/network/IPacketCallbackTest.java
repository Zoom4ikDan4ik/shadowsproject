package ru.xlv.core.common.network;

import cpw.mods.fml.relauncher.Side;
import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import ru.xlv.core.common.util.ByteBufInputStream;

import java.io.IOException;

@Deprecated
public interface IPacketCallbackTest extends IPacket {

    /**
     * Calls on requester's side
     * */
    default void write(EntityPlayer entityPlayer, ByteBufOutputStream bbos) throws IOException {}

    /**
     * Calls on requested side
     * @param bbos is callback output stream or null if callback is not expected
     * */
    default void read(EntityPlayer entityPlayer, ByteBufInputStream bbis, ByteBufOutputStream bbos) throws IOException {}

    /**
     * Calls on receiver's side and contains callback/answer from other side
     * */
    default void callback(EntityPlayer entityPlayer, ByteBufInputStream bbis) throws IOException {}

    /**
     * @return the requester's side. It needs to determinate the recursion
     * */
    Side getRequesterSide();
}
