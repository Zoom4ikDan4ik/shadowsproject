package ru.xlv.core.common.module;

public enum ConstructType {
    PRE,
    INIT,
    POST,
    CONSTRUCTED
}
