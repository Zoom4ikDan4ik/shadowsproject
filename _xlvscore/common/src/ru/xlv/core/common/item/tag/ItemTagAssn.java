package ru.xlv.core.common.item.tag;

import net.minecraft.item.Item;

import javax.annotation.Nullable;
import java.util.*;

@Deprecated
public class ItemTagAssn {

    public static final ItemTagAssn INSTANCE = new ItemTagAssn();

    private final Map<Item, List<EnumItemTag>> tags = Collections.synchronizedMap(new HashMap<>());

    private ItemTagAssn() {}

    public void setTags(Item item, EnumItemTag... enumItemTags) {
        tags.put(item, Arrays.asList(enumItemTags));
    }

    @Nullable
    public List<EnumItemTag> getItemTags(Item item) {
        if(item instanceof ITagProvider) {
            return ((ITagProvider) item).getTagsProvided();
        } else {
            return this.tags.get(item);
        }
    }
}
