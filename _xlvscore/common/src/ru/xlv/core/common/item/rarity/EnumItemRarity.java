package ru.xlv.core.common.item.rarity;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.io.Serializable;

public enum EnumItemRarity implements Serializable {

    SLAG("Шлаковый", 0),
    COMMON("Обычный", 0),
    UNCOMMON("Необычный", 0),
    RARE("Редкий", 0),
    EPIC("Эпический", 0),
    SPECIAL("Особый", 0),
    LEGENDARY("Легендарный", 0);

    private final String displayName;
    private final int rarityValue;

    EnumItemRarity(String displayName, int rarityValue) {
        this.displayName = displayName;
        this.rarityValue = rarityValue;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getRarityValue() {
        return rarityValue;
    }

    public static void setItemRarity(ItemStack itemStack, EnumItemRarity enumItemRarity) {
        if(itemStack == null) return;
        if(itemStack.getTagCompound() == null) {
            itemStack.setTagCompound(new NBTTagCompound());
        }
        itemStack.getTagCompound().setInteger("rarityOrdinal", enumItemRarity.ordinal());
    }

    public static EnumItemRarity getItemRarity(ItemStack itemStack) {
        return (itemStack != null && itemStack.getTagCompound() != null && itemStack.getTagCompound().hasKey("rarityOrdinal"))
                ? EnumItemRarity.values()[itemStack.getTagCompound().getInteger("rarityOrdinal")] : EnumItemRarity.COMMON;
    }
}
