package ru.xlv.core.common.item;

import java.util.List;

public interface IItemAppliedInstantly {

    List<AppliedInstantly> getAppliedInstantlyList();

    boolean removeAfterApplied();
}
