package ru.xlv.core.common.item;

import ru.xlv.core.common.util.flex.Result;

public class ItemUseResult extends Result<ItemUseResult, ItemUseResult.Type> {

    public ItemUseResult(Type type) {
        super(type);
    }

    @Override
    public boolean isSuccess() {
        return getType() == Type.SUCCESS;
    }

    public enum Type {
        SUCCESS,
        FAILURE
    }
}
