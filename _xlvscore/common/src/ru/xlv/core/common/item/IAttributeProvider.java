package ru.xlv.core.common.item;

import net.minecraft.item.ItemStack;
import ru.xlv.core.common.player.character.CharacterAttributeMod;
import ru.xlv.core.common.player.character.CharacterAttributeType;

import java.util.Map;

public interface IAttributeProvider {

    Map<CharacterAttributeType, CharacterAttributeMod> getCharacterAttributeBoostMap();

    default void setCharacterAttributeBoost(CharacterAttributeType characterAttributeType, double valueAdd) {
        getCharacterAttributeBoostMap().put(characterAttributeType, CharacterAttributeMod.builder().attributeType(characterAttributeType).valueAdd(valueAdd).build());
    }

    default double getCharacterAttributeBoost(CharacterAttributeType characterAttributeType) {
        CharacterAttributeMod characterAttributeMod = getCharacterAttributeBoostMap().get(characterAttributeType);
        return characterAttributeMod == null ? 0D : characterAttributeMod.getValueAdd();
    }

    /**
     * @return completable attribute value considering durability, quality and rarity
     * */
    default double getCharacterAttributeValue(ItemStack itemStack, CharacterAttributeType characterAttributeType) {
        if(itemStack != null && getCharacterAttributeBoostMap().containsKey(characterAttributeType)) {
//            Item item = itemStack.getItem();
//            double curItemDurability = 0D;
//            double curItemDurabilityMax = 0D;
//            if(item instanceof IItemDurable && ((IItemDurable) item).affectAttributes()) {
//                curItemDurability = ((IItemDurable) item).getDurability();
//                curItemDurabilityMax = ((IItemDurable) item).getMaxDurability();
//            }
//            int qualityAndRarityValue = 0;
//            if(item instanceof IQualityProvider) {
//                EnumItemQuality enumItemQuality = EnumItemQuality.getItemQuality(itemStack);
//                qualityAndRarityValue = enumItemQuality.getQualityValue();
//            }
//            if(item instanceof IRarityProvider) {
//                EnumItemRarity enumItemRarity = EnumItemRarity.getItemRarity(itemStack);
//                qualityAndRarityValue += enumItemRarity.getRarityValue();
//            }
            CharacterAttributeMod characterAttributeMod = getCharacterAttributeBoostMap().get(characterAttributeType);
//            value = characterAttributeBoost;
//            //durability calc
//            double durability = (curItemDurability / curItemDurabilityMax) * 100;
//            value = value >= 0 ?
//                    (value - ((value * durability) / 100))
//                    :
//                    ((value * durability) / 100) + value;
//            //rar and quality calc
//            value = qualityAndRarityValue < 0 ?
//                    (value - ((value * qualityAndRarityValue) / 100))
//                    :
//                    ((value * qualityAndRarityValue) / 100) + value;
            return characterAttributeMod.getValueAdd();
        }
        return 0D;
    }
}
