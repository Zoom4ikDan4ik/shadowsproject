package ru.xlv.core.common.item.rarity;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static ru.xlv.core.common.item.rarity.EnumItemRarity.COMMON;

@Deprecated
public class ItemRarityAssn {

    public static final ItemRarityAssn INSTANCE = new ItemRarityAssn();

    private Map<Item, EnumItemRarity> map = Collections.synchronizedMap(new HashMap<>());

    private ItemRarityAssn() {}

    public void setRarity(Item item, EnumItemRarity enumItemRarity) {
        map.put(item, enumItemRarity);
    }

    @Nonnull
    public EnumItemRarity getInventoryRarity(@Nonnull IInventory inventory) {
        EnumItemRarity enumRarity = COMMON;
        for (int i = 0; i < inventory.getSizeInventory(); i++) {
            ItemStack itemStack = inventory.getStackInSlot(i);
            if(itemStack != null && itemStack.getItem() != null) {
                EnumItemRarity itemRarity = getItemRarity(itemStack.getItem());
                if(enumRarity.ordinal() < itemRarity.ordinal()) {
                    enumRarity = itemRarity;
                }
            }
        }
        return enumRarity;
    }

    @Nonnull
    public EnumItemRarity getItemRarity(Item item) {
        EnumItemRarity enumRarity = map.get(item);
        return item instanceof IRarityProvider ? ((IRarityProvider) item).getRarity() : enumRarity != null ? enumRarity : COMMON;
    }
}
