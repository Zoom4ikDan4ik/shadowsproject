package ru.xlv.core.common.item.rarity;

public interface IRarityProvider {

    EnumItemRarity getRarity();
}
