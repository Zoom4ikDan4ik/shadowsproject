package ru.xlv.core.common.item.quality;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public enum EnumItemQuality {

    GARBAGE("Мусорный", -4),
    WORN_OUT("Изношенное", -3),
    LOW_QUALITY("Низкокачественное", -2),
    FMCG("Ширпотреб", -1),
    BASIC("Базовое качество", 0),
    HIGH("Повышенное качество", 1),
    DISTINCTIVE("Отличительное качество", 2),
    HIGHEST("Наивысшее качество", 3),
    PERFECT("Идеальное качество", 4),
    CUSTOMIZED("Заказное качество", 5);

    private final String displayName;
    private final int qualityValue;

    EnumItemQuality(String displayName, int qualityValue) {
        this.displayName = displayName;
        this.qualityValue = qualityValue;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getQualityValue() {
        return qualityValue;
    }

    public static void setItemQuality(ItemStack itemStack, EnumItemQuality enumItemQuality) {
        if(itemStack == null) return;
        if(itemStack.getTagCompound() == null) {
            itemStack.setTagCompound(new NBTTagCompound());
        }
        itemStack.getTagCompound().setInteger("qualityOrdinal", enumItemQuality.ordinal());
    }

    public static EnumItemQuality getItemQuality(ItemStack itemStack) {
        return (itemStack != null && itemStack.getTagCompound() != null && itemStack.getTagCompound().hasKey("qualityOrdinal"))
                ? EnumItemQuality.values()[itemStack.getTagCompound().getInteger("qualityOrdinal")] : EnumItemQuality.BASIC;
    }
}
