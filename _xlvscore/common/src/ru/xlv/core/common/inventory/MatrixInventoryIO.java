package ru.xlv.core.common.inventory;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBufOutputStream;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.util.ByteBufInputStream;

import javax.annotation.Nonnull;
import java.io.IOException;

public class MatrixInventoryIO {

    public static void write(MatrixInventory matrixInventory, ByteBufOutputStream byteBufOutputStream) throws IOException {
        byteBufOutputStream.writeInt(matrixInventory == null ? 0 : matrixInventory.getWidth());
        byteBufOutputStream.writeInt(matrixInventory == null ? 0 : matrixInventory.getHeight());
        byteBufOutputStream.writeInt(matrixInventory == null ? 0 : matrixInventory.getItems().size());
        if(matrixInventory != null) {
            for (Integer id : matrixInventory.getItems().keySet()) {
                int[] posOfItem = matrixInventory.getPosOfItem(id);
                if (posOfItem == null)
                    throw new IOException("An error has occurred during composing an open matrix inv packet.");
                byteBufOutputStream.writeInt(posOfItem[0]);
                byteBufOutputStream.writeInt(posOfItem[1]);
                ByteBufUtils.writeItemStack(byteBufOutputStream.buffer(), matrixInventory.getItems().get(id));
            }
        }
    }

    public static void read(@Nonnull MatrixInventory matrixInventory, ByteBufInputStream byteBufInputStream) throws IOException {
        int width = byteBufInputStream.readInt();
        int height = byteBufInputStream.readInt();
        matrixInventory.setMatrixSize(width, height);
        matrixInventory.clear();
        int c = byteBufInputStream.readInt();
        for (int i = 0; i < c; i++) {
            int startX = byteBufInputStream.readInt();
            int startY = byteBufInputStream.readInt();
            ItemStack itemStack = ByteBufUtils.readItemStack(byteBufInputStream.getBuffer());
            matrixInventory.setItem(startX, startY, itemStack);
        }
        matrixInventory.setNeedSync(true);
    }
}
