package ru.xlv.core.common.block;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

public class BlockRegistry {

    public void register(Block block) {
        GameRegistry.registerBlock(block, block.getUnlocalizedName());
    }
}
