package ru.xlv.core.achievement;

import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.network.PacketAchievementList;
import ru.xlv.core.util.SoundType;
import ru.xlv.core.util.SoundUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AchievementHandler {

    @Getter
    private final Map<String, List<Achievement>> achievements = new HashMap<>();

    public void sync(String username) {//todo
        XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketAchievementList(username)).thenAcceptSync(simpleAchievements -> {
            synchronized (achievements) {
                achievements.clear();
                achievements.put(username, simpleAchievements);
            }
        });
    }

    public void onAchieved(Achievement achievement) {
        //todo событие получения новой ачивки
        Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Новая ачивка!"));
        Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText(achievement.toString()));
        SoundUtils.playGuiSound(SoundType.NEW_ACHIEVEMENT);
    }
}
