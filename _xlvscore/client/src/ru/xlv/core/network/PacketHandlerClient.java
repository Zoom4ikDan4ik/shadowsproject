package ru.xlv.core.network;

import gnu.trove.map.hash.TIntObjectHashMap;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import lombok.SneakyThrows;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.network.*;
import ru.xlv.core.common.util.ByteBufInputStream;
import ru.xlv.core.common.util.SyncResultHandler;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PacketHandlerClient extends PacketHandler {

    private static final long DEFAULT_CHECK_RESULT_PERIOD = 0L;
    private static final long CALLBACK_RESULT_WAIT_TIMEOUT = 2000L;

    private final TIntObjectHashMap<IPacketCallback> callbackMap = new TIntObjectHashMap<>();
    private final TIntObjectHashMap<Object> callbackResultMap = new TIntObjectHashMap<>();

    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    public PacketHandlerClient(PacketRegistry packetRegistry) {
        super(packetRegistry);
    }

    @Override
    protected void onClientPacketReceived(ByteBufInputStream bbis) {
        XlvsCore.INSTANCE.runUsingMainThread(() -> {
            try {
                int pid = bbis.readInt();
                IPacket packet = getPacketById(pid);
                if (packet != null) {
                    try {
                        if (packet instanceof IPacketCallback) {
                            processPacketCallback1OnClient(bbis);
                        } else if (packet instanceof IPacketIn) {
                            ((IPacketIn) packet).read(bbis);
                        }
                    } catch (Throwable throwable) {
                        logger.warning("An error has occurred during executing a packet " + pid + "#" + packet);
                        throwable.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void processPacketCallback1OnClient(ByteBufInputStream bbis) throws IOException {
        int callbackId = bbis.readInt();
        IPacketCallback packetCallback;
        synchronized (callbackMap) {
            packetCallback = callbackMap.remove(callbackId);
        }
        if (packetCallback != null) {
            packetCallback.read(bbis);
            if(packetCallback instanceof IPacketCallbackEffective) {
                synchronized (callbackResultMap) {
                    //noinspection rawtypes
                    callbackResultMap.put(callbackId, ((IPacketCallbackEffective) packetCallback).getResult());
                }
            }
        }
    }

    /**
     * Позволяет отсылать пакет, у объекта которого вызовется метод {@link IPacketCallback#read(ByteBufInputStream)},
     * когда(если) сервер отошлет ответный пакет с тем же packetId и callbackId обратно клиенту.
     *
     * @return callbackId
     * */
    @SneakyThrows
    public int sendPacketCallback(IPacketCallback packet) {
        int id = genCallbackId();
        synchronized (callbackMap) {
            callbackMap.put(id, packet);
        }
        ByteBufOutputStream byteBufOutputStream = new ByteBufOutputStream(Unpooled.buffer());
        byteBufOutputStream.writeInt(getPacketId(packet));
        byteBufOutputStream.writeInt(id);
        packet.write(byteBufOutputStream);
        sendPacketToServer(byteBufOutputStream);
        return id;
    }

    /**
     * @return {@link SyncResultHandler}, который позволяет выполнить обработку ответа синхронно в основном потоке.
     * Возвращаемый {@link SyncResultHandler} содержит not null результат.
     * */
    public <T> SyncResultHandler<T> sendPacketEffectiveCallback(IPacketCallbackEffective<T> packet) {
        return new SyncResultHandler<>(sendPacketCallbackAsync(packet));
    }

    /**
     * Позволяет отсылать пакет, у объекта которого вызовется метод {@link IPacketCallbackEffective#read(ByteBufInputStream)},
     * когда(если) сервер отошлет ответный пакет с тем же packetId и callbackId обратно клиенту.
     *
     * @return {@link CompletableFuture}, который будет содержать ответ от сервера или NULL, если ответ не был получен
     * или был неверно сконструирован.
     * */
    public <T> CompletableFuture<T> sendPacketCallbackAsync(IPacketCallbackEffective<T> packet) {
        return sendPacketCallbackAsync(packet, DEFAULT_CHECK_RESULT_PERIOD);
    }

    /**
     * Позволяет отсылать пакет, у объекта которого вызовется метод {@link IPacketCallbackEffective#read(ByteBufInputStream)},
     * когда(если) сервер отошлет ответный пакет с тем же packetId и callbackId обратно клиенту.
     *
     * @param checkResultPeriod период проверки подготовленного ответа.
     *
     * @return {@link CompletableFuture}, который будет содержать ответ от сервера или NULL, если ответ не был получен
     * или был неверно сконструирован.
     * */
    public <T> CompletableFuture<T> sendPacketCallbackAsync(IPacketCallbackEffective<T> packet, long checkResultPeriod) {
        final int id = sendPacketCallback(packet);
        return CompletableFuture.supplyAsync(() -> {
            long l = System.currentTimeMillis();
            while (true) {
                synchronized (callbackResultMap) {
                    if (callbackResultMap.containsKey(id)) {
                        //noinspection unchecked
                        return (T) callbackResultMap.remove(id);
                    }
                }
                if (System.currentTimeMillis() - l >= CALLBACK_RESULT_WAIT_TIMEOUT) {
                    break;
                }
                if(checkResultPeriod <= 0) continue;
                try {
                    TimeUnit.MILLISECONDS.sleep(checkResultPeriod);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }, executorService);
    }

    private int genCallbackId() {
        int i = 1;
        while(callbackMap.containsKey(i)) i++;
        return i;
    }
}
