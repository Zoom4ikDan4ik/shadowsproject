package ru.xlv.core.network.matrix;

import io.netty.buffer.ByteBufOutputStream;
import lombok.NoArgsConstructor;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.network.IPacketOut;

import java.io.IOException;

@NoArgsConstructor
public class PacketMatrixInventorySpecSpecItemMove implements IPacketOut {

    private MatrixInventory.SlotType slotTypeFrom, slotTypeTo;

    public PacketMatrixInventorySpecSpecItemMove(MatrixInventory.SlotType slotTypeFrom, MatrixInventory.SlotType slotTypeTo) {
        this.slotTypeFrom = slotTypeFrom;
        this.slotTypeTo = slotTypeTo;
    }

    @Override
    public void write(ByteBufOutputStream bbos) throws IOException {
        bbos.writeInt(slotTypeFrom.ordinal());
        bbos.writeInt(slotTypeTo.ordinal());
    }
}
