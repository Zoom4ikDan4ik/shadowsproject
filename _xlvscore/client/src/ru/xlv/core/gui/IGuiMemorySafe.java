package ru.xlv.core.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.util.ResourceLocation;

import java.util.*;

public interface IGuiMemorySafe {

    Map<Class<?>, Set<ResourceLocation>> registry = new HashMap<>();

    default ResourceLocation loadTexture(String domain, String path) {
        return loadTexture(domain + ":" + path);
    }

    default ResourceLocation loadTexture(String path) {
        ResourceLocation resourceLocation = new ResourceLocation(path);
        if (loadTexture(resourceLocation)) {
            return resourceLocation;
        }
        return null;
    }

    default boolean loadTexture(ResourceLocation resourceLocation) {
        return loadTexture(resourceLocation, new SimpleTexture(resourceLocation));
    }

    default boolean loadTexture(ResourceLocation resourceLocation, ITextureObject iTextureObject) {
        boolean result = Minecraft.getMinecraft().getTextureManager().loadTexture(resourceLocation, iTextureObject);
        if(result) {
            Set<ResourceLocation> set = registry.getOrDefault(getClass(), new HashSet<>());
            if (set.add(resourceLocation)) {
                registry.put(getClass(), set);
            }
        }
        return result;
    }

    default void unloadAllTextures() {
        Set<ResourceLocation> set = registry.get(getClass());
        if(set != null) {
            set.forEach(resourceLocation -> Minecraft.getMinecraft().getTextureManager().deleteTexture(resourceLocation));
            registry.remove(getClass());
        }
    }
}
