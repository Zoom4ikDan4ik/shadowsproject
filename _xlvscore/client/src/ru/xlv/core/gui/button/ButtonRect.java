package ru.xlv.core.gui.button;

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

public class ButtonRect extends Button {

    private int color, color_pressed;

    public ButtonRect(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
        super(buttonId, x, y, widthIn, heightIn, buttonText);
    }

    public ButtonRect setColorPressed(int color) {
        this.color_pressed = color;
        return this;
    }

    public ButtonRect setColor(int color) {
        this.color = color;
        return this;
    }

    @Override
    public void drawButtonBody(Minecraft mc, int mouseX, int mouseY) {
        GL11.glColor4f(1, 1, 1, 1);
        field_146123_n = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
        drawRect(xPosition, yPosition, xPosition + width, yPosition + height, field_146123_n ? color_pressed : color);
        this.mouseDragged(mc, mouseX, mouseY);
    }
}
