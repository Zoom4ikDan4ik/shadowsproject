package ru.xlv.core.model;

import com.google.common.collect.Maps;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.IModelCustomLoader;
import net.minecraftforge.client.model.ModelFormatException;
import net.minecraftforge.client.model.obj.WavefrontObject;
import ru.xlv.core.util.CoreUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Common interface for advanced model loading from files, based on file suffix
 * Model support can be queried through the {@link #getSupportedSuffixes()} method.
 * Instances can be created by calling {@link # loadModel(String)} with a class-loadable-path
 *
 * @author cpw
 *
 */
@SideOnly(Side.CLIENT)
public class ModelLoader {

    private static final Map<String, IModelCustomLoader> instances = Maps.newHashMap();

    private static final Map<ResourceLocation, IModelCustom> loadedModelMap = new HashMap<>();

    /**
     * Register a new model handler
     *
     * @param modelHandler The model handler to register
     */
    public static void registerModelHandler(IModelCustomLoader modelHandler) {
        for (String suffix : modelHandler.getSuffixes()) {
            instances.put(suffix, modelHandler);
        }
    }

    /**
     * Load the model from the supplied classpath resolvable resource name
     *
     * @param resource The resource name
     * @return A model
     * @throws IllegalArgumentException if the resource name cannot be understood
     * @throws ModelFormatException     if the underlying model handler cannot parse the model format
     */
    public static IModelCustom loadModel(ResourceLocation resource) throws IllegalArgumentException, ModelFormatException {
        IModelCustom iModelCustom = loadedModelMap.get(resource);
        if (iModelCustom != null) {
            return iModelCustom;
        }
        String name = resource.getResourcePath();
        int i = name.lastIndexOf('.');
        if (i == -1) {
            FMLLog.severe("The resource name %s is not valid", resource);
            throw new IllegalArgumentException("The resource name is not valid");
        }
        String suffix = name.substring(i + 1);
        IModelCustomLoader loader = instances.get(suffix);
        if (loader == null) {
            FMLLog.severe("The resource name %s is not supported", resource);
            throw new IllegalArgumentException("The resource name is not supported");
        }

        iModelCustom = loader.loadInstance(resource);
        loadedModelMap.put(resource, iModelCustom);
        return iModelCustom;
    }

    public static Collection<String> getSupportedSuffixes() {
        return instances.keySet();
    }

    static {
        registerModelHandler(new IModelCustomLoader() {
            @Override
            public String getType() {
                return "OBJ model";
            }
            @Override
            public String[] getSuffixes() {
                return new String[]{"sp", "obj"};
            }
            @Override
            public IModelCustom loadInstance(ResourceLocation resource) throws ModelFormatException {
                return new WavefrontObject(resource.toString(), CoreUtils.getResourceInputStream(resource.getResourceDomain() + "/" + resource.getResourcePath()));
            }
        });
    }
}