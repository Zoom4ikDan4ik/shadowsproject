package ru.xlv.core.player;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectDoubleHashMap;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.inventory.MatrixInventoryFactory;
import ru.xlv.core.common.item.IItemUsable;
import ru.xlv.core.common.player.character.*;
import ru.xlv.core.skill.Skill;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public class ClientMainPlayer {

    public static final int HOT_SLOT_AMOUNT = 4;

    private final List<Skill> characterSkills = new ArrayList<>();
    private final TIntList learnedSkillIds = new TIntArrayList();
    private final TIntObjectMap<int[]> activeSkills = new TIntObjectHashMap<>();
    private final TIntObjectMap<int[]> passiveSkills = new TIntObjectHashMap<>();
    private final TObjectDoubleMap<ExperienceType> experienceMap = new TObjectDoubleHashMap<>();

    private final List<CharacterAttribute> characterAttributes = new ArrayList<>();

    private final List<String> discoveredLocationNames = new ArrayList<>();

    private final Map<CharacterDebuffType, CharacterDebuff> characterDebuffMap = new HashMap<>();

    private final MatrixInventory matrixInventory = MatrixInventoryFactory.create();
    private final MatrixInventory transactionMatrixInventory = MatrixInventoryFactory.create();

    private CharacterType characterType = CharacterType.MEDIC;
    private ItemStack currentUsableItem;

    private int currentSkillBuildIndex;
    private int credits;
    private double platinum;
    private int availableHotSlotAmount;
    private int availableActiveSkillSlotAmount;
    private int availablePassiveSkillSlotAmount;

    public ClientMainPlayer() {
        for (CharacterAttributeType value : CharacterAttributeType.values()) {
            characterAttributes.add(new CharacterAttribute(value));
        }
    }

    public void updateDiscoveredLocationNames(List<String> list) {
        synchronized (discoveredLocationNames) {
            discoveredLocationNames.clear();
            discoveredLocationNames.addAll(list);
        }
    }

    public void updateExperience(ExperienceType experienceType, double value) {
        synchronized (experienceMap) {
            experienceMap.put(experienceType, value);
        }
    }

    public void updateCharacterAttribute(CharacterAttributeType characterAttributeType, double value, double maxValue) {
        CharacterAttribute characterAttribute = getCharacterAttribute(characterAttributeType);
        if (characterAttribute == null) {
            characterAttributes.add(characterAttribute = new CharacterAttribute(characterAttributeType, value, maxValue));
        }
        characterAttribute.setValue(value);
        characterAttribute.setMaxValue(maxValue);
    }

    public CharacterAttribute getCharacterAttribute(CharacterAttributeType characterAttributeType) {
        for (CharacterAttribute characterAttribute : characterAttributes) {
            if(characterAttribute.getType() == characterAttributeType) {
                return characterAttribute;
            }
        }
        return null;
    }

    public void updateCharacterSkills(List<Skill> list) {
        characterSkills.clear();
        characterSkills.addAll(list);
    }

    public void updateCharacterSkill(Skill skill) {
        characterSkills.removeIf(skill1 -> skill.getId() == skill1.getId());
        characterSkills.add(skill);
    }

    public void updateLearnedSkills(List<Integer> skillIds) {
        this.learnedSkillIds.clear();
        this.learnedSkillIds.addAll(skillIds);
    }

    public void updateUsableItem() {
        if(currentUsableItem != null) {
            Item item = currentUsableItem.getItem();
            if (item instanceof IItemUsable) {
                ((IItemUsable) item).update(currentUsableItem);
            }
        }
    }

    public void setActiveSkill(int skillBuildIndex, int[] skillIds) {
        activeSkills.put(skillBuildIndex, skillIds);
    }

    public void setPassiveSkill(int skillBuildIndex, int[] skillIds) {
        passiveSkills.put(skillBuildIndex, skillIds);
    }

    public Skill getSkillById(int id) {
        for (Skill characterSkill : characterSkills) {
            if(characterSkill.getId() == id) {
                return characterSkill;
            }
        }
        return null;
    }

    public Skill getHotActiveSkill(int slotIndex) {
        int[] skillBuild = activeSkills.get(currentSkillBuildIndex);
        if(skillBuild != null) {
            int id = skillBuild[slotIndex];
            if (id != -1) return getSkillById(id);
        }
        return null;
    }

    public Skill getHotPassiveSkill(int slotIndex) {
        int[] skillBuild = passiveSkills.get(currentSkillBuildIndex);
        if(skillBuild != null) {
            int id = skillBuild[slotIndex];
            if (id != -1) return getSkillById(id);
        }
        return null;
    }

    public int getLvl() {
        return learnedSkillIds.size();
    }

    public boolean canUseItemFromHotSlot() {
        return currentUsableItem == null;
    }
}
