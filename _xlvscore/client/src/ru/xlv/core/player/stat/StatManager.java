package ru.xlv.core.player.stat;

import lombok.Getter;
import ru.xlv.core.XlvsCore;
import ru.xlv.core.common.player.stat.IStatProvider;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.core.network.PacketStatSync;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatManager {

    @Getter
    private final Map<String, IStatProvider> statProviderMap = new HashMap<>();

    public SyncResultHandler<List<Stat>> sync() {//todo
        return XlvsCore.INSTANCE.getPacketHandler().sendPacketEffectiveCallback(new PacketStatSync());
    }
}
