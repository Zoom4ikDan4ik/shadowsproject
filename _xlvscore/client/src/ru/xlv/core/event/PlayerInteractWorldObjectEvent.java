package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.minecraft.entity.Entity;
import net.minecraft.util.MovingObjectPosition;

/**
 * Следует отменять событие, если был получен нужный результат.
 * */
@Getter
@RequiredArgsConstructor
@Cancelable
public class PlayerInteractWorldObjectEvent extends Event {

    private final Entity entity;
    private final MovingObjectPosition movingObjectPosition;
}
