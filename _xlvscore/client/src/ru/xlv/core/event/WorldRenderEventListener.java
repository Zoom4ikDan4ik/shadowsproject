package ru.xlv.core.event;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.init.Blocks;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.opengl.GL11;

public class WorldRenderEventListener {

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void event(RenderWorldLastEvent event) {
        if (RenderManager.debugBoundingBox && !Minecraft.getMinecraft().thePlayer.capabilities.isCreativeMode) {
            RenderManager.debugBoundingBox = false;
        }
        if (Minecraft.getMinecraft().objectMouseOver != null && Minecraft.getMinecraft().theWorld != null) {
            Block block = Minecraft.getMinecraft().theWorld.getBlock(Minecraft.getMinecraft().objectMouseOver.blockX, Minecraft.getMinecraft().objectMouseOver.blockY, Minecraft.getMinecraft().objectMouseOver.blockZ);
            if (block.equals(Blocks.wooden_door)) {
                GL11.glPushMatrix();
                GL11.glTranslated(-RenderManager.renderPosX, -RenderManager.renderPosY, -RenderManager.renderPosZ);
                GL11.glTranslated(Minecraft.getMinecraft().objectMouseOver.hitVec.xCoord, Minecraft.getMinecraft().objectMouseOver.hitVec.yCoord, Minecraft.getMinecraft().objectMouseOver.hitVec.zCoord);
                GL11.glScalef(0.025f, 0.025f, 0.025f);
                GL11.glRotatef(180, 0, 0, 1);
                int sideHit = Minecraft.getMinecraft().objectMouseOver.sideHit;
                if (sideHit == 1)
                    GL11.glRotatef(-90, 1, 0, 0);
                if (sideHit == 0)
                    GL11.glRotatef(90, 1, 0, 0);
                if (sideHit == 4)
                    GL11.glRotatef(-90, 0, 1, 0);
                if (sideHit == 5)
                    GL11.glRotatef(90, 0, 1, 0);
                if (sideHit == 3)
                    GL11.glRotatef(180, 0, 1, 0);
                GL11.glTranslatef(0, 0, -0.1f);
                String out = "[F]";
                Minecraft.getMinecraft().fontRenderer.drawString(out, -Minecraft.getMinecraft().fontRenderer.getStringWidth(out) / 2 + 1, 3, 0xffffff);
                GL11.glPopMatrix();
            }
        }
    }
}
