package ru.xlv.core;

import com.google.common.util.concurrent.ListenableFuture;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import ru.xlv.core.achievement.AchievementHandler;
import ru.xlv.core.common.XlvsCoreCommon;
import ru.xlv.core.common.block.BlockRegistry;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemRegistry;
import ru.xlv.core.common.module.Module;
import ru.xlv.core.common.network.PacketRegistry;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.core.common.util.CommonUtils;
import ru.xlv.core.common.util.SyncResultHandler;
import ru.xlv.core.event.*;
import ru.xlv.core.gui.GameOverlayManager;
import ru.xlv.core.gui.overlay.GameOverlayEnterArea;
import ru.xlv.core.network.*;
import ru.xlv.core.network.matrix.*;
import ru.xlv.core.network.skill.*;
import ru.xlv.core.player.stat.StatManager;
import ru.xlv.core.renderer.texture.TextureLoader;
import ru.xlv.core.resource.preload.PreLoadableResourceManager;
import ru.xlv.core.util.ConfigLastCharacterData;
import ru.xlv.core.util.KeyBindingDrop;
import ru.xlv.mochar.XlvsMainMod;

import java.util.concurrent.Callable;

@Mod(
        name = "XlvsCore",
        modid = XlvsCore.MODID,
        version = "1.0"
)
@Getter
public class XlvsCore {

    public static final String MODID = "xlvscore";

    @Mod.Instance(MODID)
    public static XlvsCore INSTANCE;

    private BlockRegistry blockRegistry;
    private PacketHandlerClient packetHandler;
    private final AchievementHandler achievementHandler = new AchievementHandler();
    private final StatManager statManager = new StatManager();
    private final TextureLoader textureLoader = new TextureLoader();
    private GameOverlayManager gameOverlayManager;

    private final ConfigLastCharacterData configLastCharacterData = new ConfigLastCharacterData();

    @Mod.EventHandler
    public void event(FMLPreInitializationEvent event) {
        XlvsCoreCommon.scanForModules(Module.class);
        MinecraftForge.EVENT_BUS.register(new PreLoadableResourceManager());
        CommonUtils.registerFMLEvents(new TickListener());
        CommonUtils.registerEvents(new DebufEventListener());
        CommonUtils.registerEvents(new WorldRenderEventListener());
        OpenDoorEventListener object = new OpenDoorEventListener();
        CommonUtils.registerEvents(object);
        XlvsCoreCommon.EVENT_BUS.register(object);
        XlvsCoreCommon.EVENT_BUS.register(new ItemPickUpEventListener());
        ItemRegistry.register();
        blockRegistry = new BlockRegistry();

        PacketRegistry packetRegistry = new PacketRegistry();
        packetRegistry.register(MODID,
                new PacketSkillList(),
                new PacketSkillUse(),
                new PacketSkillSelect(),
                new PacketEnterLocation(),
                new PacketMatrixPlayerInventorySync(),
                new PacketMatrixInventoryItemMove(),
                new PacketMatrixInventorySpecItemMove(),
                new PacketMatrixInventoryTransactionSync(),
                new PacketMatrixInventoryDropItem(),
                new PacketClientPlayerSync(),
                new PacketStatSync(),
                new PacketAchievementList(),
                new PacketAchievementNew(),
                new PacketItemPickUp(),
                new PacketMatrixInventorySpecSpecItemMove(),
                new PacketPlayerMainSync(),
                new PacketSkillLearn(),
                new PacketSkillAllList(),
                new PacketSkillBuildCreate(),
                new PacketSkillBuildRemove(),
                new PacketSkillBuildSync(),
                new PacketSkillBuildChange(),
                new PacketItemBaseSync(),
                new PacketPlayerAttributesGet(),
                new PacketMatrixInventoryTransactionOpenClose(),
                new PacketMatrixInventoryTransactionItemMove(),
                new PacketMatrixInventoryTransactionTakeAll(),
                new PacketPlayerDeathEvent(),
                new PacketMatrixInventoryTransactionItemMultiMove(),
                new PacketItemUse(),
                new PacketPlaySound(),
                new PacketSkillSync(),
                new PacketMatrixInventoryTransactionGuiOpen(),
                new PacketClientPlayerGet(),
                new PacketDoorOpen(),
                new PacketSysTimeGet()
        );
        packetHandler = new PacketHandlerClient(packetRegistry);
        SyncResultHandler.setMainThreadExecutor(this::runUsingMainThread);

        try {
            configLastCharacterData.load();
            for (CharacterType value : CharacterType.values()) {
                if(configLastCharacterData.getCharacterData(value) != null) continue;
                configLastCharacterData.setCharacterData(new ConfigLastCharacterData.CharacterData(
                        value.ordinal(), "", "", "", "", "", 0
                ));
            }
        } catch (Exception e) {
            System.out.println("char load err");
        }

        configLastCharacterData.save();

        Minecraft.getMinecraft().gameSettings.keyBindDrop = new KeyBindingDrop("key.drop", 16, "key.categories.gameplay");
        XlvsCoreCommon.MODULE_MANAGER.preInit();
    }

    @Mod.EventHandler
    public void event(FMLInitializationEvent event) {
        gameOverlayManager = new GameOverlayManager();
        gameOverlayManager.registerElement(new GameOverlayEnterArea());
        XlvsCoreCommon.MODULE_MANAGER.init();
    }

    @Mod.EventHandler
    public void event(FMLPostInitializationEvent event) {
        XlvsCoreCommon.EVENT_BUS.register(new SoundEventListener());
        XlvsCoreCommon.MODULE_MANAGER.postInit();
        getPacketHandler().getPacketRegistry().applyRegistration();
    }

    public void saveLastArmorConfig() {
        if(Minecraft.getMinecraft().thePlayer != null) {
            ItemStack head = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.HEAD.getAssociatedSlotIndex());
            ItemStack body = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.BODY.getAssociatedSlotIndex());
            ItemStack bracers = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.BRACERS.getAssociatedSlotIndex());
            ItemStack pants = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.LEGS.getAssociatedSlotIndex());
            ItemStack bots = Minecraft.getMinecraft().thePlayer.inventory.getStackInSlot(MatrixInventory.SlotType.FEET.getAssociatedSlotIndex());
            configLastCharacterData.setCharacterData(new ConfigLastCharacterData.CharacterData(
                    XlvsMainMod.INSTANCE.getClientMainPlayer().getCharacterType().ordinal(),
                    (head != null && head.getItem() != null) ? head.getItem().getUnlocalizedName() : "",
                    (body != null && body.getItem() != null) ? body.getItem().getUnlocalizedName() : "",
                    (bracers != null && bracers.getItem() != null) ? bracers.getItem().getUnlocalizedName() : "",
                    (pants != null && pants.getItem() != null) ? pants.getItem().getUnlocalizedName() : "",
                    (bots != null && bots.getItem() != null) ? bots.getItem().getUnlocalizedName() : "",
                    XlvsMainMod.INSTANCE.getClientMainPlayer().getLvl()
            ));
            configLastCharacterData.save();
        }
    }

    @SuppressWarnings("unchecked")
    public <T> ListenableFuture<T> runUsingMainThread(Runnable runnable) {
        return (ListenableFuture<T>) Minecraft.getMinecraft().func_152344_a(runnable);
    }

    @SuppressWarnings("unchecked")
    public <T> ListenableFuture<T> runUsingMainThread(Callable<T> runnable) {
        return (ListenableFuture<T>) Minecraft.getMinecraft().func_152343_a(runnable);
    }
}
