package ru.xlv.core.entity;

import ru.xlv.core.renderer.entity.IEntityRenderCustom;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class EntityRenderCustomManager {

    private static final Map<String, IEntityRenderCustom> REGISTRY = new HashMap<>();

    public static void registerRender(String uniqueEntityName, IEntityRenderCustom entityRenderCustom) {
        if(REGISTRY.get(uniqueEntityName) != null) throw new RuntimeException("Already registered!");
        REGISTRY.put(uniqueEntityName, entityRenderCustom);
    }

    @Nullable
    public static IEntityRenderCustom getCustomRender(String uniqueEntityName) {
        return REGISTRY.get(uniqueEntityName);
    }
}
