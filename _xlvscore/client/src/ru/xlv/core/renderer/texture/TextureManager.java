package ru.xlv.core.renderer.texture;

import net.minecraft.client.renderer.texture.ITextureObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class TextureManager<K> {

    private final Map<K, ITextureObject> register = Collections.synchronizedMap(new HashMap<>());

    public abstract boolean loadTexture0(K key, ITextureObject texture);

    public abstract void unloadTexture0(K key, ITextureObject texture);

    public abstract void bindTexture(K key);

    protected void loadTexture(K key, Texture value) {
        if (loadTexture0(key, value)) {
            register.put(key, value);
        }
    }

    protected void unload(K key) {
        ITextureObject texture = register.remove(key);
        unloadTexture0(key, texture);
    }

    public void unloadAll() {
        register.forEach((k, texture) -> unload(k));
    }

    public synchronized Map<K, ITextureObject> getRegister() {
        return register;
    }
}
