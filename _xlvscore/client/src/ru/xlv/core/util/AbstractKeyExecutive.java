package ru.xlv.core.util;

import net.minecraft.client.settings.KeyBinding;

public abstract class AbstractKeyExecutive extends KeyBinding {

    public AbstractKeyExecutive(String p_i45001_1_, int p_i45001_2_, String p_i45001_3_) {
        super(p_i45001_1_, p_i45001_2_, p_i45001_3_);
    }

    public abstract void execute();
}
