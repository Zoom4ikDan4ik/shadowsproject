package ru.xlv.core.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.item.IItemUsable;
import ru.xlv.core.common.item.ItemUseResult;
import ru.xlv.mochar.XlvsMainMod;

public class KeyUseHotSlot extends AbstractKeyExecutive {

    private static final Minecraft mc = Minecraft.getMinecraft();
    private final int slotIndex;

    public KeyUseHotSlot(String description, int keyId, String category, int slotIndex) {
        super(description, keyId, category);
        this.slotIndex = slotIndex;
    }

    @Override
    public void execute() {
        EntityClientPlayerMP thePlayer = mc.thePlayer;
        if(thePlayer != null) {
            if(XlvsMainMod.INSTANCE.getClientMainPlayer().canUseItemFromHotSlot()) {
                ItemStack itemStack = thePlayer.inventory.getStackInSlot(slotIndex);
                if (itemStack != null) {
                    Item item = itemStack.getItem();
                    if (item instanceof IItemUsable) {
                        ItemUseResult itemUseResult = ((IItemUsable) itemStack.getItem()).useItem(thePlayer, itemStack, slotIndex);
                        if (itemUseResult.isSuccess()) {
                            XlvsMainMod.INSTANCE.getClientMainPlayer().setCurrentUsableItem(itemStack);
                        }
                    }
                }
            }
        }
    }
}
