package ru.xlv.core.util.config;

import com.google.gson.internal.Primitives;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ru.xlv.core.common.util.config.Configurable;
import ru.xlv.core.util.Flex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;

@RequiredArgsConstructor
public abstract class SimplePrimitivesConfig {

    @RequiredArgsConstructor
    private static class ConfigField {
        private final String fieldName;
        private final String value;
    }

    private final String filePath;

    @SneakyThrows
    public final void load() {
        @Cleanup FileReader fileReader = new FileReader(new File(filePath));
        @Cleanup BufferedReader bufferedReader = new BufferedReader(fileReader);
        bufferedReader
                .lines()
                .map(s -> s.split(":"))
                .map(strings -> new ConfigField(strings[0], strings[1]))
                .forEach(configField -> {
                    Field arrayElement = Flex.getArrayElement(getClass().getDeclaredFields(), field -> field.getName().equals(configField.fieldName));
                    if (arrayElement == null) {
                        throw new RuntimeException();
                    }
                    Object value = getValue(arrayElement.getType(), configField.value);
                    arrayElement.setAccessible(true);
                    setValue(arrayElement, value);
                });
    }

    @SneakyThrows
    public final void save() {
        @Cleanup FileWriter fileWriter = new FileWriter(new File(filePath));
        for (Field field : getClass().getDeclaredFields()) {
            if(field.getAnnotation(Configurable.class) != null && Primitives.isPrimitive(field.getType())) {
                field.setAccessible(true);
                Object o = field.get(this);
                fileWriter.write(field.getName() + ":" + o.toString());
            }
        }
        fileWriter.flush();
    }

    @SneakyThrows
    private void setValue(Field field, Object value) {
        field.set(this, value);
    }

    private Object getValue(Class<?> clazz, String value) {
        if(clazz == String.class) {
            return value;
        } else if(clazz == Integer.class || clazz == int.class) {
            return Integer.parseInt(value);
        } else if(clazz == Double.class || clazz == double.class) {
            return Double.parseDouble(value);
        } else if(clazz == Float.class || clazz == float.class) {
            return Float.parseFloat(value);
        } else if(clazz == Boolean.class || clazz == boolean.class) {
            return Boolean.parseBoolean(value);
        } else if(clazz == Byte.class || clazz == byte.class) {
            return Byte.parseByte(value);
        } else if(clazz == Short.class || clazz == short.class) {
            return Short.parseShort(value);
        } else if(clazz == Long.class || clazz == long.class) {
            return Long.parseLong(value);
        }
        throw new RuntimeException();
    }
}
