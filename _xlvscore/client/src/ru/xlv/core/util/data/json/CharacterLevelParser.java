package ru.xlv.core.util.data.json;

import org.json.JSONObject;
import ru.xlv.core.common.player.character.CharacterType;
import ru.xlv.parser.IJsonParser;

import java.util.HashMap;
import java.util.Map;

public class CharacterLevelParser implements IJsonParser<Map<CharacterType, Integer>> {

    private final JSONObject mainJsonObject;

    private Map<CharacterType, Integer> result;

    public CharacterLevelParser(String source) {
        mainJsonObject = new JSONObject(source);
    }

    @Override
    public void parse() {
        //todo выгружать картинку и уровень

        result = new HashMap<>();
//        for (Object name : mainJsonObject.getJSONObject("data").getJSONObject("characters").names()) {
//            result.put(CharacterType.valueOf(((String) name).toUpperCase()), mainJsonObject.getJSONObject("data").getJSONObject("characters").getJSONObject((String) name).getInt("level"));
//        }//todo
    }

    @Override
    public Map<CharacterType, Integer> getResult() {
        return result;
    }

//    public static void main(String... ss) {
//        RequestHandler requestHandler = new RequestHandler();
//        requestHandler.loadCharacterLevels(data -> {
//            for (CharacterType characterType : data.keySet()) {
//                System.out.println(characterType.getDisplayName());
//            }
//        });
//    }
}
