package ru.xlv.core.util.data.json;

import org.json.JSONObject;
import ru.xlv.core.util.data.News;
import ru.xlv.core.util.data.NewsList;
import ru.xlv.parser.IJsonParser;

import java.util.ArrayList;
import java.util.List;

public class NewsParser implements IJsonParser<NewsList> {

    private static String baseUrl = "https://shadowsproject.ru";

    private final JSONObject mainJsonObject;

    private NewsList result;

    public NewsParser(String source) {
        mainJsonObject = new JSONObject(source);
    }

    @Override
    public void parse() {
//        System.out.println(mainJsonObject.getInt("total"));
        List<News> list = new ArrayList<>();

        for (int i = 0; i < Math.min(mainJsonObject.getInt("total"), 10); i++) {
            list.add(new News(
                    mainJsonObject.getJSONArray("data").getJSONObject(i).getString("meta_title"),
                    mainJsonObject.getJSONArray("data").getJSONObject(i).getString("raw_full_story"),
                    baseUrl + mainJsonObject.getJSONArray("data").getJSONObject(i).getString("main_image"),
                    "https://shadowsproject.ru/news/" + mainJsonObject.getJSONArray("data").getJSONObject(i).getInt("id")
            ));
//            System.out.println(list.get(i));
        }

        result = new NewsList(list);
    }

    @Override
    public NewsList getResult() {
        return result;
    }

//    public static void main(String... ss) {
//        RequestHandler requestHandler = new RequestHandler();
//        requestHandler.loadNews(news -> {
//
//        });
//
////        byte[] bytes = Utils.getBytesFromFile(new File("btn.png"));
////
////        File file = new File("testb");
////        try {
////            FileOutputStream fileOutputStream = new FileOutputStream(file);
////            fileOutputStream.write(bytes);
////            fileOutputStream.close();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////
////        BufferedImage bufferedImage = Utils.getImageFromBytes(bytes);
////        try {
////            ImageIO.write(bufferedImage, "png", new File("test.png"));
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//    }
}
