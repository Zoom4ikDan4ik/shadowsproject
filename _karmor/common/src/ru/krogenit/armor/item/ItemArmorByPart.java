package ru.krogenit.armor.item;

import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import ru.xlv.core.common.inventory.MatrixInventory;
import ru.xlv.core.common.item.ItemArmor;

public class ItemArmorByPart extends ItemArmor {

	public ItemArmorByPart(String name, MatrixInventory.SlotType slotType, SetFamily setFamily) {
		this(name, slotType);
		this.setFamily = setFamily;
	}

	public ItemArmorByPart(String name, MatrixInventory.SlotType slotType) {
		super(name);
		setMaxStackSize(1);
		addSlotType(slotType);
	}

	public boolean isValidArmor(ItemStack stack, int armorType, Entity entity, int slotIndex) {
		Item item = stack.getItem();
		if(item instanceof ItemArmorByPart) {
			ItemArmorByPart itemArmorByPart = (ItemArmorByPart) item;
			return itemArmorByPart.getSlotTypes().get(0).getAssociatedSlotIndex() == slotIndex;
		}

		return false;
	}
}
